﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusinessObjects.Communication
{

    public static class CommonThread
    {
        #region GenerateEmailThread
        public static object GenerateEmailThread(ThreadStart objFunction)
        {

            try
            {
                Thread xThread = new Thread(objFunction);
                xThread.Name = "";
                xThread.Start();
            }
            catch (Exception oEx)
            {
                throw new Exception(oEx.Message);
            }
            return null;
        }

        #endregion
    }

}
