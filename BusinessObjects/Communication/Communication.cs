﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using BusinessObjects.Log;
using System.Configuration;
using BusinessObjects.Constants;


namespace BusinessObjects.Communication
{
    public class Communication
    {
        #region Public Methods

        public string strToMailId { get; set; }
        public string strUserId { get; set; }
        public string strPass { get; set; }
        public string strFName { get; set; }
        public string strLName { get; set; }
        public string body { get; set; }
        public string mailSubject { get; set; }

         public void SendRegistrationMail()
        {
            SendRegistrationMail(strToMailId, mailSubject);
        }

        public bool SendRegistrationMail(string strToMailId,string mailSubject)
        {
            Logger log;
            try
            {
                //string strFromMailId = ConfigurationManager.AppSettings["FromMailId"].ToString();
                //string strPass = ConfigurationManager.AppSettings["FromPass"].ToString();
                string strFromMailId = BusinessConstants.FROM_MAIL_ID;
                string strPass = BusinessConstants.MAIL_PASSWORD;
                MailMessage mail = new MailMessage();
                mail.To.Add(strToMailId);
                mail.From = new MailAddress(strFromMailId);
                mail.Subject = mailSubject;

                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.Credentials = new System.Net.NetworkCredential(strFromMailId, strPass);
                smtp.Send(mail);
                return true;
            }
            catch (Exception Ex)
            {
                log = new Logger();
                log.LogToFile("Error", "Communication : Send Mail", Ex.Message, "Problem In Sending a Mail");
                return false;
            }
            finally
            {
                log = null;
            }
        }



        #endregion

        public string FROM_MAIL_ID { get; set; }
    }
}

        