﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.Constants
{
    public class BusinessConstants
    {
        #region Registration Mail Details
        public const string FROM_MAIL_ID = "stampede.optisol@gmail.com";
        public const string MAIL_PASSWORD = "Optisol_123";
        //public const string MAIL_SIGNUP_URL = "http://182.73.71.66:880/Account/Login";
        //public const string MAIL_SIGNUPs_URL = "http://182.73.71.66:880/Account/Login";

        public const string MAIL_SIGNUP_URL = "http://54.183.15.6:880/Account/Login";
        public const string MAIL_SIGNUPs_URL = "http://54.183.15.6:880/Account/Login";
      
        #endregion

        public class Status
        {
            #region Status Code Details

            public const int Active = 1;
            public const int InActive = 2;
            public const int Deleted = 3;

            #endregion

        }

        public class UserType
        {
            #region Status Code Details

            public const int WebAdmin = 1;
            public const int CoachAdmin = 2;
            public const int AsstCoach = 3;
            public const int Athlete = 4;

            #endregion

        }

    }

  
}
