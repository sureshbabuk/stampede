﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace BusinessObjects.Log
{
    public class Logger
    {
        /// <summary>
        /// Memeber Variable for StreamWriter as object
        /// </summary>
        protected StreamWriter m_FileWriter;

        /// <summary>
        /// Member Variable to store Log File Path
        /// </summary>

        //protected String m_strLogFilePath;
    //string m_strLogFilePath = "D:\\STAMPEDE\\Log\\";
    //   string m_strLogFilePath = ConfigurationManager.AppSettings["LogPath"].ToString();
      string m_strLogFilePath = ConfigurationManager.AppSettings["LogPath"].ToString();

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="strLogPath">Gets Log Path as a string</param>

        public Logger()
        {
            //m_strLogFilePath = strLogPath;
            //m_strLogFilePath = "@c:\\Optisol\Development\\Log\\";
        }

        #endregion

        #region File Open
        /// <summary>
        /// This Method is Used to Open a Text File 
        /// </summary>
        /// <returns>Boolean </returns>

        public bool Open()
        {
            string v_LogFileName;
            try
            {
                v_LogFileName = m_strLogFilePath + DateTime.Now.ToString("dd-MM-yyyy");
                v_LogFileName = v_LogFileName + ".log";
                m_FileWriter = new StreamWriter(v_LogFileName, true);
                return true;
            }
            catch (Exception e_GeneralException)
            {
                Console.WriteLine(e_GeneralException.StackTrace);
                return false;
            }
        }


        #endregion

        #region File Close
        /// <summary>
        /// This Method is Used to Close the Text File.
        /// </summary>
        /// <returns>Boolean</returns>

        public bool Close()
        {
            try
            {
                m_FileWriter.Flush();
                m_FileWriter.Close();
                return true;
            }
            catch (Exception e_GeneralException)
            {
                Console.WriteLine(e_GeneralException.StackTrace);
                return false;
            }
        }

        #endregion

        #region Write Success,Error,Warning MessageTo File
        /// <summary>
        /// This Method is Used to Write the Log in Text Files with Data's Passed.
        /// </summary>
        /// <param name="strLogType">Gets a Log Type Success or Error as a String</param>
        /// <param name="strSource">Gets error source from where error has occured as string</param>
        /// <param name="strMsgFromSys">Gets error message from a system as a string</param>
        /// <param name="strMsgFromObject">Gets error message from an object as a string</param>

        public void LogToFile(string strLogType, string strSource, string strMsgFromSys, string strMsgFromObject)
        {
            string v_strLogMessage;
            try
            {
                v_strLogMessage = DateTime.Now.ToString("HH:mm:ss.fff").ToString() + " | ";
                v_strLogMessage = v_strLogMessage + strLogType + " | ";
                v_strLogMessage = v_strLogMessage + strSource + " | ";
                v_strLogMessage = v_strLogMessage + strMsgFromSys + " | ";
                v_strLogMessage = v_strLogMessage + strMsgFromObject;
                Open();
                m_FileWriter.WriteLine(v_strLogMessage);
                Close();
                return;
            }
            catch (Exception Ex)
            {
                return;
            }
        }

        #endregion
    }
}
