﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Security.Cryptography;
using System.IO;
using BusinessObjects.Log;

namespace BusinessObjects.Utility
{
    public class Utility
    {
        /// <summary>
        /// This method is used to generate random password
        /// </summary>
        /// <returns>string</returns>
        public string GeneratePassword()
        {
            try
            {
                StringBuilder v_strRandom = new StringBuilder();
                v_strRandom.Append(RandomString(6, true));
                v_strRandom.Append(RandomNumber(10000, 99999));
                //v_strRandom.Append(RandomString(4, false));
                return v_strRandom.ToString();
            }
            catch (Exception Ex)
            {
                new Logger().LogToFile("Error", "Utility : Generate Password", Ex.Message.ToString(), "Problem In Generating Password");
                throw;
            }
        }
        /// <summary>
        /// This method is used to generate random number for generating password
        /// </summary>
        /// <param name="min">random number generation start number</param>
        /// <param name="max">random number generation max number</param>
        /// <returns>number</returns>
        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        /// <summary>
        /// This method is used to generate random string.
        /// </summary>
        /// <param name="size">lenght of the string has to be generated</param>
        /// <param name="lowerCase">string has to be in lower case or upper case</param>
        /// <returns>string</returns>
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        //public string GenerateRandomString()
        //{
        //    try
        //    {
        //        string alphanumericCharacters =
        //        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
        //        "abcdefghijklmnopqrstuvwxyz" +
        //        "0123456789";
        //        return GetRandomString(15, alphanumericCharacters);
        //    }
        //    catch (Exception Ex)
        //    {
        //        new Logger().LogToFile("Error", "Utility : Generate Password", Ex.Message.ToString(), "Problem In Generating Password");
        //        throw;
        //    }
        //}
        public string GenerateRandomPassword()
        {
            try
            {
                string alphanumericCharacters =
                //"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                //"abcdefghijklmnopqrstuvwxyz" +
                "0123456789";
                return GetRandomString(5, alphanumericCharacters);
            }
            catch (Exception Ex)
            {
                new Logger().LogToFile("Error", "Utility : Generate Password", Ex.Message.ToString(), "Problem In Generating Password");
                throw;
            }
        }

        private string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            var result = new char[length];
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }

        public string Encrypt(string a_strToEncrypt)
        {
            try
            {
                string pstrEncrKey = "1239;[pewGKG)NisarFidesTech";
                byte[] byKey = { };
                byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
                byKey = System.Text.Encoding.UTF8.GetBytes(pstrEncrKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(a_strToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());

            }
            catch (Exception Ex)
            {
                return null; //MessageBox.Show(Ex.Message.ToString()); 
            }
        }
        
        public string Decrypt(string a_strToDecrypt)
        {
            try
            {
                string toDecryptText = a_strToDecrypt;
                toDecryptText = toDecryptText.Replace(" ", "+");
                string pstrDecrKey = "1239;[pewGKG)NisarFidesTech";
                byte[] byKey = { };
                byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
                byte[] inputByteArray = new byte[toDecryptText.Length];

                byKey = System.Text.Encoding.UTF8.GetBytes(pstrDecrKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(toDecryptText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                //return encoding.GetString(ms.ToArray());
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception Ex)
            {
                return null; // MessageBox.Show(Ex.Message.ToString()); 
            }
        }



        public string Encryptdata(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }
        public string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public DateTime? FormatToDate(string value = "")
        {
            DateTime? _value = null;
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] date = value.Split('-');
                    string[] _date = value.Split('/');

                    string month = "";
                    string dd = "";
                    string year = "";

                    if (date.Length > 1 || _date.Length > 1)
                    {
                        if (date.Length > 1)
                        {
                            month = Convert.ToInt32(date[0]) > 12 ? date[1] : date[0];
                            dd = Convert.ToInt32(date[0]) > 12 ? date[0] : date[1]; ;
                            year = date[2];

                            value = string.Format("{0}/{1}/{2}", year, month, dd);
                        }
                        else
                        {
                            month = Convert.ToInt32(_date[0]) > 12 ? _date[1] : _date[0];
                            dd = Convert.ToInt32(_date[0]) > 12 ? _date[0] : _date[1]; ;
                            year = _date[2];

                            value = string.Format("{0}/{1}/{2}", year, month, dd);
                        }
                        _value = Convert.ToDateTime(value);
                    }
                }
                return _value;
            }
            catch (Exception)
            {
                return _value;
            }
        }
       
    }
}
