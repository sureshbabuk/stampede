﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Net.Http.Headers;
using Stampade.Services.Models.Business;
using BusinessObjects.Communication;
using BusinessObjects.Constants;
using System.IO;

namespace Stampade.Services.Controllers
{
    public class AccountController : BaseController
    {
        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion


        #region Property

        public string encryptPass = "";
        public string decryptPass = "";
        public string emailId = null;
        public string userId = null;
        public string userName = "";
        public long id;
        public string uName = "";
        public string pswd = "";
        public string password = "";
        public string email = "";
        public string newPassword = null;
        #endregion



        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage ForgetPassword()
        {
            try
            {
                if (Request.Method == HttpMethod.Options)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                string status = "";
                var Parseddata = Request.RequestUri.ParseQueryString();
                string email = Parseddata["MAILID"];
                string Pwd = Parseddata["NEWPWD"];
                string OLDPWD = Parseddata["OLDPWD"];

                string temp_cmpPwd = EncodePassword(OLDPWD.Trim(), "SAM1");

                // check whether given account in db or not
                var loginResult = from log in context.STAM_MA_USERS where log.STAM_USER_USERNAME == email && log.STAM_USER_PSWD == temp_cmpPwd select log;
                int countUserDetails = 0;
                countUserDetails = loginResult.Count();
                string getSelcectOption = null;



                if (countUserDetails >= 1)
                {
                    IList<STAM_MA_USERS> logins = loginResult.ToList<STAM_MA_USERS>();

                    userName = logins[0].STAM_USER_FNAME.ToString() + "" + logins[0].STAM_USER_LNAME.ToString();
                    emailId = logins[0].STAM_USER_USERNAME.ToString();
                    userId = logins[0].STAM_USERID.ToString();

                    if (Pwd == null || Pwd == "")
                    {


                        Utility utility = new Utility();
                        string randPass = utility.GenerateRandomPassword();
                        string encodedPass = EncodePassword(randPass, "SAM1");

                        password = randPass;
                        logins[0].STAM_USER_PSWD = Convert.ToString(encodedPass);
                        logins[0].STAM_ACTIVATED_STATUS = true;

                        context.Entry(logins[0]).State = EntityState.Modified;
                        context.SaveChanges();

                        AccountController obj = new AccountController();
                        status = "Y";
                        //if (logins[0].STAM_USER_TYPE_KEY_ID == 5)
                        //    eempid = ManageForPartner(userId);
                        //else if (logins[0].SAM_USER_STATUS_CODE == 11)
                        //    eempid = logins[0].SAM_USER_STATUS_CODE;

                        obj.sendForgetPasswordMail(emailId, getSelcectOption, userName, password);
                    }


                    else
                    {
                        Utility utility = new Utility();
                        if (OLDPWD != null && OLDPWD != "")
                        {
                            string temppwd = logins[0].STAM_USER_PSWD.ToString();
                            temp_cmpPwd = EncodePassword(OLDPWD.Trim(), "SAM1");
                            if (temppwd != temp_cmpPwd)
                            {
                                status = "N";
                                return Request.CreateResponse(HttpStatusCode.BadRequest, new { userId = userId, status = status });
                            }
                        }
                        string randPass = Pwd;
                        string encodedPass = EncodePassword(randPass, "SAM1");

                        password = randPass;
                        logins[0].STAM_USER_PSWD = Convert.ToString(encodedPass);
                        logins[0].STAM_ACTIVATED_STATUS = true;
                        context.Entry(logins[0]).State = EntityState.Modified;
                        context.SaveChanges();
                        AccountController obj = new AccountController();
                        status = "Y";
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new { userId = userId, status = status });
                }
                else
                {
                    string emailErrorMessage = "Please enter a valid emailId";
                    ModelState.AddModelError("Login.email", emailErrorMessage);
                    status = "N";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { userId = userId, status = status });
                }
            }
            catch (Exception Ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccountController : forgotpassword ", Ex.Message, "");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetLogin(string SAM_USER_USERNAME = "", string SAM_USER_PSWD = "", string newPassword = "")
        {
            Int64 eempid = 0;
            try
            {
                if (Request.Method == HttpMethod.Options)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }

                string userName = SAM_USER_USERNAME;
                string pswd = SAM_USER_PSWD;
                string newPswd = newPassword;
                long Corres_UserID = 0;

                string encodedPass = AccountController.EncodePassword(pswd, "sid1");
                string currentPass = pswd;
                string pswd1 = AccountController.EncodePassword(pswd, "SAM1");
                // check whether given account in db or not
                var loginResult = from log in context.STAM_MA_USERS where log.STAM_USER_USERNAME == userName && log.STAM_USER_PSWD == pswd1 select log;
                int loginCount = loginResult.Count();
                if (loginResult != null)
                {
                    IList<STAM_MA_USERS> logins = loginResult.ToList<STAM_MA_USERS>();

                    if (logins[0].STAM_USER_PSWD == pswd1)
                    {
                        uName = logins[0].STAM_USER_USERNAME.ToString();
                        password = logins[0].STAM_USER_PSWD.ToString();
                        userId = logins[0].STAM_USERID.ToString();
                        if (logins[0].STAM_USER_TYPE == 2)
                        {
                            Coach obj_Coach = new Coach();
                            STAM_MA_COACH _objTeam = obj_Coach.GetCoachByID(logins[0].STAM_USER_TYPE_KEY_ID.Value);
                            logins[0].STAM_ACTIVATED_STATUS = _objTeam.STAM_COACH_STATUS == 1 ? true : false;
                            Corres_UserID = _objTeam.STAM_COACH_ID != null ? _objTeam.STAM_COACH_ID : 0;
                        }
                        if (logins[0].STAM_USER_TYPE == 3)
                        {
                            AsstCoach obj_AsstCoach = new AsstCoach();
                            STAM_MA_ASSTCOACH _objAsstCoach = obj_AsstCoach.GetAsstCoachByID(logins[0].STAM_USER_TYPE_KEY_ID.Value);
                            logins[0].STAM_ACTIVATED_STATUS = _objAsstCoach.STAM_ASSTCOACH_STATUS == 1 ? true : false;
                            Corres_UserID = _objAsstCoach.STAM_ASSTCOACH_ID != null ? _objAsstCoach.STAM_ASSTCOACH_ID : 0;
                        }

                        if (logins[0].STAM_USER_TYPE == 4)
                        {
                            Athlete obj_Ath = new Athlete();
                            STAM_MA_ATHLETE _objAth = obj_Ath.GetAthByID(logins[0].STAM_USER_TYPE_KEY_ID.Value);
                            if (_objAth != null)
                            {
                                logins[0].STAM_ACTIVATED_STATUS = _objAth.STAM_ATH_STATUS == 1 ? true : false;
                                Corres_UserID = _objAth.STAM_ATH_ID != null ? _objAth.STAM_ATH_ID : 0;
                            }
                            //else
                            //{
                            //    AsstCoachAthlete obj_Ath1 = new AsstCoachAthlete();
                            //    STAM_MA_ASST_ATHLETE _objAth1 = obj_Ath1.GetAthByID(logins[0].STAM_USER_TYPE_KEY_ID.Value);
                            //    logins[0].STAM_ACTIVATED_STATUS = _objAth1.STAM_ASST_ATH_STATUS == 1 ? true : false;
                            //    Corres_UserID = _objAth1.STAM_ASST_ATH_ID != null ? _objAth1.STAM_ASST_ATH_ID : 0;
                            //}
                        }
                    }

                    if (password.ToString() == pswd1.ToString() && uName.Equals(userName.ToString(), StringComparison.InvariantCulture))
                    {
                        if (!String.IsNullOrEmpty(newPswd))
                        {
                            ChangePswd(uName, pswd, newPswd);
                        }

                        try
                        {
                            //if (logins[0].STAM_USER_TYPE_KEY_ID == 6)
                            //    eempid = ManageForemployer(userId);
                            //if (logins[0].STAM_USER_TYPE_KEY_ID == 5)
                            //    eempid = ManageForPartner(userId);
                            //else if (logins[0].SAM_USER_STATUS_CODE == 11)
                            //    eempid = logins[0].SAM_USER_STATUS_CODE;
                        }
                        catch (Exception ex) { }
                        return Request.CreateResponse(HttpStatusCode.OK, new { userId = userId, token = userName, UserType = (logins[0].STAM_USER_TYPE != null ? logins[0].STAM_USER_TYPE : null), ResourceId = Corres_UserID, activeInActiveStatus = logins[0].STAM_ACTIVATED_STATUS });
                    }


                    else
                    {
                        string passErrorMessage = "Password is Invalid";
                        ModelState.AddModelError("model.password", passErrorMessage);
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    string userErrorMessage = "Invalid Account!";
                    ModelState.AddModelError(userName, userErrorMessage);
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccountsController : Login ", ex.Message, "");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }




        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage ChangePswd(string name, string pswd, string newPswd)
        {
            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            var getUserForChangePassword = from getuser in context.STAM_MA_USERS where getuser.STAM_USER_USERNAME == name select getuser;

            if (getUserForChangePassword != null)
            {
                int getUserForChangePasswordCount = 0;
                getUserForChangePasswordCount = getUserForChangePassword.Count();
                if (getUserForChangePasswordCount >= 1)
                {
                    IList<STAM_MA_USERS> listUsers = getUserForChangePassword.ToList<STAM_MA_USERS>();
                    string encodedPswd = AccountController.EncodePassword(pswd, "SAM1");
                    string newncodedPswd = AccountController.EncodePassword(newPswd, "SAM1");
                    if (listUsers[0].STAM_USER_PSWD == encodedPswd)
                    {
                        try
                        {

                            listUsers[0].STAM_USER_PSWD = newncodedPswd;
                            listUsers[0].STAM_LAST_UPDATED_DATE = DateTime.Now;
                            listUsers[0].STAM_LAST_UPDATED_USER = listUsers[0].STAM_USERID;
                            context.Entry(listUsers[0]).State = EntityState.Modified;
                            context.SaveChanges();
                            //string enceodedPswd = SAM.WEBAPI.Controllers.AccountController.EncodePassword(pswd, "SAM1");
                            //SAM_MA_USERS user = new SAM_MA_USERS();
                            //user.SAM_USER_PSWD = enceodedPswd;
                            //user.SAM_LAST_UPDATED_DATE = DateTime.Now;
                            //user.SAM_LAST_UPDATED_USER = uName;
                            //context.Entry(user).State = EntityState.Modified;
                            //context.SaveChanges();
                            return Request.CreateResponse(HttpStatusCode.OK);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            Logger log = new Logger();
                            log.LogToFile("Error", "PartnerLeadController : EditLead ", ex.Message, "");
                            return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                        }
                    }
                }
                return null;
            }
            return null;
        }

        # region Encode Password

        public static string EncodePassword(string pass, string salt)
        {
            try
            {
                //var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(salt);
                byte[] bytes = Encoding.Unicode.GetBytes(pass);
                byte[] src = Convert.FromBase64String(salt);
                byte[] dst = new byte[src.Length + bytes.Length];
                Buffer.BlockCopy(src, 0, dst, 0, src.Length);
                Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
                HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
                byte[] inArray = algorithm.ComputeHash(dst);
                return Convert.ToBase64String(inArray);
            }
            catch (Exception Ex)
            {
                Logger v_oLog = new Logger();
                v_oLog.LogToFile("Error", "AccountController : EncodePassword ", Ex.Message, "");

                return "";
            }
        }
        #endregion

        #region Forget password

        public bool sendForgetPasswordMail(string strToMailId, string getSelcectOption, string uName, string pswd)
        {
            try
            {
                Communication comm = new Communication();
                comm.strToMailId = strToMailId;
                var fileName = System.Web.HttpContext.Current.Server.MapPath("/Content/email/email_template1.html");
                var sr = new StreamReader(fileName);

                string bodystring = "";
                string mailSubject = "";

                bodystring += "Login Credentials are : <br><br> your user name : " + strToMailId + "<br> your password is : " + pswd + "<br>";
                mailSubject = "Your forgotten User credentials Details";

                string body = sr.ReadToEnd();
                body = body.Replace("{Name}", uName);
                body = body.Replace("{LOGIN_URL}", BusinessObjects.Constants.BusinessConstants.MAIL_SIGNUPs_URL);
                body = body.Replace("{BODY}", bodystring);
                comm.body = body;
                comm.mailSubject = mailSubject;
                var retstatus = CommonThread.GenerateEmailThread(comm.SendRegistrationMail);
                return true;
            }
            catch (Exception Ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccoundController : sendMail", Ex.Message, "Problem In Sending a Mail");
                return false;
            }
        }
        #endregion

        [HttpGet]
        public STAM_MA_COACH_WORKOUT GetWorkoutDate(string WorkoutDate)
        {
            Login obj_lg = new Login();
            try
            {
                STAM_MA_COACH_WORKOUT _objUser = obj_lg.GetWorkoutDate(WorkoutDate);
                return _objUser;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "User : Getting USer", "Problem In getting User", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        [HttpGet]
        public STAM_MA_USERS GetMailID(string mailID)
        {
            Login obj_lg = new Login();
            try
            {
                STAM_MA_USERS _objUser = obj_lg.GetMailID(mailID);
                return _objUser;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "User : Getting USer", "Problem In getting User", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_USERS GetUserbyID(long userID)
        {
            Login obj_lg = new Login();
            try
            {
                STAM_MA_USERS _objUser = obj_lg.GetUser(userID);
                return _objUser;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "User : Getting USer", "Problem In getting User", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        /// <summary>
        /// check a user details is invalid or not and get a user details
        /// </summary>
        /// <param name="sUserName"></param>
        /// <param name="sPassword"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage Login(string sUserName, string sPassword)
        {
            try
            {
                Account objAccount = new Account();
                List<Coachs> lstcoach = new List<Coachs>();
                if (Request.Method == HttpMethod.Options)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                string pswd1 = AccountController.EncodePassword(sPassword, "SAM1");
                // check whether given account in db or not
                STAM_MA_USERS objUser = context.STAM_MA_USERS.Where(p => p.STAM_USER_USERNAME == sUserName && p.STAM_USER_PSWD == pswd1).SingleOrDefault();
                if (objUser != null)
                {
                    objAccount.UserId = objUser.STAM_USERID;
                    objAccount.UserType = objUser.STAM_USER_TYPE;
                    objAccount.UserStatusCode = objUser.STAM_USER_STATUS_CODE;
                    objAccount.UserRole = objUser.SAM_USER_ROLE;
                    objAccount.UserFName = objUser.STAM_USER_FNAME;
                    objAccount.UserLName = objUser.STAM_USER_LNAME;
                    objAccount.UserGender = objUser.STAM_USER_GENDER;
                    objAccount.UserAdd1 = objUser.STAM_USER_ADD1;
                    objAccount.UserAdd2 = objUser.STAM_USER_ADD2;
                    objAccount.UserTelephone = objUser.STAM_USER_TELEPHONE;
                    objAccount.UserMobile = objUser.STAM_USER_MOBILE;
                    objAccount.UserWebsite = objUser.STAM_USER_WEBSITE;
                    objAccount.UserEMail = objUser.STAM_USER_EMAIL;
                    objAccount.UserImage = objUser.STAM_USER_IMAGE;
                    objAccount.UserUsername = objUser.STAM_USER_USERNAME;
                    objAccount.UserPassword = objUser.STAM_USER_PSWD;
                    objAccount.UserIsActivated = objUser.STAM_IS_ACTIVATED;
                    objAccount.UserActivatedString = objUser.STAM_ACTIVATED_STRING;
                    objAccount.UserTypeKeyId = objUser.STAM_USER_TYPE_KEY_ID;
                    objAccount.UserActivatedStatus = objUser.STAM_ACTIVATED_STATUS;
                    objAccount.UserCreatedUser = objUser.SAM_CREATED_USER;
                    objAccount.UserCreatedDate = objUser.STAM_CREATED_DATE;
                    objAccount.UserLastUpdatedUser = objUser.STAM_LAST_UPDATED_USER;
                    objAccount.UserLastUpdatedDate = objUser.STAM_LAST_UPDATED_DATE;
                    var objTeam = (from coach in context.STAM_MA_COACH
                                   join team in context.STAM_MA_TEAM on coach.STAM_COACH_TEAMID equals team.STAM_TEAM_ID
                                   where team.STAM_CREATED_USER == objAccount.UserId && coach.STAM_CREATED_USER == objAccount.UserId
                                   select coach).ToList();
                    if (objTeam.Count != 0)
                    {
                        foreach (var item in objTeam)
                        {
                            Coachs objcoach = new Coachs();
                            objcoach.coachId = item.STAM_COACH_ID;
                            objcoach.coachFName = item.STAM_COACH_FIRSTNAME;
                            objcoach.coachLName = item.STAM_COACH_LASTNAME;
                            objcoach.coachMobile = item.STAM_COACH_MOBILENO;
                            objcoach.coachStatus = item.STAM_COACH_STATUS;
                            objcoach.coachCreatedDate = item.STAM_CREATED_DATE;
                            objcoach.coachLastUpdatedUser = item.STAM_LAST_UPDATED_USER;
                            objcoach.coachLastUpdatedDate = item.STAM_LAST_UPDATED_DATE;
                            objcoach.teamId = item.STAM_COACH_TEAMID;
                            objcoach.teamName = item.STAM_COACH_TEAMNAME;
                            objcoach.coachEMail = item.STAM_COACH_EMAILADD;
                            lstcoach.Add(objcoach);
                        }
                        objAccount.lstCoachs = lstcoach;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, objAccount);
                }
                else
                {
                    string userErrorMessage = "Invalid Account!";
                    ModelState.AddModelError(userName, userErrorMessage);
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccountsController : Login ", ex.Message, "");
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public STAM_MA_USERS GetAdminUser(long iCoach)
        {

            Coach obj_Coach = new Coach();
            try
            {
                STAM_MA_USERS _objTeam = obj_Coach.GetAdminByID(iCoach);
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage AdminCreation(STAM_MA_USERS objCoach)
        {

            Coach obj_Coach = new Coach();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (objCoach.STAM_USERID == 0)
                {
                    //if (!obj_Coach.Create_Coach(objCoach))
                    //{
                    //    objLog.LogToFile("Error", "CoachController : CreateCoach", "Problem In Creating a new Team inside create Coach Admin", "");
                    //    objLog = null;
                    //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    //}
                    //HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                    //response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    //obj_Coach = null;

                    return null;
                }
                else
                {
                    if (!obj_Coach.Update_admin(objCoach))
                    {
                        objLog.LogToFile("Error", "CoachController : UpdateAdmin", "Problem In Updating a  ", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    obj_Coach = null;
                    return Request.CreateResponse(HttpStatusCode.OK);
                }

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Admin", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

    }
}
