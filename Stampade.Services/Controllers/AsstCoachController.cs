﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
   [Authorize]
     public class AsstCoachController : BaseController
    {      

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion  

        
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateAsstCoach(STAM_MA_ASSTCOACH objAsstCoach)
        {
            AsstCoach obj_AsstCoach = new AsstCoach();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (objAsstCoach.STAM_ASSTCOACH_ID == 0)
                    {
                        if (!obj_AsstCoach.Create_AsstCoach(objAsstCoach))
                        {
                            objLog.LogToFile("Error", "AsstCoachController : CreateCoach", "Problem In Creating a new Team inside create AsstCoach Admin", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                        obj_AsstCoach = null;
                                           
                        return response;
                    }
                    else
                    {
                        if (!obj_AsstCoach.Update_AsstCoach(objAsstCoach))
                        {
                            objLog.LogToFile("Error", "AsstCoachController : UpdateAsstCoach", "Problem In Updating a AsstCoach ", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        obj_AsstCoach = null;
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "AsstCoach : CreateCoach", "Problem In Creating a new AsstCoach Admin", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        public IEnumerable<STAM_MA_ASSTCOACH> GetAsstCoachs(long CoachID)
        {
            AsstCoach obj_AsstCoach = new AsstCoach();
            try
            {
                IList<STAM_MA_ASSTCOACH> _objAsstCoach = obj_AsstCoach.GetAsstCoachList(CoachID);
                return _objAsstCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "AsstCoach : Getting AsstCoachList", "Problem In getting AsstCoach Admin List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ASSTCOACH GetAsstCoach(long iCoach)
        {
            AsstCoach obj_AsstCoach = new AsstCoach();
            try
            {
                STAM_MA_ASSTCOACH _objAsstCoach = obj_AsstCoach.GetAsstCoachByID(iCoach);
                return _objAsstCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "AsstCoach : Getting CoachList", "Problem In getting AsstCoach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ASSTCOACH GetAsstCoachByMailID(string mailID)
        {
            AsstCoach obj_AsstCoach = new AsstCoach();
            try
            {
                STAM_MA_ASSTCOACH _objAsstCoach = obj_AsstCoach.GetAsstCoachByMail(mailID);
                return _objAsstCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "AsstCoach : Getting CoachList", "Problem In getting AsstCoach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ASSTCOACH GetLastCoach()
        {
            AsstCoach obj_AsstCoach = new AsstCoach();
            try
            {
                STAM_MA_ASSTCOACH _objTeam = obj_AsstCoach.GetLastAsstCoach();
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_ASSTCOACH> GetAsstCoachByGroupID(string igroup)
        {
            AsstCoach obj_AsstCoach = new AsstCoach();
            try
            {
                IList<STAM_MA_ASSTCOACH> _objAsstCoach = obj_AsstCoach.GetAsstCoachByGrpID(igroup);
                return _objAsstCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "AsstCoach : Getting CoachList", "Problem In getting AsstCoach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public string GetAsstCoachIDListByCoachID(long UserID)
        {
            AsstCoach obj_AsstCoach = new AsstCoach();
            try
            {
                string _objAsstCoach = obj_AsstCoach.GetAsstCoacListhByCoachAdminID(UserID);
                return _objAsstCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "AsstCoach : Getting CoachList", "Problem In getting AsstCoach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
      
    }
}
