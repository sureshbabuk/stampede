﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    [Authorize]
    public class AthleteController : BaseController
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion


        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage CreateAthlete(STAM_MA_ATHLETE objAth)
        {
            Athlete obj_Ath = new Athlete();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {

                if (!obj_Ath.Create_Ath(objAth))
                {
                    objLog.LogToFile("Error", "Athlete : CreateCoach", "Problem In Creating a new Team inside create Athlete", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                obj_Ath = null;

                return response;

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "AsstCoach : CreateCoach", "Problem In Creating a new AsstCoach Admin", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage UpdateAthlete(STAM_MA_ATHLETE objAth)
        {
            Athlete obj_Ath = new Athlete();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (!obj_Ath.Update_Ath(objAth))
                {
                    objLog.LogToFile("Error", "AsstCoachController : UpdateAsstCoach", "Problem In Updating a AsstCoach ", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                obj_Ath = null;
                return Request.CreateResponse(HttpStatusCode.OK);


            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : UpdateCoach", "Problem In Creating a new Update Athlete", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        public IEnumerable<STAM_MA_ATHLETE> GetAthletes(string AthID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAthletesList(AthID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting AthleteList", "Problem In getting Athlete List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }



        public IEnumerable<Stam_Athlete> GetAthletesDevice(string AthID)
        {

            Athlete obj_Ath = new Athlete();
            try
            {
                IList<Stam_Athlete> _objAth = obj_Ath.GetAthletesListDevice(AthID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting AthleteList", "Problem In getting Athlete List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }



        public IEnumerable<Devices> GetDeviceList()
        {

            Athlete obj_Ath = new Athlete();
            try
            {
                IList<Devices> _objAth = obj_Ath.ListDevices();
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting AthleteList", "Problem In getting Athlete List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public IEnumerable<Stam_Athlete> GetAthletestest(string AthID)
        {

            Athlete obj_Ath = new Athlete();
            try
            {
                IList<Stam_Athlete> _objAth = obj_Ath.GetAthletesListnew(AthID);

                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting AthleteList", "Problem In getting Athlete List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE GetAthlete(long iAth)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                STAM_MA_ATHLETE _objAth = obj_Ath.GetAthByID(iAth);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting CoachList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }





        [HttpGet]
        public Stam_Athlete GetAthletes(long iAth)
        {

            Athlete obj_Ath = new Athlete();
            try
            {
                Stam_Athlete _objAth = obj_Ath.GetAthleteByID(iAth);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting CoachList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetAthleteByGroup(string grp)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAthByGroup(grp);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting CoachList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE GetAthByMailID(string mailID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                STAM_MA_ATHLETE _objAth = obj_Ath.GetAthByMail(mailID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting Athlete", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetAthleteByAthSec(long iAth)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAthByAthSec(iAth);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting CoachList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetAthleteByGroupID(string GroupID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAthByGroupID(GroupID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting CoachList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE GetLastAthlete()
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                STAM_MA_ATHLETE _objAth = obj_Ath.GetLastAth();
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting Athlete", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetAssignedAthleteListByID(string iAth)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAssignedAthlete(iAth);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : Getting CoachList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetCoachAdminGroupList(string grp, string UserID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetCoachGroupList(grp, UserID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : GetCoachAdminGroupList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<Stam_Athlete> GetCoachAdminGroupListNew(string grp, string UserID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<Stam_Athlete> _objAth = obj_Ath.GetCoachGroupListNew(grp, UserID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : GetCoachAdminGroupList", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        #region Hema
        /// <summary>
        /// Get a athelet list with authenticated user id
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetAllAthletes(long UserID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAllAthletes(UserID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletes", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// Get a athelet list with authenticated group Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage listAthelets(long groupId)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                List<Athletes> _objAth = obj_Ath.listAthelets(groupId);
                if (_objAth.Count != 0)
                {
                    return Request.CreateResponse<List<Athletes>>(HttpStatusCode.OK, _objAth);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Athlets Not Found");
                }
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletes", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting Athlete");
            }
        }

        /// <summary>
        /// Get a athelet list with GroupID id
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE> GetAllAthletesForGroup(long GroupID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                IList<STAM_MA_ATHLETE> _objAth = obj_Ath.GetAllAthletesForGroup(GroupID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletes", "Problem In getting Athlete", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }



        /// <summary>
        /// Get a athelet list with SectionID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        [HttpGet]
        public List<STAM_MA_ATHLETE> GetAthleteForSection(string SectionName)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                List<STAM_MA_ATHLETE> _lstAthleteSection = obj_Ath.GetAthleteForSection(SectionName);
                return _lstAthleteSection;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section : Getting Athlete", "Problem In getting Athlete Section", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        [HttpGet]
        public List<STAM_MA_ATHLETE> GetAllAthletesForSection(long SectionID)
        {
            Athlete obj_Ath = new Athlete();
            try
            {
                List<STAM_MA_ATHLETE> _lstAthleteSection = obj_Ath.GetAllAthletesForSection(SectionID);
                return _lstAthleteSection;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section : Getting Athlete", "Problem In getting Athlete Section", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// Create a device
        /// </summary>
        /// <param name="objDEVICES"></param>
        /// <returns></returns>
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage saveDevice(Request objRequest)
        {
            Athlete obj_Ath = new Athlete();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in creating");
            }

            try
            {
                if (!obj_Ath.Check_Devce(objRequest))
                {
                    if (!obj_Ath.Create_SaveDevce(objRequest))
                    {
                        objLog.LogToFile("Error", "Athlete : CreateSaveDevice", "Problem In Creating a new device", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Device name and Device Id already exists...");
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Created Successfully");
                }
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Device name and Device Id already exists...");
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "AsstCoach : CreateSaveDevice", "Problem In Creating a new device", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In Creating a new device");
            }

        }



        /// <summary>
        /// Update a notification status and date in athletes 
        /// </summary>
        /// <param name="iDeviceId"></param>
        /// <param name="iGroupID"></param>
        /// <returns></returns>
        /// 



        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage updateDevice(Request objRequest)
        {

            Athlete obj_Ath = new Athlete();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Updating a  device");
            }

            try
            {
                if (!obj_Ath.Check_Devce(objRequest))
                {
                    if (!obj_Ath.Update_SaveDevices(objRequest))
                    {
                        objLog.LogToFile("Error", "Athlete : UpdateAthleteSaveDevice", "Problem in Updating a  device", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Updating a  device");
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Updated Successfully");
                }
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Device Id and group Id not found...");
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : UpdateAthleteSaveDevice", "Problem in Updating a  device", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Update a  device");
            }

        }

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage updateRecord(Request objRequest)
        {
            Athlete obj_Ath = new Athlete();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Updating a  device");
            }

            try
            {
                if (!obj_Ath.Check_UpdateDevce(objRequest))
                {
                    if (!obj_Ath.Update_SaveDevce(objRequest))
                    {
                        objLog.LogToFile("Error", "Athlete : UpdateAthleteSaveDevice", "Problem in Updating a  device", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Updating a  device");
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Updated Successfully");
                }
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Device Id and group Id not found...");
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : UpdateAthleteSaveDevice", "Problem in Updating a  device", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Update a  device");
            }

        }


        //List Devices



        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public List<Devices> ListDevices()
        {


           List<Devices> _objdev = new List<Devices>();

            Athlete obj_Ath = new Athlete();



            _objdev = obj_Ath.ListDevices();

            return _objdev;
            
            
            
        }


        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public Devices EditDevice(long DeviceID=0)
        {


          Devices _objdev = new Devices();

            Athlete obj_Ath = new Athlete();



            _objdev = obj_Ath.EditDevice(DeviceID);

            return _objdev;



        }

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage DeleteDevice(Request objRequest)
        {

            Athlete obj_Ath = new Athlete();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in creating");
            }

            try
            {
               
                    if (!obj_Ath.DeleteDevice(objRequest))
                    {
                        objLog.LogToFile("Error", "Athlete : CreateSaveDevice", "Problem In deleting a new device", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Device name and Device Id already exists...");
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Deleted Successfully");
                
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "AsstCoach : CreateSaveDevice", "Problem In deleting a new device", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In deleting a new device");
            }

        }


        #endregion
    }
}
