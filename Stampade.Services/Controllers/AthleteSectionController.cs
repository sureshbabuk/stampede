﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
   [Authorize]
     public class AthleteSectionController : BaseController
    {      

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion  

        
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage CreateAthleteSection(STAM_MA_ATHLETE_SECTION objAth)
        {
            AthleteSection obj_Ath = new AthleteSection();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                
                        if (!obj_Ath.Create_AthSec(objAth))
                        {
                            objLog.LogToFile("Error", "Athlete Section: CreateCoach", "Problem In Creating a new Team inside create Athlete Section", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                        obj_Ath = null;
                                           
                        return response;
                                    
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Athlete Section: CreateCoach", "Problem In Creating a new Athlete Section", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage UpdateAthleteSection(STAM_MA_ATHLETE_SECTION objAth)
        {
            AthleteSection obj_Ath = new AthleteSection();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                    if (!obj_Ath.Update_AthSec(objAth))
                    {
                        objLog.LogToFile("Error", "AAthlete Section : Update Athlete Section", "Problem In Updating a Athlete Section ", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    obj_Ath = null;
                    return Request.CreateResponse(HttpStatusCode.OK);
               

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Athlete Section : Update Athlete Section", "Problem In Creating a new Update Athlete Section", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        public IEnumerable<STAM_MA_ATHLETE_SECTION> GetAthleteSections(string AthID)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                IList<STAM_MA_ATHLETE_SECTION> _objAth = obj_Ath.GetAthleteSectionList(AthID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section : Getting Athlete Section", "Problem In getting Athlete Section", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE_SECTION GetAthleteSection(long iAth)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                STAM_MA_ATHLETE_SECTION _objAth = obj_Ath.GetAthSecByID(iAth);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section: Getting Athlete Section", "Problem In getting Athlete Section", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE_SECTION GetAthSecByName(string Name,string Group)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                 STAM_MA_ATHLETE_SECTION _objAth = obj_Ath.GetAthSecByName(Name, Group);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section : Getting Athlete", "Problem In getting Athlete Section", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE_SECTION> GetAthSecByNameList(string Name)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                IList<STAM_MA_ATHLETE_SECTION> _objAth = obj_Ath.GetAthSecByNameList(Name);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section  : Getting Athlete", "Problem In getting Athlete Section List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE_SECTION> GetAthleteSection(string Name,string UserId)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                IList<STAM_MA_ATHLETE_SECTION> _objAth = obj_Ath.GetAthleteSection(Name, UserId);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section  : Getting Athlete", "Problem In getting Athlete Section List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE_SECTION GetAthSecByGroupID(long grpId)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                STAM_MA_ATHLETE_SECTION _objAth = obj_Ath.GetAthSecByGroupID(grpId);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section  : Getting Athlete", "Problem In getting Athlete Section List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_ATHLETE_SECTION GetLastAthSec()
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                STAM_MA_ATHLETE_SECTION _objAth = obj_Ath.GetLastAthSec();
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section  : Getting Athlete", "Problem In getting Athlete Section List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
       //for merging table

        [HttpGet]
        public IEnumerable<STAM_MA_ATHLETE_SECTION> GetAthSecByGroupList(string Group, string UserID)
        {
            AthleteSection obj_Ath = new AthleteSection();
            try
            {
                IList<STAM_MA_ATHLETE_SECTION> _objAth = obj_Ath.GetAthSecByCoachGroup(Group, UserID);
                return _objAth;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section  : Getting Athlete", "Problem In getting Athlete Section List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        #region Hema
        [HttpGet]

        public object GetAllSectionsForGroup(long GroupID,long UserID)
        {


            AthleteSection obj_AthleteSection = new AthleteSection();
            try
            {
                var _lstAthleteSection = obj_AthleteSection.GetAllSectionsForGroup(GroupID, UserID);



            return _lstAthleteSection.Select(z=>new
               {
                  SectionID = z.STAM_ATHSEC_ID,
                 SectionName =  z.STAM_ATHSEC_NAME
               }).ToList();

               //return Request.CreateResponse(HttpStatusCode.OK, 
               //    new { _lstAthleteSection.Select(z=>new
               //{
               //    z.STAM_ATHSEC_ID,
               //    z.STAM_ATHSEC_NAME
               //}
                   
                   
               //    ).ToList()});
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Athlete Section : Getting Athlete", "Problem In getting Athlete Section", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        #endregion

    }
}
