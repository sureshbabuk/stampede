﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using System.Web.Http;
using System.Collections;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Text;

namespace Stampade.Services.Controllers
{
    public class BaseController : ApiController
    {
        //public IEnumerable getErrorMessages()
        //{
        //    return ModelState.ToDictionary(kvp => kvp.Key,
        //            kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()).Where(m => m.Value.Count() > 0);
        //}

        //public IEnumerable getErrorMessages(string sKey)
        //{
        //    return ModelState.ToDictionary(kvp => kvp.Key,
        //            kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()).Where(m => m.Key == sKey);
        //}
        public string RemoveNull(string str)
        {
            if (str == "null")
            {
                string strGet = str.ToUpper();
                strGet = Convert.ToString(strGet.Replace(strGet, ""));
                return strGet;
            }
            else
            {
                return str;
            }
        }
        public class TextPlainFormatter : MediaTypeFormatter
        {
            public TextPlainFormatter()
            {
                this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));
                this.SupportedEncodings.Add(Encoding.UTF8);
            }

            public override bool CanWriteType(Type type)
            {
                return type == typeof(string);
            }

            public override bool CanReadType(Type type)
            {
                return false;
            }

            // NOTE: this works
            public override async Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
            {
                StreamWriter sw = new StreamWriter(writeStream);
                await sw.WriteAsync(value.ToString());
                await sw.FlushAsync();
            }
           
            //public override async Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content,
            //TransportContext transportContext, CancellationToken cancellationToken)
            //{
            //    StreamWriter sw = new StreamWriter(writeStream);
            //    await sw.WriteAsync(value.ToString());
            //    await sw.FlushAsync();
            //}
        }
    }
}