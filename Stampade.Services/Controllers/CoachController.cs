﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    public class CoachController : BaseController
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion  

        
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateCoach(STAM_MA_COACH objCoach)
        {
            Coach obj_Coach = new Coach();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (objCoach.STAM_COACH_ID == 0)
                    {
                        if (!obj_Coach.Create_Coach(objCoach))
                        {
                            objLog.LogToFile("Error", "CoachController : CreateCoach", "Problem In Creating a new Team inside create Coach Admin", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                        obj_Coach = null;
                                           
                        return response;
                    }
                    else
                    {
                        if (!obj_Coach.Update_Coach(objCoach))
                        {
                            objLog.LogToFile("Error", "CoachController : UpdateCoach", "Problem In Updating a  ", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        obj_Coach = null;
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Admin", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

         [HttpGet]
        public IEnumerable<STAM_MA_COACH> GetCoachs(long CreateUser)
        {

            Coach obj_Coach = new Coach();
            try
            {
                IList<STAM_MA_COACH> _objCoach = obj_Coach.GetCoachList(CreateUser);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach Admin List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH GetCoach(long iCoach)
        {
            Coach obj_Coach = new Coach();
            try
            {
                STAM_MA_COACH _objTeam = obj_Coach.GetCoachByID(iCoach);
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH GetCoachByMailID(string mailID)
        {
            Coach obj_Coach = new Coach();
            try
            {
                STAM_MA_COACH _objTeam = obj_Coach.GetCoachByMail(mailID);
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH GetLastCoach()
        {
            Coach obj_Coach = new Coach();
            try
            {
                STAM_MA_COACH _objTeam = obj_Coach.GetLastCoach();
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


      
    }
}
