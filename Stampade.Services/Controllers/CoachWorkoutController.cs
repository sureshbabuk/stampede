﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Stampade.Models;

namespace Stampade.Services.Controllers
{
   [Authorize]
     public class CoachWorkoutController : BaseController
    {      


          public class STAM_CUSTOM_MODEL
        {

            public List<string> STAM_SEC_TARGET_TIME { get; set; }
            public string STAM_SEC_ID { get; set; }
            public List<long> STAM_SECID { get; set; }


        }
        // GE

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion  

        
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateCoachWorkout(STAM_MA_COACH_WORKOUT obj_work)
        {
            CoachWorkout obj_Coach = new CoachWorkout();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {                
                        if (!obj_Coach.Create_CoachWorkout(obj_work))
                        {
                            objLog.LogToFile("Error", "CoachWorkoutController : CreateCoach", "Problem In Creating a new Team inside create CoachWorkout Admin", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                        obj_Coach = null;
                                           
                        return response;
                    
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "CoachWorkoutController : CoachWorkout", "Problem In Creating a new CoachWorkout ", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage UpdateCoachWorkout(STAM_MA_COACH_WORKOUT obj_work)
        {
            CoachWorkout obj_Coach = new CoachWorkout();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
               
                    if (!obj_Coach.Update_CoachWorkout(obj_work))
                    {
                        objLog.LogToFile("Error", "CoachWorkoutController : UpdatCoach", "Problem In Updating a CoachWorkout ", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    obj_Coach = null;
                    return Request.CreateResponse(HttpStatusCode.OK);              

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "CoachWorkoutController : CoachWorkout", "Problem In Creating a new CoachWorkout ", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        public IEnumerable<STAM_MA_COACH_WORKOUT> GetCoachWorkouts(string CoachID)
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                IList<STAM_MA_COACH_WORKOUT> _objCoach = obj_Coach.GetCoachWorkoutList(CoachID);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkoutList", "Problem In getting CoachWorkout List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH_WORKOUT GetCoachWorkout(long iCoach)
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                STAM_MA_COACH_WORKOUT _objCoach = obj_Coach.GetCoachWorkoutByID(iCoach);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkout", "Problem In getting CoachWorkout", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH_WORKOUT GetCoachWorkoutByName(string Name,string Dist, int Runs, string Group)
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                STAM_MA_COACH_WORKOUT _objCoach = obj_Coach.GetCoachWorkoutByName(Name, Dist, Runs, Group);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkout", "Problem In getting CoachWorkout", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH_WORKOUT chkCoachWorkout(string Name, string Group, string objdate,string Userid)
        {

            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                STAM_MA_COACH_WORKOUT _objCoach = obj_Coach.ChkCoachWorkoutByGroup(Name, Group, objdate,Userid);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkout", "Problem In getting CoachWorkout", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_COACH_WORKOUT GetLastCoachWorkout()
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                STAM_MA_COACH_WORKOUT _objCoach = obj_Coach.GetLastCoachWorkout();
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Coach : Getting CoachList", "Problem In getting Coach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_COACH_WORKOUT> GetCoachWorkoutByGroupName(string Name,string UserID)
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                IList<STAM_MA_COACH_WORKOUT> _objCoach = obj_Coach.GetCoachWorkoutByGroupName(Name, UserID);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkout", "Problem In getting CoachWorkout", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_COACH_WORKOUT> GetCoachWorkoutByGroupID(long GroupID)
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                IList<STAM_MA_COACH_WORKOUT> _objCoach = obj_Coach.GetCoachWorkoutByGroupID(GroupID);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkout", "Problem In getting CoachWorkout", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<STAM_MA_COACH_WORKOUT> GetCoachWorkoutByAthSecID(long secID)
        {
            CoachWorkout obj_Coach = new CoachWorkout();
            try
            {
                IList<STAM_MA_COACH_WORKOUT> _objCoach = obj_Coach.GetCoachWorkoutByAthSecID(secID);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkout", "Problem In getting CoachWorkout", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        [HttpGet]
        public STAM_MA_COACH_WORKOUT GetCoachWorkoutByDistance(string iDistance)
        {
            CoachWorkout obj_Workouts = new CoachWorkout();
            try
            {
                STAM_MA_COACH_WORKOUT _objWorkout = obj_Workouts.GetCoachWorkoutsByDistance(iDistance);
                return _objWorkout;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Workouts : Getting Workouts", "Problem In getting Workouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }












        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateNewCoachWorkout(TR_WORKOUTS trworkout)
        {
            STAM_TR_WORKOUTS objJobRef = new STAM_TR_WORKOUTS();

            STAM_TR_DETAILS objJobRefDetail = new STAM_TR_DETAILS();


            STAM_CUSTOM_MODEL objJobRefSec = new STAM_CUSTOM_MODEL();

            TRWORKOUTS trwrkout = new TRWORKOUTS();

            objJobRef.STAM_WORKOUT_DATE = trworkout.STAM_WORKOUT_DATE;


            objJobRef.STAM_WORKOUT_TITLE = trworkout.STAM_WORKOUT_TITLE;

            objJobRef.STAM_WORKOUT_GROUPID = trworkout.STAM_WORKOUT_GROUPID;
            objJobRef.STAM_WORKOUT_DISTANCE = trworkout.STAM_WORKOUT_DISTANCE;
            objJobRef.STAM_WORKOUT_RUNS = trworkout.STAM_WORKOUT_RUNS;
            objJobRef.STAM_WORKOUT_STATUS = trworkout.STAM_WORKOUT_STATUS;
            objJobRef.STAM_CREATED_USER = trworkout.UserID;
            objJobRef.STAM_WORKOUT_TYPE = trworkout.STAM_WORKOUT_TYPE;
            objJobRef.STAM_WORKOUT_NOTES = trworkout.STAM_WORKOUT_NOTES;

            objJobRef.STAM_CREATED_DATE = DateTime.Now;
            objJobRef.STAM_LAST_UPDATED_USER = trworkout.UserID;
            objJobRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

            objJobRef.STAM_WORKOUT_GROUP = trworkout.STAM_WORKOUT_GROUP;

            objJobRefDetail.STAM_WORKOUT_ID = trworkout.STAM_WORKOUT_ID;


            objJobRefDetail.STAM_WORKOUT_DETAILS_ID= trworkout.STAM_WORKOUT_DETAILS_ID??0;

           
            objJobRefDetail.STAM_CREATED_DATE = DateTime.Now;

            objJobRefSec.STAM_SECID = trworkout.STAM_SECID;
            objJobRefSec.STAM_SEC_TARGET_TIME = trworkout.STAM_SEC_TARGET_TIME1;
           int? run = trworkout.STAM_WORKOUT_RUNS;

            int i;
            CoachWorkout obj_Coach = new CoachWorkout();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                int k = 0;
                if (!(trwrkout.Create_CoachWorkout(objJobRef)))
                {
                    objLog.LogToFile("Error", "CoachWorkoutController : CreatenewCoach", "Problem In Creating a new Team inside create CoachWorkout Admin", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                for (i = 0; i < run; i++)
                {
                  //  int j=1;
                    objJobRefDetail.STAM_WORKOUT_ID = trwrkout.WorkOutID;



                        foreach (var item in objJobRefSec.STAM_SECID)
                    {

                        List<string> str = trworkout.STAM_SEC_TARGET_TIME1;
                        var target = str[k];


                       // STAM_TR_DETAILS_SECTION objTrsec = new STAM_TR_DETAILS_SECTION();
                        //objTrsec.STAM_WORKOUT_DETAILS_ID = trwrkout.WorkOutDetailID;
                        objJobRefDetail.STAM_SEC_ID = item;
                            
                        objJobRefDetail.STAM_SEC_TARGET_TIME = target;
                       // bool tr1 = trwrkout.Create_CoachWorkoutSection(objTrsec);

                        //objJobRefDetail.STAM_NO_ROW = i;

                        if (objJobRef.STAM_WORKOUT_TYPE == "Mixed")
                        {
                            List<int> d = trworkout.DISTANCE;
                            objJobRefDetail.STAM_RUN = d[i];
                            bool tr = trwrkout.Create_CoachWorkoutDetail(objJobRefDetail);
                        }

                        else
                        {
                            objJobRefDetail.STAM_RUN = (int)Convert.ToInt32(trworkout.STAM_WORKOUT_DISTANCE);
                            //objJobRefDetail.STAM_SEC_TARGET_TIME=
                            bool tr = trwrkout.Create_CoachWorkoutDetail(objJobRefDetail);
                        }
                        k++;

                        
                        }



               

               

                    //foreach (var item in objJobRefSec.STAM_SECID)
                    //{
                        
                    //    List<string> str = trworkout.STAM_SEC_TARGET_TIME1;
                    //   var target= str[k];


                    //    STAM_TR_DETAILS_SECTION objTrsec = new STAM_TR_DETAILS_SECTION();
                    //    objTrsec.STAM_WORKOUT_DETAILS_ID = trwrkout.WorkOutDetailID;
                    //    objTrsec.STAM_SEC_ID =item;

                    //    objTrsec.STAM_SEC_TARGET_TIME = target;
                    //    bool tr1 = trwrkout.Create_CoachWorkoutSection(objTrsec);
                        
                    //  //  k++;
                    //}

                }

                HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "Coachworkout created successfully", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                obj_Coach = null;

                return response;

            
            }

            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "CoachWorkoutController : CoachWorkout", "Problem In Creating a new CoachWorkout ", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return null;
        }






        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetUpdateNewCoachWorkout(TR_WORKOUTS trworkout)
        {

            STAM_TR_WORKOUTS objJobRef = new STAM_TR_WORKOUTS();

            STAM_TR_DETAILS objJobRefDetail = new STAM_TR_DETAILS();


            STAM_CUSTOM_MODEL objJobRefSec = new STAM_CUSTOM_MODEL();

            TRWORKOUTS trwrkout = new TRWORKOUTS();

            objJobRef.STAM_WORKOUT_DATE = trworkout.STAM_WORKOUT_DATE;

            trwrkout.WorkOutID = trworkout.STAM_WORKOUT_ID;
            objJobRef.STAM_WORKOUT_TITLE = trworkout.STAM_WORKOUT_TITLE;

            objJobRef.STAM_WORKOUT_GROUPID = trworkout.STAM_WORKOUT_GROUPID;
            objJobRef.STAM_WORKOUT_DISTANCE = trworkout.STAM_WORKOUT_DISTANCE;
            objJobRef.STAM_WORKOUT_RUNS = trworkout.STAM_WORKOUT_RUNS;
            objJobRef.STAM_WORKOUT_STATUS = trworkout.STAM_WORKOUT_STATUS;
            objJobRef.STAM_CREATED_USER = trworkout.UserID;
            objJobRef.STAM_WORKOUT_TYPE = trworkout.STAM_WORKOUT_TYPE;
            objJobRef.STAM_WORKOUT_NOTES = trworkout.STAM_WORKOUT_NOTES;
            objJobRef.STAM_WORKOUT_ID = trworkout.STAM_WORKOUT_ID;

            objJobRef.STAM_CREATED_DATE = DateTime.Now;
            objJobRef.STAM_LAST_UPDATED_USER = trworkout.UserID;
            objJobRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
            objJobRef.STAM_WORKOUT_GROUP = trworkout.STAM_WORKOUT_GROUP;
            objJobRefDetail.STAM_WORKOUT_ID = trworkout.STAM_WORKOUT_ID;
            objJobRef.STAM_WORKOUT_STATUS = trworkout.STAM_WORKOUT_STATUS;

            objJobRefDetail.STAM_WORKOUT_DETAILS_ID = trworkout.STAM_WORKOUT_DETAILS_ID ?? 0;


            objJobRefDetail.STAM_CREATED_DATE = DateTime.Now;

            objJobRefSec.STAM_SECID = trworkout.STAM_SECID;
            objJobRefSec.STAM_SEC_TARGET_TIME = trworkout.STAM_SEC_TARGET_TIME1;
            int? run = trworkout.STAM_WORKOUT_RUNS;

            int i;
            CoachWorkout obj_Coach = new CoachWorkout();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                int k = 0;
                if (!(trwrkout.Update_CoachWorkout(objJobRef)))
                {
                    objLog.LogToFile("Error", "CoachWorkoutController : CreatenewCoach", "Problem In Creating a new Team inside create CoachWorkout Admin", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                for (i = 0; i < run; i++)
                {
                    foreach (var item in objJobRefSec.STAM_SECID)
                    {

                        List<string> str = trworkout.STAM_SEC_TARGET_TIME1;
                        var target = str[k];


                        STAM_TR_DETAILS_SECTION objTrsec = new STAM_TR_DETAILS_SECTION();
                        objTrsec.STAM_WORKOUT_DETAILS_ID = trwrkout.WorkOutDetailID;
                       // objTrsec.STAM_SEC_ID = item;

                      //  objTrsec.STAM_SEC_TARGET_TIME = target;
                        bool tr1 = trwrkout.Update_CoachWorkoutSection(objTrsec);

                       
                        objJobRefDetail.STAM_WORKOUT_ID = trwrkout.WorkOutID;
                        objJobRefDetail.STAM_SEC_ID = item;

                        objJobRefDetail.STAM_SEC_TARGET_TIME = target;
                        // bool tr1 = trwrkout.Create_CoachWorkoutSection(objTrsec);

                       // objJobRefDetail.STAM_NO_ROW = i;
                        if (objJobRef.STAM_WORKOUT_TYPE == "Mixed")
                        {
                            List<int> d = trworkout.DISTANCE;
                            objJobRefDetail.STAM_RUN = d[i];
                            bool tr = trwrkout.Update_CoachWorkoutDetail(objJobRefDetail);
                        }

                        else
                        {
                            objJobRefDetail.STAM_RUN = (int)Convert.ToInt32(trworkout.STAM_WORKOUT_DISTANCE);
                            bool tr = trwrkout.Update_CoachWorkoutDetail(objJobRefDetail);
                        }
                        k++;

                    }


                    foreach (var item in objJobRefSec.STAM_SECID)
                    {

                        //List<string> str = trworkout.STAM_SEC_TARGET_TIME1;
                        //var target = str[k];


                        //STAM_TR_DETAILS_SECTION objTrsec = new STAM_TR_DETAILS_SECTION();
                        //objTrsec.STAM_WORKOUT_DETAILS_ID = trwrkout.WorkOutDetailID;
                        //objTrsec.STAM_SEC_ID = item;

                        //objTrsec.STAM_SEC_TARGET_TIME = target;
                        //bool tr1 = trwrkout.Update_CoachWorkoutSection(objTrsec);

                        //k++;
                    }

                }

                HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "Coachworkout created successfully", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                obj_Coach = null;

                return response;


            }

            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "CoachWorkoutController : CoachWorkout", "Problem In Creating a new CoachWorkout ", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return null;
        }




        public List<TR_WORKOUTS> GetNewCoachWorkouts(string CoachID)
        {

            TRWORKOUTS obj_Coach = new TRWORKOUTS();
          //  return null;
            try
            {
                var  _objCoach = obj_Coach.GetNewCoachWorkoutList(CoachID);

                List<TR_WORKOUTS> obj_CoachWork = _objCoach.ToList<TR_WORKOUTS>();

                return obj_CoachWork;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting NewCoachWorkoutList", "Problem In getting CoachWorkout List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }






        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage DeleteNewCoachWorkout(STAM_TR_WORKOUTS objJobRef)
        {

            TRWORKOUTS obj_Coach = new TRWORKOUTS();


            var _objCoach = obj_Coach.DeleteCoachWorkout(objJobRef.STAM_WORKOUT_ID);
            HttpResponseMessage response;

            if(!_objCoach)
            {
                response = Request.CreateResponse(System.Net.HttpStatusCode.NotFound, "Selected Workoput not alllowed to delete", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                obj_Coach = null;

                return response;
            }
           

            //HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, _objCoach==false ?"");
            //response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
           // obj_Coach = null;

           // return response;
           return new HttpResponseMessage(HttpStatusCode.OK);
        }




          [HttpGet]
        public TR_WORKOUTS EditNewWorkouts(string CoachID)
        {


            TRWORKOUTS obj_Coach = new TRWORKOUTS();
            try
            {
                TR_WORKOUTS _objCoach = obj_Coach.EditNewWorkouts(CoachID);
                return _objCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkoutList", "Problem In getting CoachWorkout List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }




          [HttpGet]
          [AcceptVerbs("OPTIONS")]
          public WorkoutModel EditNewWorkoutSections(string CoachID)
          {




              TRWORKOUTS obj_Coach = new TRWORKOUTS();
              WorkoutModel _objCoach = new WorkoutModel();
              try
              {
                  _objCoach = obj_Coach.EditNewWorkoutections(CoachID);
                  return _objCoach;
              }
              catch (Exception Ex)
              {
                  objLog.LogToFile("Error", "CoachWorkoutController : Getting CoachWorkoutList", "Problem In getting CoachWorkout List", Ex.Message.ToString());
                  objLog = null;
                  return null;
              }
          }





          public HttpResponseMessage UpdateNewCoachWorkoutStatus(TR_WORKOUTS trworkout)
          {

              TRWORKOUTS obj_Coach = new TRWORKOUTS();
              STAM_TR_WORKOUTS objJobRef = new STAM_TR_WORKOUTS();
              objJobRef.STAM_WORKOUT_ID = trworkout.STAM_WORKOUT_ID;
              objJobRef.STAM_LAST_UPDATED_USER = trworkout.UserID;
              objJobRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
              objJobRef.STAM_WORKOUT_STATUS = trworkout.STAM_WORKOUT_STATUS;


              if (Request.Method == HttpMethod.Options)
              {
                  return new HttpResponseMessage(HttpStatusCode.OK);
              }
              if (!ModelState.IsValid)
              {
                  return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
              }
              try
              {

                  if (!obj_Coach.Update_CoachWorkoutStatus(objJobRef))
                  {
                      objLog.LogToFile("Error", "CoachWorkoutController : UpdatCoach", "Problem In Updating a CoachWorkout ", "");
                      objLog = null;
                      return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                  }
                  obj_Coach = null;
                  return Request.CreateResponse(HttpStatusCode.OK);

              }
              catch (Exception Ex)
              {
                  objLog = new Logger();
                  objLog.LogToFile("Error", "CoachWorkoutController : CoachWorkout", "Problem In Creating a new CoachWorkout ", Ex.Message.ToString());
                  objLog = null;
                  return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
              }
          }


          [HttpGet]
          public STAM_TR_WORKOUTS_MODEL chkNewCoachWorkout(string Name, string Group, string objdate, string Userid)
          {


              TRWORKOUTS obj_Coach = new TRWORKOUTS();
              try
              {
                  STAM_TR_WORKOUTS_MODEL _objCoach = obj_Coach.ChkNewCoachWorkoutByGroup(Name, Group, objdate, Userid);

                  return _objCoach;
              }
              catch (Exception Ex)
              {
                  objLog.LogToFile("Error", "CoachWorkoutController : Getting NewCoachWorkoutTitle", "Problem In getting NewCoachWorkoutTitle", Ex.Message.ToString());
                  objLog = null;
                  return null;
              }
          }
    }
}
