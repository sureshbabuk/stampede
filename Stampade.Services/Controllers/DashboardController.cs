﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    public class DashboardController : ApiController
    {
        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetNewWorkoutResults(long UserId, DateTime StartDay, DateTime EndDate)
        {
            try
            {
                Dashboard objDashboard = new Dashboard();
                List<NewWorkout> ListOfUserBasedWorkout = new List<NewWorkout>();
                List<NewWorkout> ListNewWorkout = objDashboard.GetDashboardResults(StartDay, EndDate, UserId);
              //  ListOfUserBasedWorkout = objDashboard.GetListOfUserBasedWorkout(ListNewWorkout, UserId);

                if (ListNewWorkout.Count == 0)
                {
                    objLog.LogToFile("Error", "DashboardController : GetNewWorkoutResults", "Problem In getting a GetNewWorkoutResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<NewWorkout>>(HttpStatusCode.OK, ListNewWorkout);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "DashboardController : GetNewWorkoutResults", "Problem In getting a GetNewWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetNewWorkoutResults");
            }
        }



        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllNewWorkoutResults(long UserId)
        {
            try
            {
                Dashboard objDashboard = new Dashboard();
                List<NewWorkout> ListOfUserBasedWorkout = new List<NewWorkout>();
                List<NewWorkout> ListNewWorkout = objDashboard.GetAllNewWorkoutResults(UserId);
                //  ListOfUserBasedWorkout = objDashboard.GetListOfUserBasedWorkout(ListNewWorkout, UserId);

                if (ListNewWorkout.Count == 0)
                {
                    objLog.LogToFile("Error", "DashboardController : GetNewWorkoutResults", "Problem In getting a GetNewWorkoutResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<NewWorkout>>(HttpStatusCode.OK, ListNewWorkout);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "DashboardController : GetNewWorkoutResults", "Problem In getting a GetNewWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetNewWorkoutResults");
            }
        }


        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetNewSectionWorkoutResults(long SectionId, long Userid, DateTime StartDay, DateTime EndDate)
        {
            try
            {
                Dashboard objDashboard = new Dashboard();
                List<NewWorkout> ListOfUserBasedWorkout = new List<NewWorkout>();
                List<NewWorkout> ListNewWorkout = objDashboard.GetNewSectionWorkoutResults(SectionId, Userid, StartDay, EndDate);
                //  ListOfUserBasedWorkout = objDashboard.GetListOfUserBasedWorkout(ListNewWorkout, UserId);

                if (ListNewWorkout.Count == 0)
                {
                    objLog.LogToFile("Error", "DashboardController : GetNewSectionWorkoutResults", "Problem In getting a GetNewSectionWorkoutResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<NewWorkout>>(HttpStatusCode.OK, ListNewWorkout);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "DashboardController : GetNewSectionWorkoutResults", "Problem In getting a GetNewSectionWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetNewWorkoutResults");
            }
        }
    }
}