﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    public class DistanceController : ApiController
    {
        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion   

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateDistance(STAM_MA_DISTANCE objDistance)
        {
            Distance objCreateDistance = new Distance();          

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                         if (objDistance.STAM_DISTANCE_ID == 0)
                        {
                            if (!objCreateDistance.Create_Distance(objDistance))
                            {
                                objLog.LogToFile("Error", "DistanceController : CreateJob", "Problem In Creating a new Distance inside create Distance", "");
                                objLog = null;
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                            }
                            HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                            objCreateDistance = null;
                            return response;
                        }
                        else
                        {
                            if (!objCreateDistance.Update_Distance(objDistance))
                            {
                                objLog.LogToFile("Error", "DistanceController : UpdateJob", "Problem In Updating a Distance ", "");
                                objLog = null;
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                            }
                            objCreateDistance = null;
                            return Request.CreateResponse(HttpStatusCode.OK);
                        }
                                     
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "DistanceController : CreateTeam", "Problem In Creating a new Distance", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }


        [HttpGet]
        public IEnumerable<STAM_MA_DISTANCE> DistancesList()
        {
            Distance objUpdateDistance = new Distance();
            try
            {
                IList<STAM_MA_DISTANCE> _objDistance = objUpdateDistance.GetDistanceList();
                return _objDistance;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "DistanceController : Getting DistanceList", "Problem In getting List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_DISTANCE GetDistance(long iDistance)
        {
            Distance objUpdateDistance = new Distance();
            try
            {
                STAM_MA_DISTANCE _objDistance = objUpdateDistance.GetDistanceByID(iDistance);
                return _objDistance;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Distance : Getting DistanceList", "Problem In getting Distance", Ex.Message.ToString());
                objLog = null;
                return null;
            }           
        }

        [HttpGet]
        public STAM_MA_DISTANCE GetDistanceByName(string Name)
        {
            Distance objDistance = new Distance();
            try
            {
                STAM_MA_DISTANCE _objobjDistance = objDistance.GetDistanceByDistanceName(Name);
                return _objobjDistance;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Distance : Getting Distance", "Problem In getting Distance", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

    }
}
