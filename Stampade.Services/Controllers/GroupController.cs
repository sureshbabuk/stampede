﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;


namespace Stampade.Services.Controllers
{
    public class GroupController : ApiController
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateGroup(STAM_MA_GROUP objGroup)
        {
            Group objCreateGroup = new Group();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (objGroup.STAM_GROUP_ID == 0)
                {
                    if (!objCreateGroup.Create_Group(objGroup))
                    {
                        objLog.LogToFile("Error", "GroupController : CreateGroup", "Problem In Creating a new Group inside create Group", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    objCreateGroup = null;
                    return response;
                }
                else
                {
                    if (!objCreateGroup.Update_Group(objGroup))
                    {
                        objLog.LogToFile("Error", "GroupController : UpdateGroup", "Problem In Updating a Group ", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    objCreateGroup = null;
                    return Request.CreateResponse(HttpStatusCode.OK);
                }

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "GroupController : CreateTeam", "Problem In Creating a new Group", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }



        [HttpGet]
        public IEnumerable<STAM_MA_GROUP> GetGroups()
        {
            Group objGroup = new Group();
            try
            {
                IList<STAM_MA_GROUP> _objGroup = objGroup.GetGroupList();
                return _objGroup;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Group : Getting TeamList", "Problem In getting Group List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        //[HttpGet]
        //public object GetObjGroups()
        //{
        //    Group objGroup = new Group();
        //    try
        //    {
        //        var _objGroup = objGroup.GetObjGroupList();
        //        return _objGroup;
        //    }
        //    catch (Exception Ex)
        //    {
        //        objLog.LogToFile("Error", "Group : Getting TeamList", "Problem In getting Group List", Ex.Message.ToString());
        //        objLog = null;
        //        return null;
        //    }
        //}
        [HttpGet]
        public STAM_MA_GROUP GetGroup(long iGroup)
        {
            Group objGroup = new Group();
            try
            {
                STAM_MA_GROUP _objGroup = objGroup.GetGroupByID(iGroup);
                return _objGroup;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Group : Getting GroupList", "Problem In getting Group", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_GROUP GetGroupByName(string Name)
        {
            Group objGroup = new Group();
            try
            {
                STAM_MA_GROUP _objGroup = objGroup.GetGroupByName(Name);
                return _objGroup;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Group : Getting Group", "Problem In getting Group", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        [HttpGet]
        public IEnumerable<STAM_MA_GROUP> GetGroupsByCoachID(string CoachID)
        {
            Group objGroup = new Group();
            try
            {
                IList<STAM_MA_GROUP> _objGroup = objGroup.GetGroupsByUserID(CoachID);
                return _objGroup;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Group : Getting TeamList", "Problem In getting Group List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        #region Hema
        [HttpGet]
        public IEnumerable<STAM_MA_GROUP> GetAllGroups(long UserId)
        {
            Group objGroup = new Group();
            try
            {
                IList<STAM_MA_GROUP> _objGroup = objGroup.GetAllGroups(UserId);
                return _objGroup;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Group : GetAllGroups", "Problem In getting Group List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage listGroup()
        {
            Group objGroup = new Group();
            try
            {
                List<Groups> _objGroup = objGroup.listGroup();
                if (_objGroup.Count != 0)
                {
                    return Request.CreateResponse<List<Groups>>(HttpStatusCode.OK, _objGroup);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, " Group Not Found");
                }
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Group : GetAllGroups", "Problem In getting Group List", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, " Problem In getting Group List");
            }
        }
        #endregion
    }
}
