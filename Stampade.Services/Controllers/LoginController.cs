﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    public class LoginController : ApiController
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion


        #region Property

        public string encryptPass = "";
        public string decryptPass = "";
        public string emailId = null;
        public string userId = null;
        public string userName = "";
        public long id;
        public string uName = "";
        public string pswd = "";
        public string password = "";
        public string email = "";
        public string newPassword = null;
        #endregion

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateUser(STAM_MA_USERS objUser)
        {
            Login objCreateUser = new Login();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                string randPass = objUtility.GenerateRandomPassword();
                string encryptId = AccountController.EncodePassword(randPass, "SAM1");
                objUser.STAM_USER_PSWD = encryptId;
                string name=objUser.STAM_USER_FNAME +" "+objUser.STAM_USER_LNAME;
                string coachAdminName = objUser.STAM_ACTIVATED_STRING;
                objUser.STAM_ACTIVATED_STRING = null;

                if (!objCreateUser.Create_User(objUser))
                {
                    objLog.LogToFile("Error", "Account : CreateUser", "Problem In Creating a new User inside create Coach", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                objCreateUser = null;

                sendMails(objUser.STAM_USER_USERNAME, objUser.SAM_CREATED_USER.ToString(), randPass, name, objUser.STAM_USER_TYPE.Value, coachAdminName);
                return response;

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Account : CreateUser", "Problem In Creating a new Coach", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        public bool sendMails(string strToMailId, string userId, string pass,string name,int type,string CoachName)
        {
            try
            {
                Communication comm = new Communication();
                comm.strToMailId = strToMailId;
                var fileName = "";

                string bodystring = "";
                string mailSubject = "";

                if (type == BusinessConstants.UserType.CoachAdmin)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath("/Content/email/email_template_for_user.html");
                    mailSubject = "Coach Admin";
                }
                else if (type == BusinessConstants.UserType.AsstCoach)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath("/Content/email/email_template_for_user -AsstCoach.html");
                    mailSubject = "AsstCoach Admin";
                    //bodystring += "You have been added as Asst Coach by " + CoachName +"<br>";
                }
                else if (type == BusinessConstants.UserType.Athlete)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath("/Content/email/email_template_for_user -Athlete.html");
                    mailSubject = "Athlete Admin";
                }
                var sr = new StreamReader(fileName);
                Utility utility = new Utility();

                // string encryptId = utility.Encryptdata(utility.GenerateRandomString());

                bodystring += "Please Find your login Credentials<br>";
                bodystring += "<br><br> your user name : " + strToMailId + "<br> your password is : " + pass + "<br>";

                string body = sr.ReadToEnd();
                body = body.Replace("{LOGIN_URL}", BusinessConstants.MAIL_SIGNUPs_URL + "?Flag=" + userId);
                body = body.Replace("{UserName}", name);
                body = body.Replace("{BODY}", bodystring);
                comm.body = body;
                comm.mailSubject = mailSubject;

                var retstatus = CommonThread.GenerateEmailThread(comm.SendRegistrationMail);

                return true;
            }
            catch (Exception Ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccountController : sendMail", Ex.Message, "Problem In Sending a Mail");
                return false;
            }
        }

        [HttpGet]
        public string GetUserIDByTypeID(string typeID, int UserType, long UserID)
        {
            Login obj_AsstCoach = new Login();
            try
            {
                string _objAsstCoach = obj_AsstCoach.GetUserTypeID(typeID, UserType, UserID);
                return _objAsstCoach;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "AsstCoach : Getting CoachList", "Problem In getting AsstCoach", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// check a user details is invalid or not and get a user details
        /// </summary>
        /// <param name="sUserName"></param>
        /// <param name="sPassword"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage Login(string sUserName, string sPassword)
        {
            try
            {
                Accounts objAccount = new Accounts();
                List<Coachs> lstcoach = new List<Coachs>();
                if (Request.Method == HttpMethod.Options)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                string pswd1 = AccountController.EncodePassword(sPassword, "SAM1");
                // check whether given account in db or not
                STAM_MA_USERS objUser = context.STAM_MA_USERS.Where(p => p.STAM_USER_USERNAME == sUserName && p.STAM_USER_PSWD == pswd1).SingleOrDefault();
                if (objUser != null)
                {
                    objAccount.UserId = objUser.STAM_USERID;
                    objAccount.UserType = objUser.STAM_USER_TYPE;
                    objAccount.UserStatusCode = objUser.STAM_USER_STATUS_CODE;
                    objAccount.UserRole = objUser.SAM_USER_ROLE;
                    objAccount.UserFName = objUser.STAM_USER_FNAME;
                    objAccount.UserLName = objUser.STAM_USER_LNAME;
                    objAccount.UserUsername = objUser.STAM_USER_USERNAME;
                    objAccount.UserPassword = objUser.STAM_USER_PSWD;
                    objAccount.UserActivatedStatus = objUser.STAM_ACTIVATED_STATUS;
                    objAccount.UserCreatedUser = objUser.SAM_CREATED_USER;
                    objAccount.UserCreatedDate = objUser.STAM_CREATED_DATE;
                    var objTeam = (from coach in context.STAM_MA_COACH
                                   join team in context.STAM_MA_TEAM on coach.STAM_COACH_TEAMID equals team.STAM_TEAM_ID
                                   where team.STAM_CREATED_USER == objAccount.UserId && coach.STAM_CREATED_USER == objAccount.UserId
                                   select coach).ToList();
                    if (objTeam.Count != 0)
                    {
                        foreach (var item in objTeam)
                        {
                            Coachs objcoach = new Coachs();
                            objcoach.coachId = item.STAM_COACH_ID;
                            objcoach.coachFName = item.STAM_COACH_FIRSTNAME;
                            objcoach.coachLName = item.STAM_COACH_LASTNAME;
                            objcoach.coachMobile = item.STAM_COACH_MOBILENO;
                            objcoach.coachStatus = item.STAM_COACH_STATUS;
                            objcoach.coachCreatedUser = item.STAM_CREATED_USER;
                            objcoach.coachCreatedDate = item.STAM_CREATED_DATE;
                            objcoach.teamId = item.STAM_COACH_TEAMID;
                            objcoach.teamName = item.STAM_COACH_TEAMNAME;
                            objcoach.coachEMail = item.STAM_COACH_EMAILADD;
                            lstcoach.Add(objcoach);
                        }
                        objAccount.lstCoachs = lstcoach;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, objAccount);
                }
                else
                {
                    string userErrorMessage = "Invalid Account!";
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, userErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccountsController : Login ", ex.Message, "");
                string userErrorMessage = "Problem to getting userdetails!";
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, userErrorMessage);
            }
        }

        /// <summary>
        /// Send a password through mail to specify user
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage ForgetPassword(string email)
        {
            try
            {
                if (Request.Method == HttpMethod.Options)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                string status = "";
                // check whether given account in db or not
                var loginResult = from log in context.STAM_MA_USERS where log.STAM_USER_USERNAME == email select log;
                int countUserDetails = 0;
                countUserDetails = loginResult.Count();
                string getSelcectOption = null;
                if (countUserDetails >= 1)
                {
                    IList<STAM_MA_USERS> logins = loginResult.ToList<STAM_MA_USERS>();
                    userName = logins[0].STAM_USER_FNAME.ToString() + "" + logins[0].STAM_USER_LNAME.ToString();
                    emailId = logins[0].STAM_USER_USERNAME.ToString();
                    userId = logins[0].STAM_USERID.ToString();

                        Utility utility = new Utility();
                        string randPass = utility.GenerateRandomPassword();
                        string encodedPass = EncodePassword(randPass, "SAM1");

                        password = randPass;
                        logins[0].STAM_USER_PSWD = Convert.ToString(encodedPass);
                        logins[0].STAM_ACTIVATED_STATUS = true;

                        context.Entry(logins[0]).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();

                        AccountController obj = new AccountController();
                        status = "Y";
                        //if (logins[0].STAM_USER_TYPE_KEY_ID == 5)
                        //    eempid = ManageForPartner(userId);
                        //else if (logins[0].SAM_USER_STATUS_CODE == 11)
                        //    eempid = logins[0].SAM_USER_STATUS_CODE;

                        obj.sendForgetPasswordMail(emailId, getSelcectOption, userName, password);
                        string emailSuccessMessage = "Mail sent successfully";
                        return Request.CreateErrorResponse(HttpStatusCode.OK, emailSuccessMessage);
                }
                else
                {
                    string emailErrorMessage = "Please enter a valid emailId";
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, emailErrorMessage);
                }
            }
            catch (Exception Ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccountController : forgotpassword ", Ex.Message, "");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Problem in forgot password");
            }
        }


        #region Forget password

        public bool sendForgetPasswordMail(string strToMailId, string getSelcectOption, string uName, string pswd)
        {
            try
            {
                Communication comm = new Communication();
                comm.strToMailId = strToMailId;
                var fileName = System.Web.HttpContext.Current.Server.MapPath("/Content/email/email_template1.html");
                var sr = new StreamReader(fileName);

                string bodystring = "";
                string mailSubject = "";

                bodystring += "Login Credentials are : <br><br> your user name : " + strToMailId + "<br> your password is : " + pswd + "<br>";
                mailSubject = "Your forgotten User credentials Details";

                string body = sr.ReadToEnd();
                body = body.Replace("{Name}", uName);
                body = body.Replace("{LOGIN_URL}", BusinessObjects.Constants.BusinessConstants.MAIL_SIGNUPs_URL);
                body = body.Replace("{BODY}", bodystring);
                comm.body = body;
                comm.mailSubject = mailSubject;
                var retstatus = CommonThread.GenerateEmailThread(comm.SendRegistrationMail);
                return true;
            }
            catch (Exception Ex)
            {
                Logger log = new Logger();
                log.LogToFile("Error", "AccoundController : sendMail", Ex.Message, "Problem In Sending a Mail");
                return false;
            }
        }
        #endregion


        # region Encode Password

        public static string EncodePassword(string pass, string salt)
        {
            try
            {
                //var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(salt);
                byte[] bytes = Encoding.Unicode.GetBytes(pass);
                byte[] src = Convert.FromBase64String(salt);
                byte[] dst = new byte[src.Length + bytes.Length];
                Buffer.BlockCopy(src, 0, dst, 0, src.Length);
                Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
                HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
                byte[] inArray = algorithm.ComputeHash(dst);
                return Convert.ToBase64String(inArray);
            }
            catch (Exception Ex)
            {
                Logger v_oLog = new Logger();
                v_oLog.LogToFile("Error", "AccountController : EncodePassword ", Ex.Message, "");

                return "";
            }
        }
        #endregion
    }
}
