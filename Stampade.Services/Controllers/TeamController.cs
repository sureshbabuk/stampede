﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;


namespace Stampade.Services.Controllers
{
    public class TeamController : ApiController
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateTeam(STAM_MA_TEAM objTeam)
        {
            Team objCreateTeam = new Team();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (objTeam.STAM_TEAM_ID == 0)
                {
                    if (!objCreateTeam.Create_Team(objTeam))
                    {
                                objLog.LogToFile("Error", "TeamController : CreateJob", "Problem In Creating a new Team inside create Team", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    objCreateTeam = null;
                    return response;
                }
                else
                {
                    if (!objCreateTeam.Update_Team(objTeam))
                    {
                                objLog.LogToFile("Error", "TeamController : UpdateJob", "Problem In Updating a Team ", "");
                        objLog = null;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    objCreateTeam = null;
                    return Request.CreateResponse(HttpStatusCode.OK);
                }

            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "TeamController : CreateTeam", "Problem In Creating a new Team", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }



        [HttpGet]
        public IEnumerable<STAM_MA_TEAM> GetTeams(long UserId)
        {
            Team objUpdateTeam = new Team();
            try
            {
                IList<STAM_MA_TEAM> _objTeam = objUpdateTeam.GetTeamList(UserId);
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Team : Getting TeamList", "Problem In getting List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_TEAM GetTeam(long iTeam)
        {
            Team objUpdateTeam = new Team();
            try
            {
                STAM_MA_TEAM _objTeam = objUpdateTeam.GetTeamByID(iTeam);
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Team : Getting TeamList", "Problem In getting team", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_TEAM GetTeamByName(string Name)
        {
            Team objTeam = new Team();
            try
            {
                STAM_MA_TEAM _objTeam = objTeam.GetTeamByTeamName(Name);
                return _objTeam;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Team : Getting Team", "Problem In getting Team", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

    }
}
