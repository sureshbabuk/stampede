﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    public class WorkOutResultController : BaseController
    {
        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion


        /// <summary>
        /// Create workout for result details
        /// </summary>
        /// <param name="objWorkResultAndDetailed"></param>
        /// <returns></returns>
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage PostResultForWorkout(WorkoutResuls objWorkoutResuls)
        {
            WorkoutResuls objWorkoutResult = objWorkoutResuls;
            List<WorkoutSectionResult> lstWorkoutDetailedResult = objWorkoutResuls.WorkoutResultDetails;
            try
            {
                bool flag ;
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                if (_objWorkoutResult.CheckWorkout(objWorkoutResult))
                {
                    flag = true;                
                }
                else
                {
                    flag = false;                    
                }
                if (!_objWorkoutResult.AddWorkoutResult(objWorkoutResult, lstWorkoutDetailedResult))
                {
                    objLog.LogToFile("Error", "WorkOutResult : AddResultForWorkout", "Problem In Creating a  WorkOutResult", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Creating a  WorkOutResult");
                }
                if(flag)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Created Successfully");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Updated Successfully");
                }
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : AddResultForWorkout", "Problem In Creating a WorkOutResult", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In Creating a WorkOutResult");
            }
        }


        /// <summary>
        /// Get for all workouts based on start date and end date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllWorkouts(DateTime StartDate, DateTime EndDate, long Userid)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllWorkouts(StartDate, EndDate, Userid);
                if (objWorkResultandDetailed==null || objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a WorkOutResult");
            }
        }


        /// <summary>
        /// Get for all workouts based on start date and end date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetWorkouts(DateTime WorkoutDate, string GroupID, string SectionID)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetWorkouts(WorkoutDate, GroupID, SectionID);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetWorkouts", "Problem In getting a GetWorkouts", "null");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetWorkouts", "Problem In getting a GetWorkouts", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetWorkouts");
            }
        }

        /// <summary>
        /// Get for all results with start and end date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllResults(DateTime StartDate, DateTime EndDate, long Userid)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllResults(StartDate, EndDate, Userid);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllResults", "Problem In getting a GetAllResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllResults", "Problem In getting a GetAllResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetAllResults");
            }
        }


        /// <summary>
        /// Get for all results with group based on groupID
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="lGroupID"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllResultsByGroup(DateTime StartDate, DateTime EndDate, long GroupID)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllResultsByGroup(StartDate, EndDate, GroupID);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllResults", "Problem In getting a GetAllResultsByGroup", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllResultsByGroup", "Problem In getting a GetAllResultsByGroup", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetAllResultsByGroup");
            }
        }


        /// <summary>
        /// Get for all results with section based on sectionID
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="lSectionID"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllResultsBySection(DateTime StartDate, DateTime EndDate, long SectionID)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllResultsBySection(StartDate, EndDate, SectionID);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllResultsBySection", "Problem In getting a GetAllResultsBySection", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllResultsBySection", "Problem In getting a GetAllResultsBySection", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetAllResultsBySection");
            }
        }



        /// <summary>
        /// Get for all results based on workout ID
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="lWorkoutID"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetResultForWorkout(long WorkoutID)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetResultForWorkout(WorkoutID);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetResultForWorkout", "Problem In getting a GetResultForWorkout", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetResultForWorkout", "Problem In getting a GetResultForWorkout", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetResultForWorkout");
            }
        }


        ///// <summary>
        ///// Get for all results with section based on sectionID
        ///// </summary>
        ///// <param name="StartDate"></param>
        ///// <param name="EndDate"></param>
        ///// <param name="lSectionID"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[AcceptVerbs("OPTIONS")]
        //public HttpResponseMessage GetAllWorkoutResults(Request objRequest)
        //{
        //    try
        //    {
        //        WorkoutResult _objWorkoutResult = new WorkoutResult();
        //        List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllWorkoutResults(objRequest);
        //        if (objWorkResultandDetailed.Count == 0)
        //        {
        //            objLog.LogToFile("Error", "WorkOutResult : GetAllResults", "Problem In getting a GetAllResults", "");
        //            objLog = null;
        //            return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
        //        }
        //        return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
        //    }
        //    catch (Exception Ex)
        //    {
        //        objLog = new Logger();
        //        objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", Ex.Message.ToString());
        //        objLog = null;
        //        return null;
        //    }
        //}

        /// <summary>
        /// Get for all results with section based on sectionID
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="lSectionID"></param>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllWorkoutResults(DateTime selectedDate,long coachId,long groupId)
        {
            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllWorkoutResults(selectedDate, coachId, groupId);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutResuls>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetAllWorkoutResults");
            }
        }



        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllNewWorkouts(DateTime selectedDate, long coachId, long groupId)
        {

            try
            {
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                List<WorkoutServiceModel> objWorkResultandDetailed = _objWorkoutResult.GetAllNewWorkouts1(selectedDate, coachId, groupId);
                if (objWorkResultandDetailed.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<WorkoutServiceModel>>(HttpStatusCode.OK, objWorkResultandDetailed);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetAllWorkoutResults");
            }





                
          



              //TRWORKOUTS obj_Coach = new TRWORKOUTS();
              //WorkoutModel _objCoach = new WorkoutModel();



              //_objCoach = obj_Coach.EditNewWorkoutections(CoachID);




             
            //              List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();


            //              List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();
            //              _objSTAM_TR_DETAILS = _objCoach._objSTAM_TR_DETAILS;
  
  
  
            //              List<string> dts=new List<string>();
  
            //              foreach(var data in _objSTAM_TR_DETAILS )
            //              {
            //                  int? r=data.STAM_RUN;
      
            //                  dts.Add(r.ToString());
            //              }


            //              var tr_detail = _objSTAM_TR_DETAILS.GroupBy(x => x.STAM_WORKOUT_DETAILS_ID).ToList();

















            //try
            //{
            //    WorkoutResult _objWorkoutResult = new WorkoutResult();
            //    List<WorkoutResuls> objWorkResultandDetailed = _objWorkoutResult.GetAllWorkouts(StartDate, EndDate, Userid);
            //    if (objWorkResultandDetailed == null || objWorkResultandDetailed.Count == 0)
            //    {
            //        objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", "");
            //        objLog = null;
            //        return Request.CreateErrorResponse(HttpStatusCode.OK, "Workouts Not Found");
            //    }
            //    return Request.CreateResponse(HttpStatusCode.OK, _objSTAM_TR_DETAILS);

            //}
            //catch (Exception Ex)
            //{
            //    objLog = new Logger();
            //    objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
            //    objLog = null;
            //    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a WorkOutResult");
            //}
        }





        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage PostResultForNewWorkout(STAM_TR_WORKOUTS_Model objWorkoutResuls)
        {

            STAM_TR_WORKOUTS_Model objWorkoutResult = objWorkoutResuls;
            List<STAM_TR_DETAILS_Model> lstWorkoutDetailedResult = objWorkoutResult.STAM_TR_DETAILS;
           // List<STAM_TR_DETAILS_SECTION> lstWorkoutSecDetailedResult = objWorkoutResult.;

            try
            {
                bool flag;
                WorkoutResult _objWorkoutResult = new WorkoutResult();
                if (_objWorkoutResult.CheckNewWorkout(objWorkoutResult))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                if (!_objWorkoutResult.AddNewWorkoutResult(objWorkoutResult, lstWorkoutDetailedResult))//, lstWorkoutSecDetailedResult))
                {
                    objLog.LogToFile("Error", "WorkOutResult : AddResultForWorkout", "Problem In Creating a  WorkOutResult", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem in Creating a  WorkOutResult");
                }
                if (flag)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Created Successfully");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Updated Successfully");
                }
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : AddResultForWorkout", "Problem In Creating a WorkOutResult", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In Creating a WorkOutResult");
            }
        }





        /// <summary>
        /// Create and update for new workout with lap.......
        /// </summary>
        /// <param name="objNewWorkout"></param>
        /// <returns></returns>

        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage PostForNewWorkout(NewWorkout objNewWorkout)
        {

            NewWorkout CusNewWorkout = new NewWorkout();
            try
            {
                    WorkoutResult objWorkoutResult = new WorkoutResult();
                    int ResultFlag = objWorkoutResult.CreateWorkoutResult(objNewWorkout);
                if(ResultFlag == 1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Created Successfully");
                }
                else if (ResultFlag == 2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "Updated Successfully");
                }
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In Creating a WorkOutResult");
              
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : PostForNewWorkout", "Problem In Creating a WorkOutResult", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In Creating a WorkOutResult");
            }
        }


        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetAllNewWorkoutResults(DateTime StartDate, DateTime EndDate, long Userid)
        {
            try
            {
                WorkoutResult objWorkoutResult = new WorkoutResult();
                List<NewWorkout> listNewWorkout = objWorkoutResult.GetAllNewWorkoutResults(StartDate, EndDate, Userid);
                if (listNewWorkout == null || listNewWorkout.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<NewWorkout>>(HttpStatusCode.OK, listNewWorkout);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a WorkOutResult");
            }
        }




        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetWorkouts(DateTime StartDate, DateTime EndDate, string GroupId, string SectionID, string AtheleteID, string WorkoutTitle, string UserId)
        {
            try
            {
                WorkoutResult objWorkoutResult = new WorkoutResult();
                List<NewWorkout> ListNewWorkout = new List<NewWorkout>();
                List<NewWorkout> ListOfUserBasedWorkout = new List<NewWorkout>();
                long Userid = 0;
                Userid = Convert.ToInt32(UserId);
                ListNewWorkout = objWorkoutResult.GetWorkouts(StartDate, EndDate, GroupId, SectionID, AtheleteID, WorkoutTitle);
                ListOfUserBasedWorkout = objWorkoutResult.GetListOfUserBasedWorkout(ListNewWorkout, Userid);
                return Request.CreateResponse<List<NewWorkout>>(HttpStatusCode.OK, ListOfUserBasedWorkout);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllWorkouts", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a WorkOutResult");
            }
        }


        [HttpGet]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetNewWorkoutResults(DateTime selectedDate, long coachId, long groupId)
        {
            try
            {
                WorkoutResult objWorkoutResult = new WorkoutResult();
                List<NewWorkout> ListNewWorkout = objWorkoutResult.GetNewWorkoutResults(selectedDate, coachId, groupId);
                if (ListNewWorkout.Count == 0)
                {
                    objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", "");
                    objLog = null;
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workouts Not Found");
                }
                return Request.CreateResponse<List<NewWorkout>>(HttpStatusCode.OK, ListNewWorkout);
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "WorkOutResult : GetAllWorkoutResults", "Problem In getting a GetAllWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Problem In getting a GetAllWorkoutResults");
            }
        }
    }

}