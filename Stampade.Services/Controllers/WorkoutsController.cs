﻿using BusinessObjects.Communication;
using BusinessObjects.Constants;
using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using Stampade.Services.Models.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Stampade.Services.Controllers
{
    public class WorkoutsController : BaseController
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility objUtility = new Utility();

        #endregion  

        
        [HttpPost]
        [AcceptVerbs("OPTIONS")]
        public HttpResponseMessage GetCreateWorkouts(STAM_MA_WORKOUTS objWorkouts)
        {
            Workouts obj_Workouts = new Workouts();

            if (Request.Method == HttpMethod.Options)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                if (objWorkouts.STAM_WORKOUT_ID == 0)
                    {
                        if (!obj_Workouts.Create_Workouts(objWorkouts))
                        {
                            objLog.LogToFile("Error", "WorkoutsController : CreateWorkouts", "Problem In Creating a new Workouts inside create Workouts Admin", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.OK, "", new Stampade.Services.Controllers.BaseController.TextPlainFormatter());
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                        obj_Workouts = null;
                                           
                        return response;
                    }
                    else
                    {
                        if (!obj_Workouts.Update_Workouts(objWorkouts))
                        {
                            objLog.LogToFile("Error", "WorkoutsController : UpdateWorkouts", "Problem In Updating a Workouts", "");
                            objLog = null;
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                        }
                        obj_Workouts = null;
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Workouts : WorkoutsController", "Problem In Creating a new Workouts Admin", Ex.Message.ToString());
                objLog = null;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
         [HttpGet]
        public IEnumerable<STAM_MA_WORKOUTS> GetWorkouts(string createdUser)
        {
            Workouts obj_Workouts = new Workouts();
            try
            {
                IList<STAM_MA_WORKOUTS> _objWorkouts = obj_Workouts.GetWorkoutsList(createdUser);
                return _objWorkouts;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Workouts : Getting Workouts", "Problem In getting Workouts Admin List", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_WORKOUTS GetWorkout(long iWorkouts)
        {
            Workouts obj_Workouts = new Workouts();
            try
            {
                STAM_MA_WORKOUTS _objWorkout = obj_Workouts.GetWorkoutsByID(iWorkouts);
                return _objWorkout;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Workouts : Getting Workouts", "Problem In getting Workouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        [HttpGet]
        public STAM_MA_WORKOUTS GetWorkoutByDistance(string iWorkouts)
        {
            Workouts obj_Workouts = new Workouts();
            try
            {
                STAM_MA_WORKOUTS _objWorkout = obj_Workouts.GetWorkoutsByDistanceID(iWorkouts);
                return _objWorkout;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Workouts : Getting Workouts", "Problem In getting Workouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
        [HttpGet]
        public STAM_MA_WORKOUTS GetWorkoutByTitle(string title,string Userid)
        {
            Workouts obj_Workouts = new Workouts();
            try
            {
                STAM_MA_WORKOUTS _objWorkouts = obj_Workouts.GetWorkoutsBytitle(title,Userid);
                return _objWorkouts;
            }
            catch (Exception Ex)
            {
                objLog.LogToFile("Error", "Workouts : Getting Workouts", "Problem In getting Workouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

      
    }
}
