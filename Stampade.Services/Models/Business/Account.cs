﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Account
    {
        //User
        public long UserId { get; set; }
        public Nullable<int> UserType { get; set; }
        public Nullable<int> UserStatusCode { get; set; }
        public Nullable<int> UserRole { get; set; }
        public string UserFName { get; set; }
        public string UserLName { get; set; }
        public string UserGender { get; set; }
        public string UserAdd1 { get; set; }
        public string UserAdd2 { get; set; }
        public string UserTelephone { get; set; }
        public string UserMobile { get; set; }
        public string UserWebsite { get; set; }
        public string UserEMail { get; set; }
        public byte[] UserImage { get; set; }
        public string UserUsername { get; set; }
        public string UserPassword { get; set; }
        public Nullable<bool> UserIsActivated { get; set; }
        public string UserActivatedString { get; set; }
        public Nullable<long> UserTypeKeyId { get; set; }
        public Nullable<bool> UserActivatedStatus { get; set; }
        public Nullable<long> UserCreatedUser { get; set; }
        public Nullable<System.DateTime> UserCreatedDate { get; set; }
        public Nullable<long> UserLastUpdatedUser { get; set; }
        public Nullable<System.DateTime> UserLastUpdatedDate { get; set; }
        public List<Account> lstAccount { get; set; }
        public Coachs objCoach { get; set; }
        public List<Coachs> lstCoachs { get; set; }
    }
}