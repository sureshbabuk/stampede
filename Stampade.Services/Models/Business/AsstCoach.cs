﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;

namespace Stampade.Services.Models.Business
{
    public class AsstCoach
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();

        #endregion

        #region AsstCoach

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        
        public bool Create_AsstCoach(STAM_MA_ASSTCOACH objAsstCoach)
        {
            try
            {
                STAM_MA_ASSTCOACH objCreateAsstCoach = new STAM_MA_ASSTCOACH()
                {
                    STAM_ASSTCOACH_STATUS = objAsstCoach.STAM_ASSTCOACH_STATUS,
                    STAM_ASSTCOACH_FIRSTNAME = objAsstCoach.STAM_ASSTCOACH_FIRSTNAME,
                    STAM_ASSTCOACH_LASTNAME = objAsstCoach.STAM_ASSTCOACH_LASTNAME,
                    STAM_ASSTCOACH_EMAILADD = objAsstCoach.STAM_ASSTCOACH_EMAILADD,
                    STAM_ASSTCOACH_MOBILENO = objAsstCoach.STAM_ASSTCOACH_MOBILENO,
                    STAM_ASSTCOACH_GROUPID = objAsstCoach.STAM_ASSTCOACH_GROUPID,
                    STAM_ASSTCOACH_GROUPNAME = objAsstCoach.STAM_ASSTCOACH_GROUPNAME,
                    STAM_CREATED_USER = objAsstCoach.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objAsstCoach.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objAsstCoach.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objAsstCoach.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_ASSTCOACH.Add(objCreateAsstCoach);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "AsstCoach : CreateTeam", "Problem In Creating a new AsstCoach", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_AsstCoach(STAM_MA_ASSTCOACH objAsstCoach)
        {
            try
            {
                var objUpdateAsstCoach = objContext.STAM_MA_ASSTCOACH.Where(x => x.STAM_ASSTCOACH_ID == objAsstCoach.STAM_ASSTCOACH_ID).SingleOrDefault();
                {
                    objUpdateAsstCoach.STAM_ASSTCOACH_STATUS = objAsstCoach.STAM_ASSTCOACH_STATUS;
                    objUpdateAsstCoach.STAM_ASSTCOACH_FIRSTNAME = objAsstCoach.STAM_ASSTCOACH_FIRSTNAME;
                    objUpdateAsstCoach.STAM_ASSTCOACH_LASTNAME = objAsstCoach.STAM_ASSTCOACH_LASTNAME;
                    objUpdateAsstCoach.STAM_ASSTCOACH_EMAILADD = objAsstCoach.STAM_ASSTCOACH_EMAILADD;
                    objUpdateAsstCoach.STAM_ASSTCOACH_MOBILENO = objAsstCoach.STAM_ASSTCOACH_MOBILENO;
                    objUpdateAsstCoach.STAM_ASSTCOACH_GROUPID = objAsstCoach.STAM_ASSTCOACH_GROUPID;
                    objUpdateAsstCoach.STAM_ASSTCOACH_GROUPNAME = objAsstCoach.STAM_ASSTCOACH_GROUPNAME;

                    objUpdateAsstCoach.STAM_CREATED_USER = objAsstCoach.STAM_CREATED_USER;
                    objUpdateAsstCoach.STAM_CREATED_DATE = objAsstCoach.STAM_CREATED_DATE;
                    objUpdateAsstCoach.STAM_LAST_UPDATED_USER = objAsstCoach.STAM_LAST_UPDATED_USER;
                    objUpdateAsstCoach.STAM_LAST_UPDATED_DATE = objAsstCoach.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateAsstCoach).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "AsstCoach : UpdateAsstCoach", "Problem In Updating a AsstCoach", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_ASSTCOACH> GetAsstCoachList(long CoachID)
        {
            try
            {
                var obj_AsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_ASSTCOACH_STATUS != 3 && Asstcoachs.STAM_CREATED_USER == CoachID orderby Asstcoachs.STAM_ASSTCOACH_ID descending select Asstcoachs;
                List<STAM_MA_ASSTCOACH> objAsstCoachs = obj_AsstCoach.ToList<STAM_MA_ASSTCOACH>();
                return objAsstCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "AsstCoach : GetAsstCoachs", "Problem In Getting a list of AsstCoach Admins", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ASSTCOACH GetAsstCoachByID(long iAsstCoach)
        {
            try
            {
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_ASSTCOACH_ID == iAsstCoach select Asstcoachs;
                STAM_MA_ASSTCOACH objAsstCoachs = objAsstCoach.FirstOrDefault();
                return objAsstCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "AsstCoach : GetAsstCoachByID", "Problem In Getting a AsstCoach", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ASSTCOACH GetAsstCoachByMail(string MailAdd)
        {
            try
            {
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_ASSTCOACH_EMAILADD == MailAdd && Asstcoachs.STAM_ASSTCOACH_STATUS != 3 select Asstcoachs;
                STAM_MA_ASSTCOACH objAsstCoachs = objAsstCoach.FirstOrDefault();
                return objAsstCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "AsstCoach : GetAsstCoachByID", "Problem In Getting a AsstCoach", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ASSTCOACH GetLastAsstCoach()
        {
            try
            {
                var obj_Coach = from coachs in objContext.STAM_MA_ASSTCOACH orderby coachs.STAM_ASSTCOACH_ID descending select coachs;
                STAM_MA_ASSTCOACH objCoachs = obj_Coach.FirstOrDefault();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach : GetCoachs", "Problem In Getting a list of Coach Admins", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ASSTCOACH> GetAsstCoachByGrpID(string GroupID)
        {
            try
            {
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_ASSTCOACH_GROUPID.Contains(GroupID) && Asstcoachs.STAM_ASSTCOACH_STATUS != 3 select Asstcoachs;
                List<STAM_MA_ASSTCOACH> objAsstCoachs = objAsstCoach.ToList<STAM_MA_ASSTCOACH>();
                return objAsstCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "AsstCoach : GetAsstCoachByID", "Problem In Getting a AsstCoach", ex.Message.ToString());
                return null;
            }
        }


        public string GetAsstCoacListhByCoachAdminID(long UserID)
        {
            try
            {
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == UserID && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                List<STAM_MA_ASSTCOACH> objAsstCoachs = objAsstCoach.ToList<STAM_MA_ASSTCOACH>();

                string result = "";
                foreach (var item in objAsstCoachs)
                {
                    result += item.STAM_ASSTCOACH_ID +",";
                }
                return result;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "AsstCoach : GetAsstCoachByID", "Problem In Getting a AsstCoach", ex.Message.ToString());
                return null;
            }
        }
        
        #endregion
    }
}