﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;

namespace Stampade.Services.Models.Business
{
    public class Athlete
    {
        public int ID { get; set; }
        public string AthleteName { get; set; }
        public int AthleteID { get; set; }
        public int GroupID { get; set; }

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();

        #endregion

        #region AsstCoach

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 

        public bool Create_Ath(STAM_MA_ATHLETE objAth)
        {
            try
            {
                STAM_MA_ATHLETE objCreateAth = new STAM_MA_ATHLETE()
                {
                    STAM_ATH_STATUS = objAth.STAM_ATH_STATUS,
                    STAM_ATH_FIRTSNAME = objAth.STAM_ATH_FIRTSNAME,
                    STAM_ATH_LASTNAME = objAth.STAM_ATH_LASTNAME,
                    STAM_ATH_EMAILADD = objAth.STAM_ATH_EMAILADD,
                    STAM_ATH_MOBILENO = objAth.STAM_ATH_MOBILENO,
                    STAM_ATH_GENDER = objAth.STAM_ATH_GENDER,
                    STAM_ATH_ASSIGNGROUPID = objAth.STAM_ATH_ASSIGNGROUPID,
                    STAM_ATH_ASSIGNGROUP = objAth.STAM_ATH_ASSIGNGROUP,
                    STAM_ATH_SECTIONID = objAth.STAM_ATH_SECTIONID,
                    STAM_ATH_SECTION = objAth.STAM_ATH_SECTION,
                    STAM_USERTYPE = objAth.STAM_USERTYPE,
                    STAM_CREATED_USER = objAth.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objAth.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objAth.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objAth.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_ATHLETE.Add(objCreateAth);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : CreateTeam", "Problem In Creating a new Athlete", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_Ath(STAM_MA_ATHLETE objAth)
        {
            try
            {
                var objUpdateAth = objContext.STAM_MA_ATHLETE.Where(x => x.STAM_ATH_ID == objAth.STAM_ATH_ID).SingleOrDefault();
                {
                    objUpdateAth.STAM_ATH_STATUS = objAth.STAM_ATH_STATUS;
                    objUpdateAth.STAM_ATH_FIRTSNAME = objAth.STAM_ATH_FIRTSNAME;
                    objUpdateAth.STAM_ATH_LASTNAME = objAth.STAM_ATH_LASTNAME;
                    objUpdateAth.STAM_ATH_EMAILADD = objAth.STAM_ATH_EMAILADD;
                    objUpdateAth.STAM_ATH_MOBILENO = objAth.STAM_ATH_MOBILENO;
                    objUpdateAth.STAM_ATH_GENDER = objAth.STAM_ATH_GENDER;
                    objUpdateAth.STAM_ATH_ASSIGNGROUPID = objAth.STAM_ATH_ASSIGNGROUPID;
                    objUpdateAth.STAM_ATH_ASSIGNGROUP = objAth.STAM_ATH_ASSIGNGROUP;
                    objUpdateAth.STAM_ATH_SECTIONID = objAth.STAM_ATH_SECTIONID;
                    objUpdateAth.STAM_ATH_SECTION = objAth.STAM_ATH_SECTION;
                    objUpdateAth.STAM_USERTYPE = objAth.STAM_USERTYPE;
                    objUpdateAth.STAM_CREATED_USER = objAth.STAM_CREATED_USER;
                    objUpdateAth.STAM_CREATED_DATE = objAth.STAM_CREATED_DATE;
                    objUpdateAth.STAM_LAST_UPDATED_USER = objAth.STAM_LAST_UPDATED_USER;
                    objUpdateAth.STAM_LAST_UPDATED_DATE = objAth.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateAth).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : UpdateAth", "Problem In Updating a Athlete", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_ATHLETE> GetAthletesList(string AthID)
        {
            try
            {
                string[] _ath = null;
                _ath = AthID.ToString().TrimEnd(',').Split(',');
                List<STAM_MA_ATHLETE> _objRes = new List<STAM_MA_ATHLETE>();
                List<Stam_Athlete> _objRes1 = new List<Stam_Athlete>();

                foreach (var item in _ath)
                {
                    long item1 = Convert.ToInt64(item);
                    var obj_Ath = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_STATUS != 3 && Ath.STAM_CREATED_USER == item1 orderby Ath.STAM_ATH_ID descending select Ath;
                    if (obj_Ath != null)
                    {
                        _objRes.AddRange(obj_Ath);
                    }

                    var obj_Ath1 = (from Ath in objContext.STAM_MA_ATHLETE
                                    where Ath.STAM_ATH_STATUS != 3 && Ath.STAM_CREATED_USER == item1
                                    orderby Ath.STAM_ATH_ID descending
                                    select new Stam_Athlete()
                                    {
                                        STAM_ATH_ASSIGNGROUP = Ath.STAM_ATH_ASSIGNGROUP,
                                        STAM_ATH_EMAILADD = Ath.STAM_ATH_ASSIGNGROUPID,
                                        STAM_ATH_FIRTSNAME = Ath.STAM_ATH_FIRTSNAME,
                                        STAM_ATH_GENDER = Ath.STAM_ATH_GENDER,
                                        STAM_ATH_ID = Ath.STAM_ATH_ID,
                                        STAM_ATH_LASTNAME = Ath.STAM_ATH_LASTNAME,
                                        STAM_ATH_MOBILENO = Ath.STAM_ATH_MOBILENO,
                                        STAM_ATH_SECTION = Ath.STAM_ATH_SECTION,
                                        STAM_ATH_SECTIONID = Ath.STAM_ATH_SECTIONID,
                                        STAM_ATH_STATUS = Ath.STAM_ATH_STATUS,
                                        STAM_CREATED_DATE = Ath.STAM_CREATED_DATE,
                                        STAM_LAST_UPDATED_DATE = Ath.STAM_LAST_UPDATED_DATE,
                                        STAM_LAST_UPDATED_USER = Ath.STAM_LAST_UPDATED_USER,
                                        STAM_NOTIFICATION_DATE = Ath.STAM_NOTIFICATION_DATE,
                                        STAM_NOTIFICATION_STATUS = Ath.STAM_NOTIFICATION_STATUS,
                                        STAM_DEVICE_ID=Ath.STAM_DEVICE_ID

                                    }).ToList();
                    foreach (var itemdev in obj_Ath1)
                    {

                        int id =itemdev.STAM_DEVICE_ID==null?0: Convert.ToInt32(itemdev.STAM_DEVICE_ID.ToString());
                        itemdev.STAM_DEVICE_ID =  objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault()!=null?objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_ID:"";
                        itemdev.STAM_DEVICE_NAME =  objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault()!=null?objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_NAME:"";



                    }
                    _objRes1.AddRange(obj_Ath1);

                }



                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAth", "Problem In Getting a list of Athletes", ex.Message.ToString());
                return null;
            }
        }



        //indu

        public List<Stam_Athlete> GetAthletesListnew(string AthID)
        {


            try
            {
                string[] _ath = null;
                _ath = AthID.ToString().TrimEnd(',').Split(',');
                List<Stam_Athlete> _objRes = new List<Stam_Athlete>();
                foreach (var item in _ath)
                {
                    long item1 = Convert.ToInt64(item);
                   // var obj_Ath1 = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_STATUS != 3 && Ath.STAM_CREATED_USER == item1 orderby Ath.STAM_ATH_ID descending select Ath;

                    var obj_Ath = (from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_STATUS != 3 && Ath.STAM_CREATED_USER == item1 orderby Ath.STAM_ATH_ID descending 
                                     select  new  Stam_Athlete()
                                     {
                                         STAM_ATH_ASSIGNGROUP=Ath.STAM_ATH_ASSIGNGROUP,
                                         STAM_ATH_EMAILADD=Ath.STAM_ATH_EMAILADD,
                                         STAM_ATH_FIRTSNAME=Ath.STAM_ATH_FIRTSNAME,
                                         STAM_ATH_GENDER=Ath.STAM_ATH_GENDER,
                                         STAM_ATH_ID=Ath.STAM_ATH_ID,
                                         STAM_ATH_LASTNAME=Ath.STAM_ATH_LASTNAME,
                                         STAM_ATH_MOBILENO=Ath.STAM_ATH_MOBILENO,
                                         STAM_ATH_SECTION=Ath.STAM_ATH_SECTION,
                                         STAM_ATH_SECTIONID=Ath.STAM_ATH_SECTIONID,
                                         STAM_ATH_STATUS=Ath.STAM_ATH_STATUS,
                                         STAM_CREATED_DATE=Ath.STAM_CREATED_DATE,
                                         STAM_LAST_UPDATED_DATE=Ath.STAM_LAST_UPDATED_DATE,
                                         STAM_LAST_UPDATED_USER=Ath.STAM_LAST_UPDATED_USER,
                                         STAM_NOTIFICATION_DATE=Ath.STAM_NOTIFICATION_DATE,
                                         STAM_NOTIFICATION_STATUS=Ath.STAM_NOTIFICATION_STATUS,

                                       STAM_DEVICE_ID=Ath.STAM_DEVICE_ID,

                                       STAM_USERTYPE=Ath.STAM_USERTYPE,
                                       STAM_ATH_ASSIGNGROUPID=Ath.STAM_ATH_ASSIGNGROUPID,
                                       
                                     }).ToList();

                                  //  select Ath;
                    if (obj_Ath != null)
                    {

                        foreach(var itemdev in obj_Ath)
                        {

                            int id = itemdev.STAM_DEVICE_ID == null ? 0 : Convert.ToInt32(itemdev.STAM_DEVICE_ID.ToString());
                            itemdev.STAM_DEVICE_ID = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_ID : "";
                            itemdev.STAM_DEVICE_NAME = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_NAME : "";

                        }
                        _objRes.AddRange(obj_Ath);
                    }
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAth", "Problem In Getting a list of Athletes", ex.Message.ToString());
                return null;
            }
        }
        //




        public List<Stam_Athlete> GetAthletesListDevice(string AthID)
        {
            try
            {
                string[] _ath = null;
                _ath = AthID.ToString().TrimEnd(',').Split(',');
                List<STAM_MA_ATHLETE> _objRes = new List<STAM_MA_ATHLETE>();
                List<Stam_Athlete> _objRes1 = new List<Stam_Athlete>();


                foreach (var item in _ath)
                {
                    long item1 = Convert.ToInt64(item);
                    var obj_Ath = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_STATUS != 3 && Ath.STAM_CREATED_USER == item1 && (Ath.STAM_DEVICE_ID ==null||Ath.STAM_DEVICE_ID=="") orderby Ath.STAM_ATH_ID descending select Ath;
                    if (obj_Ath != null)
                    {
                        _objRes.AddRange(obj_Ath);
                    }

                    var obj_Ath1 = (from Ath in objContext.STAM_MA_ATHLETE
                                   // join s in objContext.STAM_MA_USERS on Ath.STAM_CREATED_USER equals s.STAM_USERID
                                  //  where (s.SAM_CREATED_USER == item1 || Ath.STAM_CREATED_USER == item1)
                                    where Ath.STAM_ATH_STATUS != 3 && Ath.STAM_CREATED_USER == item1 && (Ath.STAM_DEVICE_ID == null || Ath.STAM_DEVICE_ID == "" || Ath.STAM_DEVICE_ID =="0") 
                                    orderby Ath.STAM_ATH_ID descending
                                    select new Stam_Athlete()
                                    {
                                        STAM_ATH_ASSIGNGROUP = Ath.STAM_ATH_ASSIGNGROUP,
                                        STAM_ATH_EMAILADD = Ath.STAM_ATH_EMAILADD,
                                        STAM_ATH_FIRTSNAME = Ath.STAM_ATH_FIRTSNAME,
                                        STAM_ATH_GENDER = Ath.STAM_ATH_GENDER,
                                        STAM_ATH_ID = Ath.STAM_ATH_ID,
                                        STAM_ATH_LASTNAME = Ath.STAM_ATH_LASTNAME,
                                        STAM_ATH_MOBILENO = Ath.STAM_ATH_MOBILENO,
                                        STAM_ATH_SECTION = Ath.STAM_ATH_SECTION,
                                        STAM_ATH_SECTIONID = Ath.STAM_ATH_SECTIONID,
                                        STAM_ATH_STATUS = Ath.STAM_ATH_STATUS,
                                        STAM_CREATED_DATE = Ath.STAM_CREATED_DATE,
                                        STAM_LAST_UPDATED_DATE = Ath.STAM_LAST_UPDATED_DATE,
                                        STAM_LAST_UPDATED_USER = Ath.STAM_LAST_UPDATED_USER,
                                        STAM_NOTIFICATION_DATE = Ath.STAM_NOTIFICATION_DATE,
                                        STAM_NOTIFICATION_STATUS = Ath.STAM_NOTIFICATION_STATUS,
                                         STAM_DEVICE_ID=Ath.STAM_DEVICE_ID,
                                        STAM_USERTYPE = Ath.STAM_USERTYPE,
                                        STAM_ATH_ASSIGNGROUPID = Ath.STAM_ATH_ASSIGNGROUPID,

                                    }).ToList();
                    foreach (var itemdev in obj_Ath1)
                    {

                        int id = itemdev.STAM_DEVICE_ID == null ? 0 : Convert.ToInt32(itemdev.STAM_DEVICE_ID.ToString());
                        itemdev.STAM_DEVICE_ID = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_ID : "";
                        itemdev.STAM_DEVICE_NAME = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_NAME : "";



                    }
                    _objRes1.AddRange(obj_Ath1);

                }



                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAth", "Problem In Getting a list of Athletes", ex.Message.ToString());
                return null;
            }
        }
        public STAM_MA_ATHLETE GetAthByID(long iAth)
        {
            try
            {
                var objAth = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_ID == iAth select Ath;
                STAM_MA_ATHLETE _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }


        public Stam_Athlete GetAthleteByID(long iAth)
        {


            try
            {

                var objAth = (from Ath in objContext.STAM_MA_ATHLETE
                 where Ath.STAM_ATH_ID == iAth
             
                 select new Stam_Athlete()
                 {
                     STAM_ATH_ASSIGNGROUP = Ath.STAM_ATH_ASSIGNGROUP,
                     STAM_ATH_EMAILADD = Ath.STAM_ATH_EMAILADD,
                     STAM_ATH_FIRTSNAME = Ath.STAM_ATH_FIRTSNAME,
                     STAM_ATH_GENDER = Ath.STAM_ATH_GENDER,
                     STAM_ATH_ID = Ath.STAM_ATH_ID,
                     STAM_ATH_LASTNAME = Ath.STAM_ATH_LASTNAME,
                     STAM_ATH_MOBILENO = Ath.STAM_ATH_MOBILENO,
                     STAM_ATH_SECTION = Ath.STAM_ATH_SECTION,
                     STAM_ATH_SECTIONID = Ath.STAM_ATH_SECTIONID,
                     STAM_ATH_STATUS = Ath.STAM_ATH_STATUS,
                     STAM_CREATED_DATE = Ath.STAM_CREATED_DATE,
                     STAM_LAST_UPDATED_DATE = Ath.STAM_LAST_UPDATED_DATE,
                     STAM_LAST_UPDATED_USER = Ath.STAM_LAST_UPDATED_USER,
                     STAM_NOTIFICATION_DATE = Ath.STAM_NOTIFICATION_DATE,
                     STAM_NOTIFICATION_STATUS = Ath.STAM_NOTIFICATION_STATUS,
                       STAM_DEVICE_ID=Ath.STAM_DEVICE_ID,
                     STAM_USERTYPE = Ath.STAM_USERTYPE,
                     STAM_ATH_ASSIGNGROUPID = Ath.STAM_ATH_ASSIGNGROUPID,

                 }).ToList();

              //  var objAth = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_ID == iAth select Ath;
                
                List<Stam_Athlete> _objRes = new List<Stam_Athlete>();
                if (objAth != null)
                {

                    foreach (var itemdev in objAth)
                    {

                        int id = itemdev.STAM_DEVICE_ID == null ? 0 : Convert.ToInt32(itemdev.STAM_DEVICE_ID.ToString());
                        itemdev.STAM_DEVICE_ID = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_ID : "";
                        itemdev.STAM_DEVICE_NAME = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_NAME : "";

                    }
                    _objRes.AddRange(objAth);
                }
                Stam_Athlete _objAth = _objRes.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }
        public STAM_MA_ATHLETE GetAthByMail(string MailAdd)
        {
            try
            {
                var objAth = from ATh in objContext.STAM_MA_ATHLETE where ATh.STAM_ATH_EMAILADD == MailAdd && ATh.STAM_ATH_STATUS != 3 select ATh;
                STAM_MA_ATHLETE _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ATHLETE> GetAthByAthSec(long iAth)
        {
            try
            {
                var objAth = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_SECTIONID == iAth && Ath.STAM_ATH_STATUS != 3 select Ath;
                List<STAM_MA_ATHLETE> _objAth = objAth.ToList<STAM_MA_ATHLETE>();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ATHLETE> GetAthByGroup(string grp)
        {
            try
            {
                var objAth = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_ASSIGNGROUP == grp && Ath.STAM_ATH_STATUS != 3 select Ath;
                List<STAM_MA_ATHLETE> _objAth = objAth.ToList<STAM_MA_ATHLETE>();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ATHLETE> GetAthByGroupID(string GrpID)
        {
            try
            {
                var objAth = from Ath in objContext.STAM_MA_ATHLETE where Ath.STAM_ATH_ASSIGNGROUPID.Contains(GrpID) && Ath.STAM_ATH_STATUS != 3 select Ath;
                List<STAM_MA_ATHLETE> _objAth = objAth.ToList<STAM_MA_ATHLETE>();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ATHLETE GetLastAth()
        {
            try
            {
                var objAth = from ATh in objContext.STAM_MA_ATHLETE orderby ATh.STAM_ATH_ID descending select ATh;
                STAM_MA_ATHLETE _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ATHLETE> GetAssignedAthlete(string Ath)
        {
            try
            {
                string[] _ath = null;
                _ath = Ath.ToString().Split(',');
                List<STAM_MA_ATHLETE> _objRes = new List<STAM_MA_ATHLETE>();

                foreach (var item in _ath)
                {
                    long item1 = Convert.ToInt32(item);
                    var objAth = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_SECTIONID == item1 && ath.STAM_ATH_STATUS != 3 select ath;
                    if (objAth != null)
                    {
                        _objRes.AddRange(objAth);
                    }
                }

                //List<STAM_MA_ATHLETE> _objAth = objAth.ToList<STAM_MA_ATHLETE>();
                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAthByID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ATHLETE> GetCoachGroupList(string grp, string UserID)
        {
            try
            {
                string[] _grp = null;
                _grp = grp.ToString().TrimEnd(',').Split(',');
                string[] _UserID = null;
                _UserID = UserID.ToString().Split(',');
                List<STAM_MA_ATHLETE> _objRes = new List<STAM_MA_ATHLETE>();
                List<STAM_MA_ATHLETE> _objRes1 = new List<STAM_MA_ATHLETE>();

                foreach (var item in _grp)
                {
                    var objAth = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_ASSIGNGROUPID.Contains(item) && ath.STAM_ATH_STATUS != 3 select ath;
                    if (objAth != null)
                    {
                        _objRes.AddRange(objAth);
                    }
                }
                foreach (var item in _UserID)
                {
                    var objAth1 = from ath in _objRes where ath.STAM_CREATED_USER == long.Parse(item) && ath.STAM_ATH_STATUS != 3 select ath;
                    if (objAth1 != null)
                    {
                        _objRes1.AddRange(objAth1);
                    }
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetCoachGroupList", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }







        public List<Stam_Athlete> GetCoachGroupListNew(string grp, string UserID)
        {

            try
            {
                string[] _grp = null;
                _grp = grp.ToString().TrimEnd(',').Split(',');
                string[] _UserID = null;
                _UserID = UserID.ToString().Split(',');
                List<Stam_Athlete> _objRes = new List<Stam_Athlete>();
                List<Stam_Athlete> _objRes1 = new List<Stam_Athlete>();
                //List<Stam_Athlete> _objRes2 = new List<Stam_Athlete>();

            //    List<Stam_Athlete> _objRes3 = new List<Stam_Athlete>();


                foreach (var item in _grp)
                {
                 //  var objAth = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_ASSIGNGROUPID.Contains(item) && ath.STAM_ATH_STATUS != 3 select ath;




                   var objAth = (from Ath in objContext.STAM_MA_ATHLETE
                                   where Ath.STAM_ATH_ASSIGNGROUPID.Contains(item) && Ath.STAM_ATH_STATUS != 3 
                                   select new Stam_Athlete()
                                   {
                                       STAM_ATH_ASSIGNGROUP = Ath.STAM_ATH_ASSIGNGROUP,
                                       STAM_ATH_EMAILADD = Ath.STAM_ATH_EMAILADD,
                                       STAM_ATH_FIRTSNAME = Ath.STAM_ATH_FIRTSNAME,
                                       STAM_ATH_GENDER = Ath.STAM_ATH_GENDER,
                                       STAM_ATH_ID = Ath.STAM_ATH_ID,
                                       STAM_ATH_LASTNAME = Ath.STAM_ATH_LASTNAME,
                                       STAM_ATH_MOBILENO = Ath.STAM_ATH_MOBILENO,
                                       STAM_ATH_SECTION = Ath.STAM_ATH_SECTION,
                                       STAM_ATH_SECTIONID = Ath.STAM_ATH_SECTIONID,
                                       STAM_ATH_STATUS = Ath.STAM_ATH_STATUS,
                                       STAM_CREATED_DATE = Ath.STAM_CREATED_DATE,
                                       STAM_LAST_UPDATED_DATE = Ath.STAM_LAST_UPDATED_DATE,
                                       STAM_LAST_UPDATED_USER = Ath.STAM_LAST_UPDATED_USER,
                                       STAM_NOTIFICATION_DATE = Ath.STAM_NOTIFICATION_DATE,
                                       STAM_NOTIFICATION_STATUS = Ath.STAM_NOTIFICATION_STATUS,
                                       STAM_DEVICE_ID = Ath.STAM_DEVICE_ID,
                                       STAM_USERTYPE = Ath.STAM_USERTYPE,
                                       STAM_ATH_ASSIGNGROUPID = Ath.STAM_ATH_ASSIGNGROUPID,
                                       STAM_CREATED_USER=Ath.STAM_CREATED_USER

                                   }).ToList();
                    if (objAth != null)
                    {
                        foreach (var itemdev in objAth)
                        {

                            int id = itemdev.STAM_DEVICE_ID == null ? 0 : Convert.ToInt32(itemdev.STAM_DEVICE_ID.ToString());
                            itemdev.STAM_DEVICE_ID = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_ID : "";
                            itemdev.STAM_DEVICE_NAME = objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_DEVICES.Where(x => x.STAM_ID == id).FirstOrDefault().STAM_DEVICE_NAME : "";

                        }
                        _objRes.AddRange(objAth);
                    }

                }
                foreach (var item in _UserID)
                {
                    var objAth1 = from ath in _objRes where ath.STAM_CREATED_USER == long.Parse(item) && ath.STAM_ATH_STATUS != 3 select ath;
                    if (objAth1 != null)
                    {




                        _objRes1.AddRange(objAth1);
                    }
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetCoachGroupList", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }
        #region Hema
        public List<STAM_MA_ATHLETE> GetAllAthletes(long UserID)
        {
            try
            {
                List<STAM_MA_ATHLETE> _objRes1 = new List<STAM_MA_ATHLETE>();
                var objAth1 = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_CREATED_USER == UserID && ath.STAM_ATH_STATUS == 1 select ath;
                if (objAth1 != null)
                {
                    _objRes1 = objAth1.ToList();
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletes", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }


        public List<STAM_MA_ATHLETE> GetAllAthletesForGroup(long GroupID)
        {
            try
            {
                string sGroupID = GroupID.ToString();
                List<STAM_MA_ATHLETE> _objRes1 = new List<STAM_MA_ATHLETE>();
                var objAth1 = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_ASSIGNGROUPID == sGroupID && ath.STAM_ATH_STATUS == 1 select ath;
                if (objAth1 != null)
                {
                    _objRes1 = objAth1.ToList();
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletesForGroup", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }



        public List<STAM_MA_ATHLETE> GetAllAthletesForSection(long SectionID)
        {
            try
            {
                List<STAM_MA_ATHLETE> _objRes1 = new List<STAM_MA_ATHLETE>();
                var objAth1 = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_SECTIONID == SectionID && ath.STAM_ATH_STATUS == 1 select ath;
                if (objAth1 != null)
                {
                    _objRes1 = objAth1.ToList();
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletesForSection", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }


        public List<Athletes> listAthelets(long groupId)
        {
            try
            {
                string sgroupID = Convert.ToString(groupId);
                List<Athletes> _objRes1 = new List<Athletes>();
                var objAth1 = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_ASSIGNGROUPID == sgroupID  select ath;
                if (objAth1 != null)
                {
                    foreach (var item in objAth1)
                    {
                        Athletes obj_Ath = new Athletes();
                        obj_Ath.athleteID = Convert.ToInt32(item.STAM_ATH_ID);
                        obj_Ath.athleteName = item.STAM_ATH_FIRTSNAME+""+item.STAM_ATH_LASTNAME;
                        obj_Ath.firstName = item.STAM_ATH_FIRTSNAME;
                        obj_Ath.lastName = item.STAM_ATH_LASTNAME;
                        obj_Ath.mobileNo = item.STAM_ATH_MOBILENO;
                        obj_Ath.sectionID = Convert.ToInt32(item.STAM_ATH_SECTIONID);
                        obj_Ath.sectionName = item.STAM_ATH_SECTION;
                        obj_Ath.groupName = item.STAM_ATH_ASSIGNGROUP;
                        if(item.STAM_ATH_STATUS == 1)
                        {
                        obj_Ath.status = "Activated";
                        }
                        else
                        {
                        obj_Ath.status = "DeActivated";
                        }
                        obj_Ath.emailAdd = item.STAM_ATH_EMAILADD;
                        obj_Ath.gender = item.STAM_ATH_GENDER;
                        obj_Ath.groupID = Convert.ToInt32(item.STAM_ATH_ASSIGNGROUPID);
                        if (item.STAM_DEVICE_ID != null)
                        {
                            int deviceId=Convert.ToInt32(item.STAM_DEVICE_ID);
                            STAM_MA_DEVICES objDevice = objContext.STAM_MA_DEVICES.Where(d => d.STAM_ID == deviceId).SingleOrDefault();
                            obj_Ath.deviceID = objDevice.STAM_DEVICE_ID;
                        }
                        else
                        {
                            obj_Ath.deviceID = item.STAM_DEVICE_ID;
                        }
                        obj_Ath.message = item.STAM_NOTIFICATION_STATUS;
                        _objRes1.Add(obj_Ath);
                    }
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletes", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }


        public bool Create_SaveDevce(Request objDEVICES)
        {
            try
            {
                var ListOfItems = objContext.STAM_MA_DEVICES.Where(p => p.STAM_DEVICE_ID == objDEVICES.deviceID && p.STAM_DEVICE_NAME == objDEVICES.deviceName).ToList();
                STAM_MA_ATHLETE _ObjAthlets = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ID==objDEVICES.AthleteID).SingleOrDefault();

                if (ListOfItems.Count == 0)
                {
                    STAM_MA_DEVICES _objStamDevice = new STAM_MA_DEVICES()
                    {
                        STAM_DEVICE_ID = objDEVICES.deviceID,
                        STAM_DEVICE_NAME = objDEVICES.deviceName,
                    };
                    objContext.STAM_MA_DEVICES.Add(_objStamDevice);
                    objContext.SaveChanges();
                    if (_ObjAthlets != null)
                    {
                        _ObjAthlets.STAM_DEVICE_ID =_objStamDevice.STAM_ID.ToString();
                        objContext.SaveChanges();
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : CreateTeam", "Problem In Creating a new Device", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public bool Check_Devce(Request objDEVICES)
        {
            try
            {
                List<STAM_MA_DEVICES> ListOfItems = null;

                if (objDEVICES.ID == 0)
                {
                     ListOfItems = objContext.STAM_MA_DEVICES.Where(p => p.STAM_DEVICE_ID == objDEVICES.deviceID || p.STAM_DEVICE_NAME == objDEVICES.deviceName).ToList();
                }
                else
                {
                    ListOfItems = objContext.STAM_MA_DEVICES.Where(p => (p.STAM_DEVICE_ID == objDEVICES.deviceID || p.STAM_DEVICE_NAME == objDEVICES.deviceName) && p.STAM_ID != objDEVICES.ID).ToList();

                }
               // var ListOfItems = objContext.STAM_MA_DEVICES.Where(p => p.STAM_DEVICE_ID == objDEVICES.deviceID || p.STAM_DEVICE_NAME == objDEVICES.deviceName).ToList();
                if (ListOfItems.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : Check_Devce", "Problem In Check a Device for create", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        public bool Check_UpdateDevce(Request objRequest)
        {
            try
            {
                string deviceID = string.Empty;
                string sgroup = Convert.ToString(objRequest.groupID);
                STAM_MA_DEVICES _objDev = objContext.STAM_MA_DEVICES.Where(p => p.STAM_DEVICE_ID == objRequest.deviceID).SingleOrDefault();
                if (_objDev != null)
                {
                    deviceID = Convert.ToString(_objDev.STAM_ID);
                }
                STAM_MA_ATHLETE _ObjAthlets = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ASSIGNGROUPID == sgroup && p.STAM_DEVICE_ID == deviceID).SingleOrDefault();
                if (_ObjAthlets != null && _objDev != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : CreateTeam", "Problem In Check a Device for update", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }



        public bool Update_SaveDevices(Request objRequest)
        {

            try
            {
                string deviceID = string.Empty;
                string sgroup = Convert.ToString(objRequest.groupID);
                STAM_MA_DEVICES _objDev = objContext.STAM_MA_DEVICES.Where(p => p.STAM_ID == objRequest.ID).SingleOrDefault();

                if (_objDev != null)
                {




                    var _objDevup = objContext.STAM_MA_DEVICES.Where(p => p.STAM_ID == objRequest.ID).SingleOrDefault();
                    {
                        _objDevup.STAM_DEVICE_NAME = objRequest.deviceName;

                _objDevup.STAM_DEVICE_ID=objRequest.deviceID;
                    };
                    objContext.Entry(_objDevup).State = EntityState.Modified;
                    objContext.SaveChanges();
                    deviceID = Convert.ToString(_objDev.STAM_ID);



                }
                  STAM_MA_ATHLETE _ObjAthlets=new STAM_MA_ATHLETE();
                if (objRequest.AthleteID != 0)
                {
                    string s = Convert.ToString(_objDev.STAM_ID);
                    _ObjAthlets = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_DEVICE_ID == s).SingleOrDefault();
                    if (_ObjAthlets != null && _objDev != null)
                    {
                        _ObjAthlets.STAM_DEVICE_ID = "0";
                        _ObjAthlets.STAM_NOTIFICATION_DATE = DateTime.Now;
                        _ObjAthlets.STAM_NOTIFICATION_STATUS = objRequest.message;
                        objContext.SaveChanges();
                    }
                    _ObjAthlets = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ID == objRequest.AthleteID).SingleOrDefault();
                    if (_ObjAthlets != null && _objDev != null)
                    {
                        _ObjAthlets.STAM_DEVICE_ID = Convert.ToString(_objDev.STAM_ID);
                        _ObjAthlets.STAM_NOTIFICATION_DATE = DateTime.Now;
                        _ObjAthlets.STAM_NOTIFICATION_STATUS = objRequest.message;
                        objContext.SaveChanges();
                        return true;
                    }
                    
                }
                else
                {
                    string s = Convert.ToString(_objDev.STAM_ID);
                    _ObjAthlets = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_DEVICE_ID == s).SingleOrDefault();
                    if (_ObjAthlets != null && _objDev != null)
                    {
                        _ObjAthlets.STAM_DEVICE_ID = "0";
                        _ObjAthlets.STAM_NOTIFICATION_DATE = DateTime.Now;
                        _ObjAthlets.STAM_NOTIFICATION_STATUS = objRequest.message;
                        objContext.SaveChanges();
                        return true;
                    }
                    return true;
                }
              
                
                return false;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : Update_SaveDevce", "Problem In Check a Device for save", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public bool Update_SaveDevce(Request objRequest)
        {
            try
            {
                string deviceID=string.Empty;
                string sgroup = Convert.ToString(objRequest.groupID);
                STAM_MA_DEVICES _objDev = objContext.STAM_MA_DEVICES.Where(p => p.STAM_ID == objRequest.ID).SingleOrDefault();
                if (_objDev != null)
                {
                    deviceID = Convert.ToString(_objDev.STAM_ID);
                }
                STAM_MA_ATHLETE _ObjAthlets = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ASSIGNGROUPID == sgroup && p.STAM_DEVICE_ID == deviceID).SingleOrDefault();
                if (_ObjAthlets != null && _objDev != null)
                {
                    _ObjAthlets.STAM_DEVICE_ID = Convert.ToString(_objDev.STAM_ID);
                    _ObjAthlets.STAM_NOTIFICATION_DATE = DateTime.Now;
                    _ObjAthlets.STAM_NOTIFICATION_STATUS = objRequest.message;
                    objContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete : Update_SaveDevce", "Problem In Check a Device for save", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        public List<Devices> ListDevices()
        {

            try
            {
                List<Devices> _objRes1 = new List<Devices>();
                var objAth1 = (from ath in objContext.STAM_MA_DEVICES 
                              select new Devices(){
                                  ID=ath.STAM_ID,
                                  DeviceID=ath.STAM_DEVICE_ID,
                                  DeviceName=ath.STAM_DEVICE_NAME,
                                  //AthleteID = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == ath.STAM_DEVICE_ID).FirstOrDefault().STAM_ATH_ID,
                                //  AthleteName = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == ath.STAM_DEVICE_ID).FirstOrDefault().STAM_ATH_FIRTSNAME,
                                  }).ToList();
            foreach(var item in objAth1)
            {
                string id = item.ID.ToString();

                item.AthleteID = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault()!=null? objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_ID:0;
                item.AthleteName = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_FIRTSNAME : "";
                item.Name = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_FIRTSNAME + " " + objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_LASTNAME : "";

            }
                                 
                if (objAth1 != null)
                {
                    _objRes1 = objAth1.ToList();
                }
                return _objRes1;


                return null;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete :ListDevce", "Problem In List Device", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }







        public Devices EditDevice(long DeviceID=0)
        {

            try
            {
              Devices _objRes1 = new Devices();
                var objAth1 = (from ath in objContext.STAM_MA_DEVICES
                               select new Devices()
                               {
                                   ID = ath.STAM_ID,
                                   DeviceID = ath.STAM_DEVICE_ID,
                                   DeviceName = ath.STAM_DEVICE_NAME,
                                   //AthleteID = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID ==ath.STAM_DEVICE_ID).FirstOrDefault().STAM_ATH_ID,
                                   //AthleteName = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID ==ath.STAM_DEVICE_ID).FirstOrDefault().STAM_ATH_FIRTSNAME,
                               }).Where(x => x.ID == DeviceID).FirstOrDefault();



                
                if (objAth1 != null)
                {
                    string id = objAth1.ID.ToString();
                    objAth1.AthleteID = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_ID : 0;
                    objAth1.AthleteName = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_FIRTSNAME : "";
                    objAth1.Name = objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault() != null ? objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_FIRTSNAME + "" + objContext.STAM_MA_ATHLETE.Where(y => y.STAM_DEVICE_ID == id).FirstOrDefault().STAM_ATH_LASTNAME : "";
                    _objRes1 = objAth1;
                }
                return _objRes1;


                return null;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete :ListDevce", "Problem In List Device", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        public bool DeleteDevice(Request objRequest)
        {

            try
            {

                var _objDev = objContext.STAM_MA_DEVICES.Where(p => p.STAM_ID == objRequest.ID).ToList();
                
                _objDev.ForEach(cs => objContext.STAM_MA_DEVICES.Remove(cs));

                objContext.SaveChanges();
                return true;

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete :ListDevce", "Problem In List Device", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        #endregion

        #endregion



        public List<STAM_MA_ATHLETE> GetAthleteForSection(string SectionName)
        {
            try
            {
                STAM_MA_ATHLETE_SECTION objSection =objContext.STAM_MA_ATHLETE_SECTION.Where(p=>p.STAM_ATHSEC_NAME== SectionName).SingleOrDefault();
                List<STAM_MA_ATHLETE> _objRes1 = new List<STAM_MA_ATHLETE>();
                if (objSection != null)
                {
                    var objAth1 = from ath in objContext.STAM_MA_ATHLETE where ath.STAM_ATH_SECTIONID == objSection.STAM_ATHSEC_ID && ath.STAM_ATH_STATUS == 1 select ath;
                    if (objAth1 != null)
                    {
                        _objRes1 = objAth1.ToList();
                    }
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete : GetAllAthletesForSection", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }
    }
}