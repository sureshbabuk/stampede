﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;

namespace Stampade.Services.Models.Business
{
    public class AthleteSection
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();

        #endregion

        #region AsstCoach

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        
        public bool Create_AthSec(STAM_MA_ATHLETE_SECTION objAth)
        {
            try
            {
                STAM_MA_ATHLETE_SECTION objCreateAth = new STAM_MA_ATHLETE_SECTION()
                {
                    STAM_ATHSEC_STATUS = objAth.STAM_ATHSEC_STATUS,
                    STAM_ATHSEC_NAME = objAth.STAM_ATHSEC_NAME,
                    STAM_ATHSEC_GROUPID = objAth.STAM_ATHSEC_GROUPID,
                    STAM_ATHSEC_GROUPNAME = objAth.STAM_ATHSEC_GROUPNAME,
                    STAM_ATHSEC_ATHLETEID = objAth.STAM_ATHSEC_ATHLETEID,
                    STAM_ATHSEC_ATHLETENAME = objAth.STAM_ATHSEC_ATHLETENAME,
                    STAM_USERTYPE = objAth.STAM_USERTYPE,
                    
                    STAM_CREATED_USER = objAth.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objAth.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objAth.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objAth.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_ATHLETE_SECTION.Add(objCreateAth);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete Section : CreateTeam", "Problem In Creating a new Athlete Section", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_AthSec(STAM_MA_ATHLETE_SECTION objAth)
        {
            try
            {
                var objUpdateAth = objContext.STAM_MA_ATHLETE_SECTION.Where(x => x.STAM_ATHSEC_ID == objAth.STAM_ATHSEC_ID).SingleOrDefault();
                {
                    objUpdateAth.STAM_ATHSEC_STATUS = objAth.STAM_ATHSEC_STATUS;
                    objUpdateAth.STAM_ATHSEC_NAME = objAth.STAM_ATHSEC_NAME;
                    objUpdateAth.STAM_ATHSEC_GROUPID = objAth.STAM_ATHSEC_GROUPID;
                    objUpdateAth.STAM_ATHSEC_GROUPNAME = objAth.STAM_ATHSEC_GROUPNAME;
                    objUpdateAth.STAM_ATHSEC_ATHLETEID = objAth.STAM_ATHSEC_ATHLETEID;
                    objUpdateAth.STAM_ATHSEC_ATHLETENAME = objAth.STAM_ATHSEC_ATHLETENAME;
                    objUpdateAth.STAM_USERTYPE = objAth.STAM_USERTYPE;
                   
                    objUpdateAth.STAM_CREATED_USER = objAth.STAM_CREATED_USER;
                    objUpdateAth.STAM_CREATED_DATE = objAth.STAM_CREATED_DATE;                   
                    objUpdateAth.STAM_LAST_UPDATED_USER = objAth.STAM_LAST_UPDATED_USER;
                    objUpdateAth.STAM_LAST_UPDATED_DATE = objAth.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateAth).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Athlete Section: UpdateAth", "Problem In Updating a Athlete Section", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_ATHLETE_SECTION> GetAthleteSectionList(string AthID)
        {
            try
            {
                string[] _ath = null;
                _ath = AthID.ToString().TrimEnd(',').Split(',');
                List<STAM_MA_ATHLETE_SECTION> _objRes = new List<STAM_MA_ATHLETE_SECTION>();
                foreach(var item in _ath)
                {
                    long item1 = Convert.ToInt64(item);
                    var obj_Ath = from Ath in objContext.STAM_MA_ATHLETE_SECTION where Ath.STAM_ATHSEC_STATUS != 3 && Ath.STAM_CREATED_USER == item1  select Ath;
                    if (obj_Ath != null)
                    {
                        _objRes.AddRange(obj_Ath);
                    }
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAth", "Problem In Getting a list of Athletes Section", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ATHLETE_SECTION GetAthSecByID(long iAth)
        {
            try
            {
                var objAth = from Ath in objContext.STAM_MA_ATHLETE_SECTION where Ath.STAM_ATHSEC_ID == iAth select Ath;
                STAM_MA_ATHLETE_SECTION _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAthByID", "Problem In Getting a Athletes Section", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ATHLETE_SECTION GetAthSecByName(string Name,string Group)
        {
            try
            {
                var objAth = from ATh in objContext.STAM_MA_ATHLETE_SECTION where ATh.STAM_ATHSEC_NAME == Name && ATh.STAM_ATHSEC_GROUPNAME == Group && ATh.STAM_ATHSEC_STATUS != 3 select ATh;
                STAM_MA_ATHLETE_SECTION _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public  List<STAM_MA_ATHLETE_SECTION> GetAthSecByNameList(string Name)
        {
            try
            {
                var objAth = from ATh in objContext.STAM_MA_ATHLETE_SECTION where ATh.STAM_ATHSEC_GROUPNAME == Name && ATh.STAM_ATHSEC_STATUS != 3 select ATh;
                List<STAM_MA_ATHLETE_SECTION> _objAth = objAth.ToList();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_ATHLETE_SECTION> GetAthleteSection(string Name, string userid)
        {

            try
            {
                int UserID = Convert.ToInt32(userid);
                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection = new List<STAM_MA_ATHLETE_SECTION>();
                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSections = new List<STAM_MA_ATHLETE_SECTION>();



                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == UserID).FirstOrDefault();
                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection1 = new List<STAM_MA_ATHLETE_SECTION>();



                long? userids = User.SAM_CREATED_USER;
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                if (User.STAM_USER_TYPE == 3)
                {
                    var lstAthleteSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                            join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                            where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == UserID || s.STAM_USERID == User.SAM_CREATED_USER)
                                            where ATh.STAM_ATHSEC_GROUPNAME == Name
                                            && ATh.STAM_ATHSEC_STATUS == 1
                                            select ATh;
                    if (lstAthleteSection != null)
                    {
                        STAM_MA_ASSTCOACH asstUser = objContext.STAM_MA_ASSTCOACH.Where(p => p.STAM_ASSTCOACH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                        string[] _ath = null;
                        _ath = asstUser.STAM_ASSTCOACH_GROUPID.ToString().TrimEnd(',').Split(',');
                        foreach (var item in _ath)
                        {
                            long item1 = Convert.ToInt64(item);
                            List<STAM_MA_ATHLETE_SECTION> objser = lstAthleteSection.Where(p => p.STAM_ATHSEC_GROUPID == item1).ToList();
                            if (objser.Count != 0)
                            {
                                _lstAthleteSection = objser.ToList();
                            }
                        }
                    }
                    return _lstAthleteSection;

                }
                if (User.STAM_USER_TYPE == 4)
                {
                    var lstAthleteSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                            join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                            where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == UserID || s.STAM_USERID == User.SAM_CREATED_USER)
                                            where ATh.STAM_ATHSEC_GROUPNAME == Name
                                            && ATh.STAM_ATHSEC_STATUS == 1
                                            select ATh;
                    if (lstAthleteSection != null)
                    {
                        STAM_MA_ATHLETE asstUser = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                        string[] _ath = null;
                        _ath = asstUser.STAM_ATH_ASSIGNGROUPID.ToString().TrimEnd(',').Split(',');
                        foreach (var item in _ath)
                        {
                            long item1 = Convert.ToInt64(item);
                            List<STAM_MA_ATHLETE_SECTION> objser = lstAthleteSection.Where(p => p.STAM_ATHSEC_GROUPID == item1).ToList();
                            if (objser.Count != 0)
                            {
                                _lstAthleteSection = objser.ToList();
                            }
                        }
                    }
                    return _lstAthleteSection;

                }
                if (User.STAM_USER_TYPE == 2 || User.STAM_USER_TYPE == 1)
                {
                    var lstAthleteSection1 = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                             join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                             where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == UserID)
                                             where ATh.STAM_ATHSEC_GROUPNAME == Name
                                             && ATh.STAM_ATHSEC_STATUS == 1
                                             select ATh;
                    if (lstAthleteSection1 != null)
                    {
                        _lstAthleteSection = lstAthleteSection1.ToList();
                    }
                    return _lstAthleteSection;


                }

                return null;

            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAllSectionsForGroup", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_ATHLETE_SECTION GetAthSecByGroupID(long GrpId)
        {
            try
            {
                var objAth = from ATh in objContext.STAM_MA_ATHLETE_SECTION where ATh.STAM_ATHSEC_GROUPID == GrpId && ATh.STAM_ATHSEC_STATUS != 3 select ATh;
                STAM_MA_ATHLETE_SECTION _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }


        public STAM_MA_ATHLETE_SECTION GetLastAthSec()
        {
            try
            {
                var objAth = from ATh in objContext.STAM_MA_ATHLETE_SECTION orderby ATh.STAM_ATHSEC_ID descending select ATh;
                STAM_MA_ATHLETE_SECTION _objAth = objAth.FirstOrDefault();
                return _objAth;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }


        public List<STAM_MA_ATHLETE_SECTION> GetAthSecByCoachGroup(string Group, string UserID)
        {
            try
            {
                string[] _grp = null;
                _grp = Group.ToString().TrimEnd(',').Split(',');
                string[] _UserID = null;
                _UserID = UserID.ToString().Split(',');
                List<STAM_MA_ATHLETE_SECTION> _objRes = new List<STAM_MA_ATHLETE_SECTION>();
                List<STAM_MA_ATHLETE_SECTION> _objRes1 = new List<STAM_MA_ATHLETE_SECTION>();

                foreach (var item in _grp)
                {
                    long item1 = Convert.ToInt64(item);
                    var objAth = from ATh in objContext.STAM_MA_ATHLETE_SECTION where ATh.STAM_ATHSEC_GROUPID == item1 && ATh.STAM_ATHSEC_STATUS != 3 select ATh;
                    if (objAth != null)
                    {
                        _objRes.AddRange(objAth);
                    }
                }
                foreach (var item in _UserID)
                {
                    var objAth1 = from ath in _objRes where ath.STAM_CREATED_USER == long.Parse(item) && ath.STAM_ATHSEC_STATUS != 3 select ath;
                    if (objAth1 != null)
                    {
                        _objRes1.AddRange(objAth1);
                    }
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAthByMailID", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }

        #region Hema
        //public List<STAM_MA_ATHLETE_SECTION> GetAllSectionsForGroup(long GroupID, long UserID)
        //{

        //    try
        //    {
        //        List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection = new List<STAM_MA_ATHLETE_SECTION>();
        //        List<STAM_MA_ATHLETE_SECTION> _lstAthleteSections = new List<STAM_MA_ATHLETE_SECTION>();



        //        STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == UserID).FirstOrDefault();
        //        List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection1 = new List<STAM_MA_ATHLETE_SECTION>();



        //        long? userids = User.SAM_CREATED_USER;
        //        List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
        //        if (User.STAM_USER_TYPE == 3)
        //        {
        //            var lstAthleteSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
        //                                    join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
        //                                    where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == UserID || s.STAM_USERID == User.SAM_CREATED_USER)
        //                                    where ATh.STAM_ATHSEC_GROUPID == GroupID
        //                                    && ATh.STAM_ATHSEC_STATUS == 1
        //                                    select ATh;
        //            if (lstAthleteSection != null)
        //            {
        //                _lstAthleteSection = lstAthleteSection.ToList();
        //            }
        //            return _lstAthleteSection;

        //        }
        //        if (User.STAM_USER_TYPE == 2)
        //        {
        //            var lstAthleteSection1 = from ATh in objContext.STAM_MA_ATHLETE_SECTION
        //                                     join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
        //                                     where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == UserID)
        //                                     where ATh.STAM_ATHSEC_GROUPID == GroupID
        //                                     && ATh.STAM_ATHSEC_STATUS == 1
        //                                     select ATh;
        //            if (lstAthleteSection1 != null)
        //            {
        //                _lstAthleteSection = lstAthleteSection1.ToList();
        //            }
        //            return _lstAthleteSection;


        //        }

        //        return null;

        //    }
        //    catch (Exception ex)
        //    {
        //        objLog.LogToFile("Error", "Athlete Section: GetAllSectionsForGroup", "Problem In Getting a Athletes", ex.Message.ToString());
        //        return null;
        //    }
        //}
        public List<STAM_MA_ATHLETE_SECTION> GetAllSectionsForGroup(long GroupID, long UserID)
        {

            try
            {
                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection = new List<STAM_MA_ATHLETE_SECTION>();
                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSections = new List<STAM_MA_ATHLETE_SECTION>();



                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == UserID).FirstOrDefault();
                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection1 = new List<STAM_MA_ATHLETE_SECTION>();



                long? userids = User.SAM_CREATED_USER;
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                if (User.STAM_USER_TYPE == 3)
                {
                    var lstAthleteSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                            join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                            where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == UserID || s.STAM_USERID == User.SAM_CREATED_USER)
                                            where ATh.STAM_ATHSEC_GROUPID == GroupID
                                            && ATh.STAM_ATHSEC_STATUS == 1
                                            select ATh;
                    if (lstAthleteSection != null)
                    {
                        STAM_MA_ASSTCOACH asstUser = objContext.STAM_MA_ASSTCOACH.Where(p => p.STAM_ASSTCOACH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                        string[] _ath = null;
                        _ath = asstUser.STAM_ASSTCOACH_GROUPID.ToString().TrimEnd(',').Split(',');
                        foreach (var item in _ath)
                        {
                            long item1 = Convert.ToInt64(item);
                            List<STAM_MA_ATHLETE_SECTION> objser = lstAthleteSection.Where(p => p.STAM_ATHSEC_GROUPID == item1).ToList();
                            if (objser.Count != 0)
                            {
                                _lstAthleteSection = objser.ToList();
                            }
                        }
                    }
                    return _lstAthleteSection.OrderBy(p=>p.STAM_ATHSEC_NAME).ToList();

                }
                if (User.STAM_USER_TYPE == 2)
                {
                    var lstAthleteSection1 = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                             join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                             where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == UserID)
                                             where ATh.STAM_ATHSEC_GROUPID == GroupID
                                             && ATh.STAM_ATHSEC_STATUS == 1
                                             select ATh;
                    if (lstAthleteSection1 != null)
                    {
                        _lstAthleteSection = lstAthleteSection1.ToList();
                    }
                    return _lstAthleteSection.OrderBy(p => p.STAM_ATHSEC_NAME).ToList();


                }
               
                return null;

            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Athlete Section: GetAllSectionsForGroup", "Problem In Getting a Athletes", ex.Message.ToString());
                return null;
            }
        }
        #endregion

        #endregion
    }
}