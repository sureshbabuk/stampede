﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Athletes
    {
        public long athleteID { get; set; }
        public int groupID { get; set; }
        public int sectionID { get; set; }
        public string sectionName { get; set; }
        public string message { get; set; }
        public string groupName { get; set; }
        public string athleteName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string status { get; set; }
        public string emailAdd { get; set; }
        public string gender { get; set; }
        public string deviceID { get; set; }
        public Nullable<long> mobileNo { get; set; }
    }


    public class Devices
    {


        public long ID { get; set; }

        
        public string DeviceID { get; set; }

        public string DeviceName { get; set; }

        public  Nullable<long> AthleteID { get; set; }


        public string AthleteName { get; set; }

        public string Name { get; set; }


    }

}