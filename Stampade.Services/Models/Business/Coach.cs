﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;

namespace Stampade.Services.Models.Business
{
    public class Coach
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();

        #endregion

        #region Coach

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        //add new job
        public bool Create_Coach(STAM_MA_COACH objCoach)
        {
            try
            {
                STAM_MA_COACH objCreateCoach = new STAM_MA_COACH()
                {
                    STAM_COACH_STATUS = objCoach.STAM_COACH_STATUS,
                    STAM_COACH_FIRSTNAME = objCoach.STAM_COACH_FIRSTNAME,
                    STAM_COACH_LASTNAME = objCoach.STAM_COACH_LASTNAME,
                    STAM_COACH_EMAILADD = objCoach.STAM_COACH_EMAILADD,
                    STAM_COACH_MOBILENO = objCoach.STAM_COACH_MOBILENO,
                    STAM_COACH_TEAMID = objCoach.STAM_COACH_TEAMID,
                    STAM_COACH_TEAMNAME = objCoach.STAM_COACH_TEAMNAME,
                    STAM_CREATED_USER = objCoach.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_COACH.Add(objCreateCoach);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateTeam", "Problem In Creating a new Coach", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_Coach(STAM_MA_COACH objCoach)
        {
            try
            {
                var objUpdateCoach = objContext.STAM_MA_COACH.Where(x => x.STAM_COACH_ID == objCoach.STAM_COACH_ID).SingleOrDefault();
                {
                    objUpdateCoach.STAM_COACH_STATUS = objCoach.STAM_COACH_STATUS;
                    objUpdateCoach.STAM_COACH_FIRSTNAME = objCoach.STAM_COACH_FIRSTNAME;
                    objUpdateCoach.STAM_COACH_LASTNAME = objCoach.STAM_COACH_LASTNAME;
                    objUpdateCoach.STAM_COACH_EMAILADD = objCoach.STAM_COACH_EMAILADD;
                    objUpdateCoach.STAM_COACH_MOBILENO = objCoach.STAM_COACH_MOBILENO;
                    objUpdateCoach.STAM_COACH_TEAMID = objCoach.STAM_COACH_TEAMID;
                    objUpdateCoach.STAM_COACH_TEAMNAME = objCoach.STAM_COACH_TEAMNAME;
                    objUpdateCoach.STAM_CREATED_USER = objCoach.STAM_CREATED_USER;
                    objUpdateCoach.STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE;
                    objUpdateCoach.STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER;
                    objUpdateCoach.STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateCoach).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : UpdateCoach", "Problem In Updating a Coach", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_COACH> GetCoachList(long UserID)
        {
            try
            {
                var obj_Coach = from coachs in objContext.STAM_MA_COACH where coachs.STAM_COACH_STATUS != 3 && coachs.STAM_CREATED_USER == UserID orderby coachs.STAM_COACH_ID descending select coachs;
                List<STAM_MA_COACH> objCoachs = obj_Coach.ToList<STAM_MA_COACH>();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach : GetCoachs", "Problem In Getting a list of Coach Admins", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_COACH GetCoachByID(long iCoach)
        {
            try
            {
                var objCoach = from coachs in objContext.STAM_MA_COACH where coachs.STAM_COACH_ID == iCoach select coachs;
                STAM_MA_COACH objCoachs = objCoach.FirstOrDefault();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach : GetCoachByID", "Problem In Getting a Coach", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_COACH GetCoachByMail(string MailAdd)
        {
            try
            {
                var objCoach = from coachs in objContext.STAM_MA_COACH where coachs.STAM_COACH_EMAILADD == MailAdd && coachs.STAM_COACH_STATUS != 3 select coachs;
                STAM_MA_COACH objCoachs = objCoach.FirstOrDefault();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach : GetCoachByID", "Problem In Getting a Coach", ex.Message.ToString());
                return null;
            }
        }


        public STAM_MA_COACH GetLastCoach()
        {
            try
            {
                var obj_Coach = from coachs in objContext.STAM_MA_COACH orderby coachs.STAM_COACH_ID descending select coachs;
                STAM_MA_COACH objCoachs = obj_Coach.FirstOrDefault();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach : GetCoachs", "Problem In Getting a list of Coach Admins", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_USERS GetAdminByID(long iCoach)
        {

            try
            {
                var objCoach = from coachs in objContext.STAM_MA_USERS where coachs.STAM_USERID == iCoach select coachs;
                STAM_MA_USERS objCoachs = objCoach.FirstOrDefault();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach : GetCoachByID", "Problem In Getting a Coach", ex.Message.ToString());
                return null;
            }
        }

        public bool Update_admin(STAM_MA_USERS objCoach)
        {

            try
            {
                var objUpdateCoach = objContext.STAM_MA_USERS.Where(x => x.STAM_USERID == objCoach.STAM_USERID).SingleOrDefault();
                {

                    objUpdateCoach.STAM_USER_FNAME = objCoach.STAM_USER_FNAME;
                    objUpdateCoach.STAM_USER_LNAME = objCoach.STAM_USER_LNAME;
                    objUpdateCoach.STAM_USER_EMAIL = objCoach.STAM_USER_EMAIL;
                    objUpdateCoach.STAM_USER_MOBILE = objCoach.STAM_USER_MOBILE;

                    objUpdateCoach.STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE;
                    objUpdateCoach.STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER;
                    objUpdateCoach.STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateCoach).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : UpdateCoach", "Problem In Updating a Coach", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        #endregion
    }
}