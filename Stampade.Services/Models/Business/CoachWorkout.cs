﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;
using System.Globalization;

namespace Stampade.Services.Models.Business
{
    public class CoachWorkout
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();
        Utility utility = new Utility();
        
        #endregion

        #region AsstCoach

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        
        public bool Create_CoachWorkout(STAM_MA_COACH_WORKOUT objCoach)
        {
            try
            {
                STAM_MA_COACH_WORKOUT objCoachWorkout = new STAM_MA_COACH_WORKOUT()
                {
                    STAM_COACH_WORKOUT_STATUS = objCoach.STAM_COACH_WORKOUT_STATUS,
                    STAM_COACH_WORKOUT_DATE = objCoach.STAM_COACH_WORKOUT_DATE,
                    STAM_COACH_WORKOUT_TITLE = objCoach.STAM_COACH_WORKOUT_TITLE,
                    STAM_COACH_WORKOUT_DISTANCE = objCoach.STAM_COACH_WORKOUT_DISTANCE,
                    STAM_COACH_WORKOUT_RUNS = objCoach.STAM_COACH_WORKOUT_RUNS,
                    STAM_COACH_WORKOUT_TARGET_MM = objCoach.STAM_COACH_WORKOUT_TARGET_MM,
                    STAM_COACH_WORKOUT_TARGET_SS = objCoach.STAM_COACH_WORKOUT_TARGET_SS,
                    STAM_COACH_WORKOUT_GROUPID = objCoach.STAM_COACH_WORKOUT_GROUPID,
                    STAM_COACH_WORKOUT_GROUPNAME = objCoach.STAM_COACH_WORKOUT_GROUPNAME,
                    STAM_COACH_WORKOUT_NOTES = objCoach.STAM_COACH_WORKOUT_NOTES,
                    STAM_COACH_WORKOUT_ATHSECID = objCoach.STAM_COACH_WORKOUT_ATHSECID,
                    STAM_COACH_WORKOUT_ATHSECNAME = objCoach.STAM_COACH_WORKOUT_ATHSECNAME,
                    STAM_USERTYPE = objCoach.STAM_USERTYPE,
                  
                    STAM_CREATED_USER = objCoach.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_COACH_WORKOUT.Add(objCoachWorkout);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_CoachWorkout(STAM_MA_COACH_WORKOUT objCoach)
        {
            try
            {
                var objUpdateCoach = objContext.STAM_MA_COACH_WORKOUT.Where(x => x.STAM_COACH_WORKOUT_ID == objCoach.STAM_COACH_WORKOUT_ID).SingleOrDefault();
                {
                    objUpdateCoach.STAM_COACH_WORKOUT_STATUS = objCoach.STAM_COACH_WORKOUT_STATUS;
                    objUpdateCoach.STAM_COACH_WORKOUT_DATE = objCoach.STAM_COACH_WORKOUT_DATE;
                    objUpdateCoach.STAM_COACH_WORKOUT_TITLE = objCoach.STAM_COACH_WORKOUT_TITLE;
                    objUpdateCoach.STAM_COACH_WORKOUT_DISTANCE = objCoach.STAM_COACH_WORKOUT_DISTANCE;
                    objUpdateCoach.STAM_COACH_WORKOUT_RUNS = objCoach.STAM_COACH_WORKOUT_RUNS;
                    objUpdateCoach.STAM_COACH_WORKOUT_TARGET_MM = objCoach.STAM_COACH_WORKOUT_TARGET_MM;
                    objUpdateCoach.STAM_COACH_WORKOUT_TARGET_SS = objCoach.STAM_COACH_WORKOUT_TARGET_SS;
                    objUpdateCoach.STAM_COACH_WORKOUT_GROUPID = objCoach.STAM_COACH_WORKOUT_GROUPID;
                    objUpdateCoach.STAM_COACH_WORKOUT_GROUPNAME = objCoach.STAM_COACH_WORKOUT_GROUPNAME;
                    objUpdateCoach.STAM_COACH_WORKOUT_NOTES = objCoach.STAM_COACH_WORKOUT_NOTES;
                    objUpdateCoach.STAM_COACH_WORKOUT_ATHSECID = objCoach.STAM_COACH_WORKOUT_ATHSECID;
                    objUpdateCoach.STAM_COACH_WORKOUT_ATHSECNAME = objCoach.STAM_COACH_WORKOUT_ATHSECNAME;
                    objUpdateCoach.STAM_USERTYPE = objCoach.STAM_USERTYPE;

                    objUpdateCoach.STAM_CREATED_USER = objCoach.STAM_CREATED_USER;
                    objUpdateCoach.STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE;
                    objUpdateCoach.STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER;
                    objUpdateCoach.STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateCoach).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "CoachWorkout : UpdateCoachWorkout", "Problem In Updating a Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_COACH_WORKOUT> GetCoachWorkoutList(string CoachID)
        {
            try
            {
                string[] _ath = null;
                _ath = CoachID.ToString().TrimEnd(',').Split(',');
                List<STAM_MA_COACH_WORKOUT> _objRes = new List<STAM_MA_COACH_WORKOUT>();
                foreach (var item in _ath)
                {
                    long item1 = Convert.ToInt64(item);
                    var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 orderby work.STAM_COACH_WORKOUT_ID descending select work;
                    if (obj_Coach != null)
                    {
                        _objRes.AddRange(obj_Coach);
                    }
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkout", "Problem In Getting a list of Workout", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_COACH_WORKOUT GetCoachWorkoutByID(long iWork)
        {
            try
            {
                var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_ID == iWork select work;
                STAM_MA_COACH_WORKOUT obj_CoachWork = obj_Coach.FirstOrDefault();
                return obj_CoachWork;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByID", "Problem In Getting a Coach Workout", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_COACH_WORKOUT GetCoachWorkoutByName(string Name,string Dist,int Runs,string Group)
        {
            try
            {
                var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_TITLE == Name && work.STAM_COACH_WORKOUT_DISTANCE == Dist && work.STAM_COACH_WORKOUT_RUNS == Runs && work.STAM_COACH_WORKOUT_GROUPNAME == Group && work.STAM_COACH_WORKOUT_STATUS != 3 select work;
                STAM_MA_COACH_WORKOUT obj_CoachWork = obj_Coach.FirstOrDefault();
                return obj_CoachWork;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByName", "Problem In Getting a CoachWorkout", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_COACH_WORKOUT ChkCoachWorkoutByGroup(string title, string Group, string _date,string Userid)
        {
            try
            {
                DateTime _dat = utility.FormatToDate(_date).Value;
                //var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_DATE == _dat && work.STAM_COACH_WORKOUT_STATUS != 3 select work;

                // var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_TITLE == title && work.STAM_COACH_WORKOUT_GROUPNAME == Group && work.STAM_COACH_WORKOUT_DATE == _dat && work.STAM_COACH_WORKOUT_STATUS != 3 select work;


                 string[] _grp = null;
                 _grp = Userid.ToString().TrimEnd(',').Split(',');
              //  string[] _UserID = null;
                //_UserID = Userid.ToString().Split(',');
                List<STAM_MA_COACH_WORKOUT> _objRes = new List<STAM_MA_COACH_WORKOUT>();
                List<STAM_MA_COACH_WORKOUT> _objRes1 = new List<STAM_MA_COACH_WORKOUT>();

                //STAM_MA_COACH_WORKOUT obj_Coach;


                foreach (var item in _grp)
                {
                    long item1 = (long)Convert.ToDouble(item.ToString());




                var  obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_GROUPNAME==Group  && work.STAM_COACH_WORKOUT_DATE == _dat && work.STAM_COACH_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 select work;

                   
                  //  var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where  work.STAM_COACH_WORKOUT_TITLE == title && work.STAM_COACH_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 select work;
                   _objRes.AddRange(obj_Coach);

                }

                STAM_MA_COACH_WORKOUT obj_CoachWork = _objRes.FirstOrDefault();
                return obj_CoachWork;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByName", "Problem In Getting a CoachWorkout", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_COACH_WORKOUT GetLastCoachWorkout()
        {
            try
            {
                var obj_Coach = from coachs in objContext.STAM_MA_COACH_WORKOUT orderby coachs.STAM_COACH_WORKOUT_ID descending select coachs;
                STAM_MA_COACH_WORKOUT objCoachs = obj_Coach.FirstOrDefault();
                return objCoachs;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Coach Workout : GetCoachs", "Problem In Getting a list of Coach Admins", ex.Message.ToString());
                return null;
            }
        }
        public List<STAM_MA_COACH_WORKOUT> GetCoachWorkoutByGroupName(string Name,string UserId)
        {
            try
            {
                string[] _grp = null;
                _grp = Name.ToString().TrimEnd(',').Split(',');
                string[] _UserID = null;
                _UserID = UserId.ToString().Split(',');
                List<STAM_MA_COACH_WORKOUT> _objRes = new List<STAM_MA_COACH_WORKOUT>();
                List<STAM_MA_COACH_WORKOUT> _objRes1 = new List<STAM_MA_COACH_WORKOUT>();

                foreach (var item in _grp)
                {                   
                    long item1 = Convert.ToInt64(item);
                    var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_GROUPID == item1 && work.STAM_COACH_WORKOUT_STATUS != 3 select work;
                    if (obj_Coach != null)
                    {
                        _objRes.AddRange(obj_Coach);
                    }
                }
                foreach (var item in _UserID)
                {
                    var obj_Coach1 = from ath in _objRes where ath.STAM_CREATED_USER == long.Parse(item) && ath.STAM_COACH_WORKOUT_STATUS != 3 select ath;
                    if (obj_Coach1 != null)
                    {
                        _objRes1.AddRange(obj_Coach1);
                    }
                }
                return _objRes1;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByName", "Problem In Getting a CoachWorkout", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_COACH_WORKOUT> GetCoachWorkoutByGroupID(long GroupID)
        {
            try
            {
                var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_GROUPID == GroupID && work.STAM_COACH_WORKOUT_STATUS != 3 select work;
                List<STAM_MA_COACH_WORKOUT> obj_CoachWork = obj_Coach.ToList<STAM_MA_COACH_WORKOUT>();
                return obj_CoachWork;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByName", "Problem In Getting a CoachWorkout", ex.Message.ToString());
                return null;
            }
        }

        public List<STAM_MA_COACH_WORKOUT> GetCoachWorkoutByAthSecID(long SecID)
        {
            try
            {
                var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_ATHSECID.Contains(SecID.ToString()) && work.STAM_COACH_WORKOUT_STATUS != 3 select work;
                List<STAM_MA_COACH_WORKOUT> obj_CoachWork = obj_Coach.ToList<STAM_MA_COACH_WORKOUT>();
                return obj_CoachWork;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByName", "Problem In Getting a CoachWorkout", ex.Message.ToString());
                return null;
            }
        }


        public STAM_MA_COACH_WORKOUT GetCoachWorkoutsByDistance(string iDistance)
        {
            try
            {
                var objWorkouts = from works in objContext.STAM_MA_COACH_WORKOUT where works.STAM_COACH_WORKOUT_DISTANCE == iDistance && works.STAM_COACH_WORKOUT_STATUS != 3 select works;
                STAM_MA_COACH_WORKOUT obj_Workouts = objWorkouts.FirstOrDefault();
                return obj_Workouts;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Workouts : GetWorkoutsByID", "Problem In Getting a Workouts", ex.Message.ToString());
                return null;
            }
        }





        public List<STAM_TR_WORKOUTS> GetNewCoachWorkoutList(string CoachID)
        {

            try
            {
                string[] _ath = null;
                _ath = CoachID.ToString().TrimEnd(',').Split(',');
                List<STAM_TR_WORKOUTS> _objRes = new List<STAM_TR_WORKOUTS>();
                foreach (var item in _ath)
                {
                    long item1 = Convert.ToInt64(item);
                    var obj_Coach = from work in objContext.STAM_TR_WORKOUTS where work.STAM_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 orderby work.STAM_WORKOUT_ID descending select work;
                    if (obj_Coach != null)
                    {
                        _objRes.AddRange(obj_Coach);
                    }
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetNewCoachWorkout", "Problem In Getting a list of NewWorkout", ex.Message.ToString());
                return null;
            }
        }
        
        #endregion
    }
}