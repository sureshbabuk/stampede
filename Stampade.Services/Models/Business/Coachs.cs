﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Coachs
    {
        public long coachId { get; set; }
        public Nullable<int> coachStatus { get; set; }
        public string coachFName { get; set; }
        public string coachLName { get; set; }
        public Nullable<long> coachMobile { get; set; }
        public string coachEMail { get; set; }
        public Nullable<long> teamId { get; set; }
        public string teamName { get; set; }
        public Nullable<long> coachCreatedUser { get; set; }
        public Nullable<System.DateTime> coachCreatedDate { get; set; }
        public Nullable<long> coachLastUpdatedUser { get; set; }
        public Nullable<System.DateTime> coachLastUpdatedDate { get; set; }
    }

    public class Accounts
    {
        public long UserId { get; set; }
        public Nullable<int> UserType { get; set; }
        public Nullable<int> UserStatusCode { get; set; }
        public Nullable<int> UserRole { get; set; }
        public string UserFName { get; set; }
        public string UserLName { get; set; }        
        public string UserUsername { get; set; }
        public string UserPassword { get; set; }
        public Nullable<bool> UserActivatedStatus { get; set; }
        public Nullable<long> UserCreatedUser { get; set; }
        public Nullable<System.DateTime> UserCreatedDate { get; set; }
        public List<Coachs> lstCoachs { get; set; }
    }
}