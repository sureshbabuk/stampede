﻿using BusinessObjects.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Dashboard
    {
        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();

        Logger objLog = new Logger();

        #endregion



        public List<NewWorkout> GetDashboardResults(DateTime StartDate, DateTime EndDate,long Userid)
        {
            try
            {
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();
                long? userids = User.SAM_CREATED_USER;
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                if (User.STAM_USER_TYPE == 3 || User.STAM_USER_TYPE == 1)
                {
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                    join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                    where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                    && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate.Date)
                                    select e).ToList();
                if (coachwrkouts != null)
                {
                    foreach (var itemcoach in coachwrkouts)
                    {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {

                                List<Details> lstDetails = new List<Details>();
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection != null)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                    }
                    
                    return lstNewWorkout;
                }
            }


                if (User.STAM_USER_TYPE == 4)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    var ath = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ID == User.STAM_USER_TYPE_KEY_ID).SingleOrDefault();
                    int groupid = Convert.ToInt32(ath.STAM_ATH_ASSIGNGROUPID);
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                        &&e.STAM_WORKOUT_GROUPID == groupid && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate.Date)
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {

                                List<Details> lstDetails = new List<Details>();
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection != null)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }

                        return lstNewWorkout;
                    }
                }
                if (User.STAM_USER_TYPE == 2)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid)
                                        && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate.Date)
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {

                                List<Details> lstDetails = new List<Details>();
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection != null)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }

                        return lstNewWorkout;
                    }
                }
                return null;
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : GetDashboardResults", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }



        public List<NewWorkout> GetAllNewWorkoutResults(long Userid)
        {
            try
            {
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();
                long? userids = User.SAM_CREATED_USER;
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                if (User.STAM_USER_TYPE == 3 || User.STAM_USER_TYPE == 1)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))                                        
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            lstNewWorkout.Add(objNewWorkout);
                        }

                        return lstNewWorkout;
                    }
                }


                if (User.STAM_USER_TYPE == 4)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    var ath = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ID == User.STAM_USER_TYPE_KEY_ID).SingleOrDefault();
                    int groupid = Convert.ToInt32(ath.STAM_ATH_ASSIGNGROUPID);
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))                                       
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            lstNewWorkout.Add(objNewWorkout);
                        }

                        return lstNewWorkout;
                    }
                }
                if (User.STAM_USER_TYPE == 2)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid)
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            lstNewWorkout.Add(objNewWorkout);
                        }

                        return lstNewWorkout;
                    }
                }
                return null;
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : GetDashboardResults", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        public List<NewWorkout> GetNewSectionWorkoutResults(long SectionId, long Userid, DateTime StartDate, DateTime EndDate)
        {
            try
            {

                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();
                long? userids = User.SAM_CREATED_USER;
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                if (User.STAM_USER_TYPE == 3 || User.STAM_USER_TYPE == 1)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                        && (e.STAM_WORKOUT_GROUPID == SectionId && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate.Date)
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                List<Details> lstDetails = new List<Details>();
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection != null)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }

                        return lstNewWorkout;
                    }
                }
                if (User.STAM_USER_TYPE == 4)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    var ath = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ID == User.STAM_USER_TYPE_KEY_ID).SingleOrDefault();
                    int groupid = Convert.ToInt32(ath.STAM_ATH_ASSIGNGROUPID);
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                        && e.STAM_WORKOUT_GROUPID == groupid && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate.Date)
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                List<Details> lstDetails = new List<Details>();
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection != null)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }

                        return lstNewWorkout;
                    }
                }

                if (User.STAM_USER_TYPE == 2)
                {
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    //var coachwrkouts = objContext.STAM_TR_WORKOUTS.Where(work => EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date).ToList();
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid)
                                        && (e.STAM_WORKOUT_GROUPID == SectionId && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate.Date)
                                        select e).ToList();
                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                List<Details> lstDetails = new List<Details>();
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection != null)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }

                        return lstNewWorkout;
                    }
                }
                return null;
            }
            catch (Exception Ex)
            {
                objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : GetNewSectionWorkoutResults", "Problem In getting a GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
    }
}