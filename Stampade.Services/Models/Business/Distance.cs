﻿using BusinessObjects.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Distance
    {
        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();
       
        #endregion

        #region Distance

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        //add new job
        public bool Create_Distance(STAM_MA_DISTANCE objDistance)
        {
            try
            {
                STAM_MA_DISTANCE objCreateDistance = new STAM_MA_DISTANCE()
                {
                    STAM_DISTANCE_STATUS = objDistance.STAM_DISTANCE_STATUS,
                    STAM_DISTANCE_NAME = objDistance.STAM_DISTANCE_NAME,
                    STAM_CREATED_USER = objDistance.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objDistance.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objDistance.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objDistance.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_DISTANCE.Add(objDistance);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Distance : CreateDistance", "Problem In Creating a new Distance", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_Distance(STAM_MA_DISTANCE objDistance)
        {
            try
            {
                var objUpdateDistance = objContext.STAM_MA_DISTANCE.Where(x => x.STAM_DISTANCE_ID == objDistance.STAM_DISTANCE_ID).SingleOrDefault();
                {
                    objUpdateDistance.STAM_DISTANCE_STATUS = objDistance.STAM_DISTANCE_STATUS;
                    objUpdateDistance.STAM_DISTANCE_NAME = objDistance.STAM_DISTANCE_NAME;
                    objUpdateDistance.STAM_CREATED_USER = objDistance.STAM_CREATED_USER;
                    objUpdateDistance.STAM_CREATED_DATE = objDistance.STAM_CREATED_DATE;
                    objUpdateDistance.STAM_LAST_UPDATED_USER = objDistance.STAM_LAST_UPDATED_USER;
                    objUpdateDistance.STAM_LAST_UPDATED_DATE = objDistance.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateDistance).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Distance : UpdateDistance", "Problem In Updating a Distance", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_DISTANCE> GetDistanceList()
        {
            try
            {
                var obj_Distance = from Dist in objContext.STAM_MA_DISTANCE where Dist.STAM_DISTANCE_STATUS != 3 orderby Dist.STAM_DISTANCE_ID descending select Dist;
                List<STAM_MA_DISTANCE> objDistances = obj_Distance.ToList<STAM_MA_DISTANCE>();
                return objDistances;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Distance : GetDistances", "Problem In Getting a list of Distance", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_DISTANCE GetDistanceByID(long iDistance)
        {
            try
            {
                var objDistance = from Dist in objContext.STAM_MA_DISTANCE where Dist.STAM_DISTANCE_ID == iDistance select Dist;
                STAM_MA_DISTANCE objDistances = objDistance.FirstOrDefault();
                return objDistances;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Distance : GetDistance", "Problem In Getting a Distance", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_DISTANCE GetDistanceByDistanceName(string Name)
        {
            try
            {
                var objDistance = from Dist in objContext.STAM_MA_DISTANCE where Dist.STAM_DISTANCE_NAME == Name && Dist.STAM_DISTANCE_STATUS != 3 select Dist;
                STAM_MA_DISTANCE objDistances = objDistance.FirstOrDefault();
                return objDistances;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Distance : GetDistancebyName", "Problem In Getting a Distance", ex.Message.ToString());
                return null;
            }
        }

       

        #endregion
    }
}