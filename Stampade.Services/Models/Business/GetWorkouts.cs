﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class GetWorkouts
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string GroupId { get; set; }
        public string SectionID { get; set; }
        public string AtheleteID { get; set; }
        public string WorkoutTitle { get; set; }
    }
}