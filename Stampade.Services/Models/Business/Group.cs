﻿using BusinessObjects.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Group
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();

        #endregion

        #region Group

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        //add new job
        public bool Create_Group(STAM_MA_GROUP objGroup)
        {
            try
            {
                STAM_MA_GROUP objCreateGroup = new STAM_MA_GROUP()
                {
                    STAM_GROUP_STATUS = objGroup.STAM_GROUP_STATUS,
                    STAM_GROUP_NAME = objGroup.STAM_GROUP_NAME,
                    STAM_CREATED_USER = objGroup.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objGroup.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objGroup.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objGroup.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_GROUP.Add(objCreateGroup);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Group : CreateGroup", "Problem In Creating a new Group", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_Group(STAM_MA_GROUP objGroup)
        {
            try
            {
                var objUpdateGroup = objContext.STAM_MA_GROUP.Where(x => x.STAM_GROUP_ID == objGroup.STAM_GROUP_ID).SingleOrDefault();
                {
                    objUpdateGroup.STAM_GROUP_STATUS = objGroup.STAM_GROUP_STATUS;
                    objUpdateGroup.STAM_GROUP_NAME = objGroup.STAM_GROUP_NAME;
                    objUpdateGroup.STAM_CREATED_USER = objGroup.STAM_CREATED_USER;
                    objUpdateGroup.STAM_CREATED_DATE = objGroup.STAM_CREATED_DATE;
                    objUpdateGroup.STAM_LAST_UPDATED_USER = objGroup.STAM_LAST_UPDATED_USER;
                    objUpdateGroup.STAM_LAST_UPDATED_DATE = objGroup.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateGroup).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Group : UpdateGroup", "Problem In Updating a Group", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_GROUP> GetGroupList()
        {
            try
            {
                var obj_Group = from groups in objContext.STAM_MA_GROUP where groups.STAM_GROUP_STATUS != 3 orderby groups.STAM_GROUP_ID descending select groups;
                List<STAM_MA_GROUP> obj_Groups = obj_Group.ToList<STAM_MA_GROUP>();
                return obj_Groups;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Group : GetGroups", "Problem In Getting a list of Groups", ex.Message.ToString());
                return null;
            }
        }

        //public object GetObjGroupList()
        //{
        //    try
        //    {
        //        var obj_Coach = (from x in objContext.STAM_MA_GROUP
        //                         orderby x.STAM_GROUP_ID descending
        //                         select new
        //                         {
        //                             STAM_GROUP_ID = x.STAM_GROUP_ID,
        //                             STAM_GROUP_STATUS = x.STAM_GROUP_STATUS,
        //                             STAM_GROUP_NAME = x.STAM_GROUP_NAME,
        //                             STAM_CREATED_USER = x.STAM_CREATED_USER,
        //                             STAM_CREATED_DATE = x.STAM_CREATED_DATE,
        //                             STAM_LAST_UPDATED_USER = x.STAM_LAST_UPDATED_USER,
        //                             STAM_LAST_UPDATED_DATE = x.STAM_LAST_UPDATED_DATE,
        //                             STAM_ASST_ATH__ASSIGNGROUP = (objContext.STAM_ASST_ATH__ASSIGNGROUP.Where(y => y.STAM_ASST_ATH__AG_GROUPID == x.STAM_GROUP_ID)).
        //                                                  Select(y => new
        //                                                  {
        //                                                      y.STAM_ASST_ATH__AG_ID,
        //                                                      y.STAM_ASST_ATH__AG_GROUPID,
        //                                                      y.STAM_ASST_ATH__AG_GROUPNAME,
        //                                                  }).FirstOrDefault(),
        //                             STAM_MA_ASSTCOACH_WORKOUT = (objContext.STAM_MA_ASSTCOACH_WORKOUT.Where(y => y.STAM_ASSTCOACH_WORKOUT_GROUPID == x.STAM_GROUP_ID)).
        //                                                Select(y => new
        //                                                {
        //                                                    y.STAM_ASSTCOACH_WORKOUT_ID,
        //                                                    y.STAM_ASSTCOACH_WORKOUT_GROUPID,
        //                                                    y.STAM_ASSTCOACH_WORKOUT_GROUPNAME,
        //                                                }).FirstOrDefault(),
        //                         }).ToList();
        //        return obj_Coach;
        //    }
        //    catch (Exception ex)
        //    {
        //        objLog.LogToFile("Error", "Group : GetGroups", "Problem In Getting a list of Groups", ex.Message.ToString());
        //        return null;
        //    }
        //}
        public STAM_MA_GROUP GetGroupByID(long iGroup)
        {
            try
            {
                var objGroup = from groups in objContext.STAM_MA_GROUP where groups.STAM_GROUP_ID == iGroup select groups;
                STAM_MA_GROUP objGroups = objGroup.FirstOrDefault();
                return objGroups;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Group : GetGroupByID", "Problem In Getting a Group", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_GROUP GetGroupByName(string GroupName)
        {
            try
            {
                var objGroup = from groups in objContext.STAM_MA_GROUP where groups.STAM_GROUP_NAME == GroupName && groups.STAM_GROUP_STATUS != 3 select groups;
                STAM_MA_GROUP objGroups = objGroup.FirstOrDefault();
                return objGroups;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Group : GetGroupByName", "Problem In Getting a Group", ex.Message.ToString());
                return null;
            }
        }


        public List<STAM_MA_GROUP> GetGroupsByUserID(string GroupID)
        {
            try
            {
                string[] grp = null;
                grp = GroupID.ToString().Split(',');
                List<STAM_MA_GROUP> _objRes = new List<STAM_MA_GROUP>();

                foreach (var item in grp)
                {
                    long item1 = Convert.ToInt32(item);
                    var objAth = from ath in objContext.STAM_MA_GROUP where ath.STAM_GROUP_ID == item1 && ath.STAM_GROUP_STATUS != 3 select ath;
                    if (objAth != null)
                    {
                        _objRes.AddRange(objAth);
                    }
                }
                return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Group : GetGroups", "Problem In Getting a list of Groups", ex.Message.ToString());
                return null;
            }
        }

        #region Hema
        public List<STAM_MA_GROUP> GetAllGroups(long UserID)
        {
            try
            {
                List<STAM_MA_GROUP> _objRes = new List<STAM_MA_GROUP>();
                var objAth = from ath in objContext.STAM_MA_GROUP where ath.STAM_GROUP_STATUS == 1 select ath;
                if (objAth != null)
                {
                    _objRes = objAth.ToList();
                }
                return _objRes;

            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Group : GetAllGroups", "Problem In Getting a list of Groups", ex.Message.ToString());
                return null;
            }
        }

        public List<Groups> listGroup()
        {
            try
            {
                List<Groups> _objRes = new List<Groups>();
               
                var objAth = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_STATUS == 1).ToList();
                if (objAth != null)
                {
                    foreach (var item in objAth)
                    {
                        Groups objGroup = new Groups();
                        objGroup.name = item.STAM_GROUP_NAME;
                        objGroup.groupId = Convert.ToInt32(item.STAM_GROUP_ID);
                        if(item.STAM_GROUP_STATUS == 1)
                        {
                            objGroup.status = "Activated";
                        }
                        else
                        {
                            objGroup.status = "DeActivated";
                        }
                        _objRes.Add(objGroup);
                    }
                }
                return _objRes;

            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Group : GetAllGroups", "Problem In Getting a list of Groups", ex.Message.ToString());
                return null;
            }
        }
        #endregion
        #endregion


       
    }
}