﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Groups
    {
        public int groupId { get; set; }
        public string name { get; set; }
        public string status { get; set; }
    }
}