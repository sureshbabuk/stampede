﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Services.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Net.Http.Headers;

namespace Stampade.Services.Models.Business
{
    public class Login
    {

        #region Entities

        public STAMPEDEEntities context = new STAMPEDEEntities();

        #endregion

        public bool Create_User(STAM_MA_USERS objUser)
        {
            try
            {
                STAM_MA_USERS objCreateUser = new STAM_MA_USERS()
                {
                    STAM_USER_TYPE = objUser.STAM_USER_TYPE,
                    STAM_USER_STATUS_CODE = objUser.STAM_USER_STATUS_CODE,
                    SAM_USER_ROLE = objUser.SAM_USER_ROLE,
                    STAM_USER_FNAME = objUser.STAM_USER_FNAME,
                    STAM_USER_LNAME = objUser.STAM_USER_LNAME,
                    STAM_USER_GENDER = objUser.STAM_USER_GENDER,
                    STAM_USER_ADD1 = objUser.STAM_USER_ADD1,
                    STAM_USER_ADD2 = objUser.STAM_USER_ADD2,
                    STAM_USER_TELEPHONE = objUser.STAM_USER_TELEPHONE,
                    STAM_USER_MOBILE = objUser.STAM_USER_MOBILE,
                    STAM_USER_WEBSITE = objUser.STAM_USER_WEBSITE,
                    STAM_USER_EMAIL = objUser.STAM_USER_EMAIL,
                    STAM_USER_IMAGE = objUser.STAM_USER_IMAGE,
                    STAM_USER_USERNAME = objUser.STAM_USER_USERNAME,
                    STAM_USER_PSWD = objUser.STAM_USER_PSWD,
                    STAM_IS_ACTIVATED = objUser.STAM_IS_ACTIVATED,
                    STAM_ACTIVATED_STRING = objUser.STAM_ACTIVATED_STRING,
                    STAM_USER_TYPE_KEY_ID = objUser.STAM_USER_TYPE_KEY_ID,
                    STAM_ACTIVATED_STATUS = objUser.STAM_ACTIVATED_STATUS,
                    SAM_CREATED_USER = objUser.SAM_CREATED_USER,
                    STAM_CREATED_DATE = objUser.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objUser.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objUser.STAM_LAST_UPDATED_DATE,
                };

                context.STAM_MA_USERS.Add(objCreateUser);
                context.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Accont : CreateUser", "Problem In Creating a new User", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public STAM_MA_COACH_WORKOUT GetWorkoutDate(string WorkoutDate)
        {
            try
            {
                DateTime dtWorkoutdate = DateTime.ParseExact(WorkoutDate, "dd/MM/yyyy", null);
                var user = from us in context.STAM_MA_COACH_WORKOUT where us.STAM_COACH_WORKOUT_DATE == dtWorkoutdate select us;
                STAM_MA_COACH_WORKOUT _objUser = user.FirstOrDefault();
                return _objUser;
            }
            catch (Exception ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "User : GetMailID", "Problem In Getting a User", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_USERS GetMailID(string MailAdd)
        {
            try
            {
                var user = from us in context.STAM_MA_USERS where us.STAM_USER_USERNAME == MailAdd select us;
                STAM_MA_USERS _objUser = user.FirstOrDefault();
                return _objUser;
            }
            catch (Exception ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "User : GetMailID", "Problem In Getting a User", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_USERS GetUser(long UserID)
        {
            try
            {
                var user = from us in context.STAM_MA_USERS where us.STAM_USERID == UserID select us;
                STAM_MA_USERS _objUser = user.FirstOrDefault();
                return _objUser;
            }
            catch (Exception ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "User : GetMailID", "Problem In Getting a User", ex.Message.ToString());
                return null;
            }
        }

        public string GetUserTypeID(string UserID, int UserType, long createdID)
        {
            try
            {

                string[] _grp = null;
                _grp = UserID.ToString().TrimEnd(',').Split(',');
                List<STAM_MA_USERS> _objRes = new List<STAM_MA_USERS>();
                string result = "";

                foreach (var item in _grp)
                {
                    long item1 = Convert.ToInt32(item);
                    var objAth = from ath in context.STAM_MA_USERS where ath.STAM_USER_TYPE_KEY_ID == item1 &&  ath.STAM_USER_TYPE ==UserType &&  ath.SAM_CREATED_USER == createdID select ath;
                    if (objAth != null)
                    {
                        _objRes.AddRange(objAth);
                    }
                }

                foreach (var item in _objRes)
                {
                    result += item.STAM_USERID + ",";
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "User : GetMailID", "Problem In Getting a User", ex.Message.ToString());
                return null;
            }
        }
       

    }
}