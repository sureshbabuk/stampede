﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Request
    {
        public int ID { get; set; }
        public string deviceName { get; set; }
        public string deviceID { get; set; }
        public int groupID { get; set; }
        public string message { get; set; }
        public int coachID { get; set; }
        public int sectionID { get; set; }
        public DateTime selectedDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public Nullable<long> AthleteID { get; set; }


        public string AthleteName { get; set; }

    }
}