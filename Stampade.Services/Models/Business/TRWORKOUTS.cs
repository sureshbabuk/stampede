﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;
using System.Globalization;
using Stampade.Models;

namespace Stampade.Services.Models.Business
{
      
    public class TRWORKOUTS
    {
        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();

        Logger objLog = new Logger();
        Utility utility = new Utility();
        public long WorkOutID;
        public long WorkOutDetailID;



        public class Workout_Model
        {
            public List<string> Sections { get; set; }
            public int Runs { get; set; }
            public int Distance { get; set; }
            public List<string> SectionIDs { get; set; }


            public List<STAM_TR_DETAILS> _objsecDetail1 = new List<STAM_TR_DETAILS>();

            public List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();

            public List<STAM_TR_DETAILS_SECTION> _objsecDetail = new List<STAM_TR_DETAILS_SECTION>();


        



        }
        #endregion

            
        public bool Create_CoachWorkout(STAM_TR_WORKOUTS objCoach)
        {
            try
            {
                STAM_TR_WORKOUTS objCoachWorkout = new STAM_TR_WORKOUTS()
                {
                 
                  STAM_WORKOUT_RUNS=objCoach.STAM_WORKOUT_RUNS,
                  STAM_WORKOUT_DATE=objCoach.STAM_WORKOUT_DATE,
                  STAM_WORKOUT_STATUS=objCoach.STAM_WORKOUT_STATUS,
                  STAM_WORKOUT_TITLE=objCoach.STAM_WORKOUT_TITLE,
                  STAM_WORKOUT_DISTANCE=objCoach.STAM_WORKOUT_DISTANCE,
                    STAM_WORKOUT_NOTES=objCoach.STAM_WORKOUT_NOTES,
                      STAM_WORKOUT_GROUPID=objCoach.STAM_WORKOUT_GROUPID,
                    STAM_WORKOUT_TYPE=objCoach.STAM_WORKOUT_TYPE,
                    STAM_CREATED_USER = objCoach.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE,
                  STAM_WORKOUT_GROUP=objCoach.STAM_WORKOUT_GROUP
                };

                objContext.STAM_TR_WORKOUTS.Add(objCoachWorkout);
                objContext.SaveChanges();
                WorkOutID = Convert.ToInt32(objCoachWorkout.STAM_WORKOUT_ID);
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        public bool Create_CoachWorkoutDetail(STAM_TR_DETAILS objCoach)
        {
            try
            {
                STAM_TR_DETAILS objCoachWorkout = new STAM_TR_DETAILS()
                {

                   STAM_WORKOUT_ID=objCoach.STAM_WORKOUT_ID,
                   STAM_RUN=objCoach.STAM_RUN,
                   STAM_CREATED_DATE=objCoach.STAM_CREATED_DATE,
                   STAM_SEC_ID=objCoach.STAM_SEC_ID,
                   STAM_SEC_TARGET_TIME=objCoach.STAM_SEC_TARGET_TIME,
                  // STAM_NO_ROW=objCoach.STAM_NO_ROW
                };

                objContext.STAM_TR_DETAILS.Add(objCoachWorkout);
                objContext.SaveChanges();
                WorkOutDetailID = Convert.ToInt32(objCoachWorkout.STAM_WORKOUT_DETAILS_ID);
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public bool Create_CoachWorkoutSection(STAM_TR_DETAILS_SECTION objCoach)
        {

            try
            {
                STAM_TR_DETAILS_SECTION objCoachWorkout = new STAM_TR_DETAILS_SECTION()
                {
//STAM_WORKOUT_DETAILS_ID=objCoach.STAM_WORKOUT_DETAILS_ID,
//STAM_SEC_TARGET_TIME=objCoach.STAM_SEC_TARGET_TIME,
//STAM_SEC_ID=objCoach.STAM_SEC_ID

                };

                //objContext.STAM_TR_DETAILS_SECTION.Add(objCoachWorkout);
                //objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }














        public bool Update_CoachWorkout(STAM_TR_WORKOUTS objCoach)
        {

            try
            {
                //STAM_TR_WORKOUTS objCoachWorkout = new STAM_TR_WORKOUTS()
                //{

                //    STAM_WORKOUT_RUNS = objCoach.STAM_WORKOUT_RUNS,
                //    STAM_WORKOUT_DATE = objCoach.STAM_WORKOUT_DATE,
                //    STAM_WORKOUT_STATUS = objCoach.STAM_WORKOUT_STATUS,
                //    STAM_WORKOUT_TITLE = objCoach.STAM_WORKOUT_TITLE,
                //    STAM_WORKOUT_DISTANCE = objCoach.STAM_WORKOUT_DISTANCE,
                //    STAM_WORKOUT_NOTES = objCoach.STAM_WORKOUT_NOTES,
                //    STAM_WORKOUT_GROUPID = objCoach.STAM_WORKOUT_GROUPID,
                //    STAM_WORKOUT_TYPE = objCoach.STAM_WORKOUT_TYPE,
                //    STAM_CREATED_USER = objCoach.STAM_CREATED_USER,
                //    STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE,
                //    STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER,
                //    STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE,
                //    STAM_WORKOUT_GROUP = objCoach.STAM_WORKOUT_GROUP
                //};





                var objUpdateCoach = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == objCoach.STAM_WORKOUT_ID).SingleOrDefault();
                {


                       objUpdateCoach.STAM_WORKOUT_RUNS = objCoach.STAM_WORKOUT_RUNS;
                       objUpdateCoach.STAM_WORKOUT_DATE = objCoach.STAM_WORKOUT_DATE;
                        objUpdateCoach.STAM_WORKOUT_STATUS = objCoach.STAM_WORKOUT_STATUS;
                         objUpdateCoach.STAM_WORKOUT_TITLE = objCoach.STAM_WORKOUT_TITLE;
                         objUpdateCoach.STAM_WORKOUT_DISTANCE = objCoach.STAM_WORKOUT_DISTANCE;
                         objUpdateCoach.STAM_WORKOUT_NOTES = objCoach.STAM_WORKOUT_NOTES;
                         objUpdateCoach.STAM_WORKOUT_GROUPID = objCoach.STAM_WORKOUT_GROUPID;
                         objUpdateCoach.STAM_WORKOUT_TYPE = objCoach.STAM_WORKOUT_TYPE;
                         objUpdateCoach.STAM_CREATED_USER = objCoach.STAM_CREATED_USER;
                         objUpdateCoach.STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE;
                         objUpdateCoach.STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER;
                         objUpdateCoach.STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE;
                         objUpdateCoach.STAM_WORKOUT_GROUP = objCoach.STAM_WORKOUT_GROUP;
                   
                  
                };
                objContext.Entry(objUpdateCoach).State = EntityState.Modified;
                objContext.SaveChanges();


                var del = objContext.STAM_TR_DETAILS.Where(y => y.STAM_WORKOUT_ID == objCoach.STAM_WORKOUT_ID).ToList();

                if (del != null)
                {
                    foreach (var item in del)
                    {

                        //var delete = objContext.STAM_TR_DETAILS_SECTION.Where(y => y.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();

                        //delete.ForEach(cs => objContext.STAM_TR_DETAILS_SECTION.Remove(cs));


                    }



                    del.ForEach(cs => objContext.STAM_TR_DETAILS.Remove(cs));

                    objContext.SaveChanges();
                }






             //   objContext.STAM_TR_WORKOUTS.add(objCoachWorkout);
               // objContext.SaveChanges();
                //WorkOutID = Convert.ToInt32(objCoachWorkout.STAM_WORKOUT_ID);
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }



        public bool Update_CoachWorkoutStatus(STAM_TR_WORKOUTS objCoach)
        {
            try
            {
                var objUpdateCoach = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == objCoach.STAM_WORKOUT_ID).SingleOrDefault();
                {



                    objUpdateCoach.STAM_WORKOUT_STATUS = objCoach.STAM_WORKOUT_STATUS;

                    objUpdateCoach.STAM_LAST_UPDATED_USER = objCoach.STAM_LAST_UPDATED_USER;
                    objUpdateCoach.STAM_LAST_UPDATED_DATE = objCoach.STAM_LAST_UPDATED_DATE;



                };
                objContext.Entry(objUpdateCoach).State = EntityState.Modified;
                objContext.SaveChanges();
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }

        }

        public bool Update_CoachWorkoutDetail(STAM_TR_DETAILS objCoach)
        {
            try
            {


              
                


           


                STAM_TR_DETAILS objCoachWorkout = new STAM_TR_DETAILS()
                {

                    STAM_WORKOUT_ID = objCoach.STAM_WORKOUT_ID,
                    STAM_RUN = objCoach.STAM_RUN,
                    STAM_CREATED_DATE = objCoach.STAM_CREATED_DATE,
                    STAM_SEC_ID = objCoach.STAM_SEC_ID,
                    STAM_SEC_TARGET_TIME = objCoach.STAM_SEC_TARGET_TIME,
                   // STAM_NO_ROW = objCoach.STAM_NO_ROW

                };

                objContext.STAM_TR_DETAILS.Add(objCoachWorkout);
                objContext.SaveChanges();
                WorkOutDetailID = Convert.ToInt32(objCoachWorkout.STAM_WORKOUT_DETAILS_ID);
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public bool Update_CoachWorkoutSection(STAM_TR_DETAILS_SECTION objCoach)
        {


            try
            {


               
                   



                 






                STAM_TR_DETAILS_SECTION objCoachWorkout = new STAM_TR_DETAILS_SECTION()
                {
                    //STAM_WORKOUT_DETAILS_ID = objCoach.STAM_WORKOUT_DETAILS_ID,
                    //STAM_SEC_TARGET_TIME = objCoach.STAM_SEC_TARGET_TIME,
                  //  STAM_SEC_ID = objCoach.STAM_SEC_ID

                };

                //objContext.STAM_TR_DETAILS_SECTION.Add(objCoachWorkout);
                //objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Coach : CreateCoach", "Problem In Creating a new Coach Workout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }
















        public IEnumerable<TR_WORKOUTS> GetNewCoachWorkoutList(string CoachID)
        {

            try
            {
                string[] _ath = null;
                _ath = CoachID.ToString().TrimEnd(',').Split(',');
                List<STAM_TR_WORKOUTS> _objRes = new List<STAM_TR_WORKOUTS>();

                //foreach (var item in _ath)
                //{
                    long item1 = Convert.ToInt64(_ath[0]);



                    STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID ==item1).FirstOrDefault();

                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                long? userids = User.SAM_CREATED_USER;

                if (User.STAM_USER_TYPE == 3)
                {
                    //var obj_Coach = from work in objContext.STAM_TR_WORKOUTS where work.STAM_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 select work;


                        var obj_Coach = (from e in objContext.STAM_TR_WORKOUTS 
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                       where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == item1 || e.STAM_CREATED_USER==userids)
                                  
                                       && e.STAM_WORKOUT_STATUS != 3
                                         select new TR_WORKOUTS()
                                         {
                                             
                                            STAM_WORKOUT_ID= e.STAM_WORKOUT_ID,
                                           STAM_WORKOUT_NOTES=  e.STAM_WORKOUT_NOTES,
                                            STAM_WORKOUT_RUNS= e.STAM_WORKOUT_RUNS,
                                          STAM_WORKOUT_STATUS=   e.STAM_WORKOUT_STATUS,
                                            STAM_WORKOUT_TITLE= e.STAM_WORKOUT_TITLE,
                                           STAM_WORKOUT_TYPE=  e.STAM_WORKOUT_TYPE,
                                            STAM_WORKOUT_DATE = e.STAM_WORKOUT_DATE,
                                            STAM_WORKOUT_GROUP = e.STAM_WORKOUT_GROUP,
                                            STAM_CREATED_DATE=e.STAM_CREATED_DATE
                                         }).ToList();

                        //List<STAM_TR_WORKOUTS> obj_CoachWork = obj_Coach.ToList<STAM_TR_WORKOUTS>();
                        return obj_Coach.ToList();
                        //if (obj_Coach != null)
                        //{
                        //    _objRes.AddRange(obj_Coach);
                        //}

                }
                if (User.STAM_USER_TYPE == 4)
                {
                    //var obj_Coach = from work in objContext.STAM_TR_WORKOUTS where work.STAM_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 select work;

                    var ath = objContext.STAM_MA_ATHLETE.Where(p => p.STAM_ATH_ID == User.STAM_USER_TYPE_KEY_ID).SingleOrDefault();
                    int groupid =Convert.ToInt32(ath.STAM_ATH_ASSIGNGROUPID);
                    var obj_Coach = (from e in objContext.STAM_TR_WORKOUTS
                                     join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID                                     
                                     where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == item1 || e.STAM_CREATED_USER == userids)
                                     && e.STAM_WORKOUT_STATUS != 3 && e.STAM_WORKOUT_GROUPID == groupid
                                     select new TR_WORKOUTS()
                                     {

                                         STAM_WORKOUT_ID = e.STAM_WORKOUT_ID,
                                         STAM_WORKOUT_NOTES = e.STAM_WORKOUT_NOTES,
                                         STAM_WORKOUT_RUNS = e.STAM_WORKOUT_RUNS,
                                         STAM_WORKOUT_STATUS = e.STAM_WORKOUT_STATUS,
                                         STAM_WORKOUT_TITLE = e.STAM_WORKOUT_TITLE,
                                         STAM_WORKOUT_TYPE = e.STAM_WORKOUT_TYPE,
                                         STAM_WORKOUT_DATE = e.STAM_WORKOUT_DATE,
                                         STAM_WORKOUT_GROUP = e.STAM_WORKOUT_GROUP,
                                         STAM_CREATED_DATE = e.STAM_CREATED_DATE
                                     }).ToList();

                    //List<STAM_TR_WORKOUTS> obj_CoachWork = obj_Coach.ToList<STAM_TR_WORKOUTS>();
                    return obj_Coach.ToList();
                    //if (obj_Coach != null)
                    //{
                    //    _objRes.AddRange(obj_Coach);
                    //}

                }
                if (User.STAM_USER_TYPE == 2)
                {
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                       // join t in objContext.STAM_TR_DETAILS on e.STAM_WORKOUT_ID equals t.STAM_WORKOUT_ID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || e.STAM_CREATED_USER == item1)
                                           && e.STAM_WORKOUT_STATUS != 3
                                     // select e).ToList();
                                         select new TR_WORKOUTS()
                                        {

                                            STAM_WORKOUT_ID = e.STAM_WORKOUT_ID,
                                            STAM_WORKOUT_NOTES = e.STAM_WORKOUT_NOTES,
                                            STAM_WORKOUT_RUNS = e.STAM_WORKOUT_RUNS,
                                            STAM_WORKOUT_STATUS = e.STAM_WORKOUT_STATUS,
                                            STAM_WORKOUT_TITLE = e.STAM_WORKOUT_TITLE,
                                            STAM_WORKOUT_TYPE = e.STAM_WORKOUT_TYPE,
                                            STAM_WORKOUT_DATE=e.STAM_WORKOUT_DATE,
                                            STAM_WORKOUT_GROUP=e.STAM_WORKOUT_GROUP,
                                            STAM_CREATED_DATE=e.STAM_CREATED_DATE
                                            //STAM_WORKOUT_DETAILS_ID=e.STAM_TR_DETAILS.FirstOrDefault().

                                        }).ToList();

                    //List<STAM_TR_WORKOUTS> obj_CoachWork = coachwrkouts.ToList<STAM_TR_WORKOUTS>();

                   // List<TR_WORKOUTS> obj_CoachWork = coachwrkouts.ToList<TR_WORKOUTS>();

                    return coachwrkouts.ToList();
                    //if (coachwrkouts != null)
                    //{
                    //    _objRes.AddRange(coachwrkouts);

                    //}
                }

                if (User.STAM_USER_TYPE == 1)
                {
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        // join t in objContext.STAM_TR_DETAILS on e.STAM_WORKOUT_ID equals t.STAM_WORKOUT_ID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || e.STAM_CREATED_USER == item1)
                                           && e.STAM_WORKOUT_STATUS != 3
                                        // select e).ToList();
                                        select new TR_WORKOUTS()
                                        {

                                            STAM_WORKOUT_ID = e.STAM_WORKOUT_ID,
                                            STAM_WORKOUT_NOTES = e.STAM_WORKOUT_NOTES,
                                            STAM_WORKOUT_RUNS = e.STAM_WORKOUT_RUNS,
                                            STAM_WORKOUT_STATUS = e.STAM_WORKOUT_STATUS,
                                            STAM_WORKOUT_TITLE = e.STAM_WORKOUT_TITLE,
                                            STAM_WORKOUT_TYPE = e.STAM_WORKOUT_TYPE,
                                            STAM_WORKOUT_DATE = e.STAM_WORKOUT_DATE,
                                            STAM_WORKOUT_GROUP = e.STAM_WORKOUT_GROUP,
                                            STAM_CREATED_DATE = e.STAM_CREATED_DATE
                                            //STAM_WORKOUT_DETAILS_ID=e.STAM_TR_DETAILS.FirstOrDefault().

                                        }).ToList();

                    //List<STAM_TR_WORKOUTS> obj_CoachWork = coachwrkouts.ToList<STAM_TR_WORKOUTS>();

                    // List<TR_WORKOUTS> obj_CoachWork = coachwrkouts.ToList<TR_WORKOUTS>();

                    return coachwrkouts.ToList();
                    //if (coachwrkouts != null)
                    //{
                    //    _objRes.AddRange(coachwrkouts);

                    //}
                }
                return null;

                    //                && work.STAM_CREATED_USER == item1 
                    //var obj_Coach = from work in objContext.STAM_TR_WORKOUTS
                    //                 join wrkdetail in objContext.STAM_TR_DETAILS on work.STAM_WORKOUT_ID equals wrkdetail.STAM_WORKOUT_ID
                    //                 join  wrkdetailsec in  objContext.STAM_TR_DETAILS_SECTION on wrkdetail.STAM_WORKOUT_DETAILS_ID equals wrkdetailsec.STAM_WORKOUT_DETAILS_ID


                    //                 where work.STAM_WORKOUT_STATUS != 3 
                    //                && work.STAM_CREATED_USER == item1 
                    //                select new 
                    //                {



                    //                    STAM_WORKOUT_ID = work.STAM_WORKOUT_ID,
                    //                    work.STAM_WORKOUT_DATE,
                    //                    work.STAM_WORKOUT_DISTANCE,
                    //                    work.STAM_WORKOUT_GROUP,
                    //                    work.STAM_WORKOUT_NOTES,
                    //                    work.STAM_WORKOUT_TITLE,
                    //                    work.STAM_WORKOUT_TYPE,
                    //                    work.STAM_WORKOUT_RUNS,
                    //                    work.STAM_WORKOUT_GROUPID,
                    //                    wrkdetail.STAM_RUN,
                    //                    wrkdetail.STAM_WORKOUT_DETAILS_ID,
                    //                    wrkdetail.






                    //                }

                                    //orderby work.STAM_COACH_WORKOUT_ID 
                                    
                                    //descending select work;
                                    
                                    
                                    //where work.STAM_COACH_WORKOUT_STATUS != 3 
                                    //&& work.STAM_CREATED_USER == item1 
                                    //orderby work.STAM_COACH_WORKOUT_ID descending select work;
                   
               // }
              
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkout", "Problem In Getting a list of Workout", ex.Message.ToString());
                return null;
            }
        }




        public bool DeleteCoachWorkout(long WorkoutID)
        {
            try
            {
                STAM_TR_WORKOUTS wrkout = new STAM_TR_WORKOUTS();

                wrkout = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == WorkoutID).FirstOrDefault();

            


                if (wrkout != null)
                {
                    var objAth = from ATh in objContext.STAM_MA_ATHLETE_SECTION where ATh.STAM_ATHSEC_GROUPID == wrkout.STAM_WORKOUT_GROUPID && ATh.STAM_ATHSEC_STATUS != 3 select ATh;
                    STAM_MA_ATHLETE_SECTION _objAth = objAth.FirstOrDefault();

                    if (_objAth != null)
                    {
                        if (_objAth.STAM_ATHSEC_GROUPID == wrkout.STAM_WORKOUT_GROUPID)
                        {
                           return false;

                        }
                    }


                    var delete = objContext.STAM_TR_DETAILS.Where(x => x.STAM_WORKOUT_ID == wrkout.STAM_WORKOUT_ID).ToList();
                    if (delete != null)
                    {
                        foreach (var item in delete)
                        {
                            //var del = objContext.STAM_TR_DETAILS_SECTION.Where(y => y.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                            //del.ForEach(cs => objContext.STAM_TR_DETAILS_SECTION.Remove(cs));

                        }



                        delete.ForEach(cs => objContext.STAM_TR_DETAILS.Remove(cs));
                        var wrkoutdel = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == wrkout.STAM_WORKOUT_ID).ToList();
                        wrkoutdel.ForEach(cs => objContext.STAM_TR_WORKOUTS.Remove(cs));
                        objContext.SaveChanges();
                    }
                    
                    return true;
                }
                else
                {

                    return false;
                }

            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "NewCoachWorkout : DeleteNewCoachWorkout", "Problem In deleting new Workout", ex.Message.ToString());
                return false;
            }


        }







        public TR_WORKOUTS EditNewWorkouts(string CoachID)
        {

            try
            {
                string[] _ath = null;
                _ath = CoachID.ToString().TrimEnd(',').Split(',');
                List<TRWORKOUTS> _objRes = new List<TRWORKOUTS>();
                // foreach (var item in _ath)
                // {
                long item1 = Convert.ToInt64(_ath[0]);


               

               // var itemsec=null;

             

                var obj_Coach = (from work in objContext.STAM_TR_WORKOUTS
                                 join wrkout in objContext.STAM_TR_DETAILS on work.STAM_WORKOUT_ID equals wrkout.STAM_WORKOUT_ID
                                 where work.STAM_WORKOUT_STATUS != 3
                             && work.STAM_WORKOUT_ID == item1
                                 select new TR_WORKOUTS()
                                       {

                                           STAM_WORKOUT_ID = work.STAM_WORKOUT_ID,
                                           STAM_WORKOUT_NOTES = work.STAM_WORKOUT_NOTES,
                                           STAM_WORKOUT_RUNS = work.STAM_WORKOUT_RUNS,
                                           STAM_WORKOUT_STATUS = work.STAM_WORKOUT_STATUS,
                                           STAM_WORKOUT_TITLE = work.STAM_WORKOUT_TITLE,
                                           STAM_WORKOUT_TYPE = work.STAM_WORKOUT_TYPE,
                                           STAM_WORKOUT_DATE = work.STAM_WORKOUT_DATE,
                                           STAM_WORKOUT_GROUP = work.STAM_WORKOUT_GROUP,
                                           STAM_WORKOUT_GROUPID=work.STAM_WORKOUT_GROUPID,
                                           STAM_WORKOUT_DISTANCE=work.STAM_WORKOUT_DISTANCE,

                                           STAM_WORKOUT_DETAILS_ID=wrkout.STAM_WORKOUT_DETAILS_ID
                                       }).FirstOrDefault();
                            

                                    
                                //    && work.STAM_CREATED_USER == item1
                                //orderby work.STAM_COACH_WORKOUT_ID descending
                                //select work;



            return obj_Coach;
                //STAM_TR_WORKOUTS wrkout = new STAM_TR_WORKOUTS();

                //wrkout = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == item1).FirstOrDefault();




                //if (wrkout != null)
                //{



                //    var delete = objContext.STAM_TR_DETAILS.Where(x => x.STAM_WORKOUT_ID == wrkout.STAM_WORKOUT_ID).ToList();
                //    if (delete != null)
                //    {
                //        foreach (var item in delete)
                //        {
                //            var del = objContext.STAM_TR_DETAILS_SECTION.Where(y => y.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                //            // del.ForEach(cs => objContext.STAM_TR_DETAILS_SECTION.Remove(cs));

                //        }



                //        //  delete.ForEach(cs => objContext.STAM_TR_DETAILS.Remove(cs));

                //        //objContext.SaveChanges();
                //    }

                //    //if (obj_Coach != null)
                //    //{
                //    //    _objRes.AddRange(obj_Coach);
                //    //}
                //}
                //return _objRes;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkout", "Problem In Getting a list of Workout", ex.Message.ToString());
                return null;
            }
        }




        public WorkoutModel EditNewWorkoutections(string CoachID)
        {


            try
            {
                string[] _ath = null;
                _ath = CoachID.ToString().TrimEnd(',').Split(',');
              WorkoutModel _objRes = new WorkoutModel();


                
                long item1 = Convert.ToInt64(_ath[0]);


               //_objRes = (from work in objContext.STAM_TR_DETAILS
               //            where work.STAM_WORKOUT_ID == item1
               //            select new STAM_TR_DETAILS_MODEL()
               //                {

               //                    STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
               //                    //_objSTAM_TR_DETAILS_SECTION = work.STAM_TR_DETAILS_SECTION.ToList()
               //                    //_objSTAM_TR_DETAILS_SECTION=work.STAM_TR_DETAILS_SECTION

               //                }).ToList();


               var itemdetail = from wrkoutsec in objContext.STAM_TR_DETAILS where wrkoutsec.STAM_WORKOUT_ID == item1 select wrkoutsec;
                //foreach(var data in _objRes)
                //{
                //    var itemsec = objContext.STAM_TR_DETAILS_SECTION.Where(x => x.STAM_WORKOUT_DETAILS_ID == data.STAM_WORKOUT_DETAILS_ID);
                //    _objRes._objSTAM_TR_DETAILS_SECTION.AddRange(itemsec);

                    
                //}
               _objRes._objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();
                foreach (var item in itemdetail)
                {



                    //var itemdetailsec = from wrkoutsec in objContext.STAM_TR_DETAILS_SECTION where wrkoutsec.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID select wrkoutsec;



                    //var itemdetailsec = (from work in objContext.STAM_TR_DETAILS_SECTION
                    //                     where work.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID
                    //           select new STAM_TR_DETAILS_SECTION_MODEL()
                    //               {

                    //                   STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
                    //                   STAM_SEC_TARGET_TIME=work.STAM_SEC_TARGET_TIME
                    //                   //_objSTAM_TR_DETAILS_SECTION = work.STAM_TR_DETAILS_SECTION.ToList()
                    //                   //_objSTAM_TR_DETAILS_SECTION=work.STAM_TR_DETAILS_SECTION

                    //               }).ToList();


                    //var item2 = from wrkdt in objContext.STAM_TR_DETAILS where wrkdt.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID select wrkdt;
                  //  var item2 = from wrkdt in objContext.STAM_TR_DETAILS where wrkdt.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID select wrkdt;
                    var item2 = (from work in objContext.STAM_TR_DETAILS
                               where work.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID
                               select new STAM_TR_DETAILS_MODEL()
                                   {

                                       STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
                                       STAM_RUN=work.STAM_RUN,
                                       STAM_SEC_TARGET_TIME = work.STAM_SEC_TARGET_TIME,
                                       STAM_SEC_ID = work.STAM_SEC_ID,
                                     //  STAM_NO_ROW=work.STAM_NO_ROW
                                       //_objSTAM_TR_DETAILS_SECTION = work.STAM_TR_DETAILS_SECTION.ToList()
                                       //_objSTAM_TR_DETAILS_SECTION=work.STAM_TR_DETAILS_SECTION

                                   }).ToList();
                   // _objRes._objSTAM_TR_DETAILS_SECTION.AddRange(itemdetailsec);


                   
                     _objRes._objSTAM_TR_DETAILS.AddRange(item2);

                   // _objRes._objSTAM_TR_DETAILS.Add(item2);
                    //_objRes._objsecDetail1.Add(item);


                



                }
                //    _objRes._objsecDetail1 = null;

                var obj_Coach = (from work in objContext.STAM_TR_DETAILS
                                 join wrkout in objContext.STAM_TR_DETAILS_SECTION on work.STAM_WORKOUT_DETAILS_ID equals wrkout.STAM_WORKOUT_DETAILS_ID
                                 // where work.STAM_WORKOUT_STATUS != 3
                                 where work.STAM_WORKOUT_ID == item1
                                 select new Workout_Model
                                 {

                                     //_objsecDetail=wrkout,


                                 }).ToList();



                //wrkout;



                //    && work.STAM_CREATED_USER == item1
                //orderby work.STAM_COACH_WORKOUT_ID descending
                //select work;



                return _objRes;
                //STAM_TR_WORKOUTS wrkout = new STAM_TR_WORKOUTS();

                //wrkout = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == item1).FirstOrDefault();




                //if (wrkout != null)
                //{



                //    var delete = objContext.STAM_TR_DETAILS.Where(x => x.STAM_WORKOUT_ID == wrkout.STAM_WORKOUT_ID).ToList();
                //    if (delete != null)
                //    {
                //        foreach (var item in delete)
                //        {
                //            var del = objContext.STAM_TR_DETAILS_SECTION.Where(y => y.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                //            // del.ForEach(cs => objContext.STAM_TR_DETAILS_SECTION.Remove(cs));

                //        }



                //        //  delete.ForEach(cs => objContext.STAM_TR_DETAILS.Remove(cs));

                //        //objContext.SaveChanges();
                //    }

                //    //if (obj_Coach != null)
                //    //{
                //    //    _objRes.AddRange(obj_Coach);
                //    //}
                //}
                //return _objRes;



             //   _objRes = objContext.STAM_TR_DETAILS.Where(x=>x.STAM_WORKOUT_ID == item1).ToList();
                return _objRes;


            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkout", "Problem In Getting a list of Workout", ex.Message.ToString());
                return null;
            }
        }

















        public STAM_TR_WORKOUTS_MODEL ChkNewCoachWorkoutByGroup(string title, string Group, string _date, string Userid)
        {



            try
            {
                DateTime _dat = utility.FormatToDate(_date).Value;
                //var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_DATE == _dat && work.STAM_COACH_WORKOUT_STATUS != 3 select work;

                // var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where work.STAM_COACH_WORKOUT_TITLE == title && work.STAM_COACH_WORKOUT_GROUPNAME == Group && work.STAM_COACH_WORKOUT_DATE == _dat && work.STAM_COACH_WORKOUT_STATUS != 3 select work;


                string[] _grp = null;
                _grp = Userid.ToString().TrimEnd(',').Split(',');
                //  string[] _UserID = null;
                //_UserID = Userid.ToString().Split(',');
                List<STAM_TR_WORKOUTS_MODEL> _objRes = new List<STAM_TR_WORKOUTS_MODEL>();
                List<STAM_TR_WORKOUTS> _objRes1 = new List<STAM_TR_WORKOUTS>();

                //STAM_MA_COACH_WORKOUT obj_Coach;


                foreach (var item in _grp)
                {
                    long item1 = (long)Convert.ToDouble(item.ToString());




                    var obj_Coach = (from work in objContext.STAM_TR_WORKOUTS where work.STAM_WORKOUT_GROUP == Group && work.STAM_WORKOUT_DATE == _dat && work.STAM_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 
                                    
                                    select new STAM_TR_WORKOUTS_MODEL()
                    {
                        STAM_WORKOUT_DATE=work.STAM_WORKOUT_DATE,
                        STAM_WORKOUT_ID=work.STAM_WORKOUT_ID
                    }).ToList();


                    //  var obj_Coach = from work in objContext.STAM_MA_COACH_WORKOUT where  work.STAM_COACH_WORKOUT_TITLE == title && work.STAM_COACH_WORKOUT_STATUS != 3 && work.STAM_CREATED_USER == item1 select work;
                    _objRes.AddRange(obj_Coach);

                }

                STAM_TR_WORKOUTS_MODEL obj_CoachWork = _objRes.FirstOrDefault();
                return obj_CoachWork;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "CoachWorkout : GetCoachWorkoutByName", "Problem In Getting a CoachWorkout", ex.Message.ToString());
                return null;
            }
        }
    }
      

       


}