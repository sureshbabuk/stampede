﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;
using System.Globalization;


namespace Stampade.Models
{
    public class TR_WORKOUTS
    {



        public long STAM_WORKOUT_ID { get; set; }
            public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
            public string STAM_WORKOUT_TITLE { get; set; }
            public string STAM_WORKOUT_NOTES { get; set; }
            public string STAM_WORKOUT_TYPE { get; set; }
            public string STAM_WORKOUT_DISTANCE { get; set; }
            public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
            public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
            public Nullable<long> STAM_CREATED_USER { get; set; }
            public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
            public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
            public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
            public Nullable<int> STAM_WORKOUT_STATUS { get; set; }
            public string STAM_WORKOUT_GROUP { get; set; }
            public long STAM_WORKOUT_DETAILS_SEC_ID { get; set; }
            public Nullable<long> STAM_WORKOUT_DETAILS_ID { get; set; }
            public Nullable<long> STAM_SEC_ID { get; set; }
            public Nullable<System.DateTime> STAM_SEC_TARGET_TIME { get; set; }


 


        
            public Nullable<int> STAM_RUN { get; set; }

            public List<string> STAM_SEC_TARGET_TIME1 { get; set; }

        

            public string STAM_SEC_ID1 { get; set; }
       

            public List<long> STAM_SECID { get; set; }


            public List<int> DISTANCE { get; set; }

            public long UserID;

     
    }


}