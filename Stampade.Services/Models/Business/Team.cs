﻿using BusinessObjects.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class Team
    {
        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();
       
        #endregion

        #region Team

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        //add new job
        public bool Create_Team(STAM_MA_TEAM objTeam)
        {
            try
            {
                STAM_MA_TEAM objCreateTeam = new STAM_MA_TEAM()
                {
                    STAM_STATUS = objTeam.STAM_STATUS,
                    STAM_TEAM_NAME = objTeam.STAM_TEAM_NAME,
                    STAM_ASSIGN_COACHID = objTeam.STAM_ASSIGN_COACHID,
                    STAM_COACHASSIGNED = objTeam.STAM_COACHASSIGNED,
                    STAM_CREATED_USER = objTeam.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objTeam.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objTeam.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objTeam.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_TEAM.Add(objCreateTeam);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Team : CreateTeam", "Problem In Creating a new Team", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_Team(STAM_MA_TEAM objTeam)
        {
            try
            {
                var objUpdateTeam = objContext.STAM_MA_TEAM.Where(x => x.STAM_TEAM_ID == objTeam.STAM_TEAM_ID).SingleOrDefault();
                {
                    objUpdateTeam.STAM_STATUS = objTeam.STAM_STATUS;
                    objUpdateTeam.STAM_TEAM_NAME = objTeam.STAM_TEAM_NAME;
                    objUpdateTeam.STAM_ASSIGN_COACHID = objTeam.STAM_ASSIGN_COACHID;
                    objUpdateTeam.STAM_COACHASSIGNED = objTeam.STAM_COACHASSIGNED;
                    objUpdateTeam.STAM_COACHASSIGNED = objTeam.STAM_COACHASSIGNED;
                    objUpdateTeam.STAM_CREATED_USER = objTeam.STAM_CREATED_USER;
                    objUpdateTeam.STAM_CREATED_DATE = objTeam.STAM_CREATED_DATE;
                    objUpdateTeam.STAM_LAST_UPDATED_USER = objTeam.STAM_LAST_UPDATED_USER;
                    objUpdateTeam.STAM_LAST_UPDATED_DATE = objTeam.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateTeam).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Team : UpdateTeam", "Problem In Updating a Team", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_TEAM>  GetTeamList(long UserId)
        {
            try
            {
                var obj_Team = from Teams in objContext.STAM_MA_TEAM where Teams.STAM_STATUS != 3 && Teams.STAM_CREATED_USER == UserId orderby Teams.STAM_TEAM_ID descending select Teams;
                List<STAM_MA_TEAM> objTeams = obj_Team.ToList<STAM_MA_TEAM>();
                return objTeams;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Team : GetTeams", "Problem In Getting a list of Teams", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_TEAM GetTeamByID(long iTeam)
        {
            try
            {
                var objTeam = from Teams in objContext.STAM_MA_TEAM where Teams.STAM_TEAM_ID == iTeam select Teams;
                STAM_MA_TEAM objTeams = objTeam.FirstOrDefault();
                return objTeams;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "TeamController : GetJobs", "Problem In Getting a Team", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_TEAM GetTeamByTeamName(string Name)
        {
            try
            {
                var objTeam = from Teams in objContext.STAM_MA_TEAM where Teams.STAM_TEAM_NAME == Name && Teams.STAM_STATUS != 3 select Teams;
                STAM_MA_TEAM objTeams = objTeam.FirstOrDefault();
                return objTeams;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Team : GetTeambyName", "Problem In Getting a Team", ex.Message.ToString());
                return null;
            }
        }

       

        #endregion
    }
}