﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models.Business
{
    public class WorkoutResuls
    {
        public Nullable<long> WorkoutID { get; set; }
        public string WorkoutTitle { get; set; }
        public Nullable<long> WorkoutCoachId { get; set; }
        public Nullable<long> WorkoutGroupID { get; set; }
        public string WorkoutGroupName { get; set; }
        public string WorkoutTargetTime { get; set; }
        public Nullable<System.DateTime> WorkoutDate { get; set; }
        public Nullable<decimal> WorkoutCompleteTime { get; set; }
        public string WorkoutDistance { get; set; }

        public string WorkoutNotes { get; set; }

        public Nullable<int> WorkoutRuns { get; set; }
        public Nullable<long> WorkoutCreateUser { get; set; }
        public Nullable<System.DateTime> WorkoutCreateDate { get; set; }
        public List<WorkoutSectionResult> WorkoutResultDetails { get; set; }        
    }
    public class WorkoutSectionResult
    {
        public Nullable<long> WorkoutSectionId { get; set; }
        public string WorkoutSectionName { get; set; }
        public Nullable<decimal> WorkoutSectionRun { get; set; }
        public Nullable<decimal> WorkoutSectionTime { get; set; }
        public Nullable<int> WorkoutSectionSplit { get; set; }
    }
    public class STAM_TR_WORKOUTS_Model
    {


        public long STAM_WORKOUT_ID { get; set; }
        public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public string STAM_WORKOUT_NOTES { get; set; }
        public string STAM_WORKOUT_TYPE { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
        public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_WORKOUT_STATUS { get; set; }

        public string STAM_WORKOUT_GROUP { get; set; }


        public string STAM_WORKOUT_DATE1 { get; set; }




        public List<STAM_TR_DETAILS_Model> STAM_TR_DETAILS { get; set; }


        


    }

    public partial class STAM_TR_DETAILS_Model
    {

        public long STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_ID { get; set; }
        public Nullable<int> STAM_RUN { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_SEC_ID { get; set; }
        public string STAM_SEC_TARGET_TIME { get; set; }
        public Nullable<int> STAM_NO_ROW { get; set; }

        public  ICollection<STAM_TR_DETAILS_SECTION> STAM_TR_DETAILS_SECTION { get; set; }
       // public virtual STAM_TR_WORKOUTS STAM_TR_WORKOUTS { get; set; }
    }


}