﻿using BusinessObjects.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Stampade.Services.Models.Business
{

    public class WorkoutResult
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();

        Logger objLog = new Logger();

        #endregion


        /// <summary>
        /// Create/Save a workout result
        /// </summary>
        /// <param name="objWorkoutResult"></param>
        ///  <param name="lstWorkoutDetailedResult"></param>
        /// <returns></returns>
        public bool AddWorkoutResult(WorkoutResuls objWorkoutResult, List<WorkoutSectionResult> lstWorkoutDetailedResult)
        {
            try
            {
                STAM_MA_COACH_WORKOUT objcoach = objContext.STAM_MA_COACH_WORKOUT.Where(p => p.STAM_COACH_WORKOUT_ID == objWorkoutResult.WorkoutID).SingleOrDefault();
                if (objcoach != null)
                {
                    STAM_MA_WORKOUT_RESULT itemOfWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == objWorkoutResult.WorkoutID).FirstOrDefault();
                    if (itemOfWorkoutResult == null)
                    {
                        STAM_MA_WORKOUT_RESULT AddWorkoutResult = new STAM_MA_WORKOUT_RESULT()
                        {
                            WORKOUT_ID = objWorkoutResult.WorkoutID,
                            WORKOUT_COACH_ID = objWorkoutResult.WorkoutCoachId,
                            WORKOUT_TARGET_TIME = objWorkoutResult.WorkoutCompleteTime,
                            WORKOUT_GROUP_ID = objWorkoutResult.WorkoutGroupID,
                            WORKOUT_DATE = objWorkoutResult.WorkoutDate,
                            WORKOUT_CREATED_DATE = DateTime.Now,
                            WORKOUT_CREATED_USER = objWorkoutResult.WorkoutCreateUser,
                        };
                        objContext.STAM_MA_WORKOUT_RESULT.Add(AddWorkoutResult);
                        objContext.SaveChanges();
                        long iWorkoutId = Convert.ToInt32(AddWorkoutResult.WORKOUT_RESULT_ID);
                        bool result = AddWorkoutSectionResult(lstWorkoutDetailedResult, iWorkoutId);
                        return result;
                    }
                    else
                    {
                        itemOfWorkoutResult.WORKOUT_ID = objWorkoutResult.WorkoutID;
                        itemOfWorkoutResult.WORKOUT_COACH_ID = objWorkoutResult.WorkoutCoachId;
                        itemOfWorkoutResult.WORKOUT_TARGET_TIME = objWorkoutResult.WorkoutCompleteTime;
                        itemOfWorkoutResult.WORKOUT_GROUP_ID = objWorkoutResult.WorkoutGroupID;
                        itemOfWorkoutResult.WORKOUT_DATE = objWorkoutResult.WorkoutDate;
                        itemOfWorkoutResult.WORKOUT_LASTUPDATED_DATE = DateTime.Now;
                        itemOfWorkoutResult.WORKOUT_LASTUPDATED_USER = 0;
                        objContext.SaveChanges();
                        long iWorkoutId = Convert.ToInt32(itemOfWorkoutResult.WORKOUT_RESULT_ID);
                        bool result = AddWorkoutSectionResult(lstWorkoutDetailedResult, iWorkoutId);
                        return result;
                    }
                }
                return false;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : AddWorkoutResult", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        /// <summary>
        ///   Create/Save a workout result  detailed 
        /// <param name="lstWorkoutDetailedResult"></param>
        /// <param name="iWorkoutId">Refernce for workout result id</param>
        /// <returns></returns>
        public bool AddWorkoutSectionResult(List<WorkoutSectionResult> lstWorkoutDetailedResult, long iWorkoutId)
        {
            try
            {
                int i = 0;
                foreach (var item in lstWorkoutDetailedResult)
                {
                    STAM_TR_WORKOUT_SECTION_RESULT WorkoutSectionResult = new STAM_TR_WORKOUT_SECTION_RESULT()
                    {
                        WORKOUT_RESULT_ID = iWorkoutId,
                        WORKOUT_SECTION_ID = item.WorkoutSectionId,
                        WORKOUT_DETAILED_SPLIT = item.WorkoutSectionSplit,
                        WORKOUT_SECTION_RUN = item.WorkoutSectionRun,
                        WORKOUT_SECTION_TIME = item.WorkoutSectionTime
                    };
                    objContext.STAM_TR_WORKOUT_SECTION_RESULT.Add(WorkoutSectionResult);
                    objContext.SaveChanges();
                    i++;
                }
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : AddWorkoutResult", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        ///// <summary>
        /////  Update a workout result  detailed 
        ///// <param name="lstWorkoutDetailedResult"></param>
        ///// <param name="iWorkoutId">Refernce for workout result id</param>
        ///// <returns></returns>
        //public bool UpdateWorkoutSectionResult(List<WorkoutSectionResult> lstWorkoutDetailedResult, long iWorkoutId)
        //{
        //    try
        //    {
        //        int i = 0;
        //        var deleteallitems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == iWorkoutId).ToList();
        //        foreach (var detail in deleteallitems)
        //        {
        //            objContext.STAM_TR_WORKOUT_SECTION_RESULT.Remove(detail);
        //            objContext.SaveChanges();
        //        }
        //        foreach (var item in lstWorkoutDetailedResult)
        //        {
        //            STAM_TR_WORKOUT_SECTION_RESULT WorkoutSectionResult = new STAM_TR_WORKOUT_SECTION_RESULT()
        //            {
        //                WORKOUT_RESULT_ID = iWorkoutId,
        //                WORKOUT_SECTION_ID = item.WorkoutSectionId,
        //                WORKOUT_DETAILED_SPLIT = item.WorkoutSectionSplit,
        //                WORKOUT_SECTION_RUN = item.WorkoutSectionRun,
        //                WORKOUT_SECTION_TIME = item.WorkoutSectionTime
        //            };
        //            objContext.STAM_TR_WORKOUT_SECTION_RESULT.Add(WorkoutSectionResult);
        //            objContext.SaveChanges();
        //            i++;
        //        }
        //        return true;
        //    }
        //    catch (Exception Ex)
        //    {
        //        Logger objLog = new Logger();
        //        objLog.LogToFile("Error", "WorkoutResult : AddWorkoutResult", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
        //        objLog = null;
        //        return false;
        //    }
        //}

        /// <summary>
        /// Get all workouts based on start date and end date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<WorkoutResuls> GetAllWorkouts(DateTime StartDate, DateTime EndDate, long Userid)
        {
            try
            {
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();

                //  var coachwrkout = (dynamic)null;

                long? userids = User.SAM_CREATED_USER;

                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;



                if (User.STAM_USER_TYPE == 3)
                {
                    var coachwrkout = (from e in objContext.STAM_MA_COACH_WORKOUT

                                       join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID



                                       where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                    && (EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) <= EndDate)
                                    && e.STAM_COACH_WORKOUT_STATUS != 3
                                       select e).ToList();

                    if (coachwrkout != null)
                    {
                        foreach (var itemcoach in coachwrkout)
                        {
                            WorkoutResuls objWorkoutResults = new WorkoutResuls();
                            objWorkoutResults.WorkoutID = itemcoach.STAM_COACH_WORKOUT_ID;
                            objWorkoutResults.WorkoutTitle = itemcoach.STAM_COACH_WORKOUT_TITLE;
                            objWorkoutResults.WorkoutDistance = itemcoach.STAM_COACH_WORKOUT_DISTANCE;
                            objWorkoutResults.WorkoutRuns = itemcoach.STAM_COACH_WORKOUT_RUNS;
                            objWorkoutResults.WorkoutGroupID = itemcoach.STAM_COACH_WORKOUT_GROUPID;
                            objWorkoutResults.WorkoutGroupName = itemcoach.STAM_COACH_WORKOUT_GROUPNAME;
                            objWorkoutResults.WorkoutDate = itemcoach.STAM_COACH_WORKOUT_DATE;
                            objWorkoutResults.WorkoutCoachId = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateUser = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateDate = itemcoach.STAM_CREATED_DATE;
                            objWorkoutResults.WorkoutTargetTime = itemcoach.STAM_COACH_WORKOUT_TARGET_MM + ':' + itemcoach.STAM_COACH_WORKOUT_TARGET_SS;
                            List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == itemcoach.STAM_COACH_WORKOUT_ID).ToList();
                            if (lstWorkoutResult.Count != 0)
                            {
                                foreach (var item in lstWorkoutResult)
                                {
                                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                    objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                                    List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && (p.STAM_CREATED_USER == Userid || p.STAM_CREATED_USER == User.SAM_CREATED_USER) && p.STAM_ATHSEC_STATUS == 1).ToList();
                                    foreach (var itemSection in lstSection)
                                    {
                                        var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_RESULT_ID && p.WORKOUT_SECTION_ID == itemSection.STAM_ATHSEC_ID).ToList();
                                        if (listOfItems.Count != 0)
                                        {
                                            foreach (var item1 in listOfItems)
                                            {
                                                WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                                objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                                objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                                objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                                                objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                                                objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                                                lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                            }
                                        }
                                        else
                                        {
                                            WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                            objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                            objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                            lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                        }
                                    }
                                    objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                                }
                            }
                            else
                            {
                                List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && (p.STAM_CREATED_USER == Userid || p.STAM_CREATED_USER == User.SAM_CREATED_USER) && p.STAM_ATHSEC_STATUS == 1).ToList();
                                foreach (var itemSection in lstSection)
                                {
                                    WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                    objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                    objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                    lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                }
                                objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                            }
                            lstWorkoutResults.Add(objWorkoutResults);
                        }
                        return lstWorkoutResults.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }


                if (User.STAM_USER_TYPE == 2)
                {
                    var coachwrkouts = (from e in objContext.STAM_MA_COACH_WORKOUT
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid) && (EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) <= EndDate)
                                        && e.STAM_COACH_WORKOUT_STATUS != 3
                                        select e).ToList();


                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            WorkoutResuls objWorkoutResults = new WorkoutResuls();
                            objWorkoutResults.WorkoutID = itemcoach.STAM_COACH_WORKOUT_ID;
                            objWorkoutResults.WorkoutTitle = itemcoach.STAM_COACH_WORKOUT_TITLE;
                            objWorkoutResults.WorkoutDistance = itemcoach.STAM_COACH_WORKOUT_DISTANCE;
                            objWorkoutResults.WorkoutRuns = itemcoach.STAM_COACH_WORKOUT_RUNS;
                            objWorkoutResults.WorkoutGroupID = itemcoach.STAM_COACH_WORKOUT_GROUPID;
                            objWorkoutResults.WorkoutGroupName = itemcoach.STAM_COACH_WORKOUT_GROUPNAME;
                            objWorkoutResults.WorkoutDate = itemcoach.STAM_COACH_WORKOUT_DATE;
                            objWorkoutResults.WorkoutCoachId = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateUser = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateDate = itemcoach.STAM_CREATED_DATE;
                            objWorkoutResults.WorkoutTargetTime = itemcoach.STAM_COACH_WORKOUT_TARGET_MM + ':' + itemcoach.STAM_COACH_WORKOUT_TARGET_SS;
                            List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == itemcoach.STAM_COACH_WORKOUT_ID).ToList();
                            if (lstWorkoutResult.Count != 0)
                            {
                                foreach (var item in lstWorkoutResult)
                                {
                                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                    objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                                    List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && (p.STAM_CREATED_USER == Userid || p.STAM_CREATED_USER == User.SAM_CREATED_USER) && p.STAM_ATHSEC_STATUS == 1).ToList();
                                    foreach (var itemSection in lstSection)
                                    {
                                        var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_RESULT_ID && p.WORKOUT_SECTION_ID == itemSection.STAM_ATHSEC_ID).ToList();
                                        if (listOfItems.Count != 0)
                                        {
                                            foreach (var item1 in listOfItems)
                                            {
                                                WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                                objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                                objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                                objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                                                objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                                                objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                                                lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                            }
                                        }
                                        else
                                        {
                                            WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                            objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                            objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                            lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                        }
                                    }
                                    objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                                }
                            }
                            else
                            {
                                List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && (p.STAM_CREATED_USER == Userid || p.STAM_CREATED_USER == User.SAM_CREATED_USER) && p.STAM_ATHSEC_STATUS == 1).ToList();
                                foreach (var itemSection in lstSection)
                                {
                                    WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                    objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                    objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                    lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                }
                                objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                            }
                            lstWorkoutResults.Add(objWorkoutResults);
                        }
                        return lstWorkoutResults.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }

                return null;

                //var coachwrkout = objContext.STAM_MA_COACH_WORKOUT.Where(w => (EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) <= EndDate && (w.STAM_CREATED_USER == Userid || w.STAM_CREATED_USER == User.SAM_CREATED_USER))).ToList();

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllWorkouts", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// Get all workouts based on start date and end date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<WorkoutResuls> GetAllResults(DateTime StartDate, DateTime EndDate, long Userid)
        {
            try
            {
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();

                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                long? userids = User.SAM_CREATED_USER;

                if (User.STAM_USER_TYPE == 3)
                {
                    var coachwrkout = (from e in objContext.STAM_MA_COACH_WORKOUT
                                       join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                       where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids)
                                       && (EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) >= StartDate.Date &&
                                       EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) <= EndDate.Date)
                                       && e.STAM_COACH_WORKOUT_STATUS != 3
                                       select e).ToList();

                    //var coachwrkout = objContext.STAM_MA_COACH_WORKOUT.Where(w => (EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) <= EndDate && w.STAM_CREATED_USER == Userid)).ToList();

                    if (coachwrkout != null)
                    {

                        foreach (var itemcoach in coachwrkout)
                        {
                            WorkoutResuls objWorkoutResults = new WorkoutResuls();
                            objWorkoutResults.WorkoutID = itemcoach.STAM_COACH_WORKOUT_ID;
                            objWorkoutResults.WorkoutTitle = itemcoach.STAM_COACH_WORKOUT_TITLE;
                            objWorkoutResults.WorkoutDistance = itemcoach.STAM_COACH_WORKOUT_DISTANCE;
                            objWorkoutResults.WorkoutRuns = itemcoach.STAM_COACH_WORKOUT_RUNS;
                            objWorkoutResults.WorkoutGroupID = itemcoach.STAM_COACH_WORKOUT_GROUPID;
                            objWorkoutResults.WorkoutGroupName = itemcoach.STAM_COACH_WORKOUT_GROUPNAME;
                            objWorkoutResults.WorkoutDate = itemcoach.STAM_COACH_WORKOUT_DATE;
                            objWorkoutResults.WorkoutCoachId = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateUser = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateDate = itemcoach.STAM_CREATED_DATE;
                            objWorkoutResults.WorkoutTargetTime = itemcoach.STAM_COACH_WORKOUT_TARGET_MM + ':' + itemcoach.STAM_COACH_WORKOUT_TARGET_SS;
                            List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == itemcoach.STAM_COACH_WORKOUT_ID).ToList();
                            foreach (var item in lstWorkoutResult)
                            {
                                List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                                var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_RESULT_ID).ToList();
                                foreach (var item1 in listOfItems)
                                {
                                    WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                    objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                                    objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                                    objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                                    objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                                    STAM_MA_ATHLETE_SECTION objsection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item1.WORKOUT_SECTION_ID).SingleOrDefault();
                                    if (objsection != null)
                                    {
                                        objWorkoutSectionResult.WorkoutSectionName = objsection.STAM_ATHSEC_NAME;
                                    }
                                    lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                }
                                objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;

                            }
                            lstWorkoutResults.Add(objWorkoutResults);
                        }
                        return lstWorkoutResults.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
                if (User.STAM_USER_TYPE == 2)
                {
                    var coachwrkouts = (from e in objContext.STAM_MA_COACH_WORKOUT
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid)
                                        && (EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) >= StartDate.Date &&
                                        EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) <= EndDate.Date)
                                        && e.STAM_COACH_WORKOUT_STATUS != 3
                                        select e).ToList();


                    //var coachwrkout = objContext.STAM_MA_COACH_WORKOUT.Where(w => (EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) <= EndDate && w.STAM_CREATED_USER == Userid)).ToList();

                    if (coachwrkouts != null)
                    {

                        foreach (var itemcoach in coachwrkouts)
                        {
                            WorkoutResuls objWorkoutResults = new WorkoutResuls();
                            objWorkoutResults.WorkoutID = itemcoach.STAM_COACH_WORKOUT_ID;
                            objWorkoutResults.WorkoutTitle = itemcoach.STAM_COACH_WORKOUT_TITLE;
                            objWorkoutResults.WorkoutDistance = itemcoach.STAM_COACH_WORKOUT_DISTANCE;
                            objWorkoutResults.WorkoutRuns = itemcoach.STAM_COACH_WORKOUT_RUNS;
                            objWorkoutResults.WorkoutGroupID = itemcoach.STAM_COACH_WORKOUT_GROUPID;
                            objWorkoutResults.WorkoutGroupName = itemcoach.STAM_COACH_WORKOUT_GROUPNAME;
                            objWorkoutResults.WorkoutDate = itemcoach.STAM_COACH_WORKOUT_DATE;
                            objWorkoutResults.WorkoutCoachId = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateUser = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateDate = itemcoach.STAM_CREATED_DATE;
                            objWorkoutResults.WorkoutTargetTime = itemcoach.STAM_COACH_WORKOUT_TARGET_MM + ':' + itemcoach.STAM_COACH_WORKOUT_TARGET_SS;
                            List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == itemcoach.STAM_COACH_WORKOUT_ID).ToList();
                            foreach (var item in lstWorkoutResult)
                            {
                                List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                                var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_RESULT_ID).ToList();
                                foreach (var item1 in listOfItems)
                                {
                                    WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                    objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                                    objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                                    objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                                    objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                                    STAM_MA_ATHLETE_SECTION objsection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item1.WORKOUT_SECTION_ID).SingleOrDefault();
                                    if (objsection != null)
                                    {
                                        objWorkoutSectionResult.WorkoutSectionName = objsection.STAM_ATHSEC_NAME;
                                    }
                                    lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                }
                                objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;

                            }
                            lstWorkoutResults.Add(objWorkoutResults);
                        }
                        return lstWorkoutResults.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }

                return null;


            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllResults", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// Get all workouts based on start date , end date and groupid
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        ///  <param name="EndDate"></param>
        /// <returns></returns>
        public List<WorkoutResuls> GetAllResultsByGroup(DateTime StartDate, DateTime EndDate, long GroupID)
        {
            try
            {
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
                List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => (p.WORKOUT_GROUP_ID == GroupID) && (EntityFunctions.TruncateTime(p.WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(p.WORKOUT_DATE) <= EndDate)).ToList();
                foreach (var item in lstWorkoutResult)
                {
                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                    WorkoutResuls objWorkoutResuls = new WorkoutResuls();
                    objWorkoutResuls.WorkoutID = item.WORKOUT_ID;
                    objWorkoutResuls.WorkoutDate = item.WORKOUT_DATE;
                    objWorkoutResuls.WorkoutCoachId = item.WORKOUT_COACH_ID;
                    objWorkoutResuls.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                    objWorkoutResuls.WorkoutGroupID = item.WORKOUT_GROUP_ID;
                    objWorkoutResuls.WorkoutCreateDate = item.WORKOUT_CREATED_DATE;
                    objWorkoutResuls.WorkoutCreateUser = item.WORKOUT_CREATED_USER;
                    var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_ID).ToList();
                    foreach (var item1 in listOfItems)
                    {
                        WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                        objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                        objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                        objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                        objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                        lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                    }
                    objWorkoutResuls.WorkoutResultDetails = lstWorkoutSectionResult;
                    lstWorkoutResults.Add(objWorkoutResuls);
                }
                return lstWorkoutResults.ToList();
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllResultsByGroup", "Problem In GetAllResultsByGroup", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }



        /// <summary>
        /// Get all workouts based on start date , end date and SectionID
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<WorkoutResuls> GetAllResultsBySection(DateTime StartDate, DateTime EndDate, long SectionID)
        {
            try
            {
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
                List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => (EntityFunctions.TruncateTime(p.WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(p.WORKOUT_DATE) <= EndDate)).ToList();
                foreach (var item in lstWorkoutResult)
                {
                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                    WorkoutResuls objWorkoutResuls = new WorkoutResuls();
                    objWorkoutResuls.WorkoutID = item.WORKOUT_ID;
                    objWorkoutResuls.WorkoutDate = item.WORKOUT_DATE;
                    objWorkoutResuls.WorkoutCoachId = item.WORKOUT_COACH_ID;
                    objWorkoutResuls.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                    objWorkoutResuls.WorkoutGroupID = item.WORKOUT_GROUP_ID;
                    objWorkoutResuls.WorkoutCreateDate = item.WORKOUT_CREATED_DATE;
                    objWorkoutResuls.WorkoutCreateUser = item.WORKOUT_CREATED_USER;
                    var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => (p.WORKOUT_RESULT_ID == item.WORKOUT_ID) && (p.WORKOUT_SECTION_ID == SectionID)).ToList();
                    foreach (var item1 in listOfItems)
                    {
                        WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                        objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                        objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                        objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                        objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                        lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                    }
                    objWorkoutResuls.WorkoutResultDetails = lstWorkoutSectionResult;
                    lstWorkoutResults.Add(objWorkoutResuls);
                }
                return lstWorkoutResults.ToList();
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllResultsByGroup", "Problem In GetAllResultsByGroup", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// Get all workouts based on WorkoutID
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<WorkoutResuls> GetResultForWorkout(long WorkoutID)
        {
            try
            {
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
                STAM_MA_WORKOUT_RESULT objWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == WorkoutID).SingleOrDefault();
                if (objWorkoutResult == null)
                {
                    objLog.LogToFile("Error", "WorkOutResult : AddResultForWorkout", "Problem In getting a WorkOutResult", "");
                    objLog = null;
                    return lstWorkoutResults;
                }
                else
                {
                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                    WorkoutResuls objWorkoutResuls = new WorkoutResuls();
                    objWorkoutResuls.WorkoutID = objWorkoutResult.WORKOUT_ID;
                    objWorkoutResuls.WorkoutDate = objWorkoutResult.WORKOUT_DATE;
                    objWorkoutResuls.WorkoutCoachId = objWorkoutResult.WORKOUT_COACH_ID;
                    objWorkoutResuls.WorkoutCompleteTime = objWorkoutResult.WORKOUT_TARGET_TIME;
                    objWorkoutResuls.WorkoutGroupID = objWorkoutResult.WORKOUT_GROUP_ID;
                    objWorkoutResuls.WorkoutCreateDate = objWorkoutResult.WORKOUT_CREATED_DATE;
                    objWorkoutResuls.WorkoutCreateUser = objWorkoutResult.WORKOUT_CREATED_USER;
                    var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == objWorkoutResuls.WorkoutID).ToList();
                    foreach (var item1 in listOfItems)
                    {
                        WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                        objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                        objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                        objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                        objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                        lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                    }
                    objWorkoutResuls.WorkoutResultDetails = lstWorkoutSectionResult;
                    lstWorkoutResults.Add(objWorkoutResuls);
                }
                return lstWorkoutResults.ToList();
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllWorkouts", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }


        /// <summary>
        /// Get all workouts based on start date and end date
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<WorkoutResuls> GetWorkouts(DateTime WorkoutDate, string GroupID, string SectionID)
        {
            try
            {
                int groupId = Convert.ToInt32(GroupID);
                int sectionId = Convert.ToInt32(SectionID);
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
                List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = new List<STAM_MA_WORKOUT_RESULT>();
                lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => (EntityFunctions.TruncateTime(p.WORKOUT_DATE) >= WorkoutDate.Date && p.WORKOUT_GROUP_ID == groupId)).ToList();
                foreach (var item in lstWorkoutResult)
                {
                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                    WorkoutResuls objWorkoutResults = new WorkoutResuls();
                    objWorkoutResults.WorkoutID = item.WORKOUT_ID;
                    objWorkoutResults.WorkoutDate = item.WORKOUT_DATE;
                    objWorkoutResults.WorkoutCoachId = item.WORKOUT_COACH_ID;
                    objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                    objWorkoutResults.WorkoutGroupID = item.WORKOUT_GROUP_ID;
                    objWorkoutResults.WorkoutCreateDate = item.WORKOUT_CREATED_DATE;
                    objWorkoutResults.WorkoutCreateUser = item.WORKOUT_CREATED_USER;

                    if (sectionId == 0)
                    {
                        var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_ID).ToList();
                        foreach (var item1 in listOfItems)
                        {
                            WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                            objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                            objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                            objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                            objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                            lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                        }
                    }
                    else
                    {
                        var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => (p.WORKOUT_RESULT_ID == item.WORKOUT_ID) && (p.WORKOUT_SECTION_ID == sectionId)).ToList();
                        foreach (var item1 in listOfItems)
                        {
                            WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                            objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                            objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                            objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                            objWorkoutSectionResult.WorkoutSectionId = item1.WORKOUT_SECTION_ID;
                            lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                        }
                    }
                    objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                    lstWorkoutResults.Add(objWorkoutResults);
                }
                return lstWorkoutResults.ToList();
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllWorkouts", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        //public List<WorkoutResuls> GetAllWorkoutResults(Request objRequest)
        //{
        //    try
        //    {
        //        int coachIds = Convert.ToInt32(objRequest.coachID);
        //        int groupIds = Convert.ToInt32(objRequest.groupID);
        //        List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
        //        List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = new List<STAM_MA_WORKOUT_RESULT>();
        //        if (objRequest != null)
        //        {
        //            lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => (EntityFunctions.TruncateTime(p.WORKOUT_DATE) == objRequest.selectedDate.Date && p.WORKOUT_COACHADMIN_ID == coachIds && p.WORKOUT_GROUP_ID == groupIds)).ToList();  

        //        }
        //        foreach (var item in lstWorkoutResult)
        //        {
        //            List<WorkoutDetailedResult> lstWorkoutDetailedResult = new List<WorkoutDetailedResult>();
        //            WorkoutResuls objWorkoutResults = new WorkoutResuls();
        //            objWorkoutResults.WorkoutID = item.WORKOUT_ID;
        //            objWorkoutResults.WorkoutDate = item.WORKOUT_DATE;
        //            objWorkoutResults.WorkoutCoachId = item.WORKOUT_COACHADMIN_ID;
        //            objWorkoutResults.WorkoutGroup = item.WORKOUT_GROUP;
        //            objWorkoutResults.WorkoutGroupID = item.WORKOUT_GROUP_ID;
        //            objWorkoutResults.WorkoutSection = item.WORKOUT_SECTION;
        //            objWorkoutResults.WorkoutSectionID = item.WORKOUT_SECTION_ID;
        //            objWorkoutResults.WorkoutCreateDate = item.WORKOUT_CREATED_DATE;
        //            objWorkoutResults.WorkoutCreateUser = item.WORKOUT_CREATED_USER;
        //            objWorkoutResults.WorkoutLastUpdatedDate = item.WORKOUT_LASTUPDATED_DATE;
        //            objWorkoutResults.WorkoutLastUpdatedUser = item.WORKOUT_LASTUPDATED_USER;

        //            var listOfItems = objContext.STAM_TR_WORKOUT_DETAILED_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_ID).ToList();
        //            foreach (var item1 in listOfItems)
        //            {
        //                WorkoutDetailedResult objWorkoutDetailedResult = new WorkoutDetailedResult();
        //                objWorkoutDetailedResult.WorkoutDetailedDistance = item1.WORKOUT_DETAILED_DISTANCE;
        //                objWorkoutDetailedResult.WorkoutDetailedSplitType = item1.WORKOUT_DETAILED_SPLIT_TYPE;
        //                objWorkoutDetailedResult.WorkoutDetailedSplit = item1.WORKOUT_DETAILED_SPLIT;
        //                objWorkoutDetailedResult.WorkoutDetailedTime = item1.WORKOUT_DETAILED_TIME;
        //                objWorkoutDetailedResult.WorkoutResultID = item1.WORKOUT_RESULT_ID;
        //                objWorkoutDetailedResult.WorkoutDetailedId = item1.WORKOUT_DETAILED_ID;
        //                lstWorkoutDetailedResult.Add(objWorkoutDetailedResult);
        //            }
        //            objWorkoutResults.WorkoutResultDetails = lstWorkoutDetailedResult;
        //            lstWorkoutResults.Add(objWorkoutResults);
        //        }
        //        return lstWorkoutResults.ToList();
        //    }
        //    catch (Exception Ex)
        //    {
        //        Logger objLog = new Logger();
        //        objLog.LogToFile("Error", "WorkoutResult : GetAllWorkoutResults", "Problem In GetAllWorkoutResults", Ex.Message.ToString());
        //        objLog = null;
        //        return null;
        //    }
        //}


        public List<WorkoutResuls> GetAllWorkoutResults(DateTime selectedDate, long coachId, long groupId)
        {
            try
            {
                List<WorkoutResuls> lstWorkoutResults = new List<WorkoutResuls>();
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == coachId).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();

                List<STAM_MA_ATHLETE_SECTION> _lstAthleteSection = new List<STAM_MA_ATHLETE_SECTION>();

                if (coachId != 0 && groupId != 0)
                {
                    int coachIds = Convert.ToInt32(coachId);
                    int groupIds = Convert.ToInt32(groupId);
                    // var lstCoach = objContext.STAM_MA_COACH_WORKOUT.Where(w => (EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) == selectedDate.Date && w.STAM_CREATED_USER == coachId && w.STAM_COACH_WORKOUT_GROUPID == groupId)).ToList();
                    long? userids = User.SAM_CREATED_USER;
                    if (User.STAM_USER_TYPE == 3)
                    {
                        var lstCoach = (from e in objContext.STAM_MA_COACH_WORKOUT
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == coachId || s.STAM_USERID == User.SAM_CREATED_USER || e.STAM_CREATED_USER == userids || userids.ToString().Contains(s.SAM_CREATED_USER.ToString()))
                                        && (EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) == selectedDate.Date)
                                        && e.STAM_COACH_WORKOUT_GROUPID == groupId
                                        && e.STAM_COACH_WORKOUT_STATUS != 3
                                        select e).ToList();
                        STAM_MA_ASSTCOACH asstUser1 = objContext.STAM_MA_ASSTCOACH.Where(p => p.STAM_ASSTCOACH_GROUPID.ToString().Contains(groupId.ToString()) && p.STAM_ASSTCOACH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                        if (asstUser1 != null)
                        {
                            foreach (var itemcoach in lstCoach)
                            {
                                WorkoutResuls objWorkoutResults = new WorkoutResuls();
                                objWorkoutResults.WorkoutID = itemcoach.STAM_COACH_WORKOUT_ID;
                                objWorkoutResults.WorkoutTitle = itemcoach.STAM_COACH_WORKOUT_TITLE;
                                objWorkoutResults.WorkoutDistance = itemcoach.STAM_COACH_WORKOUT_DISTANCE;
                                objWorkoutResults.WorkoutRuns = itemcoach.STAM_COACH_WORKOUT_RUNS;
                                objWorkoutResults.WorkoutGroupID = itemcoach.STAM_COACH_WORKOUT_GROUPID;
                                objWorkoutResults.WorkoutGroupName = itemcoach.STAM_COACH_WORKOUT_GROUPNAME;
                                objWorkoutResults.WorkoutDate = itemcoach.STAM_COACH_WORKOUT_DATE;
                                objWorkoutResults.WorkoutCoachId = itemcoach.STAM_CREATED_USER;
                                objWorkoutResults.WorkoutCreateUser = itemcoach.STAM_CREATED_USER;
                                objWorkoutResults.WorkoutCreateDate = itemcoach.STAM_CREATED_DATE;
                                objWorkoutResults.WorkoutNotes = itemcoach.STAM_COACH_WORKOUT_NOTES;
                                objWorkoutResults.WorkoutTargetTime = itemcoach.STAM_COACH_WORKOUT_TARGET_MM + ':' + itemcoach.STAM_COACH_WORKOUT_TARGET_SS;
                                List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == itemcoach.STAM_COACH_WORKOUT_ID).ToList();
                                if (lstWorkoutResult.Count != 0)
                                {


                                    foreach (var item in lstWorkoutResult)
                                    {

                                        List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                        objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                                        // List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && p.STAM_CREATED_USER == coachId && p.STAM_ATHSEC_STATUS == 1).ToList();

                                        var lstSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                                         join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                                         where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == coachId || s.STAM_USERID == User.SAM_CREATED_USER)
                                                         where ATh.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID
                                                         && ATh.STAM_ATHSEC_STATUS == 1
                                                         select ATh;

                                        if (lstSection != null)
                                        {
                                            STAM_MA_ASSTCOACH asstUser = objContext.STAM_MA_ASSTCOACH.Where(p => p.STAM_ASSTCOACH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                                            string[] _ath = null;
                                            _ath = asstUser.STAM_ASSTCOACH_GROUPID.ToString().TrimEnd(',').Split(',');
                                            foreach (var itemsec in _ath)
                                            {
                                                long item1 = Convert.ToInt64(itemsec);
                                                List<STAM_MA_ATHLETE_SECTION> objser = lstSection.Where(p => p.STAM_ATHSEC_GROUPID == item1).ToList();
                                                if (objser.Count != 0)
                                                {
                                                    _lstAthleteSection = objser.ToList();
                                                }
                                            }


                                            foreach (var itemSection in _lstAthleteSection)
                                            {
                                                var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_RESULT_ID && p.WORKOUT_SECTION_ID == itemSection.STAM_ATHSEC_ID).ToList();
                                                if (listOfItems.Count != 0)
                                                {
                                                    foreach (var item1 in listOfItems)
                                                    {
                                                        WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                                        objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                                        objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                                        objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                                                        objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                                                        objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                                                        lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                                    }
                                                }
                                                else
                                                {
                                                    WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                                    objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                                    objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                                    lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                                }
                                            }
                                        }

                                        objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                                    }
                                }
                                else
                                {
                                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                    //List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p =>(p.STAM_CREATED_USER == itemcoach.STAM_CREATED_USER || p.STAM_CREATED_USER == userids) && p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && p.STAM_ATHSEC_STATUS == 1).ToList();
                                    var lstSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                                     join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                                     where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == coachId || s.STAM_USERID == User.SAM_CREATED_USER)
                                                     where ATh.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID
                                                     && ATh.STAM_ATHSEC_STATUS == 1
                                                     select ATh;

                                    if (lstSection != null)
                                    {
                                        STAM_MA_ASSTCOACH asstUser = objContext.STAM_MA_ASSTCOACH.Where(p => p.STAM_ASSTCOACH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                                        string[] _ath = null;
                                        _ath = asstUser.STAM_ASSTCOACH_GROUPID.ToString().TrimEnd(',').Split(',');
                                        foreach (var itemsec in _ath)
                                        {
                                            long item1 = Convert.ToInt64(itemsec);
                                            List<STAM_MA_ATHLETE_SECTION> objser = lstSection.Where(p => p.STAM_ATHSEC_GROUPID == item1).ToList();
                                            if (objser.Count != 0)
                                            {
                                                _lstAthleteSection = objser.ToList();
                                            }
                                        }



                                        foreach (var itemSection in _lstAthleteSection)
                                        {

                                            WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                            objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                            objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                            lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                        }
                                    }

                                    objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                                }
                                lstWorkoutResults.Add(objWorkoutResults);
                            }
                        }

                        return lstWorkoutResults.ToList();

                    }






                    if (User.STAM_USER_TYPE == 2)
                    {
                        var lstCoachs = (from e in objContext.STAM_MA_COACH_WORKOUT
                                         join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                         where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == coachId)
                                         && (EntityFunctions.TruncateTime(e.STAM_COACH_WORKOUT_DATE) == selectedDate.Date)
                                         && e.STAM_COACH_WORKOUT_GROUPID == groupId
                                         && e.STAM_COACH_WORKOUT_STATUS != 3
                                         select e).ToList();


                        foreach (var itemcoach in lstCoachs)
                        {
                            WorkoutResuls objWorkoutResults = new WorkoutResuls();
                            objWorkoutResults.WorkoutID = itemcoach.STAM_COACH_WORKOUT_ID;
                            objWorkoutResults.WorkoutTitle = itemcoach.STAM_COACH_WORKOUT_TITLE;
                            objWorkoutResults.WorkoutDistance = itemcoach.STAM_COACH_WORKOUT_DISTANCE;
                            objWorkoutResults.WorkoutRuns = itemcoach.STAM_COACH_WORKOUT_RUNS;
                            objWorkoutResults.WorkoutGroupID = itemcoach.STAM_COACH_WORKOUT_GROUPID;
                            objWorkoutResults.WorkoutGroupName = itemcoach.STAM_COACH_WORKOUT_GROUPNAME;
                            objWorkoutResults.WorkoutDate = itemcoach.STAM_COACH_WORKOUT_DATE;
                            objWorkoutResults.WorkoutCoachId = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateUser = itemcoach.STAM_CREATED_USER;
                            objWorkoutResults.WorkoutCreateDate = itemcoach.STAM_CREATED_DATE;
                            objWorkoutResults.WorkoutNotes = itemcoach.STAM_COACH_WORKOUT_NOTES;
                            objWorkoutResults.WorkoutTargetTime = itemcoach.STAM_COACH_WORKOUT_TARGET_MM + ':' + itemcoach.STAM_COACH_WORKOUT_TARGET_SS;
                            List<STAM_MA_WORKOUT_RESULT> lstWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == itemcoach.STAM_COACH_WORKOUT_ID).ToList();
                            if (lstWorkoutResult.Count != 0)
                            {
                                foreach (var item in lstWorkoutResult)
                                {
                                    List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                    objWorkoutResults.WorkoutCompleteTime = item.WORKOUT_TARGET_TIME;
                                    //  List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && p.STAM_CREATED_USER == coachId && p.STAM_ATHSEC_STATUS == 1).ToList();
                                    var lstSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                                     join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                                     where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == coachId)
                                                     where ATh.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID
                                                     && ATh.STAM_ATHSEC_STATUS == 1
                                                     select ATh;







                                    foreach (var itemSection in lstSection)
                                    {
                                        var listOfItems = objContext.STAM_TR_WORKOUT_SECTION_RESULT.Where(p => p.WORKOUT_RESULT_ID == item.WORKOUT_RESULT_ID && p.WORKOUT_SECTION_ID == itemSection.STAM_ATHSEC_ID).ToList();
                                        if (listOfItems.Count != 0)
                                        {
                                            foreach (var item1 in listOfItems)
                                            {
                                                WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                                objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                                objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                                objWorkoutSectionResult.WorkoutSectionTime = item1.WORKOUT_SECTION_TIME;
                                                objWorkoutSectionResult.WorkoutSectionSplit = item1.WORKOUT_DETAILED_SPLIT;
                                                objWorkoutSectionResult.WorkoutSectionRun = item1.WORKOUT_SECTION_RUN;
                                                lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                            }
                                        }
                                        else
                                        {
                                            WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                            objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                            objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                            lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                        }
                                    }
                                    objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                                }
                            }
                            else
                            {
                                List<WorkoutSectionResult> lstWorkoutSectionResult = new List<WorkoutSectionResult>();
                                //List<STAM_MA_ATHLETE_SECTION> lstSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID && p.STAM_CREATED_USER == coachId && p.STAM_ATHSEC_STATUS == 1).ToList();

                                //var lstSection = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                //                 join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                //                 where (s.SAM_CREATED_USER == User.SAM_CREATED_USER || s.STAM_USERID == coachId || s.STAM_USERID == User.SAM_CREATED_USER)
                                //                 where ATh.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID
                                //                 && ATh.STAM_ATHSEC_STATUS == 1
                                //                 select ATh;
                                var lstAthleteSection1 = from ATh in objContext.STAM_MA_ATHLETE_SECTION
                                                         join s in objContext.STAM_MA_USERS on ATh.STAM_CREATED_USER equals s.STAM_USERID
                                                         where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == coachId)
                                                         where ATh.STAM_ATHSEC_GROUPID == itemcoach.STAM_COACH_WORKOUT_GROUPID
                                                         && ATh.STAM_ATHSEC_STATUS == 1
                                                         select ATh;
                                //if (lstSection != null)
                                //{
                                STAM_MA_ASSTCOACH asstUser = objContext.STAM_MA_ASSTCOACH.Where(p => p.STAM_ASSTCOACH_EMAILADD == User.STAM_USER_USERNAME).SingleOrDefault();
                                //string[] _ath = null;
                                //_ath = asstUser.STAM_ASSTCOACH_GROUPID.ToString().TrimEnd(',').Split(',');
                                //foreach (var itemsec in _ath)
                                //{
                                //    long item1 = Convert.ToInt64(itemsec);
                                //    List<STAM_MA_ATHLETE_SECTION> objser = lstSection.Where(p => p.STAM_ATHSEC_GROUPID == item1).ToList();
                                //    if (objser.Count != 0)
                                //    {
                                //        _lstAthleteSection = objser.ToList();
                                //    }
                                //}
                                foreach (var itemSection in lstAthleteSection1)
                                {
                                    WorkoutSectionResult objWorkoutSectionResult = new WorkoutSectionResult();
                                    objWorkoutSectionResult.WorkoutSectionId = itemSection.STAM_ATHSEC_ID;
                                    objWorkoutSectionResult.WorkoutSectionName = itemSection.STAM_ATHSEC_NAME;
                                    lstWorkoutSectionResult.Add(objWorkoutSectionResult);
                                }
                                // }
                                objWorkoutResults.WorkoutResultDetails = lstWorkoutSectionResult;
                            }
                            lstWorkoutResults.Add(objWorkoutResults);
                        }
                        return lstWorkoutResults.ToList();
                    }



                }



                return null;


            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllWorkoutResults", "Problem In GetAllWorkoutResults", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        public bool CheckWorkout(WorkoutResuls objWorkoutResult)
        {
            try
            {
                STAM_MA_WORKOUT_RESULT itemOfWorkoutResult = objContext.STAM_MA_WORKOUT_RESULT.Where(p => p.WORKOUT_ID == objWorkoutResult.WorkoutID).FirstOrDefault();
                if (itemOfWorkoutResult == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : CheckWorkout", "Problem In CheckWorkout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        public List<WorkoutServiceModel> GetAllNewWorkouts1(DateTime selectedDate, long Userid, long groupId)
        {

            List<STAM_TR_WORKOUTS_Model> _objRes1 = new List<STAM_TR_WORKOUTS_Model>();



            List<STAM_TR_DETAILS_MODEL> _objRes_det = new List<STAM_TR_DETAILS_MODEL>();




            STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();

            List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();

            long? userids = User.SAM_CREATED_USER;
            List<WorkoutServiceModel> _objRes2 = new List<WorkoutServiceModel>();

            if (User.STAM_USER_TYPE == 3)
            {


                var coachwrkout = (from e in objContext.STAM_TR_WORKOUTS

                                   join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID



                                   where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                               && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) == selectedDate.Date)
                                && e.STAM_WORKOUT_STATUS != 3
                                   select e).ToList();



                // List<WorkoutModel> _objRes2 = new List<WorkoutModel>();

                WorkoutServiceModel _objRes = new WorkoutServiceModel();

                foreach (var itemwrkid in coachwrkout)
                {
                    _objRes.STAM_WORKOUT_DATE = itemwrkid.STAM_WORKOUT_DATE;
                    _objRes.STAM_WORKOUT_TITLE = itemwrkid.STAM_WORKOUT_TITLE;
                    _objRes.STAM_WORKOUT_RUNS = itemwrkid.STAM_WORKOUT_RUNS;


                    _objRes.STAM_WORKOUT_NOTES = itemwrkid.STAM_WORKOUT_NOTES;
                    _objRes.STAM_WORKOUT_GROUP = itemwrkid.STAM_WORKOUT_GROUP;
                    _objRes.STAM_WORKOUT_DISTANCE = itemwrkid.STAM_WORKOUT_DISTANCE;
                    _objRes.STAM_WORKOUT_TYPE = itemwrkid.STAM_WORKOUT_TYPE;
                    _objRes.STAM_WORKOUT_GROUPID = itemwrkid.STAM_WORKOUT_GROUPID;





                    //var itemdetail = from wrkoutsec in objContext.STAM_TR_DETAILS where wrkoutsec.STAM_WORKOUT_ID == itemwrkid.STAM_WORKOUT_ID select wrkoutsec;
                    var item2 = (from work in objContext.STAM_TR_DETAILS
                                 where work.STAM_WORKOUT_ID == itemwrkid.STAM_WORKOUT_ID
                                 //select work).ToList();
                                 select new STAM_TR_DETAILS_MODEL()
                                 {
                                     STAM_WORKOUT_ID = work.STAM_WORKOUT_ID,
                                     STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
                                     STAM_RUN = work.STAM_RUN,
                                     STAM_SEC_TARGET_TIME = work.STAM_SEC_TARGET_TIME == null ? "" : work.STAM_SEC_TARGET_TIME,
                                     STAM_SEC_ID = work.STAM_SEC_ID == null ? 0 : work.STAM_SEC_ID,
                                     // _objSTAM_TR_DETAILS_SECTION = work.STAM_TR_DETAILS_SECTION.ToList()
                                 }).ToList();
                    if (item2 != null)
                    {
                        foreach (var item in item2)
                        {
                            var itemdetailsec = (from work in objContext.STAM_TR_DETAILS_SECTION
                                                 where work.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID
                                                 select new STAM_TR_DETAILS_SECTION_MODEL()
                                                 {

                                                     STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
                                                     STAM_LAP = work.STAM_LAP


                                                 }).ToList();
                            item._objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();



                            item._objSTAM_TR_DETAILS_SECTION.AddRange(itemdetailsec);

                        }


                    }


                    //   _objRes_det.AddRange(item2);



                    _objRes_det.AddRange(item2);
                    _objRes._objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();

                    _objRes._objSTAM_TR_DETAILS.AddRange(_objRes_det);


                    _objRes2.Add(_objRes);


                }



            }
            if (User.STAM_USER_TYPE == 2)
            {


                var obj_Coach = from e in objContext.STAM_TR_WORKOUTS
                                join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                where (s.SAM_CREATED_USER == Userid || s.STAM_USERID == Userid || e.STAM_CREATED_USER == Userid)
                                 && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) == EntityFunctions.TruncateTime(selectedDate.Date))
                                      && e.STAM_WORKOUT_GROUPID == groupId
                                 && e.STAM_WORKOUT_STATUS != 3
                                select e;




                WorkoutServiceModel _objRes = new WorkoutServiceModel();

                foreach (var itemwrkid in obj_Coach)
                {
                    _objRes.STAM_WORKOUT_DATE = itemwrkid.STAM_WORKOUT_DATE;
                    _objRes.STAM_WORKOUT_TITLE = itemwrkid.STAM_WORKOUT_TITLE;
                    _objRes.STAM_WORKOUT_RUNS = itemwrkid.STAM_WORKOUT_RUNS;


                    _objRes.STAM_WORKOUT_NOTES = itemwrkid.STAM_WORKOUT_NOTES;
                    _objRes.STAM_WORKOUT_GROUP = itemwrkid.STAM_WORKOUT_GROUP;
                    _objRes.STAM_WORKOUT_DISTANCE = itemwrkid.STAM_WORKOUT_DISTANCE;
                    _objRes.STAM_WORKOUT_TYPE = itemwrkid.STAM_WORKOUT_TYPE;
                    _objRes.STAM_WORKOUT_GROUPID = itemwrkid.STAM_WORKOUT_GROUPID;





                    //var itemdetail = from wrkoutsec in objContext.STAM_TR_DETAILS where wrkoutsec.STAM_WORKOUT_ID == itemwrkid.STAM_WORKOUT_ID select wrkoutsec;
                    var item2 = (from work in objContext.STAM_TR_DETAILS
                                 where work.STAM_WORKOUT_ID == itemwrkid.STAM_WORKOUT_ID
                                 //select work).ToList();
                                 select new STAM_TR_DETAILS_MODEL()
                                 {
                                     STAM_WORKOUT_ID = work.STAM_WORKOUT_ID,
                                     STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
                                     STAM_RUN = work.STAM_RUN,
                                     STAM_SEC_TARGET_TIME = work.STAM_SEC_TARGET_TIME == null ? "" : work.STAM_SEC_TARGET_TIME,
                                     STAM_SEC_ID = work.STAM_SEC_ID == null ? 0 : work.STAM_SEC_ID,
                                     // _objSTAM_TR_DETAILS_SECTION = work.STAM_TR_DETAILS_SECTION.ToList()
                                 }).ToList();
                    if (item2 != null)
                    {
                        foreach (var item in item2)
                        {
                            var itemdetailsec = (from work in objContext.STAM_TR_DETAILS_SECTION
                                                 where work.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID
                                                 select new STAM_TR_DETAILS_SECTION_MODEL()
                                                 {

                                                     STAM_WORKOUT_DETAILS_ID = work.STAM_WORKOUT_DETAILS_ID,
                                                     STAM_LAP = work.STAM_LAP


                                                 }).ToList();
                            item._objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();



                            item._objSTAM_TR_DETAILS_SECTION.AddRange(itemdetailsec);

                        }


                    }


                    //   _objRes_det.AddRange(item2);



                    _objRes_det.AddRange(item2);
                    _objRes._objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();

                    _objRes._objSTAM_TR_DETAILS.AddRange(_objRes_det);


                    _objRes2.Add(_objRes);


                }





            }




            return _objRes2;
        }





        public bool CheckNewWorkout(STAM_TR_WORKOUTS_Model objWorkoutResult)
        {

            try
            {
                STAM_TR_WORKOUTS itemOfWorkoutResult = objContext.STAM_TR_WORKOUTS.Where(p => p.STAM_WORKOUT_ID == objWorkoutResult.STAM_WORKOUT_ID).FirstOrDefault();
                if (itemOfWorkoutResult == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : CheckWorkout", "Problem In CheckWorkout", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }






        public bool AddNewWorkoutResult(STAM_TR_WORKOUTS_Model objWorkoutResult, List<STAM_TR_DETAILS_Model> lstWorkoutDetailedResult)//, List<STAM_TR_DETAILS_SECTION> lstWorkoutSecDetailedResult)
        {


            try
            {
                STAM_TR_WORKOUTS objcoach = objContext.STAM_TR_WORKOUTS.Where(p => p.STAM_WORKOUT_ID == objWorkoutResult.STAM_WORKOUT_ID).SingleOrDefault();

                if (objcoach == null)
                {
                    STAM_TR_WORKOUTS itemOfWorkoutResult = objContext.STAM_TR_WORKOUTS.Where(p => p.STAM_WORKOUT_ID == objWorkoutResult.STAM_WORKOUT_ID).FirstOrDefault();
                    if (itemOfWorkoutResult == null)
                    {

                        STAM_TR_WORKOUTS objCoachWorkout = new STAM_TR_WORKOUTS()
                            {

                                STAM_WORKOUT_RUNS = objWorkoutResult.STAM_WORKOUT_RUNS,
                                STAM_WORKOUT_DATE = objWorkoutResult.STAM_WORKOUT_DATE,
                                STAM_WORKOUT_STATUS = objWorkoutResult.STAM_WORKOUT_STATUS,
                                STAM_WORKOUT_TITLE = objWorkoutResult.STAM_WORKOUT_TITLE,
                                STAM_WORKOUT_DISTANCE = objWorkoutResult.STAM_WORKOUT_DISTANCE,
                                STAM_WORKOUT_NOTES = objWorkoutResult.STAM_WORKOUT_NOTES,
                                STAM_WORKOUT_GROUPID = objWorkoutResult.STAM_WORKOUT_GROUPID,
                                STAM_WORKOUT_TYPE = objWorkoutResult.STAM_WORKOUT_TYPE,
                                STAM_CREATED_USER = objWorkoutResult.STAM_CREATED_USER,
                                STAM_CREATED_DATE = DateTime.Now,
                                STAM_LAST_UPDATED_USER = objWorkoutResult.STAM_LAST_UPDATED_USER,
                                STAM_LAST_UPDATED_DATE = DateTime.Now,
                                STAM_WORKOUT_GROUP = objWorkoutResult.STAM_WORKOUT_GROUP,

                            };

                        //  objContext.STAM_MA_WORKOUT_RESULT.Add(AddWorkoutResult);
                        objContext.STAM_TR_WORKOUTS.Add(objCoachWorkout);
                        objContext.SaveChanges();
                        long iWorkoutId = Convert.ToInt32(objCoachWorkout.STAM_WORKOUT_ID);

                        bool result = AddNewWorkoutSectionResult(lstWorkoutDetailedResult, iWorkoutId);// lstWorkoutSecDetailedResult);
                        return result;
                    }
                    else
                    {
                        var objUpdateCoach = objContext.STAM_TR_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == objWorkoutResult.STAM_WORKOUT_ID).SingleOrDefault();
                        {
                            objUpdateCoach.STAM_WORKOUT_RUNS = objWorkoutResult.STAM_WORKOUT_RUNS;
                            objUpdateCoach.STAM_WORKOUT_DATE = objWorkoutResult.STAM_WORKOUT_DATE;
                            objUpdateCoach.STAM_WORKOUT_STATUS = objWorkoutResult.STAM_WORKOUT_STATUS;
                            objUpdateCoach.STAM_WORKOUT_TITLE = objWorkoutResult.STAM_WORKOUT_TITLE;
                            objUpdateCoach.STAM_WORKOUT_DISTANCE = objWorkoutResult.STAM_WORKOUT_DISTANCE;
                            objUpdateCoach.STAM_WORKOUT_NOTES = objWorkoutResult.STAM_WORKOUT_NOTES;
                            objUpdateCoach.STAM_WORKOUT_GROUPID = objWorkoutResult.STAM_WORKOUT_GROUPID;
                            objUpdateCoach.STAM_WORKOUT_TYPE = objWorkoutResult.STAM_WORKOUT_TYPE;
                            objUpdateCoach.STAM_CREATED_USER = objWorkoutResult.STAM_CREATED_USER;
                            objUpdateCoach.STAM_CREATED_DATE = objWorkoutResult.STAM_CREATED_DATE;
                            objUpdateCoach.STAM_LAST_UPDATED_USER = objWorkoutResult.STAM_LAST_UPDATED_USER;
                            objUpdateCoach.STAM_LAST_UPDATED_DATE = objWorkoutResult.STAM_LAST_UPDATED_DATE;
                            objUpdateCoach.STAM_WORKOUT_GROUP = objWorkoutResult.STAM_WORKOUT_GROUP;
                        };
                        objContext.Entry(objUpdateCoach).State = EntityState.Modified;
                        objContext.SaveChanges();

                        long iWorkoutId = Convert.ToInt32(objUpdateCoach.STAM_WORKOUT_ID);



                        var del = objContext.STAM_TR_DETAILS.Where(y => y.STAM_WORKOUT_ID == objUpdateCoach.STAM_WORKOUT_ID).ToList();

                        if (del != null)
                        {
                            foreach (var item in del)
                            {

                                var delete = objContext.STAM_TR_DETAILS_SECTION.Where(y => y.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();

                                delete.ForEach(cs => objContext.STAM_TR_DETAILS_SECTION.Remove(cs));


                            }



                            del.ForEach(cs => objContext.STAM_TR_DETAILS.Remove(cs));

                            objContext.SaveChanges();
                        }
                        bool result = AddNewWorkoutSectionResult(lstWorkoutDetailedResult, iWorkoutId);//, lstWorkoutSecDetailedResult);

                        return result;
                    }
                }
                return false;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : AddWorkoutResult", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }


        /// <summary>
        ///   Create/Save a workout result  detailed 
        /// <param name="lstWorkoutDetailedResult"></param>
        /// <param name="iWorkoutId">Refernce for workout result id</param>
        /// <returns></returns>
        public bool AddNewWorkoutSectionResult(List<STAM_TR_DETAILS_Model> lstWorkoutDetailedResult, long iWorkoutId)//, List<STAM_TR_DETAILS_SECTION> lstWorkoutsecDetailed)
        {



            try
            {
                int i = 0;
                foreach (var item in lstWorkoutDetailedResult)
                {
                    STAM_TR_DETAILS objCoachWorkout = new STAM_TR_DETAILS()
                    {

                        STAM_WORKOUT_ID = iWorkoutId,
                        STAM_RUN = item.STAM_RUN,
                        STAM_CREATED_DATE = item.STAM_CREATED_DATE,

                        STAM_SEC_ID = item.STAM_SEC_ID,
                        STAM_SEC_TARGET_TIME = item.STAM_SEC_TARGET_TIME

                    };


                    objContext.STAM_TR_DETAILS.Add(objCoachWorkout);
                    objContext.SaveChanges();
                    if (item.STAM_TR_DETAILS_SECTION != null)
                    {
                        List<STAM_TR_DETAILS_SECTION> lstWorkoutsecDetailed = item.STAM_TR_DETAILS_SECTION.ToList();
                        foreach (var itemsec in lstWorkoutsecDetailed)
                        {
                            STAM_TR_DETAILS_SECTION objSectionCoachWorkout = new STAM_TR_DETAILS_SECTION()
                            {

                                STAM_WORKOUT_DETAILS_ID = objCoachWorkout.STAM_WORKOUT_DETAILS_ID,
                                STAM_LAP = itemsec.STAM_LAP

                            };
                            objContext.STAM_TR_DETAILS_SECTION.Add(objSectionCoachWorkout);
                            objContext.SaveChanges();
                        }

                    }




                    i++;
                }
                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : AddWorkoutResult", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public bool CheckForWorkout(NewWorkout objNewWorkout)
        {
            try
            {
                STAM_TR_WORKOUTS objWorkouts = objContext.STAM_TR_WORKOUTS.Where(p => p.STAM_WORKOUT_ID == objNewWorkout.workoutId).FirstOrDefault();
                if (objWorkouts.STAM_WORKOUT_ID != 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : CheckForWorkout", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
                return false;
            }
        }

        public int CreateWorkoutResult(NewWorkout objNewWorkout)
        {
            try
            {
                int Flag = 0;
                STAM_TR_WORKOUTS objWorkouts = objContext.STAM_TR_WORKOUTS.Where(p => p.STAM_WORKOUT_ID == objNewWorkout.workoutId).FirstOrDefault();
                if (objWorkouts.STAM_WORKOUT_ID != 0)
                {
                    List<Details> lstDetails = new List<Details>();
                    lstDetails = objNewWorkout.ListDetails;
                    foreach (var item in lstDetails)
                    {
                        List<SectionResults> lstSectionResults = new List<SectionResults>();
                        lstSectionResults = item.ListSectionResults;
                        STAM_TR_DETAILS objdetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == objWorkouts.STAM_WORKOUT_ID && p.STAM_WORKOUT_DETAILS_ID == item.DetailsID).SingleOrDefault();
                        if (objdetails.STAM_WORKOUT_DETAILS_ID != 0)
                        {
                            if (lstSectionResults.Count == 0)
                            {
                                Flag = 1;
                                foreach (var itemSection in lstSectionResults)
                                {
                                    STAM_TR_DETAILS_SECTION objDetailsSection = new STAM_TR_DETAILS_SECTION();
                                    objDetailsSection.STAM_WORKOUT_DETAILS_ID = objdetails.STAM_WORKOUT_DETAILS_ID;
                                    objDetailsSection.STAM_LAP = itemSection.Lap;
                                    objDetailsSection.STAM_LAPTIME = itemSection.LapTime;
                                    objContext.STAM_TR_DETAILS_SECTION.Add(objDetailsSection);
                                    objContext.SaveChanges();
                                }
                            }
                            else
                            {
                                Flag = 2;
                                foreach (var itemSection in lstSectionResults)
                                {
                                    STAM_TR_DETAILS_SECTION objDetailsSection = new STAM_TR_DETAILS_SECTION();
                                    objDetailsSection.STAM_WORKOUT_DETAILS_ID = objdetails.STAM_WORKOUT_DETAILS_ID;
                                    objDetailsSection.STAM_LAP = itemSection.Lap;
                                    objDetailsSection.STAM_LAPTIME = itemSection.LapTime;
                                    objContext.STAM_TR_DETAILS_SECTION.Add(objDetailsSection);
                                    objContext.SaveChanges();
                                }

                            }
                        }

                    }
                    return Flag;
                }
                return 0;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : CreateWorkoutResult", "Problem In Creating a new WorkoutResult", Ex.Message.ToString());
                objLog = null;
                return 0;
            }
        }



        public List<NewWorkout> GetAllNewWorkoutResults(DateTime StartDate, DateTime EndDate, long Userid)
        {
            try
            {
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                List<Details> lstDetails = new List<Details>();
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();
                long? userids = User.SAM_CREATED_USER;
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                if (User.STAM_USER_TYPE == 3)
                {
                    var coachwrkout = (from e in objContext.STAM_TR_WORKOUTS
                                       join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                       where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                    && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate)
                                    && e.STAM_WORKOUT_STATUS != 3
                                       select e).ToList();

                    if (coachwrkout != null)
                    {
                        foreach (var itemcoach in coachwrkout)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection.STAM_ATHSEC_ID != 0)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }
                        return lstNewWorkout;
                    }

                }
                if (User.STAM_USER_TYPE == 2)
                {
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid) && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) <= EndDate)
                                        && e.STAM_WORKOUT_STATUS != 3
                                        select e).ToList();


                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection.STAM_ATHSEC_ID != 0)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }
                        return lstNewWorkout;
                    }
                    else
                    {
                        return null;
                    }
                }

                return null;

                //var coachwrkout = objContext.STAM_MA_COACH_WORKOUT.Where(w => (EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) <= EndDate && (w.STAM_CREATED_USER == Userid || w.STAM_CREATED_USER == User.SAM_CREATED_USER))).ToList();

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetAllWorkouts", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        public List<NewWorkout> GetWorkouts(DateTime StartDate, DateTime EndDate, string GroupId, string SectionID, string AtheleteID, string WorkoutTitle)
        {
            try
            {
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                List<Details> lstDetails = new List<Details>();
                int iGroupId = 0, iSectionID = 0, iAtheleteID = 0, iworkout = 0;
                if (GroupId != null)
                {
                    iGroupId = Convert.ToInt32(GroupId);
                }
                if (GroupId != null)
                {
                    iSectionID = Convert.ToInt32(SectionID);
                }
                if (GroupId != null)
                {
                    iAtheleteID = Convert.ToInt32(AtheleteID);
                }
                if (WorkoutTitle != null)
                {
                    iworkout = Convert.ToInt32(WorkoutTitle);
                }
                if (iGroupId != 0 && iSectionID != 0 && iAtheleteID != 0)
                {
                    if (WorkoutTitle != null)
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            join title in objContext.STAM_MA_WORKOUTS on work.STAM_WORKOUT_TITLE equals title.STAM_WORKOUT_TITLE
                                            where (work.STAM_WORKOUT_GROUPID == iGroupId && title.STAM_WORKOUT_ID == iworkout)
                                           && (EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date)
                                            select work).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                List<STAM_TR_DETAILS> CheckDetails = objContext.STAM_TR_DETAILS.Where(p => (p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID) && (p.STAM_SEC_ID == iSectionID)).ToList();
                                if (CheckDetails.Count != 0)
                                {
                                    NewWorkout objNewWorkout = new NewWorkout();
                                    objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                    objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                    objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                    objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                    objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                    STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                    if (objGroup.STAM_GROUP_ID != 0)
                                    {
                                        objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                    }
                                    objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                    objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                    objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                    objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                    List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                                    if (lDetails.Count != 0)
                                    {
                                        foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                        {
                                            List<SectionResults> SectionResults = new List<SectionResults>();
                                            Details oDetails = new Details();
                                            oDetails.SectionID = item.STAM_SEC_ID;
                                            STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                            if (objSection.STAM_ATHSEC_ID != 0)
                                            {
                                                oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                            }
                                            oDetails.Run = item.STAM_RUN;
                                            oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                            oDetails.workoutId = item.STAM_WORKOUT_ID;
                                            oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                            List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                            foreach (var itemSection in lstDetialsSection)
                                            {
                                                SectionResults oSectionResults = new SectionResults();
                                                oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                                oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                                oSectionResults.Lap = itemSection.STAM_LAP;
                                                oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                                SectionResults.Add(oSectionResults);
                                                oDetails.ListSectionResults = SectionResults;
                                            }
                                            lstDetails.Add(oDetails);
                                        }
                                        objNewWorkout.ListDetails = lstDetails;
                                        lstNewWorkout.Add(objNewWorkout);
                                    }
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    else
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            where EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date
                                            select work).ToList();
                        coachwrkouts = coachwrkouts.Where(p => p.STAM_WORKOUT_GROUPID == iGroupId).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                List<STAM_TR_DETAILS> CheckDetails = objContext.STAM_TR_DETAILS.Where(p => (p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID) && (p.STAM_SEC_ID == iSectionID)).ToList();
                                if (CheckDetails.Count != 0)
                                {
                                    NewWorkout objNewWorkout = new NewWorkout();
                                    objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                    objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                    objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                    objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                    objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                    STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                    if (objGroup.STAM_GROUP_ID != 0)
                                    {
                                        objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                    }
                                    objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                    objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                    objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                    objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                    List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID && (p.STAM_SEC_ID == iSectionID)).ToList();
                                    if (lDetails.Count != 0)
                                    {
                                        foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                        {
                                            List<SectionResults> SectionResults = new List<SectionResults>();
                                            Details oDetails = new Details();
                                            oDetails.SectionID = item.STAM_SEC_ID;
                                            STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                            if (objSection.STAM_ATHSEC_ID != 0)
                                            {
                                                oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                            }
                                            oDetails.Run = item.STAM_RUN;
                                            oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                            oDetails.workoutId = item.STAM_WORKOUT_ID;
                                            oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                            List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                            foreach (var itemSection in lstDetialsSection)
                                            {
                                                SectionResults oSectionResults = new SectionResults();
                                                oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                                oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                                oSectionResults.Lap = itemSection.STAM_LAP;
                                                oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                                SectionResults.Add(oSectionResults);
                                                oDetails.ListSectionResults = SectionResults;
                                            }
                                            lstDetails.Add(oDetails);
                                        }
                                        objNewWorkout.ListDetails = lstDetails;
                                        lstNewWorkout.Add(objNewWorkout);
                                    }
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    return null;
                }
                if (iGroupId != 0 && iSectionID != 0 && iAtheleteID == 0)
                {
                    if (WorkoutTitle != null)
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            join title in objContext.STAM_MA_WORKOUTS on work.STAM_WORKOUT_TITLE equals title.STAM_WORKOUT_TITLE
                                            where (work.STAM_WORKOUT_GROUPID == iGroupId && title.STAM_WORKOUT_ID == iworkout)
                                            && (EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date)
                                            select work).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                List<STAM_TR_DETAILS> CheckDetails = objContext.STAM_TR_DETAILS.Where(p => (p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID) && (p.STAM_SEC_ID == iSectionID)).ToList();
                                if (CheckDetails.Count != 0)
                                {
                                    NewWorkout objNewWorkout = new NewWorkout();
                                    objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                    objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                    objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                    objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                    objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                    STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                    if (objGroup.STAM_GROUP_ID != 0)
                                    {
                                        objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                    }
                                    objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                    objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                    objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                    objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                    List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID && (p.STAM_SEC_ID == iSectionID)).ToList();
                                    if (lDetails.Count != 0)
                                    {
                                        foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                        {
                                            List<SectionResults> SectionResults = new List<SectionResults>();
                                            Details oDetails = new Details();
                                            oDetails.SectionID = item.STAM_SEC_ID;
                                            STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                            if (objSection.STAM_ATHSEC_ID != 0)
                                            {
                                                oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                            }
                                            oDetails.Run = item.STAM_RUN;
                                            oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                            oDetails.workoutId = item.STAM_WORKOUT_ID;
                                            oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                            List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                            foreach (var itemSection in lstDetialsSection)
                                            {
                                                SectionResults oSectionResults = new SectionResults();
                                                oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                                oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                                oSectionResults.Lap = itemSection.STAM_LAP;
                                                oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                                SectionResults.Add(oSectionResults);
                                                oDetails.ListSectionResults = SectionResults;
                                            }
                                            lstDetails.Add(oDetails);
                                        }
                                        objNewWorkout.ListDetails = lstDetails;
                                        lstNewWorkout.Add(objNewWorkout);
                                    }
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    else
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            where EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date
                                            select work).ToList();
                        coachwrkouts = coachwrkouts.Where(p => p.STAM_WORKOUT_GROUPID == iGroupId).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                List<STAM_TR_DETAILS> CheckDetails = objContext.STAM_TR_DETAILS.Where(p => (p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID) && (p.STAM_SEC_ID == iSectionID)).ToList();
                                if (CheckDetails.Count != 0)
                                {
                                    NewWorkout objNewWorkout = new NewWorkout();
                                    objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                    objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                    objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                    objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                    objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                    STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                    if (objGroup.STAM_GROUP_ID != 0)
                                    {
                                        objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                    }
                                    objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                    objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                    objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                    objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                    List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID && (p.STAM_SEC_ID == iSectionID)).ToList();
                                    if (lDetails.Count != 0)
                                    {
                                        foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                        {
                                            List<SectionResults> SectionResults = new List<SectionResults>();
                                            Details oDetails = new Details();
                                            oDetails.SectionID = item.STAM_SEC_ID;
                                            STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                            if (objSection.STAM_ATHSEC_ID != 0)
                                            {
                                                oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                            }
                                            oDetails.Run = item.STAM_RUN;
                                            oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                            oDetails.workoutId = item.STAM_WORKOUT_ID;
                                            oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                            List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                            foreach (var itemSection in lstDetialsSection)
                                            {
                                                SectionResults oSectionResults = new SectionResults();
                                                oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                                oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                                oSectionResults.Lap = itemSection.STAM_LAP;
                                                oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                                SectionResults.Add(oSectionResults);
                                                oDetails.ListSectionResults = SectionResults;
                                            }
                                            lstDetails.Add(oDetails);
                                        }
                                        objNewWorkout.ListDetails = lstDetails;
                                        lstNewWorkout.Add(objNewWorkout);
                                    }
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    return null;
                }
                if (iGroupId != 0 && iSectionID == 0 && iAtheleteID == 0)
                {
                    if (WorkoutTitle != null)
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            join title in objContext.STAM_MA_WORKOUTS on work.STAM_WORKOUT_TITLE equals title.STAM_WORKOUT_TITLE
                                            where (work.STAM_WORKOUT_GROUPID == iGroupId && title.STAM_WORKOUT_ID == iworkout)
                                            && (EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date)
                                            select work).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                NewWorkout objNewWorkout = new NewWorkout();
                                objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                if (objGroup.STAM_GROUP_ID != 0)
                                {
                                    objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                }
                                objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                                if (lDetails.Count != 0)
                                {
                                    foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                    {
                                        List<SectionResults> SectionResults = new List<SectionResults>();
                                        Details oDetails = new Details();
                                        oDetails.SectionID = item.STAM_SEC_ID;
                                        STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                        if (objSection.STAM_ATHSEC_ID != 0)
                                        {
                                            oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                        }
                                        oDetails.Run = item.STAM_RUN;
                                        oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                        oDetails.workoutId = item.STAM_WORKOUT_ID;
                                        oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                        List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                        foreach (var itemSection in lstDetialsSection)
                                        {
                                            SectionResults oSectionResults = new SectionResults();
                                            oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                            oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                            oSectionResults.Lap = itemSection.STAM_LAP;
                                            oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                            SectionResults.Add(oSectionResults);
                                            oDetails.ListSectionResults = SectionResults;
                                        }
                                        lstDetails.Add(oDetails);
                                    }
                                    objNewWorkout.ListDetails = lstDetails;
                                    lstNewWorkout.Add(objNewWorkout);
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    else
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            where EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date
                                            select work).ToList();
                        coachwrkouts = coachwrkouts.Where(p => p.STAM_WORKOUT_GROUPID == iGroupId).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                NewWorkout objNewWorkout = new NewWorkout();
                                objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                if (objGroup.STAM_GROUP_ID != 0)
                                {
                                    objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                }
                                objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                                if (lDetails.Count != 0)
                                {
                                    foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                    {
                                        List<SectionResults> SectionResults = new List<SectionResults>();
                                        Details oDetails = new Details();
                                        oDetails.SectionID = item.STAM_SEC_ID;
                                        STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                        if (objSection.STAM_ATHSEC_ID != 0)
                                        {
                                            oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                        }
                                        oDetails.Run = item.STAM_RUN;
                                        oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                        oDetails.workoutId = item.STAM_WORKOUT_ID;
                                        oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                        List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                        foreach (var itemSection in lstDetialsSection)
                                        {
                                            SectionResults oSectionResults = new SectionResults();
                                            oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                            oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                            oSectionResults.Lap = itemSection.STAM_LAP;
                                            oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                            SectionResults.Add(oSectionResults);
                                            oDetails.ListSectionResults = SectionResults;
                                        }
                                        lstDetails.Add(oDetails);
                                    }
                                    objNewWorkout.ListDetails = lstDetails;
                                    lstNewWorkout.Add(objNewWorkout);
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    return null;
                }

                if (iGroupId == 0 && iSectionID == 0 && iAtheleteID == 0)
                {
                    if (WorkoutTitle != null)
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            join title in objContext.STAM_MA_WORKOUTS on work.STAM_WORKOUT_TITLE equals title.STAM_WORKOUT_TITLE
                                            where (title.STAM_WORKOUT_ID == iworkout)
                                            && (EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date)
                                            select work).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                NewWorkout objNewWorkout = new NewWorkout();
                                objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                if (objGroup.STAM_GROUP_ID != 0)
                                {
                                    objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                }
                                objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                                if (lDetails.Count != 0)
                                {
                                    foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                    {
                                        List<SectionResults> SectionResults = new List<SectionResults>();
                                        Details oDetails = new Details();
                                        oDetails.SectionID = item.STAM_SEC_ID;
                                        STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                        if (objSection.STAM_ATHSEC_ID != 0)
                                        {
                                            oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                        }
                                        oDetails.Run = item.STAM_RUN;
                                        oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                        oDetails.workoutId = item.STAM_WORKOUT_ID;
                                        oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                        List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                        foreach (var itemSection in lstDetialsSection)
                                        {
                                            SectionResults oSectionResults = new SectionResults();
                                            oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                            oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                            oSectionResults.Lap = itemSection.STAM_LAP;
                                            oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                            SectionResults.Add(oSectionResults);
                                            oDetails.ListSectionResults = SectionResults;
                                        }
                                        lstDetails.Add(oDetails);
                                    }
                                    objNewWorkout.ListDetails = lstDetails;
                                    lstNewWorkout.Add(objNewWorkout);
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    else
                    {
                        var coachwrkouts = (from work in objContext.STAM_TR_WORKOUTS
                                            where EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(work.STAM_WORKOUT_DATE) <= EndDate.Date
                                            select work).ToList();
                        if (coachwrkouts != null)
                        {
                            foreach (var itemcoach in coachwrkouts)
                            {
                                NewWorkout objNewWorkout = new NewWorkout();
                                objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                                objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                                objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                                objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                                objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                                STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                                if (objGroup.STAM_GROUP_ID != 0)
                                {
                                    objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                                }
                                objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                                objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                                objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                                objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                                List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                                if (lDetails.Count != 0)
                                {
                                    foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                    {
                                        List<SectionResults> SectionResults = new List<SectionResults>();
                                        Details oDetails = new Details();
                                        oDetails.SectionID = item.STAM_SEC_ID;
                                        STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                        if (objSection.STAM_ATHSEC_ID != 0)
                                        {
                                            oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                        }
                                        oDetails.Run = item.STAM_RUN;
                                        oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                        oDetails.workoutId = item.STAM_WORKOUT_ID;
                                        oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                        List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                        foreach (var itemSection in lstDetialsSection)
                                        {
                                            SectionResults oSectionResults = new SectionResults();
                                            oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                            oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                            oSectionResults.Lap = itemSection.STAM_LAP;
                                            oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                            SectionResults.Add(oSectionResults);
                                            oDetails.ListSectionResults = SectionResults;
                                        }
                                        lstDetails.Add(oDetails);
                                    }
                                    objNewWorkout.ListDetails = lstDetails;
                                    lstNewWorkout.Add(objNewWorkout);
                                }
                            }
                            return lstNewWorkout;
                        }
                    }
                    return null;
                }
                return null;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetWorkouts", "Problem In GetWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        public List<NewWorkout> GetNewWorkoutResults(DateTime StartDate, long coachId, long groupId)
        {
            try
            {
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                List<Details> lstDetails = new List<Details>();
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == coachId).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();
                long? userids = User.SAM_CREATED_USER;
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                if (User.STAM_USER_TYPE == 3)
                {
                    var coachwrkout = (from e in objContext.STAM_TR_WORKOUTS
                                       join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                       where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == coachId || e.STAM_CREATED_USER == userids || userids.ToString().Contains(e.STAM_CREATED_USER.ToString()))
                                    && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) == StartDate.Date)
                                    && e.STAM_WORKOUT_STATUS != 3
                                    && e.STAM_WORKOUT_GROUPID == groupId
                                       select e).ToList();

                    if (coachwrkout != null)
                    {
                        foreach (var itemcoach in coachwrkout)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection.STAM_ATHSEC_ID != 0)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }
                        return lstNewWorkout;
                    }

                }
                if (User.STAM_USER_TYPE == 2)
                {
                    var coachwrkouts = (from e in objContext.STAM_TR_WORKOUTS
                                        join s in objContext.STAM_MA_USERS on e.STAM_CREATED_USER equals s.STAM_USERID
                                        where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == coachId) && (EntityFunctions.TruncateTime(e.STAM_WORKOUT_DATE) == StartDate.Date)
                                        && e.STAM_WORKOUT_STATUS != 3
                                         && e.STAM_WORKOUT_GROUPID == groupId
                                        select e).ToList();


                    if (coachwrkouts != null)
                    {
                        foreach (var itemcoach in coachwrkouts)
                        {
                            NewWorkout objNewWorkout = new NewWorkout();
                            objNewWorkout.workoutId = itemcoach.STAM_WORKOUT_ID;
                            objNewWorkout.WorkoutTitle = itemcoach.STAM_WORKOUT_TITLE;
                            objNewWorkout.WorkoutDistance = itemcoach.STAM_WORKOUT_DISTANCE;
                            objNewWorkout.WorkoutRuns = itemcoach.STAM_WORKOUT_RUNS;
                            objNewWorkout.WorkoutGroupId = itemcoach.STAM_WORKOUT_GROUPID;
                            STAM_MA_GROUP objGroup = objContext.STAM_MA_GROUP.Where(p => p.STAM_GROUP_ID == itemcoach.STAM_WORKOUT_GROUPID).SingleOrDefault();
                            if (objGroup.STAM_GROUP_ID != 0)
                            {
                                objNewWorkout.WorkoutGroupName = objGroup.STAM_GROUP_NAME;
                            }
                            objNewWorkout.WorkoutDate = itemcoach.STAM_WORKOUT_DATE;
                            objNewWorkout.WorkoutUserId = itemcoach.STAM_CREATED_USER;
                            objNewWorkout.WorkoutType = itemcoach.STAM_WORKOUT_TYPE;
                            objNewWorkout.WorkoutNotes = itemcoach.STAM_WORKOUT_NOTES;
                            List<STAM_TR_DETAILS> lDetails = objContext.STAM_TR_DETAILS.Where(p => p.STAM_WORKOUT_ID == itemcoach.STAM_WORKOUT_ID).ToList();
                            if (lDetails.Count != 0)
                            {
                                foreach (var item in lDetails.Where(p => p.STAM_SEC_ID != null).ToList())
                                {
                                    List<SectionResults> SectionResults = new List<SectionResults>();
                                    Details oDetails = new Details();
                                    oDetails.SectionID = item.STAM_SEC_ID;
                                    STAM_MA_ATHLETE_SECTION objSection = objContext.STAM_MA_ATHLETE_SECTION.Where(p => p.STAM_ATHSEC_ID == item.STAM_SEC_ID).SingleOrDefault();
                                    if (objSection.STAM_ATHSEC_ID != 0)
                                    {
                                        oDetails.SectionName = objSection.STAM_ATHSEC_NAME;
                                    }
                                    oDetails.Run = item.STAM_RUN;
                                    oDetails.TargetTime = item.STAM_SEC_TARGET_TIME;
                                    oDetails.workoutId = item.STAM_WORKOUT_ID;
                                    oDetails.DetailsID = item.STAM_WORKOUT_DETAILS_ID;
                                    List<STAM_TR_DETAILS_SECTION> lstDetialsSection = objContext.STAM_TR_DETAILS_SECTION.Where(p => p.STAM_WORKOUT_DETAILS_ID == item.STAM_WORKOUT_DETAILS_ID).ToList();
                                    foreach (var itemSection in lstDetialsSection)
                                    {
                                        SectionResults oSectionResults = new SectionResults();
                                        oSectionResults.SectionResultsID = itemSection.STAM_WORKOUT_DETAILS_SEC_ID;
                                        oSectionResults.DetailsID = itemSection.STAM_WORKOUT_DETAILS_ID;
                                        oSectionResults.Lap = itemSection.STAM_LAP;
                                        oSectionResults.LapTime = itemSection.STAM_LAPTIME;
                                        SectionResults.Add(oSectionResults);
                                        oDetails.ListSectionResults = SectionResults;
                                    }
                                    lstDetails.Add(oDetails);
                                }
                                objNewWorkout.ListDetails = lstDetails;
                                lstNewWorkout.Add(objNewWorkout);
                            }
                        }
                        return lstNewWorkout;
                    }
                    else
                    {
                        return null;
                    }
                }

                return null;

                //var coachwrkout = objContext.STAM_MA_COACH_WORKOUT.Where(w => (EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) >= StartDate.Date && EntityFunctions.TruncateTime(w.STAM_COACH_WORKOUT_DATE) <= EndDate && (w.STAM_CREATED_USER == Userid || w.STAM_CREATED_USER == User.SAM_CREATED_USER))).ToList();

            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetNewWorkoutResults", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }

        public List<NewWorkout> GetListOfUserBasedWorkout(List<NewWorkout> ListNewWorkout, long Userid)
        {
            try
            {
                STAM_MA_USERS User = objContext.STAM_MA_USERS.Where(p => p.STAM_USERID == Userid).FirstOrDefault();
                List<STAM_MA_USERS> UserList = objContext.STAM_MA_USERS.Where(p => p.SAM_CREATED_USER == User.STAM_USERID).ToList();
                STAM_MA_ASSTCOACH AsstUser = objContext.STAM_MA_ASSTCOACH.Where(w => w.STAM_ASSTCOACH_ID == User.STAM_USERID).FirstOrDefault();
                long? userids = User.SAM_CREATED_USER;
                var objAsstCoach = from Asstcoachs in objContext.STAM_MA_ASSTCOACH where Asstcoachs.STAM_CREATED_USER == userids && Asstcoachs.STAM_ASSTCOACH_STATUS == 1 select Asstcoachs;
                if (User.STAM_USER_TYPE == 3 || User.STAM_USER_TYPE == 4)
                {
                    List<NewWorkout> LstNewWorkouts = new List<NewWorkout>();
                    if (ListNewWorkout.Count != 0)
                    {
                        LstNewWorkouts = (from e in ListNewWorkout
                                         join s in objContext.STAM_MA_USERS on e.WorkoutUserId equals s.STAM_USERID
                                         where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid || e.WorkoutUserId == userids || userids.ToString().Contains(e.WorkoutUserId.ToString()))
                                         select e).ToList();
                    }
                    return LstNewWorkouts;
                }
                if (User.STAM_USER_TYPE == 2 || User.STAM_USER_TYPE == 1)
                {
                    List<NewWorkout> LstNewWorkouts = new List<NewWorkout>();
                    if (ListNewWorkout.Count != 0)
                    {
                        LstNewWorkouts = (from e in ListNewWorkout
                                         join s in objContext.STAM_MA_USERS on e.WorkoutUserId equals s.STAM_USERID
                                         where (s.SAM_CREATED_USER == User.STAM_USERID || s.STAM_USERID == Userid)
                                         select e).ToList();
                    }
                    return LstNewWorkouts;
                }
                return null;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "WorkoutResult : GetListOfUserBasedWorkout", "Problem In GetAllWorkouts", Ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
    }
}