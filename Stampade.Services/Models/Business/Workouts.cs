﻿using BusinessObjects.Log;
using BusinessObjects.Constants;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using BusinessObjects.Communication;
using BusinessObjects.Utility;

namespace Stampade.Services.Models.Business
{
    public class Workouts
    {

        #region Entities

        public STAMPEDEEntities objContext = new STAMPEDEEntities();
        Logger objLog = new Logger();

        #endregion

        #region Team

        /// <summary>
        /// This method is used to create new jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        /// 
        //add new job
        public bool Create_Workouts(STAM_MA_WORKOUTS objWorkouts)
        {
            try
            {
                STAM_MA_WORKOUTS objCreateWorkouts = new STAM_MA_WORKOUTS()
                {
                    STAM_WORKOUT_STATUS = objWorkouts.STAM_WORKOUT_STATUS,
                    STAM_WORKOUT_TITLE = objWorkouts.STAM_WORKOUT_TITLE,
                    STAM_WORKOUT_DISTANCEID =objWorkouts.STAM_WORKOUT_DISTANCEID,
                    STAM_WORKOUT_DISTANCE = objWorkouts.STAM_WORKOUT_DISTANCE,
                    STAM_WORKOUT_RUNS = objWorkouts.STAM_WORKOUT_RUNS,
                    STAM_WORKOUT_TARGET_MM = objWorkouts.STAM_WORKOUT_TARGET_MM,
                    STAM_WORKOUT_TARGET_SS = objWorkouts.STAM_WORKOUT_TARGET_SS,
                    STAM_CREATED_USER = objWorkouts.STAM_CREATED_USER,
                    STAM_CREATED_DATE = objWorkouts.STAM_CREATED_DATE,
                    STAM_LAST_UPDATED_USER = objWorkouts.STAM_LAST_UPDATED_USER,
                    STAM_LAST_UPDATED_DATE = objWorkouts.STAM_LAST_UPDATED_DATE,
                };

                objContext.STAM_MA_WORKOUTS.Add(objCreateWorkouts);
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Workouts : CreateWorkout", "Problem In Creating a new Workouts", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        /// <summary>
        /// This method is used to update jobs
        /// </summary>
        /// <param name="objJob"></param>
        /// <returns></returns>
        public bool Update_Workouts(STAM_MA_WORKOUTS objWorkouts)
        {
            try
            {
                var objUpdateWorkouts = objContext.STAM_MA_WORKOUTS.Where(x => x.STAM_WORKOUT_ID == objWorkouts.STAM_WORKOUT_ID).SingleOrDefault();
                {
                    objUpdateWorkouts.STAM_WORKOUT_STATUS = objWorkouts.STAM_WORKOUT_STATUS;
                    objUpdateWorkouts.STAM_WORKOUT_TITLE = objWorkouts.STAM_WORKOUT_TITLE;
                    objUpdateWorkouts.STAM_WORKOUT_DISTANCEID = objWorkouts.STAM_WORKOUT_DISTANCEID;
                    objUpdateWorkouts.STAM_WORKOUT_DISTANCE = objWorkouts.STAM_WORKOUT_DISTANCE;
                    objUpdateWorkouts.STAM_WORKOUT_RUNS = objWorkouts.STAM_WORKOUT_RUNS;
                    objUpdateWorkouts.STAM_WORKOUT_TARGET_MM = objWorkouts.STAM_WORKOUT_TARGET_MM;
                    objUpdateWorkouts.STAM_WORKOUT_TARGET_SS = objWorkouts.STAM_WORKOUT_TARGET_SS;
                    objUpdateWorkouts.STAM_CREATED_USER = objWorkouts.STAM_CREATED_USER;
                    objUpdateWorkouts.STAM_CREATED_DATE = objWorkouts.STAM_CREATED_DATE;
                    objUpdateWorkouts.STAM_LAST_UPDATED_USER = objWorkouts.STAM_LAST_UPDATED_USER;
                    objUpdateWorkouts.STAM_LAST_UPDATED_DATE = objWorkouts.STAM_LAST_UPDATED_DATE;
                };
                objContext.Entry(objUpdateWorkouts).State = EntityState.Modified;
                objContext.SaveChanges();

                return true;
            }
            catch (Exception Ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Workouts : UpdateWorkouts", "Problem In Updating a Workouts", Ex.Message.ToString());
                objLog = null;
                return false;
            }
        }

        public List<STAM_MA_WORKOUTS> GetWorkoutsList(string UserID)
        {

             try
            {
                string[] _UsrID = null;
                _UsrID = UserID.ToString().TrimEnd(',').Split(',');
                List<STAM_MA_WORKOUTS> _objRes = new List<STAM_MA_WORKOUTS>();
                foreach (var item in _UsrID)
                {
                    long item1 = Convert.ToInt64(item);
                    var objWorkouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_CREATED_USER == item1 && works.STAM_WORKOUT_STATUS != 3 orderby works.STAM_WORKOUT_ID select works;
                    if (objWorkouts != null)
                    {
                        _objRes.AddRange(objWorkouts);
                    }
                }
                return _objRes.OrderByDescending(p=>p.STAM_CREATED_DATE).ToList();
            }

            //try
            //{
            //    var obj_Workouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_WORKOUT_STATUS != 3 orderby works.STAM_WORKOUT_ID descending select works;
            //    List<STAM_MA_WORKOUTS> objWorkouts = obj_Workouts.ToList<STAM_MA_WORKOUTS>();
            //    return objWorkouts;
            //}
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Workouts : GetWorkouts", "Problem In Getting a list of Coach Admins", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_WORKOUTS GetWorkoutsByID(long iWorkouts)
        {
            try
            {
                var objWorkouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_WORKOUT_ID == iWorkouts select works;
                STAM_MA_WORKOUTS obj_Workouts = objWorkouts.FirstOrDefault();
                return obj_Workouts;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Workouts : GetWorkoutsByID", "Problem In Getting a Workouts", ex.Message.ToString());
                return null;
            }
        }
        public STAM_MA_WORKOUTS GetWorkoutsByDistanceID(string iWorkouts)
        {
            try
            {
                var objWorkouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_WORKOUT_DISTANCE == iWorkouts  && works.STAM_WORKOUT_STATUS != 3 select works;
                STAM_MA_WORKOUTS obj_Workouts = objWorkouts.FirstOrDefault();
                return obj_Workouts;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Workouts : GetWorkoutsByID", "Problem In Getting a Workouts", ex.Message.ToString());
                return null;
            }
        }

        public STAM_MA_WORKOUTS GetWorkoutsBytitle(string title,string Userid)
        {

            List<STAM_MA_WORKOUTS> _objRes = new List<STAM_MA_WORKOUTS>();
            List<STAM_MA_WORKOUTS> _objRes1 = new List<STAM_MA_WORKOUTS>();

            try
            {
                string[] _grp = null;
                _grp = Userid.ToString().TrimEnd(',').Split(',');
                //var objWorkouts = null;
                foreach (var item in _grp)
                {
                    long item1 =(long)(Convert.ToDouble(item.ToString()));


                    var objWorkouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_WORKOUT_TITLE == title && works.STAM_WORKOUT_STATUS != 3 && works.STAM_CREATED_USER == item1 select works;
                    _objRes.AddRange(objWorkouts);
                }

              //  var objWorkouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_WORKOUT_TITLE == title && works.STAM_WORKOUT_DISTANCE == Dist && works.STAM_WORKOUT_RUNS == Runs && works.STAM_WORKOUT_STATUS != 3  && works.STAM_CREATED_USER==Userid select works;
         // var objWorkouts = from works in objContext.STAM_MA_WORKOUTS where works.STAM_WORKOUT_TITLE == title  && works.STAM_WORKOUT_STATUS != 3  && works.STAM_CREATED_USER==Userid select works;
                STAM_MA_WORKOUTS obj_Workouts = _objRes.FirstOrDefault();
                return obj_Workouts;
            }
            catch (Exception ex)
            {
                objLog.LogToFile("Error", "Workouts : GetWorkoutsByID", "Problem In Getting a Workouts", ex.Message.ToString());
                return null;
            }
        }
        #endregion
    }
}