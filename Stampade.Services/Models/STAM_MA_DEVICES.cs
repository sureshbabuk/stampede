//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Stampade.Services.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class STAM_MA_DEVICES
    {
        public int STAM_ID { get; set; }
        public string STAM_DEVICE_NAME { get; set; }
        public string STAM_DEVICE_ID { get; set; }
    }
}
