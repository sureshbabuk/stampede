﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models
{
    public class WorkoutModel
    {

        public List<string> Sections { get; set; }
        public int Runs { get; set; }
        public int Distance { get; set; }
        public List<string> SectionIDs { get; set; }
        public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public string STAM_WORKOUT_NOTES { get; set; }
        public string STAM_WORKOUT_TYPE { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
        public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_WORKOUT_STATUS { get; set; }
        public string STAM_WORKOUT_GROUP { get; set; }

       // public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();
        public STAM_TR_DETAILS_MODEL _objSTAM_TR_DETAILS_1 { get; set; }


        public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS { get; set; }

        public List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();

        public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();
    }

    public class STAM_TR_DETAILS_MODEL 
    {
        public long STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_ID { get; set; }
        public Nullable<int> STAM_RUN { get; set; }
      //s  public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }

        public Nullable<long> STAM_SEC_ID { get; set; }
        public  string STAM_SEC_TARGET_TIME { get; set; }
      //  public Nullable<int> STAM_NO_ROW { get; set; }

        public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION { get; set; }
      
    }
    public  class STAM_TR_DETAILS_SECTION_MODEL
    {

        public long STAM_WORKOUT_DETAILS_SEC_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_SEC_ID { get; set; }
        public string STAM_SEC_TARGET_TIME { get; set; }

        public Nullable<decimal> STAM_LAP { get; set; }

        
    }



    //For service




    public class WorkoutServiceModel
    {


        public List<string> Sections { get; set; }
        public int Runs { get; set; }
        public int Distance { get; set; }
        public List<string> SectionIDs { get; set; }
        public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public string STAM_WORKOUT_NOTES { get; set; }
        public string STAM_WORKOUT_TYPE { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
        public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_WORKOUT_STATUS { get; set; }
        public string STAM_WORKOUT_GROUP { get; set; }

        // public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();
       // public STAM_TR_DETAILS_MODEL _objSTAM_TR_DETAILS_1 { get; set; }


        public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS { get; set; }

       // public List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();

      //  public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();
    }




}