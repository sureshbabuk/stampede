﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;
using System.Web.Script.Serialization;


namespace Stampade.Controllers
{
    public class AccountController : Controller
    {
        Login logins = new Login();

        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {   
            Login lg = new Login();
            if (Request.Cookies["SuperAdminUserName"] != null && Request.Cookies["SuperAdminUserPassword"] != null)
            {
                string username = Request.Cookies["SuperAdminUserName"].Value.ToString();
                string pass = Request.Cookies["SuperAdminUserPassword"].Value.ToString();
                lg.userName = username;
                lg.password = pass;
            }            
            return View(lg);
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChangePassword(string mailID)
        {
            ViewData["UserName"] = mailID;
            return View();
        }
        

        [HttpGet]
        [AllowAnonymous]
        public async Task<JsonResult> ForgotPasswords(Login login)
        {
            if (!string.IsNullOrEmpty(login.userName))
            {
                logins = login;
            }
            UserDetail newaccesstoken = new UserDetail();
            string userName = null;
            try
            {               

                Logger v_oLog = new Logger();
                string sEmailId = Convert.ToString(login.email);



                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
                string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
                string sPosturl = string.Format("{0}api/Account/ForgetPassword?MAILID={1}&NEWPWD={2}", sWebApiurl, sEmailId, null);

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(sPosturl));
                requestMessage.Headers.Add(strAppName, strUserId);
                var response = await client.SendAsync(requestMessage);
                string responseAsString = await response.Content.ReadAsStringAsync();
                // var body = client.GetStringAsync(new Uri(sPosturl)).Result;
                //  {"$id":"1","userId":"1","token":"abcdefgh"}
                dynamic stuff = JObject.Parse(responseAsString);
                userName = stuff.token;
                string status = stuff.status;


                return Json(new { Status = status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { data = "", Status = 400 }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<JsonResult> NewPasswords(Login login)
        {
            if (!string.IsNullOrEmpty(login.userName))
            {
                logins = login;
            }
            UserDetail newaccesstoken = new UserDetail();
            string userName = null;
            try
            {

                Logger v_oLog = new Logger();
                string sEmailId = Convert.ToString(login.email);
                string snewpwd = Convert.ToString(login.newPassword);
                string oldpwd = Convert.ToString(login.password);


                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
                string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
                string sPosturl = string.Format("{0}api/Account/ForgetPassword?MAILID={1}&NEWPWD={2}&OLDPWD={3}", sWebApiurl, sEmailId, snewpwd, oldpwd);

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(sPosturl));
                requestMessage.Headers.Add(strAppName, strUserId);
                var response = await client.SendAsync(requestMessage);
                string responseAsString = await response.Content.ReadAsStringAsync();
               
                dynamic stuff = JObject.Parse(responseAsString);
                userName = stuff.token;
                string status = stuff.status;

                return Json(new { Status = status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { data = "", Status = 400 }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<JsonResult> ReturnJson(Login login)
        {
            if (!string.IsNullOrEmpty(login.userName))
            {
                logins = login;
            }
            try
            {
                if (login.RememberMe )
                {
                    HttpCookie SuperAdminUserNameCookie = new HttpCookie("SuperAdminUserName");
                    HttpCookie SuperAdminUserPasswordCookie = new HttpCookie("SuperAdminUserPassword");
                    SuperAdminUserNameCookie.Expires = DateTime.Now.AddDays(30);
                    SuperAdminUserPasswordCookie.Expires = DateTime.Now.AddDays(30);
                    SuperAdminUserNameCookie.Value = login.userName;
                    SuperAdminUserPasswordCookie.Value = login.password;
                    Response.Cookies.Add(SuperAdminUserNameCookie);
                    Response.Cookies.Add(SuperAdminUserPasswordCookie);
                }
                string userName = null;
                Logger v_oLog = new Logger();
                string sUserName = Convert.ToString(login.userName);
                string sPassword = Convert.ToString(login.password);
                string newPassword = Convert.ToString(login.newPassword);

                var client = new HttpClient();
                string sPosturl = string.Format("{0}api/Account/GetLogin?SAM_USER_USERNAME={1}&SAM_USER_PSWD={2}&newPassword={3}", sWebApiurl, sUserName, sPassword, newPassword);
               
                //var body = client.GetStringAsync(new Uri(sPosturl)).Result;

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, new Uri(sPosturl));
                requestMessage.Headers.Add(strAppName, strUserId);
                var response = await client.SendAsync(requestMessage);
                string responseAsString = await response.Content.ReadAsStringAsync();


                if (responseAsString == "Invalid Authorization-Token")
                {
                    //return message not authorized to access the application.
                    return Json(new { data = "Not Authorized to Access", Status = 400 }, JsonRequestBehavior.AllowGet);
                }

                string des_result = responseAsString.ToString();
                dynamic stuff = JObject.Parse(responseAsString.ToString());
                userName = stuff.token;
                bool getActiveInActiveStatus = stuff.activeInActiveStatus;
                string yourString = responseAsString.ToString().Replace("\"", "");
                string userId = stuff.userId;
                string strUserType = stuff.UserType;
                string userStatus = stuff.activeInActiveStatus;
                string strResourceId = stuff.ResourceId;


                Session["USER_ID"] = userName;
                Session["USERS_ID"] = userId;
                Session["ID"] = userId;
                Session["UserType"] = strUserType;
                Session["ResourceId"] = strResourceId;

                return Json(new { data = yourString, UserType = strUserType, AthleteID = strResourceId, Status = userStatus }, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ee)
            {
                return Json(new { data = "",Message=ee.Message,result=ee.StackTrace, Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChkEmail(string MailId)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_USERS objUser = new STAM_MA_USERS();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (MailId != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Account/GetMailID?mailID=" + MailId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objUser = json_serializer.Deserialize<STAM_MA_USERS>(response);

                string returnMsg = "";
                long ID = 0;
                if (objUser != null && MailId != null)
                {

                    if (objUser.STAM_USER_TYPE == 1)
                    {
                        returnMsg = "This email address already exists. Please use a different email";
                    }
                    //Chk with Coach 
                    if(objUser.STAM_USER_TYPE == 2)
                    {                       
                        STAM_MA_COACH objCoachRef = new STAM_MA_COACH(); 
                        response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoachByMailID?mailID=" + MailId).Result;
                        objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(response);                        
                        if (objCoachRef != null)
                        {
                            ID = objCoachRef.STAM_COACH_ID;
                            returnMsg = "This email address already exists. Please use a different email";
                        }
                        
                    }
                    if (objUser.STAM_USER_TYPE == 3)//Chk with Asst Coach 
                    {
                        STAM_MA_ASSTCOACH objCoach_Ref = new STAM_MA_ASSTCOACH();
                        response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachByMailID?mailID=" + MailId).Result;
                        objCoach_Ref = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);
                        if (objCoach_Ref != null)
                        {
                            ID = objCoach_Ref.STAM_ASSTCOACH_ID;
                            returnMsg = "This email address already exists.  Please use a different email";
                        }
                    }
                    if (objUser.STAM_USER_TYPE == 4)//Chk with Coach Athlete 
                    {
                        STAM_MA_ATHLETE objAth = new STAM_MA_ATHLETE();
                        string response_obj =response_obj = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthByMailID?mailID=" + MailId).Result;

                        objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response_obj);
                      
                        if (objAth != null)
                        {
                            ID = objAth.STAM_ATH_ID;
                            returnMsg = "This email address already exists.  Please use a different email";
                        }
                        //else
                        //{
                        //    //Chk with Asst Coach Athlete 
                        //    STAM_MA_ASST_ATHLETE obj_Ath = new STAM_MA_ASST_ATHLETE();
                        //    string response_Ref = client.GetStringAsync(sWebApiurl + "api/AsstCoachAthlete/GetAthByMailID?mailID=" + MailId).Result;
                        //    obj_Ath = json_serializer.Deserialize<STAM_MA_ASST_ATHLETE>(response_Ref);

                        //    if (obj_Ath != null)
                        //    {
                        //        ID = obj_Ath.STAM_ASST_ATH_ID;
                        //        returnMsg = "This email address already exists.  Please use a different email";
                        //    }
                        //}
                    }
                }
                return Json(new { Error = false, Message = returnMsg, data = ID, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult ChkWorkoutDate(string WorkoutDate)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_USERS objUser = new STAM_MA_USERS();
                STAM_MA_COACH_WORKOUT objWorkout = new STAM_MA_COACH_WORKOUT();
                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (WorkoutDate != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Account/GetWorkoutDate?WorkoutDate=" + WorkoutDate).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objWorkout = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);

                string returnMsg = "";
                long ID = 0;
                if (objWorkout != null && WorkoutDate != null)
                {
                    returnMsg = "This email address already exists.  Please use a different email";
                    //if (objUser.STAM_USER_TYPE == 1)
                    //{
                    //    returnMsg = "This email address already exists.  Please use a different email";
                    //}
                    //Chk with Coach 
                    //if (objUser.STAM_USER_TYPE == 2)
                    //{
                    //    STAM_MA_COACH objCoachRef = new STAM_MA_COACH();
                    //    response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoachByMailID?mailID=" + MailId).Result;
                    //    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(response);
                    //    if (objCoachRef != null)
                    //    {
                    //        ID = objCoachRef.STAM_COACH_ID;
                    //        returnMsg = "This email address already exists. Please use a different email";
                    //    }

                    //}
                    //if (objUser.STAM_USER_TYPE == 3)//Chk with Asst Coach 
                    //{
                    //    STAM_MA_ASSTCOACH objCoach_Ref = new STAM_MA_ASSTCOACH();
                    //    response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachByMailID?mailID=" + MailId).Result;
                    //    objCoach_Ref = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);
                    //    if (objCoach_Ref != null)
                    //    {
                    //        ID = objCoach_Ref.STAM_ASSTCOACH_ID;
                    //        returnMsg = "This email address already exists.  Please use a different email";
                    //    }
                    //}
                    //if (objUser.STAM_USER_TYPE == 4)//Chk with Coach Athlete 
                    //{
                    //    STAM_MA_ATHLETE objAth = new STAM_MA_ATHLETE();
                    //    string response_obj = response_obj = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthByMailID?mailID=" + MailId).Result;

                    //    objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response_obj);

                    //    if (objAth != null)
                    //    {
                    //        ID = objAth.STAM_ATH_ID;
                    //        returnMsg = "This email address already exists.  Please use a different email";
                    //    }
                    //    else
                    //    {
                    //        //Chk with Asst Coach Athlete 
                    //        STAM_MA_ASST_ATHLETE obj_Ath = new STAM_MA_ASST_ATHLETE();
                    //        string response_Ref = client.GetStringAsync(sWebApiurl + "api/AsstCoachAthlete/GetAthByMailID?mailID=" + MailId).Result;
                    //        obj_Ath = json_serializer.Deserialize<STAM_MA_ASST_ATHLETE>(response_Ref);

                    //        if (obj_Ath != null)
                    //        {
                    //            ID = obj_Ath.STAM_ASST_ATH_ID;
                    //            returnMsg = "This email address already exists.  Please use a different email";
                    //        }
                    //    }
                    //}
                }
                return Json(new { Error = false, Message = returnMsg, data = ID, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Logout()
        {
            //Session["USER_ID"] = null;
            Session.Abandon();

            Session["USER_ID"] = null;
            Session["USERS_ID"] = null;
            Session["ID"] = null;
            Session["UserType"] = null;
            Session["ResourceId"] = null;
            return RedirectToAction("login", "Account");
        }
        public ActionResult Myprofile(int? CoachId)
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_USERS objCoachRef = new STAM_MA_USERS();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Account/GetAdminUser?iCoach=" + CoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<STAM_MA_USERS>(response);

            return View(objCoachRef);
        }



        public JsonResult AdminCreation(STAM_MA_USERS obj_CreateCoach)
        {

            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_COACH objJobRef = new STAM_MA_COACH();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_CreateCoach.STAM_USERID == 0)
                {


                    retunMsg = " Coach Admin Created Successfully";
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Account/GetAdminUser?iCoach=" + obj_CreateCoach.STAM_USERID).Result;

                    STAM_MA_USERS objCoachRef = new STAM_MA_USERS();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_USERS>(responses);
                    //   long tempid = objCoachRef.STAM_COACH_TEAMID;

                    objCoachRef.STAM_USER_FNAME = obj_CreateCoach.STAM_USER_FNAME;
                    objCoachRef.STAM_USER_LNAME = obj_CreateCoach.STAM_USER_LNAME;
                    objCoachRef.STAM_USER_EMAIL = obj_CreateCoach.STAM_USER_EMAIL;
                    objCoachRef.STAM_USER_MOBILE = obj_CreateCoach.STAM_USER_MOBILE;


                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Account/AdminCreation", objCoachRef).Result;


                    //Save Assigned Team  in Team Table


                    retunMsg = "Coach Admin Details Updated Successfully";
                }
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        
    }
}