﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
using System.Threading.Tasks;

namespace Stampade.Controllers
{
      
    public class AsstCoachController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }
      
        # region Manage Team

        public ActionResult ManageAsstCoach(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult AsstCoach_Table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ASSTCOACH> _objCoach = new List<STAM_MA_ASSTCOACH>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachs?CoachID=" + Session["USERS_ID"]).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objCoach = json_serializer.Deserialize<List<STAM_MA_ASSTCOACH>>(response);

            return View(_objCoach);
        }

        #endregion

        # region Create NewCoach
        public ActionResult CreateNewAsstCoach(int? AsstCoachId,string _copy)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_ASSTCOACH objCoachRef = new STAM_MA_ASSTCOACH();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (AsstCoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + AsstCoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);

            if (_copy != null)
            {
                ViewData["CopyEdit"] = "copy";
            }


            return View(objCoachRef);
        }

        public ActionResult AsstCoach_View(int? CoachId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_ASSTCOACH objCoachRef = new STAM_MA_ASSTCOACH();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + CoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);

            return View(objCoachRef);
        }

        public JsonResult AsstCoachCreation(STAM_MA_ASSTCOACH obj_CreateCoach)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_ASSTCOACH objJobRef = new STAM_MA_ASSTCOACH();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();
                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                 JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                if (obj_CreateCoach.STAM_ASSTCOACH_ID == 0)
                {
                    obj_CreateCoach.STAM_ASSTCOACH_STATUS = BusinessConstants.Status.Active;
                    obj_CreateCoach.STAM_CREATED_USER = UserID;
                    obj_CreateCoach.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateCoach.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateCoach.STAM_LAST_UPDATED_DATE = DateTime.Now;


                    response = client.PostAsJsonAsync(sWebApiurl + "api/AsstCoach/GetCreateAsstCoach", obj_CreateCoach).Result;

                    string LastCoachID = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetLastCoach").Result;
                    STAM_MA_ASSTCOACH _obj_Coach = new STAM_MA_ASSTCOACH();
                    _obj_Coach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(LastCoachID);

                    STAM_MA_USERS _objUser = new STAM_MA_USERS();
                    _objUser.STAM_USER_FNAME = obj_CreateCoach.STAM_ASSTCOACH_FIRSTNAME;
                    _objUser.STAM_USER_LNAME = obj_CreateCoach.STAM_ASSTCOACH_LASTNAME;
                    _objUser.STAM_USER_TYPE = BusinessConstants.UserType.AsstCoach;
                    _objUser.STAM_USER_USERNAME = obj_CreateCoach.STAM_ASSTCOACH_EMAILADD;
                    _objUser.STAM_USER_TYPE_KEY_ID = _obj_Coach.STAM_ASSTCOACH_ID;
                    _objUser.STAM_ACTIVATED_STRING = _obj_Coach.STAM_ASSTCOACH_FIRSTNAME + " " + _obj_Coach.STAM_ASSTCOACH_LASTNAME;

                    _objUser.SAM_CREATED_USER = UserID;
                    _objUser.STAM_CREATED_DATE = DateTime.Now;
                    _objUser.STAM_LAST_UPDATED_USER = UserID;
                    _objUser.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Login/GetCreateUser", _objUser).Result;

                    retunMsg = " AsstCoach Created Successfully";
                }
                else
                {

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + obj_CreateCoach.STAM_ASSTCOACH_ID).Result;

                    STAM_MA_ASSTCOACH objCoachRef = new STAM_MA_ASSTCOACH();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(responses);

                    objCoachRef.STAM_ASSTCOACH_FIRSTNAME = obj_CreateCoach.STAM_ASSTCOACH_FIRSTNAME;
                    objCoachRef.STAM_ASSTCOACH_LASTNAME = obj_CreateCoach.STAM_ASSTCOACH_LASTNAME;
                    objCoachRef.STAM_ASSTCOACH_EMAILADD = obj_CreateCoach.STAM_ASSTCOACH_EMAILADD;
                    objCoachRef.STAM_ASSTCOACH_MOBILENO = obj_CreateCoach.STAM_ASSTCOACH_MOBILENO;
                    objCoachRef.STAM_ASSTCOACH_GROUPID = obj_CreateCoach.STAM_ASSTCOACH_GROUPID;
                    objCoachRef.STAM_ASSTCOACH_GROUPNAME = obj_CreateCoach.STAM_ASSTCOACH_GROUPNAME;
                   

                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/AsstCoach/GetCreateAsstCoach", objCoachRef).Result;

                    retunMsg = "AsstCoach Details Updated Successfully";
                }
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Add/Update Create Asst Coach", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteAsstCoach(long CoachId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_ASSTCOACH objCoachRef = new STAM_MA_ASSTCOACH();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (CoachId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + CoachId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);

                objCoachRef.STAM_ASSTCOACH_STATUS = BusinessConstants.Status.Deleted;
                objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                //if (objCoachRef.STAM_ASSTCOACH_GROUPID != null)
                //{
                //    return Json(new { Error = false, Message = "Sorry, cannot be deleted as this AsstCoach is assigned to Athlete Section", Status = 200 }, JsonRequestBehavior.AllowGet);
                //}
                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/AsstCoach/GetCreateAsstCoach", objCoachRef).Result;

                string returnMsg = "Selected Coach Has been Deleted Successfully ";

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Coach", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult ChkAsstCoachEmailAddress(string MailId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_ASSTCOACH objCoachRef = new STAM_MA_ASSTCOACH();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (MailId != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachByMailID?mailID=" + MailId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);

                string returnMsg = "";
                if(objCoachRef != null)
                {
                    returnMsg = "AsstCoach MailID Already Exists";

                }
                return Json(new { Error = false, Message = returnMsg, data = (objCoachRef != null) ? objCoachRef.STAM_ASSTCOACH_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AsstCoachActionUpdate(long CoachId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string responses = "";
                responses = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + CoachId).Result;

                STAM_MA_ASSTCOACH objCoachRef = new STAM_MA_ASSTCOACH();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(responses);
                    

                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    objCoachRef.STAM_ASSTCOACH_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/AsstCoach/GetCreateAsstCoach", objCoachRef).Result;  

                 retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Coach", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}