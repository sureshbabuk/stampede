﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;

using System.Net;
using System.Threading.Tasks;

namespace Stampade.Controllers
{      
    public class AthleteController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }
      
        # region Manage Athlete

        public ActionResult ManageAthlete(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult Athlete_Table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            //List<STAM_MA_ATHLETE> _objAth = new List<STAM_MA_ATHLETE>();
            List<Stam_Athlete> _objAth = new List<Stam_Athlete>();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            if (Session["UserType"].ToString() == "2")//Coach
            {
            string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
            string UserSID = json_serializer.Deserialize<string>(UserSIDres);

                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
            string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);

                AsstCoachID += Session["USERS_ID"];

                string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthletestest?AthID=" + AsstCoachID).Result;
                _objAth = json_serializer.Deserialize<List<Stam_Athlete>>(response);
               // _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(response);
            }
            else//AsstCoach 
            {
                List<Stam_Athlete> _objAthlete = new List<Stam_Athlete>();

                STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                    // athlete.STAM_ATH_ASSIGNGROUPID = item.STAM_ASST_ATH_ASSIGNGROUPID;

                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);

                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);

                AsstCoachID += _objCoach.STAM_CREATED_USER;
                    // athlete.STAM_ATH_ASSIGNGROUPID = item.STAM_ASST_ATH_ASSIGNGROUPID;

                string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetCoachAdminGroupListNew?grp=" + _objCoach.STAM_ASSTCOACH_GROUPID + "&UserID=" + AsstCoachID).Result;

                _objAth = json_serializer.Deserialize<List<Stam_Athlete>>(response);

            }

            return View(_objAth);
        }

        #endregion

        # region Create NewAthlete
        public ActionResult CreateNewAthlete(int? AthId,string _copy)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            Stam_Athlete objAth = new Stam_Athlete();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);


                string response = "";
                if (AthId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthletes?iAth=" + AthId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objAth = json_serializer.Deserialize<Stam_Athlete>(response);

                   
                    // athlete.STAM_ATH_ASSIGNGROUPID = item.STAM_ASST_ATH_ASSIGNGROUPID;

                  
                    // athlete.STAM_ATH_ASSIGNGROUPID = item.STAM_ASST_ATH_ASSIGNGROUPID;
            
            if (_copy != null)
            {
                ViewData["CopyEdit"] = "copy";
            }

            return View(objAth);
        }

        public ActionResult Athlete_View(int? AthId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            Stam_Athlete objAth = new Stam_Athlete();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);


                string response = "";
                if (AthId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthletes?iAth=" + AthId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objAth = json_serializer.Deserialize<Stam_Athlete>(response);


                    // athlete.STAM_ATH_ASSIGNGROUPID = item.STAM_ASST_ATH_ASSIGNGROUPID;


                    // athlete.STAM_ATH_ASSIGNGROUPID = item.STAM_ASST_ATH_ASSIGNGROUPID;

            return View(objAth);
        }

        public JsonResult AthleteCreation(STAM_MA_ATHLETE obj_Ath)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                if (Session["UserType"].ToString() == "2")
                {
                    obj_Ath.STAM_USERTYPE = BusinessConstants.UserType.CoachAdmin;
                }
                else
                {
                    obj_Ath.STAM_USERTYPE = BusinessConstants.UserType.AsstCoach;
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                STAM_MA_ATHLETE objJobRef = new STAM_MA_ATHLETE();
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                var retunMsg = ""; 
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_Ath.STAM_ATH_ID == 0)
                {
                    obj_Ath.STAM_ATH_STATUS = BusinessConstants.Status.Active;
                    obj_Ath.STAM_CREATED_USER = UserID;
                    obj_Ath.STAM_CREATED_DATE = DateTime.Now;
                    obj_Ath.STAM_LAST_UPDATED_USER = UserID;
                    obj_Ath.STAM_LAST_UPDATED_DATE = DateTime.Now;                    

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/CreateAthlete", obj_Ath).Result;

                    string LastAth = client.GetStringAsync(sWebApiurl + "api/Athlete/GetLastAthlete").Result;
                    STAM_MA_ATHLETE _obj_Ath = new STAM_MA_ATHLETE();
                    _obj_Ath = json_serializer.Deserialize<STAM_MA_ATHLETE>(LastAth);                    

                    STAM_MA_USERS _objUser = new STAM_MA_USERS();
                    _objUser.STAM_USER_FNAME = obj_Ath.STAM_ATH_FIRTSNAME;
                    _objUser.STAM_USER_LNAME = obj_Ath.STAM_ATH_LASTNAME;
                    _objUser.STAM_USER_TYPE = BusinessConstants.UserType.Athlete;
                    _objUser.STAM_USER_USERNAME = obj_Ath.STAM_ATH_EMAILADD;
                    _objUser.STAM_USER_TYPE_KEY_ID = _obj_Ath.STAM_ATH_ID;
                    _objUser.STAM_ACTIVATED_STRING = _obj_Ath.STAM_ATH_FIRTSNAME + " " + _obj_Ath.STAM_ATH_LASTNAME;

                    _objUser.SAM_CREATED_USER = UserID;
                    _objUser.STAM_CREATED_DATE = DateTime.Now;
                    _objUser.STAM_LAST_UPDATED_USER = UserID;
                    _objUser.STAM_LAST_UPDATED_DATE = DateTime.Now;                   

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Login/GetCreateUser", _objUser).Result;

                    retunMsg = " Athlete Created Successfully";
                }
                else
                {
                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + obj_Ath.STAM_ATH_ID).Result;                    

                    STAM_MA_ATHLETE obj_ATh = new STAM_MA_ATHLETE();
                    obj_ATh = json_serializer.Deserialize<STAM_MA_ATHLETE>(responses);

                    obj_ATh.STAM_ATH_FIRTSNAME = obj_Ath.STAM_ATH_FIRTSNAME;
                    obj_ATh.STAM_ATH_LASTNAME = obj_Ath.STAM_ATH_LASTNAME;
                    obj_ATh.STAM_ATH_EMAILADD = obj_Ath.STAM_ATH_EMAILADD;
                    obj_ATh.STAM_ATH_MOBILENO = obj_Ath.STAM_ATH_MOBILENO;
                    obj_ATh.STAM_ATH_ASSIGNGROUPID = obj_Ath.STAM_ATH_ASSIGNGROUPID;
                    obj_ATh.STAM_ATH_ASSIGNGROUP = obj_Ath.STAM_ATH_ASSIGNGROUP;
                    obj_ATh.STAM_ATH_SECTION = obj_Ath.STAM_ATH_SECTION;
                    obj_ATh.STAM_ATH_SECTIONID = obj_Ath.STAM_ATH_SECTIONID;
                    obj_ATh.STAM_USERTYPE = obj_Ath.STAM_USERTYPE;

                    obj_ATh.STAM_LAST_UPDATED_USER = UserID;
                    obj_ATh.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/UpdateAthlete", obj_ATh).Result;

                    retunMsg = "Athlete Details Updated Successfully";
                }

                if (obj_Ath.STAM_ATH_SECTION != null)
                {
                    string responses_Ref = "";

                    responses_Ref = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?iAth=" + obj_Ath.STAM_ATH_SECTIONID).Result;

                    STAM_MA_ATHLETE_SECTION obj_AThRef = new STAM_MA_ATHLETE_SECTION();
                    obj_AThRef = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(responses_Ref);

                    string temp_name = obj_AThRef.STAM_ATHSEC_NAME;

                    obj_AThRef.STAM_LAST_UPDATED_USER = UserID;
                    obj_AThRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    if (obj_AThRef.STAM_ATHSEC_ATHLETEID != null && obj_AThRef.STAM_ATHSEC_ATHLETENAME != null)//if more athletes are selected
                    {
                        obj_AThRef.STAM_ATHSEC_ATHLETEID = obj_AThRef.STAM_ATHSEC_ATHLETEID + "," + obj_Ath.STAM_ATH_SECTIONID;
                        obj_AThRef.STAM_ATHSEC_ATHLETENAME = obj_AThRef.STAM_ATHSEC_ATHLETENAME + "," + obj_Ath.STAM_ATH_SECTION;
                    }
                    else 
                    {
                        obj_AThRef.STAM_ATHSEC_ATHLETEID = obj_Ath.STAM_ATH_SECTIONID.ToString();
                        obj_AThRef.STAM_ATHSEC_ATHLETENAME = obj_Ath.STAM_ATH_SECTION;
                    }
                    response = client.PostAsJsonAsync(sWebApiurl + "api/AthleteSection/UpdateAthleteSection", obj_AThRef).Result;
                }

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Add/Update Create Athlete", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteAthlete(long AthId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_ATHLETE objAth = new STAM_MA_ATHLETE();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string response = "";
                    if (AthId > 0)
                    {
                        response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + AthId).Result;
                    }

                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response);

                    objAth.STAM_ATH_STATUS = BusinessConstants.Status.Deleted;
                    objAth.STAM_LAST_UPDATED_USER = UserID;
                    objAth.STAM_LAST_UPDATED_DATE = DateTime.Now;

                //List<STAM_MA_COACH_WORKOUT> objCoachRef = new List<STAM_MA_COACH_WORKOUT>();
                //string response_Ref = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByGroupName?Name=" + objAth.STAM_ATH_ASSIGNGROUP + "&UserID=" + Session["USERS_ID"]).Result;
                //objCoachRef = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response_Ref);
                    if (objAth.STAM_ATH_SECTION != null)
                    {
                        return Json(new { Error = false, Message = "Sorry, cannot be deleted as this Athlete is assigned to coach Workout", Status = 200 }, JsonRequestBehavior.AllowGet);
                    }

                    HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/UpdateAthlete", objAth).Result;

                string returnMsg = "Selected Athlete Has been Deleted Successfully ";

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Athlete", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }





        public JsonResult ChkAThEmailAddress(string MailId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_ATHLETE objAth = new STAM_MA_ATHLETE();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (MailId != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthByMailID?mailID=" + MailId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response);

                string returnMsg = "";
                if (objAth != null)
                {
                    returnMsg = "Athlete MailID Already Exists";

                }
                return Json(new { Error = false, Message = returnMsg, data = (objAth != null) ? objAth.STAM_ATH_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AthActionUpdate(long AthId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string responses = "";
                responses = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + AthId).Result;

                   STAM_MA_ATHLETE objATh = new STAM_MA_ATHLETE();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objATh = json_serializer.Deserialize<STAM_MA_ATHLETE>(responses);


                    objATh.STAM_LAST_UPDATED_USER = UserID;
                    objATh.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    objATh.STAM_ATH_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/UpdateAthlete", objATh).Result;  

                 retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Athlete", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}