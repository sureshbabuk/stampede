﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
using System.Threading.Tasks;

namespace Stampade.Controllers
{      
    public class AthleteSectionController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }
      
        # region Manage Athlete Section

        public ActionResult ManageAthleteSection(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult AthleteSection_Table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            //string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSections?AthID=" + Session["USERS_ID"]).Result;
            if (Session["UserType"].ToString() == "2")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += Session["USERS_ID"];
                string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSections?AthID=" + AsstCoachID).Result;
            _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response);
            }
            else//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += _objCoach.STAM_CREATED_USER;
                string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthSecByGroupList?Group=" + _objCoach.STAM_ASSTCOACH_GROUPID + "&UserID=" + AsstCoachID).Result;
                _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response);
            }

            return View(_objAth);
        }

        public ActionResult ManageViewAthlete(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult ViewAssignedAthlete_Table(string Ath)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);



            List<STAM_MA_ATHLETE> _objAth = new List<STAM_MA_ATHLETE>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAssignedAthleteListByID?iAth=" + Ath).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(response);

            return View(_objAth);
        }

        #endregion

        # region Create NewAthlete Section
        public ActionResult CreateNewAthleteSection(int? AthId, string _copy)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_ATHLETE_SECTION objAth = new STAM_MA_ATHLETE_SECTION();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (AthId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?iAth=" + AthId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objAth = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(response);

            if (_copy != null)
            {
                ViewData["CopyEdit"] = "copy";
            }

            return View(objAth);
        }

        public ActionResult AthleteSection_View(int? AthId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_ATHLETE_SECTION objAth = new STAM_MA_ATHLETE_SECTION();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (AthId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?iAth=" + AthId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objAth = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(response);

            return View(objAth);
        }

        public JsonResult AthleteSectionCreation(STAM_MA_ATHLETE_SECTION obj_Ath)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);
                if (Session["UserType"].ToString() == "2")
                {
                    obj_Ath.STAM_USERTYPE = BusinessConstants.UserType.CoachAdmin;
                }
                else
                {
                    obj_Ath.STAM_USERTYPE = BusinessConstants.UserType.AsstCoach;
                }

                STAM_MA_ATHLETE_SECTION objJobRef = new STAM_MA_ATHLETE_SECTION();
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_Ath.STAM_ATHSEC_ID == 0)
                {
                    obj_Ath.STAM_ATHSEC_STATUS = BusinessConstants.Status.Active;
                    obj_Ath.STAM_CREATED_USER = UserID;
                    obj_Ath.STAM_CREATED_DATE = DateTime.Now;
                    obj_Ath.STAM_LAST_UPDATED_USER = UserID;
                    obj_Ath.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/AthleteSection/CreateAthleteSection", obj_Ath).Result;


                    //save Assigned athlete in Athlete table
                   string response_obj = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetLastAthSec").Result;
                   objJobRef = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(response_obj);
                    if (obj_Ath.STAM_ATHSEC_ATHLETEID != null)
                    {
                        STAM_MA_ATHLETE obj_AThRef = new STAM_MA_ATHLETE();
                        string responses = "";
                        foreach (var item in obj_Ath.STAM_ATHSEC_ATHLETEID.Split(','))
                        {
                            responses = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + item).Result;
                            obj_AThRef = json_serializer.Deserialize<STAM_MA_ATHLETE>(responses);
                            obj_AThRef.STAM_ATH_SECTION = obj_Ath.STAM_ATHSEC_NAME;
                            obj_AThRef.STAM_ATH_SECTIONID = objJobRef.STAM_ATHSEC_ID;

                            obj_AThRef.STAM_LAST_UPDATED_USER = UserID;
                            obj_AThRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                            response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/UpdateAthlete", obj_AThRef).Result;
                        }                        
                    }

                    retunMsg = " Athlete Section Created Successfully";
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl +"api/AthleteSection/GetAthleteSection?iAth=" + obj_Ath.STAM_ATHSEC_ID).Result;

                    STAM_MA_ATHLETE_SECTION obj_AThRef = new STAM_MA_ATHLETE_SECTION();
                    obj_AThRef = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(responses);

                    string temp_name = obj_AThRef.STAM_ATHSEC_NAME;

                    obj_AThRef.STAM_ATHSEC_NAME = obj_Ath.STAM_ATHSEC_NAME;
                    obj_AThRef.STAM_ATHSEC_GROUPNAME = obj_Ath.STAM_ATHSEC_GROUPNAME;
                    obj_AThRef.STAM_ATHSEC_GROUPID = obj_Ath.STAM_ATHSEC_GROUPID;
                    obj_AThRef.STAM_USERTYPE = obj_Ath.STAM_USERTYPE;
                    obj_AThRef.STAM_LAST_UPDATED_USER = UserID;
                    obj_AThRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    if (obj_Ath.STAM_ATHSEC_ATHLETEID != null && obj_AThRef.STAM_ATHSEC_ATHLETEID != null)//if more athletes are selected
                    {
                        obj_AThRef.STAM_ATHSEC_ATHLETEID =obj_AThRef.STAM_ATHSEC_ATHLETEID +","+ obj_Ath.STAM_ATHSEC_ATHLETEID;
                        obj_AThRef.STAM_ATHSEC_ATHLETENAME =obj_AThRef.STAM_ATHSEC_ATHLETENAME +","+ obj_Ath.STAM_ATHSEC_ATHLETENAME;
                    }
                    else if (obj_Ath.STAM_ATHSEC_ATHLETEID != null && obj_Ath.STAM_ATHSEC_ATHLETENAME != null)
                    {
                        obj_AThRef.STAM_ATHSEC_ATHLETEID =  obj_Ath.STAM_ATHSEC_ATHLETEID;
                        obj_AThRef.STAM_ATHSEC_ATHLETENAME =  obj_Ath.STAM_ATHSEC_ATHLETENAME;
                    }
                    response = client.PostAsJsonAsync(sWebApiurl + "api/AthleteSection/UpdateAthleteSection", obj_AThRef).Result;


                    //if Athlete name is changed, have to change in Athlete Table for assigned Athletes
                    if(temp_name.Trim() != obj_AThRef.STAM_ATHSEC_NAME)
                    {
                        List<STAM_MA_ATHLETE> _objAThRef = new List<STAM_MA_ATHLETE>();
                        string objresponses = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthleteByAthSec?iAth=" + obj_AThRef.STAM_ATHSEC_ID).Result;
                        _objAThRef = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(objresponses);
                        if(_objAThRef != null)
                        {
                            string resvar =null;
                            foreach(var item in _objAThRef)
                            {
                                resvar += item.STAM_ATH_ID +",";
                            }
                            obj_Ath.STAM_ATHSEC_ATHLETEID =resvar;
                        } 
                       
                    }

                    //for saving update Athlete section in Athlete Table
                    if (obj_Ath.STAM_ATHSEC_ATHLETEID != null)
                    {
                        STAM_MA_ATHLETE _objATh = new STAM_MA_ATHLETE();
                        string response_Ref = "";
                        foreach (var item in obj_Ath.STAM_ATHSEC_ATHLETEID.TrimEnd(',').Split(','))
                        {
                            response_Ref = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + item).Result;
                            _objATh = json_serializer.Deserialize<STAM_MA_ATHLETE>(response_Ref);
                            _objATh.STAM_ATH_SECTION = obj_Ath.STAM_ATHSEC_NAME;
                            _objATh.STAM_ATH_SECTIONID = obj_Ath.STAM_ATHSEC_ID;

                            _objATh.STAM_LAST_UPDATED_USER = UserID;
                            _objATh.STAM_LAST_UPDATED_DATE = DateTime.Now;

                            response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/UpdateAthlete", _objATh).Result;
                        }
                    }
                    

                    retunMsg = "Athlete Section Details Updated Successfully";
                }
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Add/Update Create Athlete Section", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteAthleteSection(long AthId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_ATHLETE_SECTION objAth = new STAM_MA_ATHLETE_SECTION();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (AthId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?iAth=" + AthId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objAth = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(response);

                objAth.STAM_ATHSEC_STATUS = BusinessConstants.Status.Deleted;
                objAth.STAM_LAST_UPDATED_USER = UserID;
                objAth.STAM_LAST_UPDATED_DATE = DateTime.Now;


                if (Convert.ToUInt32(objAth.STAM_ATHSEC_ATHLETEID) != 0 && objAth.STAM_ATHSEC_ATHLETENAME != null)
                {
                    return Json(new { Error = false, Message = "Sorry, cannot be Delete as this Section is assigned to Athlete", Status = 200 }, JsonRequestBehavior.AllowGet);
                }

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/AthleteSection/UpdateAthleteSection", objAth).Result;

                string returnMsg = "Selected Athlete Section Has Been Deleted Successfully ";

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Athlete Section", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditorUpdateSection(long AthId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["ID"].ToString(), out UserID);
            STAM_MA_ATHLETE_SECTION objAth = new STAM_MA_ATHLETE_SECTION();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            string ReturnMsg = "";

            if (AthId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?iAth=" + AthId).Result;
            }
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objAth = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(response);

            List<STAM_MA_COACH_WORKOUT> objCoachRef = new List<STAM_MA_COACH_WORKOUT>();
            string response_Ref = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByAthSecID?secID=" + AthId).Result;
           objCoachRef = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response_Ref);

           if (objCoachRef.Count >0)
            {
                ReturnMsg = "true";
            }
            return Json(new { Error = false, Message = ReturnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChkAThSecName(string Name,string Group)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_ATHLETE_SECTION objAth = new STAM_MA_ATHLETE_SECTION();
                List<STAM_MA_ATHLETE_SECTION> Sectionlist = new List<STAM_MA_ATHLETE_SECTION>();
                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";                

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                string AthSecCreatedUserID = "";

                if (Session["UserType"].ToString() == "2")//Coach
                {
                    string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                    string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                    string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                    AthSecCreatedUserID = json_serializer.Deserialize<string>(AsstCoachIDres);
                    AthSecCreatedUserID += Session["USERS_ID"];
                }
                else if (Session["UserType"].ToString() == "3")//AsstCoach 
                {
                    STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                    string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                    _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                    string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                    string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                    string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                    AthSecCreatedUserID = json_serializer.Deserialize<string>(AsstCoachIDres);
                    AthSecCreatedUserID += _objCoach.STAM_CREATED_USER;
                }


                if (Name != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthSecByName?Name=" + Name + "&Group=" + Group ).Result;
                }
                
                string response2 = "";
                response2 = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?Name=" + Group + "&UserId=" + UserID).Result;
                Sectionlist = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response2);
                objAth = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(response);
                string returnMsg = "";
                if (Sectionlist.Count <= 8)
                {                      
                if (objAth != null)
                {
                     foreach (var item in AthSecCreatedUserID.TrimEnd(',').Split(','))
                        {
                            if (objAth.STAM_CREATED_USER == long.Parse(item))
                            {
                                returnMsg = "Athlete Section Name Already Exists";
                            }
                        }
                    }
                   
                }
                else
                {
                    returnMsg = "Athlete Section count Exists";
                    string result="error";
                    return Json(new { Error = false, Message = returnMsg, data = result, Status = 200 }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { Error = false, Message = returnMsg, data = (objAth != null) ? objAth.STAM_ATHSEC_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AthSecActionUpdate(long AthId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string responses = "";
                responses = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?iAth=" + AthId).Result;

                STAM_MA_ATHLETE_SECTION objATh = new STAM_MA_ATHLETE_SECTION();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objATh = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(responses);


                    objATh.STAM_LAST_UPDATED_USER = UserID;
                    objATh.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    objATh.STAM_ATHSEC_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                    //if (Convert.ToUInt32(objATh.STAM_ATHSEC_ATHLETEID) != 0 && objATh.STAM_ATHSEC_ATHLETENAME != null)
                    //{
                    //    return Json(new { Error = false, Message = "Sorry, cannot be Change this Section is assigned to Athlete", Status = 200 }, JsonRequestBehavior.AllowGet);
                    //}

                    response = client.PostAsJsonAsync(sWebApiurl + "api/AthleteSection/UpdateAthleteSection", objATh).Result;  

                 retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Athlete Section", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}