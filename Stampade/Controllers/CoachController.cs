﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;

namespace Stampade.Controllers
{
    public class CoachController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }
      
        # region Manage Team

        public ActionResult ManageCoach(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult Coach_Table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_COACH> _objCoach = new List<STAM_MA_COACH>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoachs?CreateUser=" + UserID).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objCoach = json_serializer.Deserialize<List<STAM_MA_COACH>>(response);

            return View(_objCoach);
        }

        #endregion

        # region Create NewCoach
        public ActionResult CreateNewCoach(int? CoachId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_COACH objCoachRef = new STAM_MA_COACH();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoach?iCoach=" + CoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(response);

            return View(objCoachRef);
        }

        public ActionResult Coach_View(int? CoachId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_COACH objCoachRef = new STAM_MA_COACH();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoach?iCoach=" + CoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(response);

            return View(objCoachRef);
        }     

        public JsonResult CoachCreation(STAM_MA_COACH obj_CreateCoach)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_COACH objJobRef = new STAM_MA_COACH();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_CreateCoach.STAM_COACH_ID == 0)
                {
                    obj_CreateCoach.STAM_COACH_STATUS = BusinessConstants.Status.Active;
                    obj_CreateCoach.STAM_CREATED_USER = UserID;
                    obj_CreateCoach.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateCoach.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateCoach.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Coach/GetCreateCoach", obj_CreateCoach).Result;

                    //Save Assigned Team  in Team Table

                    string LastCoachID = client.GetStringAsync(sWebApiurl + "api/Coach/GetLastCoach").Result;
                    STAM_MA_COACH _obj_Coach = new STAM_MA_COACH();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    _obj_Coach = json_serializer.Deserialize<STAM_MA_COACH>(LastCoachID);
                    if (obj_CreateCoach.STAM_COACH_TEAMID != 0)
                    {
                      string  responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + obj_CreateCoach.STAM_COACH_TEAMID).Result;
                      STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                      
                      objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);
                      objTeamRef.STAM_COACHASSIGNED = true;
                      objTeamRef.STAM_ASSIGN_COACHID = _obj_Coach.STAM_COACH_ID;
                      response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    }

                    // add coach in userTable for login session

                    STAM_MA_USERS  _objUser = new STAM_MA_USERS();
                    _objUser.STAM_USER_FNAME = obj_CreateCoach.STAM_COACH_FIRSTNAME;
                    _objUser.STAM_USER_LNAME = obj_CreateCoach.STAM_COACH_LASTNAME;
                    _objUser.STAM_USER_TYPE = BusinessConstants.UserType.CoachAdmin;
                    _objUser.STAM_USER_USERNAME = obj_CreateCoach.STAM_COACH_EMAILADD;
                    _objUser.STAM_USER_TYPE_KEY_ID = _obj_Coach.STAM_COACH_ID;

                    _objUser.SAM_CREATED_USER = UserID;
                    _objUser.STAM_CREATED_DATE = DateTime.Now;
                    _objUser.STAM_LAST_UPDATED_USER = UserID;
                    _objUser.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Login/GetCreateUser", _objUser).Result;

                    retunMsg = " Coach Admin Created Successfully";
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoach?iCoach=" + obj_CreateCoach.STAM_COACH_ID).Result;

                    STAM_MA_COACH objCoachRef = new STAM_MA_COACH();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(responses);
                    long tempid = objCoachRef.STAM_COACH_TEAMID;

                    objCoachRef.STAM_COACH_FIRSTNAME = obj_CreateCoach.STAM_COACH_FIRSTNAME;
                    objCoachRef.STAM_COACH_LASTNAME = obj_CreateCoach.STAM_COACH_LASTNAME;
                    objCoachRef.STAM_COACH_EMAILADD = obj_CreateCoach.STAM_COACH_EMAILADD;
                    objCoachRef.STAM_COACH_MOBILENO = obj_CreateCoach.STAM_COACH_MOBILENO;
                    objCoachRef.STAM_COACH_TEAMNAME = obj_CreateCoach.STAM_COACH_TEAMNAME;
                    objCoachRef.STAM_COACH_TEAMID = obj_CreateCoach.STAM_COACH_TEAMID;

                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Coach/GetCreateCoach", objCoachRef).Result;


                    //Save Assigned Team  in Team Table

                    STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                    if (objCoachRef.STAM_COACH_TEAMID != tempid)
                    {
                        responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + objCoachRef.STAM_COACH_TEAMID).Result;
                        objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);
                        objTeamRef.STAM_COACHASSIGNED = true;
                        objTeamRef.STAM_ASSIGN_COACHID = obj_CreateCoach.STAM_COACH_ID;
                        response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;

                        responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + tempid).Result;
                        objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);
                        objTeamRef.STAM_COACHASSIGNED = false;
                        objTeamRef.STAM_ASSIGN_COACHID = obj_CreateCoach.STAM_COACH_ID;
                        response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    }
                    else
                    {
                        responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + objCoachRef.STAM_COACH_TEAMID).Result;
                        objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);
                        objTeamRef.STAM_COACHASSIGNED = true;
                        response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    }

                    retunMsg = "Coach Admin Details Updated Successfully";
                }
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteCoach(long CoachId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_COACH objCoachRef = new STAM_MA_COACH();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (CoachId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoach?iCoach=" + CoachId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(response);

                objCoachRef.STAM_COACH_STATUS = BusinessConstants.Status.Deleted;
                objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Coach/GetCreateCoach", objCoachRef).Result;

                string returnMsg = "";
                if (responses.StatusCode == HttpStatusCode.OK)
                {
                    string responseRef = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + objCoachRef.STAM_COACH_TEAMID).Result;
                    STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                    objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responseRef);
                    objTeamRef.STAM_LAST_UPDATED_USER = UserID;
                    objTeamRef.STAM_COACHASSIGNED = false;
                    objTeamRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    HttpResponseMessage response_Ref = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    returnMsg = "Selected Coach Has Been Deleted Successfully ";
                }

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult ChkCoachEmailAddress(string MailId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_COACH objCoachRef = new STAM_MA_COACH();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (MailId != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoachByMailID?mailID=" + MailId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(response);

                string returnMsg = "";
                if(objCoachRef != null)
                {
                    returnMsg = "Coach MailID Already Exists";

                }
                return Json(new { Error = false, Message = returnMsg, data = (objCoachRef != null) ? objCoachRef.STAM_COACH_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CoachActionUpdate(long CoachId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string responses = "";
                responses = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoach?iCoach=" + CoachId).Result;

                    STAM_MA_COACH objCoachRef = new STAM_MA_COACH();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH>(responses);
                    

                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    objCoachRef.STAM_COACH_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Coach/GetCreateCoach", objCoachRef).Result;  

                 retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Coach", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}




