﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
using System.Threading.Tasks;
using BusinessObjects.Utility;

namespace Stampade.Controllers
{      
    public class CoachWorkoutController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();

        Utility utility = new Utility();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }
      
        # region Manage Team

        public ActionResult ManageCoachWorkout(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult CoachWorkout_Table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_COACH_WORKOUT> _objCoach = new List<STAM_MA_COACH_WORKOUT>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            //string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkouts?CoachID=" + Session["USERS_ID"]).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            if (Session["UserType"].ToString() == "2")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
              //  string AsstCoachID="";

                AsstCoachID += Session["USERS_ID"];
                string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkouts?CoachID=" + AsstCoachID).Result;
            _objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);
            }
            else if (Session["UserType"].ToString() == "3")//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach1 = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach1 = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);

                AsstCoachID += _objCoach1.STAM_CREATED_USER;
              string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByGroupName?Name=" + _objCoach1.STAM_ASSTCOACH_GROUPID + "&UserID=" + AsstCoachID).Result;
            //    string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkouts?CoachID=" + AsstCoachID).Result;
                _objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);
            }
            else//Athlete 
            {
                int userType = 0;
                string userId = "";
                  string AsstCoachID ="";

                STAM_MA_ATHLETE _objAth = new STAM_MA_ATHLETE();
                string response1 = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + Session["ResourceId"]).Result;
                _objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objAth.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                if(UserSID == "")
                {
                    STAM_MA_USERS objUser = new STAM_MA_USERS();
                    string responseUser = client.GetStringAsync(sWebApiurl + "api/Account/GetUserbyID?userID=" + _objAth.STAM_CREATED_USER).Result;
                    objUser = json_serializer.Deserialize<STAM_MA_USERS>(responseUser);

                    UserSID = objUser.STAM_USER_TYPE_KEY_ID.ToString();
                    userType =Convert.ToInt32(objUser.STAM_USER_TYPE);
                    userId = objUser.SAM_CREATED_USER.ToString();

                    AsstCoachID =_objAth.STAM_CREATED_USER + "," + userId;
                }
                else
                {
                  // UserSID = _objAth.STAM_CREATED_USER.ToString();
                    userType = 3;
                    userId = _objAth.STAM_CREATED_USER.ToString();

                    string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + userType + "&UserID=" + userId).Result;
                     AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                    AsstCoachID += _objAth.STAM_CREATED_USER;
                   
                }
               
                //string AsstCoachID = _objAth.STAM_CREATED_USER.ToString();
                string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByGroupName?Name=" + _objAth.STAM_ATH_ASSIGNGROUPID + "&UserID=" + AsstCoachID).Result;
                _objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);
            }
            List<STAM_MA_COACH_WORKOUT> _objCoachRes = new List<STAM_MA_COACH_WORKOUT>();
            STAM_MA_COACH_WORKOUT _objCoachInd = new STAM_MA_COACH_WORKOUT();

            //foreach (var item in _objCoach)
            //{
            //    if(item.STAM_COACH_WORKOUT_ATHSECNAME != null)
            //    {  
            //        foreach (var item1 in item.STAM_COACH_WORKOUT_ATHSECNAME.Split(','))
            //        {                        
            //            _objCoachInd.STAM_COACH_WORKOUT_ATHSECNAME = item1;
            //            _objCoachRes.Add(item);
            //        }                   
            //    }
            //    else
            //    {
            //        _objCoachRes.Add(item);
            //    }
            //}

            //return View(_objCoachRes.OrderByDescending(x=>x.STAM_COACH_WORKOUT_ID).ToList());

            return View(_objCoach);
        }

        #endregion

        # region Create NewCoachWorkout

        public ActionResult CreateNewCoachWorkout(int? CoachId, string _copy)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

                               
                string response = "";

                if (CoachId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);

            if (_copy != null)
            {
                ViewData["CopyEdit"] = "copy";
            }

          
            return View(objCoachRef);
        }

        public ActionResult CreateNewWrkoutNew(int? CoachId, string _copy)
        {


            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            TR_WORKOUTS objCoachRef = new TR_WORKOUTS();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);


            string response = "";

            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/EditNewWorkouts?CoachID=" + CoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<TR_WORKOUTS>(response);

            if (_copy != null)
            {
                ViewData["CopyEdit"] = "copy";
            }


            return View(objCoachRef);
        }

        public ActionResult CoachWorkout_View(int? CoachId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);


                string response = "";

                if (CoachId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);


                    // ressample.STAM_ASSTCOACH_WORKOUT_DISTANCEID = item.;
                    //ressample.STAM_ASSTCOACH_WORKOUT_ATHSECID = 0;
                    //ressample.STAM_ASSTCOACH_WORKOUT_ATHSECNAME = null;

            return View(objCoachRef);
        }

        public JsonResult CoachWorkoutCreation(STAM_MA_COACH_WORKOUT obj_CreateCoach)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);


                if (Session["UserType"].ToString() == "2")
                {
                    obj_CreateCoach.STAM_USERTYPE = BusinessConstants.UserType.CoachAdmin;
                }
                else
                {
                    obj_CreateCoach.STAM_USERTYPE = BusinessConstants.UserType.AsstCoach;
                }

                STAM_MA_COACH_WORKOUT objJobRef = new STAM_MA_COACH_WORKOUT();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();
                obj_CreateCoach.STAM_COACH_WORKOUT_DATE = utility.FormatToDate(obj_CreateCoach.WorkoutDate).Value;

                if (obj_CreateCoach.STAM_COACH_WORKOUT_ID == 0)
                {
                    obj_CreateCoach.STAM_COACH_WORKOUT_STATUS = BusinessConstants.Status.Active;
                    obj_CreateCoach.STAM_CREATED_USER = UserID;
                    obj_CreateCoach.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateCoach.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateCoach.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/GetCreateCoachWorkout", obj_CreateCoach).Result;

                    retunMsg = "Coach Workout Created Successfully";
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + obj_CreateCoach.STAM_COACH_WORKOUT_ID).Result;

                    STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(responses);

                    objCoachRef.STAM_COACH_WORKOUT_TITLE = obj_CreateCoach.STAM_COACH_WORKOUT_TITLE;
                    objCoachRef.STAM_COACH_WORKOUT_DATE = obj_CreateCoach.STAM_COACH_WORKOUT_DATE;
                    objCoachRef.STAM_COACH_WORKOUT_DISTANCE = obj_CreateCoach.STAM_COACH_WORKOUT_DISTANCE;
                    objCoachRef.STAM_COACH_WORKOUT_RUNS = obj_CreateCoach.STAM_COACH_WORKOUT_RUNS;
                    objCoachRef.STAM_COACH_WORKOUT_TARGET_MM = obj_CreateCoach.STAM_COACH_WORKOUT_TARGET_MM;
                    objCoachRef.STAM_COACH_WORKOUT_TARGET_SS = obj_CreateCoach.STAM_COACH_WORKOUT_TARGET_SS;
                    objCoachRef.STAM_COACH_WORKOUT_GROUPID = obj_CreateCoach.STAM_COACH_WORKOUT_GROUPID;
                     objCoachRef.STAM_COACH_WORKOUT_GROUPNAME = obj_CreateCoach.STAM_COACH_WORKOUT_GROUPNAME;
                     objCoachRef.STAM_COACH_WORKOUT_NOTES = obj_CreateCoach.STAM_COACH_WORKOUT_NOTES;
                     objCoachRef.STAM_USERTYPE = obj_CreateCoach.STAM_USERTYPE;

                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/UpdateCoachWorkout", objCoachRef).Result;

                    retunMsg = "Coach Workout Details Updated Successfully";
                }
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Add/Update Create Coach Workout", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAthleteSectionList(string Name, string grpId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string AsstCoachID = "";

            if (Session["UserType"].ToString() == "2" || Session["UserType"].ToString() == "1")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += Session["USERS_ID"];
            }
            else//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += _objCoach.STAM_CREATED_USER;
            }

            if (AsstCoachID != "" && AsstCoachID != null && grpId != null)
            {
                string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthSecByGroupList?Group=" + grpId + "&UserID=" + AsstCoachID).Result;
                _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response);
            }

            ViewData["AthName"] = Name;
           _objAth = _objAth.Where(x => x.STAM_ATHSEC_STATUS == BusinessConstants.Status.Active).ToList();
           string AthSesId = "";
           string AthSecName = "";

            foreach(var item in _objAth)
            {
                    AthSecName += item.STAM_ATHSEC_NAME + ',';
                    AthSesId += Convert.ToString(item.STAM_ATHSEC_ID) + ',';
            }

            return Json(new { Error = false, AthID = AthSesId.TrimEnd(','), AthName = AthSecName.TrimEnd(','), Status = 200 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteCoachWorkout(long CoachId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);



                    string response = "";
                    if (CoachId > 0)
                    {
                        response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;
                    }

                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);

                    objCoachRef.STAM_COACH_WORKOUT_STATUS = BusinessConstants.Status.Deleted;
                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/UpdateCoachWorkout", objCoachRef).Result;

                string returnMsg = "Selected Coach Workout Has been Deleted Successfully ";


                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Coach Workout", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditorDeleteWorkout(long CoachId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            string ReturnMsg = "";
            STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;
            }
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);
            if (objCoachRef != null)
            {
                STAM_MA_ATHLETE_SECTION _objCoach = new STAM_MA_ATHLETE_SECTION();
                string responses = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthSecByGroupID?GrpId=" + objCoachRef.STAM_COACH_WORKOUT_GROUPID).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ATHLETE_SECTION>(responses);
                if (_objCoach != null)
                {
                    if (_objCoach.STAM_ATHSEC_GROUPID == objCoachRef.STAM_COACH_WORKOUT_GROUPID)
                    {
                        ReturnMsg = "true";
                    }
                }
            }
            return Json(new { Error = false, Message = ReturnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChkCoachWorkoutTitle(string Name, string GroupName, string Distance, int Runs, string _date, string Copy, long Userid)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT(); 

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = ""; string returnMsg = "";

                //if (Copy == "false")
                //{                   
                   // if (Name != null)
                    //{
                    //    response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByName?Name=" + Name + "&Dist=" + Distance + "&Runs=" + Runs + "&Group=" + GroupName).Result;
                    //}
                    
                   JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    //objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);
                  
                    //if (objCoachRef != null)
                    //{
                    //    returnMsg = "Workout Title Already Exists with Same Combination of Distance and Runs";
                    //}
                //}
                //else
                //{


                   if (Session["UserType"].ToString() == "2")//Coach
                {
                    STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();

                    string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                    string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                    string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                    string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                    AsstCoachID += _objCoach.STAM_CREATED_USER;
                    AsstCoachID += ","+ UserID;
                   // response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                    if (AsstCoachID != null)
                    {
                        DateTime _dat = utility.FormatToDate(_date).Value;
                        response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                    }
                }
                   else if (Session["UserType"].ToString() == "3")//AsstCoach 
                   {
                       STAM_MA_ASSTCOACH _objCoach1 = new STAM_MA_ASSTCOACH();
                       string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                       _objCoach1 = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                       string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                       string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                       string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                       string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                       AsstCoachID += _objCoach1.STAM_CREATED_USER;
                       if (AsstCoachID != null)
                       {
                           DateTime _dat = utility.FormatToDate(_date).Value;
                           response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                       }
                       //string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByGroupName?Name=" + _objCoach1.STAM_ASSTCOACH_GROUPID + "&UserID=" + AsstCoachID).Result;
                       //_objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);
                   }

                   

                    //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);

                    if (objCoachRef != null)
                    {
                        returnMsg = "A workout already exists for this Date and Title";
                    }
                //}

                return Json(new { Error = false, Message = returnMsg, data = (objCoachRef != null) ? objCoachRef.STAM_COACH_WORKOUT_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CoachWorkoutActionUpdate(long CoachId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";
                    responses = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;

                    STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(responses);


                    objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                    objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    objCoachRef.STAM_COACH_WORKOUT_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/UpdateCoachWorkout", objCoachRef).Result;

                 retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");


                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Coach", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateChkCoachWorkout(long CoachId)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string response = "";
                if (CoachId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);

                objCoachRef.STAM_COACH_WORKOUT_STATUS = BusinessConstants.Status.Deleted;
                objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                //STAM_MA_ATHLETE _objAth = new STAM_MA_ATHLETE();
                //if (objCoachRef.STAM_COACH_WORKOUT_ATHID > 0)
                //{
                //    response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + objCoachRef.STAM_COACH_WORKOUT_ATHID).Result;
                //    _objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response);
                //}

                //if (Convert.ToUInt32(_objAth.STAM_ATH_SECTIONID) != 0 && _objAth.STAM_ATH_SECTION != null)
                //{
                //    return Json(new { Error = false, Message = "Sorry, cannot be Update this Workout is assigned to Athlete Section", Status = 200 }, JsonRequestBehavior.AllowGet);
                //}
                string returnMsg = "";
                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In checking update Coach Workout", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region NewWorkoutCreation

        public JsonResult NewCoachWorkoutCreation(STAM_TR_WORKOUTS obj_tr_wrkouts,STAM_TR_DETAILS obj_tr_details,STAM_CUSTOM_MODEL obj_tr_wrkout_sec)
        {


            try
            {

                DateTime myDate;

                obj_tr_wrkouts.STAM_WORKOUT_DATE = utility.FormatToDate(obj_tr_wrkouts.STAM_WORKOUT_DATE1).Value;
                if (!DateTime.TryParse(obj_tr_wrkouts.STAM_WORKOUT_DATE1, out myDate))
                {
                    // handle parse failure
                }
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_TR_WORKOUTS objJobRef = new STAM_TR_WORKOUTS();

                STAM_TR_DETAILS objJobRefDetail = new STAM_TR_DETAILS();


                STAM_CUSTOM_MODEL objJobRefSec = new STAM_CUSTOM_MODEL();

                var retunMsg = "";

                List<long> secid = new List<long>(); 

                List<string> ttime=new List<string>();

                List<int> distance = new List<int>();

                string[] sid=obj_tr_wrkout_sec.STAM_SEC_ID.ToString().Split('-');

                string[] target = obj_tr_wrkout_sec.STAM_TARGET_TIME.ToString().Split(',');


              

                int i=0;


                foreach(var item in sid)
                {
                   // secid[i]=(long) Convert.ToDecimal(item);
                    if (item != "")
                    {
                        secid.Add((long)Convert.ToDecimal(item));
                    }
                    
                    //i++;
                }


                foreach(var item in target)
                {

                    if (item != "")
                    {
                        ttime.Add(item);
                    }

                }
                if (obj_tr_wrkouts.STAM_WORKOUT_TYPE == "Mixed")
                {

                    string[] dist = obj_tr_wrkout_sec.DISTANCE.ToString().Split(',');
                    foreach (var item in dist)
                    {

                        if (item != "")
                        {
                            distance.Add((int)Convert.ToInt32(item));

                        }
                        else
                        {
                            distance.Add(0);
                        }

                    }
                }

                HttpResponseMessage response = new HttpResponseMessage();
                //objJobRef.STAM_COACH_WORKOUT_DATE = utility.FormatToDate(obj_CreateCoach.WorkoutDate).Value;

                if (obj_tr_wrkouts.STAM_WORKOUT_ID == 0)
                {


                 objJobRef.STAM_WORKOUT_DATE= obj_tr_wrkouts.STAM_WORKOUT_DATE;


                    objJobRef.STAM_WORKOUT_TITLE = obj_tr_wrkouts.STAM_WORKOUT_TITLE;

                    objJobRef.STAM_WORKOUT_GROUPID = obj_tr_wrkouts.STAM_WORKOUT_GROUPID;
                    objJobRef.STAM_WORKOUT_DISTANCE = obj_tr_wrkouts.STAM_WORKOUT_DISTANCE;
                    objJobRef.STAM_WORKOUT_RUNS = obj_tr_wrkouts.STAM_WORKOUT_RUNS;
                    objJobRef.STAM_WORKOUT_STATUS = obj_tr_wrkouts.STAM_WORKOUT_STATUS;
                    objJobRef.STAM_WORKOUT_TYPE = obj_tr_wrkouts.STAM_WORKOUT_TYPE;
                    objJobRef.STAM_WORKOUT_NOTES = obj_tr_wrkouts.STAM_WORKOUT_NOTES;

                    objJobRef.STAM_CREATED_USER = UserID;
                    objJobRef.STAM_CREATED_DATE = DateTime.Now;
                    objJobRef.STAM_LAST_UPDATED_USER = UserID;
                    objJobRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    objJobRef.STAM_WORKOUT_GROUP = obj_tr_wrkouts.STAM_WORKOUT_GROUP;
                    objJobRefDetail.STAM_WORKOUT_ID = obj_tr_details.STAM_WORKOUT_ID;


                    objJobRefDetail.STAM_WORKOUT_DETAILS_ID = obj_tr_details.STAM_WORKOUT_DETAILS_ID;
                    objJobRefDetail.STAM_RUN = obj_tr_details.STAM_RUN;
                    objJobRefDetail.STAM_CREATED_DATE = DateTime.Now;

                    objJobRefSec.STAM_SECID = secid;
                    objJobRefSec.STAM_SEC_TARGET_TIME = ttime;

                    //objJobRef.STAM_COACH_WORKOUT_STATUS = BusinessConstants.Status.Active;
                    //obj_CreateCoach.STAM_CREATED_USER = UserID;
                    //obj_CreateCoach.STAM_CREATED_DATE = DateTime.Now;
                    //obj_CreateCoach.STAM_LAST_UPDATED_USER = UserID;
                    //obj_CreateCoach.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);


                    TR_WORKOUTS trworkout = new TR_WORKOUTS();

                    trworkout.STAM_WORKOUT_DATE = obj_tr_wrkouts.STAM_WORKOUT_DATE;


                    trworkout.STAM_WORKOUT_TITLE = obj_tr_wrkouts.STAM_WORKOUT_TITLE;

                    trworkout.STAM_WORKOUT_GROUPID = obj_tr_wrkouts.STAM_WORKOUT_GROUPID;
                    trworkout.STAM_WORKOUT_DISTANCE = obj_tr_wrkouts.STAM_WORKOUT_DISTANCE;
                    trworkout.STAM_WORKOUT_RUNS = obj_tr_wrkouts.STAM_WORKOUT_RUNS;
                    trworkout.STAM_WORKOUT_STATUS = obj_tr_wrkouts.STAM_WORKOUT_STATUS;
                    trworkout.STAM_WORKOUT_TYPE = obj_tr_wrkouts.STAM_WORKOUT_TYPE;
                    trworkout.STAM_WORKOUT_NOTES = obj_tr_wrkouts.STAM_WORKOUT_NOTES;
                    trworkout.STAM_CREATED_USER = UserID;
                    trworkout.STAM_CREATED_DATE = DateTime.Now;
                    trworkout.STAM_LAST_UPDATED_USER = UserID;
                    trworkout.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    trworkout.STAM_WORKOUT_DETAILS_ID = obj_tr_details.STAM_WORKOUT_DETAILS_ID;



                    trworkout.DISTANCE = distance;

                    trworkout.STAM_SECID = objJobRefSec.STAM_SECID;


                    trworkout.STAM_SEC_TARGET_TIME1 = objJobRefSec.STAM_SEC_TARGET_TIME;
                    trworkout.STAM_WORKOUT_GROUP = objJobRef.STAM_WORKOUT_GROUP;



                    trworkout.UserID = UserID;
                    //trworkout.STAM_WORKOUT_TYPE=
                  //  return null;
                  // response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/GetCreateNewCoachWorkout?obj_work_out=" + objJobRef + "&obj_tr_details=" + objJobRefDetail+"&obj_tr_wrkout_sec=" + objJobRefSec).Result;
                    response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/GetCreateNewCoachWorkout?", trworkout).Result;
                    retunMsg = "Coach Workout Created Successfully";
                }



                if (obj_tr_wrkouts.STAM_WORKOUT_ID != 0)
                {

                    objJobRef.STAM_WORKOUT_ID = obj_tr_wrkouts.STAM_WORKOUT_ID;
                    objJobRef.STAM_WORKOUT_DATE = obj_tr_wrkouts.STAM_WORKOUT_DATE;


                    objJobRef.STAM_WORKOUT_TITLE = obj_tr_wrkouts.STAM_WORKOUT_TITLE;

                    objJobRef.STAM_WORKOUT_GROUPID = obj_tr_wrkouts.STAM_WORKOUT_GROUPID;
                    objJobRef.STAM_WORKOUT_DISTANCE = obj_tr_wrkouts.STAM_WORKOUT_DISTANCE;
                    objJobRef.STAM_WORKOUT_RUNS = obj_tr_wrkouts.STAM_WORKOUT_RUNS;
                    objJobRef.STAM_WORKOUT_STATUS = obj_tr_wrkouts.STAM_WORKOUT_STATUS;
                    objJobRef.STAM_WORKOUT_TYPE = obj_tr_wrkouts.STAM_WORKOUT_TYPE;
                    objJobRef.STAM_WORKOUT_NOTES = obj_tr_wrkouts.STAM_WORKOUT_NOTES;

                    objJobRef.STAM_CREATED_USER = UserID;
                    objJobRef.STAM_CREATED_DATE = DateTime.Now;
                    objJobRef.STAM_LAST_UPDATED_USER = UserID;
                    objJobRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    objJobRef.STAM_WORKOUT_GROUP = obj_tr_wrkouts.STAM_WORKOUT_GROUP;
                    objJobRefDetail.STAM_WORKOUT_ID = obj_tr_details.STAM_WORKOUT_ID;


                    objJobRefDetail.STAM_WORKOUT_DETAILS_ID = obj_tr_details.STAM_WORKOUT_DETAILS_ID;
                    objJobRefDetail.STAM_RUN = obj_tr_details.STAM_RUN;
                    objJobRefDetail.STAM_CREATED_DATE = DateTime.Now;

                    objJobRefSec.STAM_SECID = secid;
                    objJobRefSec.STAM_SEC_TARGET_TIME = ttime;

                    //objJobRef.STAM_COACH_WORKOUT_STATUS = BusinessConstants.Status.Active;
                    //obj_CreateCoach.STAM_CREATED_USER = UserID;
                    //obj_CreateCoach.STAM_CREATED_DATE = DateTime.Now;
                    //obj_CreateCoach.STAM_LAST_UPDATED_USER = UserID;
                    //obj_CreateCoach.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);


                    TR_WORKOUTS trworkout = new TR_WORKOUTS();

                    trworkout.STAM_WORKOUT_DATE = obj_tr_wrkouts.STAM_WORKOUT_DATE;


                    trworkout.STAM_WORKOUT_TITLE = obj_tr_wrkouts.STAM_WORKOUT_TITLE;

                    trworkout.STAM_WORKOUT_GROUPID = obj_tr_wrkouts.STAM_WORKOUT_GROUPID;
                    trworkout.STAM_WORKOUT_DISTANCE = obj_tr_wrkouts.STAM_WORKOUT_DISTANCE;
                    trworkout.STAM_WORKOUT_RUNS = obj_tr_wrkouts.STAM_WORKOUT_RUNS;
                    trworkout.STAM_WORKOUT_STATUS = obj_tr_wrkouts.STAM_WORKOUT_STATUS;
                    trworkout.STAM_WORKOUT_TYPE = obj_tr_wrkouts.STAM_WORKOUT_TYPE;
                    trworkout.STAM_WORKOUT_NOTES = obj_tr_wrkouts.STAM_WORKOUT_NOTES;
                    trworkout.STAM_CREATED_USER = UserID;
                    trworkout.STAM_CREATED_DATE = DateTime.Now;
                    trworkout.STAM_LAST_UPDATED_USER = UserID;
                    trworkout.STAM_LAST_UPDATED_DATE = DateTime.Now;
                    trworkout.STAM_WORKOUT_ID = objJobRef.STAM_WORKOUT_ID;
                    trworkout.STAM_WORKOUT_DETAILS_ID = obj_tr_details.STAM_WORKOUT_DETAILS_ID;



                    trworkout.DISTANCE = distance;

                    trworkout.STAM_SECID = objJobRefSec.STAM_SECID;


                    trworkout.STAM_SEC_TARGET_TIME1 = objJobRefSec.STAM_SEC_TARGET_TIME;
                    trworkout.STAM_WORKOUT_GROUP = objJobRef.STAM_WORKOUT_GROUP;



                    trworkout.UserID = UserID;
                    //trworkout.STAM_WORKOUT_TYPE=
                    //  return null;
                    // response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/GetCreateNewCoachWorkout?obj_work_out=" + objJobRef + "&obj_tr_details=" + objJobRefDetail+"&obj_tr_wrkout_sec=" + objJobRefSec).Result;
                    response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/GetUpdateNewCoachWorkout?", trworkout).Result;
                   retunMsg = "Coach Workout Updated Successfully";
                }



            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Add/Update Create Coach Workout", Status = 400 }, JsonRequestBehavior.AllowGet);
            }


            return null;








        }

















        public ActionResult NewManageCoachWorkout(string Msg)
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }



        public ActionResult NewCoachWorkout_Table()
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<TR_WORKOUTS> _objCoach = new List<TR_WORKOUTS>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string AsstCoachID = "";
            AsstCoachID += Session["USERS_ID"];
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetNewCoachWorkouts?CoachID=" + AsstCoachID).Result;
            _objCoach = json_serializer.Deserialize<List<TR_WORKOUTS>>(response);

       

            return View(_objCoach);
        }






        public JsonResult DeleteNewCoachWorkoutAction(long WorkoutID)
        {



            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_COACH_WORKOUT objCoachRef = new STAM_MA_COACH_WORKOUT();
                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);


                JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                string response = "";

                STAM_TR_WORKOUTS objJobRef = new STAM_TR_WORKOUTS();


                objJobRef.STAM_WORKOUT_ID = WorkoutID;
              //  response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/DeleteNewCoachWorkout?WorkOutID="+ WorkoutID).Result;

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/DeleteNewCoachWorkout", objJobRef).Result;



           

                return Json(new { Error = false, Message = responses.ReasonPhrase, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Coach Workout", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditNewWorkout(long CoachId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            string ReturnMsg = "";
            TR_WORKOUTS objCoachRef = new TR_WORKOUTS();
            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/EditNewWorkouts?iCoach=" + CoachId).Result;
            }
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<TR_WORKOUTS>(response);
         
            return Json(new { Error = false, Message = ReturnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
        }






        public JsonResult NewCoachWorkoutActionUpdate(long CoachId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string responses = "";
                //responses = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkout?iCoach=" + CoachId).Result;

                STAM_TR_WORKOUTS objCoachRef = new STAM_TR_WORKOUTS();
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                //objCoachRef = json_serializer.Deserialize<STAM_TR_WORKOUTS>(responses);

                objCoachRef.STAM_WORKOUT_ID = CoachId;
                objCoachRef.STAM_LAST_UPDATED_USER = UserID;
                objCoachRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                objCoachRef.STAM_WORKOUT_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                response = client.PostAsJsonAsync(sWebApiurl + "api/CoachWorkout/UpdateNewCoachWorkoutStatus", objCoachRef).Result;

                retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");


                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Coach", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }




        public JsonResult ChkNewCoachWorkoutTitle(string Name, string GroupName, string Distance, int Runs, string _date, string Copy, long Userid)
        {

            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_TR_WORKOUTS_Model objCoachRef = new STAM_TR_WORKOUTS_Model();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = ""; string returnMsg = "";

                //if (Copy == "false")
                //{                   
                // if (Name != null)
                //{
                //    response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByName?Name=" + Name + "&Dist=" + Distance + "&Runs=" + Runs + "&Group=" + GroupName).Result;
                //}

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                //objCoachRef = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response);

                //if (objCoachRef != null)
                //{
                //    returnMsg = "Workout Title Already Exists with Same Combination of Distance and Runs";
                //}
                //}
                //else
                //{


                if (Session["UserType"].ToString() == "2")//Coach
                {
                    STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();

                    string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                    string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                    string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                    string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                    AsstCoachID += _objCoach.STAM_CREATED_USER;
                    AsstCoachID += "," + UserID;
                    // response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                    if (AsstCoachID != null)
                    {
                        DateTime _dat = utility.FormatToDate(_date).Value;
                        response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkNewCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                    }
                }
                else if (Session["UserType"].ToString() == "3")//AsstCoach 
                {
                    STAM_MA_ASSTCOACH _objCoach1 = new STAM_MA_ASSTCOACH();
                    string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                    _objCoach1 = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                    string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                    string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                    string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                    string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                    AsstCoachID += _objCoach1.STAM_CREATED_USER;
                    if (AsstCoachID != null)
                    {
                        DateTime _dat = utility.FormatToDate(_date).Value;
                        response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkNewCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                    }
                    //string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByGroupName?Name=" + _objCoach1.STAM_ASSTCOACH_GROUPID + "&UserID=" + AsstCoachID).Result;
                    //_objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);
                }



                //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objCoachRef = json_serializer.Deserialize<STAM_TR_WORKOUTS_Model>(response);

                if (objCoachRef != null)
                {
                    returnMsg = "A workout already exists for this Date and Title";
                }
                //}

                return Json(new { Error = false, Message = returnMsg, data = (objCoachRef != null) ? objCoachRef.STAM_WORKOUT_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking MailAddress", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion



    }
}