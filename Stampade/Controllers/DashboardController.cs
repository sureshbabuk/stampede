﻿using BusinessObjects.Log;
using BusinessObjects.Utility;
using Stampade.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Stampade.Controllers
{
    public class DashboardController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        Utility utility = new Utility();

        public ActionResult Dashboard()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            return View();
        }

        public ActionResult DashboardResultTable()
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                var client = new HttpClient();
                DateTime TodayDay = DateTime.Today;
                var firstDayOfMonth = new DateTime(TodayDay.Year, TodayDay.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime StartDay = firstDayOfMonth;
                DateTime EndDate = lastDayOfMonth;
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                if (TodayDay > EndDate)
                {
                    var response = client.GetAsync(sWebApiurl + "api/Dashboard/GetNewWorkoutResults?UserId=" + Session["USERS_ID"] + "&StartDay=" + StartDay + "&EndDate=" + TodayDay).Result;
                    var responseAsString1 = response.Content.ReadAsStringAsync();
                    lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
                }
                else
                {
                    var response = client.GetAsync(sWebApiurl + "api/Dashboard/GetNewWorkoutResults?UserId=" + Session["USERS_ID"] + "&StartDay=" + StartDay + "&EndDate=" + EndDate).Result;
                    var responseAsString1 = response.Content.ReadAsStringAsync();
                    lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
                }
                return View(lstNewWorkout);
            }
            catch (TaskCanceledException ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : DashboardResultTable", "Problem In getting a GetAllWorkouts", ex.Message.ToString());
                objLog = null;
                return View();
            }
        }

        public ActionResult DashboardSectionResultTable(int SectionId)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                dynamic mymodel = new ExpandoObject();
                if (SectionId == 0)
                {
                    mymodel.workout = null;
                    mymodel.sectionlist = null;
                    return View(mymodel);
                }
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();
                string GroupName = string.Empty;
                int i = 0;
                var client = new HttpClient();
                DateTime TodayDay = DateTime.Today;
                var firstDayOfMonth = new DateTime(TodayDay.Year, TodayDay.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime StartDay = firstDayOfMonth;
                DateTime EndDate = lastDayOfMonth;
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                if (TodayDay > EndDate)
                {
                    var response = client.GetAsync(sWebApiurl + "api/Dashboard/GetNewSectionWorkoutResults?SectionId=" + SectionId + "&Userid=" + Session["USERS_ID"] + "&StartDay=" + StartDay + "&EndDate=" + TodayDay).Result;
                    var responseAsString1 = response.Content.ReadAsStringAsync();
                    lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
                }
                else
                {
                    var response = client.GetAsync(sWebApiurl + "api/Dashboard/GetNewSectionWorkoutResults?SectionId=" + SectionId + "&Userid=" + Session["USERS_ID"] + "&StartDay=" + StartDay + "&EndDate=" + EndDate).Result;
                    var responseAsString1 = response.Content.ReadAsStringAsync();
                    lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
                }
                foreach (var item in lstNewWorkout)
                {
                    if (i == 0)
                    {
                        GroupName = item.WorkoutGroupName;
                        i = 0;
                    }
                }
                string response1 = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?Name=" + GroupName + "&UserId=" + Session["USERS_ID"]).Result;
                JavaScriptSerializer json_serializer1 = new JavaScriptSerializer();
                _objAth = json_serializer1.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response1);
                mymodel.workout = lstNewWorkout.ToList();
                mymodel.sectionlist = _objAth.ToList();
                return View(mymodel);
            }
            catch (TaskCanceledException ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : DashboardResultTable", "Problem In getting a GetAllWorkouts", ex.Message.ToString());
                objLog = null;
                return View();
            }
        }


        public ActionResult DashboardReport(int id)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }

                ViewData["section"] = id;
                return View(id);
            }
            catch (Exception ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : DashboardReport", "Problem In getting a DashboardReport", ex.Message.ToString());
                objLog = null;
                return View();
            }
        }

        public FileResult Chart(int SectionId)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();
                string GroupName = string.Empty;
                int i = 0;
                var client = new HttpClient();
                DateTime TodayDay = DateTime.Today;
                DateTime sundayOfThisWeek = TodayDay.AddDays(-(int)TodayDay.DayOfWeek);
                DateTime saturdayOfThisWeek = sundayOfThisWeek.AddDays(6);
                var firstDayOfMonth = new DateTime(TodayDay.Year, TodayDay.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime StartDay = firstDayOfMonth;
                DateTime EndDate = lastDayOfMonth;                
                List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                var response = client.GetAsync(sWebApiurl + "api/Dashboard/GetNewWorkoutResults?UserId=" + Session["USERS_ID"] + "&StartDay=" + sundayOfThisWeek + "&EndDate=" + saturdayOfThisWeek).Result;
                var responseAsString1 = response.Content.ReadAsStringAsync();
                lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
                List<SectionResults> section = new List<SectionResults>();
                IList<string> strings = new List<string>();
                IList<string> number = new List<string>();
                DateTime checktime;
                List<DateTime> dateList = new List<DateTime>();
                if (TodayDay > EndDate)
                {
                    lastDayOfMonth = TodayDay;
                }
                for (DateTime datecheck = firstDayOfMonth; datecheck.Date <= lastDayOfMonth.Date; datecheck = datecheck.AddDays(1))
                {
                    SectionResults sectione = new SectionResults();
                    var checkworkout = lstNewWorkout.Where(p => p.WorkoutGroupId == SectionId).ToList();
                    var checcount = checkworkout.Where(p => p.WorkoutDate == datecheck).ToList();
                    if (checcount.Count != 0)
                    {
                        int TotalThisWeek = 0;
                        foreach (var item in lstNewWorkout.Where(p => p.WorkoutGroupId == SectionId && p.WorkoutDate == datecheck).ToList())
                        {
                            if (item.ListDetails.Count != 0)
                            {
                                foreach (var details in item.ListDetails.GroupBy(x => x.SectionID).ToList().FirstOrDefault())
                                {
                                    TotalThisWeek = TotalThisWeek + Convert.ToInt32(details.Run);
                                }
                            }
                            DateTime dates = Convert.ToDateTime(item.WorkoutDate);
                            string dateformat = Convert.ToString(datecheck.Day);
                            strings.Add(dateformat);
                            number.Add(Convert.ToString(TotalThisWeek));
                        }
                    }
                    else
                    {
                        string dateformat = Convert.ToString(datecheck.Day);
                        strings.Add(dateformat);
                        number.Add(Convert.ToString(0));
                    }
                    section.Add(sectione);
                }
                string joined = string.Join(",", strings);
                string joinednumber = string.Join(",", number);
                string[] joinarray = joined.Split(',');
                string[] joinnumber = joinednumber.Split(',');
                var chart = new Chart(width: 400, height: 400, theme: ChartTheme.Green)
                .AddTitle("DistanceRunPerWeek")
                .SetXAxis("Month", 1, 31)
                .SetYAxis("Meters")
                .AddSeries(
                             chartType: "column",
                             xValue: joinarray,
                             yValues: joinnumber)
                            .GetBytes("png");
                return File(chart, "image/bytes");
            }
            catch (Exception ex)
            {
                Logger objLog = new Logger();
                objLog.LogToFile("Error", "Dashboard : DashboardReport", "Problem In getting a DashboardReport", ex.Message.ToString());
                objLog = null;
                return null;
            }
        }
    }
}