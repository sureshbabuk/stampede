﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;

namespace Stampade.Controllers
{
     public class DeviceController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ManageDevices(string Msg)
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }
        public ActionResult Device_Table()
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<Devices> _objWorkouts = new List<Devices>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Athlete/ListDevices").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objWorkouts = json_serializer.Deserialize<List<Devices>>(response);

            return View(_objWorkouts);
        }

        public ActionResult CreateNewDevice(int? CoachId, string _copy)
        {



            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            Devices objCoachRef = new Devices();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);


            string response = "";

            if (CoachId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Athlete/EditDevice?DeviceID=" + CoachId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objCoachRef = json_serializer.Deserialize<Devices>(response);

            if (_copy != null)
            {
                ViewData["CopyEdit"] = "copy";
            }


            return View(objCoachRef);
        }




        public JsonResult NewDeviceCreation(Request _objdev,string _copy="")
        {

            Request _objDev = new Request();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            HttpResponseMessage response = new HttpResponseMessage();// = client.GetStringAsync(sWebApiurl + "api/Athlete/saveDevice?objRequest=" + _objdev).Result;


            if (_objdev.ID == 0)
            {
                response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/saveDevice?", _objdev).Result;
            }
            else
            {
                response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/updateDevice?", _objdev).Result;
            }



           // _objDev = json_serializer.Deserialize<List<TR_WORKOUTS>>(response);

            bool error = false;

         string   retunMsg = "Device Created Successfully";
         if (response.StatusCode == HttpStatusCode.NoContent)
         {
             error = true;
             retunMsg = "Device name and Device Id already exists...";
         }
         else
         {
             error = false;
             if (_objdev.ID == 0)
             {
                 retunMsg = "Device Created Successfully";
             }
             else
             {
                 retunMsg = "Device Updated Successfully";
             }
         }

         return Json(new { Error = error, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
           // return null;

        }




        public JsonResult DeleteDevice(Request _objdev)
        {


            Request _objDev = new Request();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            HttpResponseMessage response = new HttpResponseMessage();// = client.GetStringAsync(sWebApiurl + "api/Athlete/saveDevice?objRequest=" + _objdev).Result;


          
                response = client.PostAsJsonAsync(sWebApiurl + "api/Athlete/DeleteDevice?", _objdev).Result;
          



            // _objDev = json_serializer.Deserialize<List<TR_WORKOUTS>>(response);

            bool error = false;

            string retunMsg = "Device deleted Successfully";
            if (response.StatusCode == HttpStatusCode.NoContent)
            {
                error = true;
                retunMsg = "Device name and Device Id already exists...";
            }
            else
            {
                error = false;
                if (_objdev.ID == 0)
                {
                    retunMsg = "Device Created Successfully";
                }
                else
                {
                    retunMsg = "Device deleted Successfully";
                }
            }

            return Json(new { Error = error, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            // return null;

        }
    }
}