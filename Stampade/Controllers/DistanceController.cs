﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
namespace Stampade.Controllers
{
    public class DistanceController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Team
        public ActionResult Index()
        {
            return View();
        }
        # region Manage Distance

        public ActionResult ManageDistance(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if(Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }
          
            return View();
        }

        public ActionResult Distance_table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_DISTANCE> _objDistance = new List<STAM_MA_DISTANCE>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Distance/DistancesList").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objDistance = json_serializer.Deserialize<List<STAM_MA_DISTANCE>>(response);

            return View(_objDistance);
        }

        #endregion

        # region Create NewTeam
       
        public ActionResult CreateNewDistance(int? DistanceId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_DISTANCE objDistanceRef = new STAM_MA_DISTANCE();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (DistanceId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Distance/GetDistance?iDistance=" + DistanceId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objDistanceRef = json_serializer.Deserialize<STAM_MA_DISTANCE>(response);

            return View(objDistanceRef);
        }

        public ActionResult Distance_View(int? DistanceId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_DISTANCE objDistanceRef = new STAM_MA_DISTANCE();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (DistanceId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Distance/GetDistance?iDistance=" + DistanceId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objDistanceRef = json_serializer.Deserialize<STAM_MA_DISTANCE>(response);

            return View(objDistanceRef);
        }

        public JsonResult DistanceCreation(STAM_MA_DISTANCE obj_CreateDistance)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_DISTANCE _objDistanceRef = new STAM_MA_DISTANCE();
               
                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_CreateDistance.STAM_DISTANCE_ID == 0)
                {
                    obj_CreateDistance.STAM_DISTANCE_STATUS = BusinessConstants.Status.Active;
                    obj_CreateDistance.STAM_CREATED_USER = UserID;
                    obj_CreateDistance.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateDistance.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateDistance.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Distance/GetCreateDistance", obj_CreateDistance).Result;

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        retunMsg = "Distance Name Created Successfully";
                    }
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];                    
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);
                   
                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Distance/GetDistance?iDistance=" + obj_CreateDistance.STAM_DISTANCE_ID).Result;

                    STAM_MA_DISTANCE objDistanceRef = new STAM_MA_DISTANCE();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objDistanceRef = json_serializer.Deserialize<STAM_MA_DISTANCE>(responses);

                    objDistanceRef.STAM_DISTANCE_NAME = obj_CreateDistance.STAM_DISTANCE_NAME;
                    objDistanceRef.STAM_LAST_UPDATED_USER = UserID;
                    objDistanceRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Distance/GetCreateDistance", objDistanceRef).Result;
                    retunMsg = "Distance Name Updated Successfully";
                }
                
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Distance", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DeleteDistance(long DistanceId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_DISTANCE objDistanceRef = new STAM_MA_DISTANCE();
                               
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);
                    string response = "";
                    if (DistanceId > 0)
                    {
                        response = client.GetStringAsync(sWebApiurl + "api/Distance/GetDistance?iDistance=" + DistanceId).Result;
                    }

                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objDistanceRef = json_serializer.Deserialize<STAM_MA_DISTANCE>(response);

                    objDistanceRef.STAM_DISTANCE_STATUS = BusinessConstants.Status.Deleted;
                    objDistanceRef.STAM_LAST_UPDATED_USER = UserID;
                    objDistanceRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();                    
                    string response_Ref = "";
                    if (objDistanceRef.STAM_DISTANCE_ID > 0)
                    {
                        response_Ref = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkoutByDistance?iWorkouts=" + objDistanceRef.STAM_DISTANCE_NAME.Trim()).Result;
                    }
                    objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(response_Ref);
                    if (objWorkoutRef != null)
                    {
                         return Json(new { Error = false, Message = "You are not able to delete this Distance because Distance has Assigned to  Workout", Status = 200 }, JsonRequestBehavior.AllowGet);
                    }

                    STAM_MA_COACH_WORKOUT _objCoach = new STAM_MA_COACH_WORKOUT();
                    string response_Ref1 = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByDistance?iDistance=" + objDistanceRef.STAM_DISTANCE_NAME.Trim()).Result;
                    _objCoach = json_serializer.Deserialize<STAM_MA_COACH_WORKOUT>(response_Ref1);

                    if (_objCoach != null)
                    {
                        return Json(new { Error = false, Message = "You are not able to delete this Distance because Distance has Assigned to Coach/AsstCoach Workout", Status = 200 }, JsonRequestBehavior.AllowGet);
                    }

                    HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Distance/GetCreateDistance", objDistanceRef).Result;

                string returnMsg = "Selected Distance Has Been Deleted Successfully ";

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Distance", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChkDistanceName(string Name)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_DISTANCE objDistanceRef = new STAM_MA_DISTANCE();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (Name != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Distance/GetDistanceByName?Name=" + Name).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objDistanceRef = json_serializer.Deserialize<STAM_MA_DISTANCE>(response);

                string returnMsg = "";
                if (objDistanceRef != null)
                {
                    returnMsg = "Distance Name Already Exists";

                }
                return Json(new { Error = false, Message = returnMsg, data =(objDistanceRef != null)?objDistanceRef.STAM_DISTANCE_ID :0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking Distance Name", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DistanceActionUpdate(long DistanceId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                STAM_MA_DISTANCE objDistanceRef = new STAM_MA_DISTANCE();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (DistanceId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Distance/GetDistance?iDistance=" + DistanceId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objDistanceRef = json_serializer.Deserialize<STAM_MA_DISTANCE>(response);
                
                objDistanceRef.STAM_LAST_UPDATED_USER = UserID;
                objDistanceRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                objDistanceRef.STAM_DISTANCE_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);               

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Distance/GetCreateDistance", objDistanceRef).Result;
                retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Group", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        

        #endregion
    }
}