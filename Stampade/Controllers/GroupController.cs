﻿using BusinessObjects.Constants;
using Stampade.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Stampade.Controllers
{
    public class GroupController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Group
        public ActionResult Index()
        {
            return View();
        }

        # region Manage Groups

        public ActionResult ManageGroup(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult Group_table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_GROUP> _objGroup = new List<STAM_MA_GROUP>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroups").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(response);

            return View(_objGroup);
        }

        #endregion

        # region Create NewGroup

        public ActionResult CreateNewGroup(int? GroupId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_GROUP objGroupRef = new STAM_MA_GROUP();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (GroupId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroup?iGroup=" + GroupId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objGroupRef = json_serializer.Deserialize<STAM_MA_GROUP>(response);

            return View(objGroupRef);
        }

        public ActionResult Group_View(int? GroupId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_GROUP objGroupRef = new STAM_MA_GROUP();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (GroupId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroup?iGroup=" + GroupId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objGroupRef = json_serializer.Deserialize<STAM_MA_GROUP>(response);

            return View(objGroupRef);
        }

        public JsonResult GroupCreation(STAM_MA_GROUP obj_CreateGroup)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_GROUP objJobRef = new STAM_MA_GROUP();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_CreateGroup.STAM_GROUP_ID == 0)
                {
                    obj_CreateGroup.STAM_GROUP_STATUS = BusinessConstants.Status.Active;
                    obj_CreateGroup.STAM_CREATED_USER = UserID;
                    obj_CreateGroup.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateGroup.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateGroup.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Group/GetCreateGroup", obj_CreateGroup).Result;

                    retunMsg = "Group Created Successfully";
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Group/GetGroup?iGroup=" + obj_CreateGroup.STAM_GROUP_ID).Result;

                    STAM_MA_GROUP objGroupRef = new STAM_MA_GROUP();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objGroupRef = json_serializer.Deserialize<STAM_MA_GROUP>(responses);

                    objGroupRef.STAM_LAST_UPDATED_USER = UserID;
                    objGroupRef.STAM_GROUP_NAME = obj_CreateGroup.STAM_GROUP_NAME;
                    objGroupRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Group/GetCreateGroup", objGroupRef).Result;
                    retunMsg = "Group Updated Successfully";
                }

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Group", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DeleteGroup(long GroupId)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_GROUP objGroupRef = new STAM_MA_GROUP();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (GroupId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroup?iGroup=" + GroupId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objGroupRef = json_serializer.Deserialize<STAM_MA_GROUP>(response);

                objGroupRef.STAM_GROUP_STATUS = BusinessConstants.Status.Deleted;
                objGroupRef.STAM_LAST_UPDATED_USER = UserID;
                objGroupRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                //if (objTeamRef.STAM_COACHASSIGNED == true)
                //{
                //    return Json(new { Error = false, Message = "You are not able to delete this Coach because Coach has Assigned", Status = 200 }, JsonRequestBehavior.AllowGet);
                //}

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Group/GetCreateGroup", objGroupRef).Result;

                string returnMsg = "Selected Group Has Been Deleted Successfully ";

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChkGroupName(string Name)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_GROUP objGroupRef = new STAM_MA_GROUP();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (Name != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroupByName?Name=" + Name).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objGroupRef = json_serializer.Deserialize<STAM_MA_GROUP>(response);

                string returnMsg = "";
                if (objGroupRef != null)
                {
                    returnMsg = "Group Name Already Exists";

                }
                return Json(new { Error = false, Message = returnMsg, data = (objGroupRef != null) ? objGroupRef.STAM_GROUP_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking Group Name", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditorUpdateGroup(long groupID)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            
            string ReturnMsg = "";
           
            if (groupID != 0)
            {
                List<STAM_MA_ASSTCOACH> objCoachRef = new List<STAM_MA_ASSTCOACH>();
                string response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachByGroupID?igroup=" + groupID.ToString()).Result;                
                objCoachRef = json_serializer.Deserialize<List<STAM_MA_ASSTCOACH>>(response);
                if (objCoachRef.Count > 0)
                {
                    ReturnMsg = "true";
                }
                else
                {
                    List<STAM_MA_ATHLETE> objAth = new List<STAM_MA_ATHLETE>();
                    string response_Ref = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthleteByGroupID?GroupID=" + groupID).Result;
                    objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(response_Ref);
                    if (objAth.Count > 0)
                    {
                        ReturnMsg = "true";
                    }
                    else
                    {
                        List<STAM_MA_COACH_WORKOUT> objCoach = new List<STAM_MA_COACH_WORKOUT>();
                        string response_Coach = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkoutByGroupID?GroupID=" + groupID).Result;
                        objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response_Coach);
                        if (objCoach.Count > 0)
                        {
                            ReturnMsg = "true";
                        }
                    }
                }
            }
            
            return Json(new { Error = false, Message = ReturnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GroupActionUpdate(long GroupId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_TEAM objJobRef = new STAM_MA_TEAM();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);

                string responses = "";

                responses = client.GetStringAsync(sWebApiurl + "api/Group/GetGroup?iGroup=" + GroupId).Result;

                STAM_MA_GROUP objGroupRef = new STAM_MA_GROUP();
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objGroupRef = json_serializer.Deserialize<STAM_MA_GROUP>(responses);

                objGroupRef.STAM_LAST_UPDATED_USER = UserID;
                objGroupRef.STAM_LAST_UPDATED_DATE = DateTime.Now;
                objGroupRef.STAM_GROUP_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                response = client.PostAsJsonAsync(sWebApiurl + "api/Group/GetCreateGroup", objGroupRef).Result;
                retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Group", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }



        #endregion
    }
}