﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Stampade.MockClass;

namespace Stampade.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Calendar()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            return View();
        }

        public JsonResult GetCalendarEvents(double start, double end)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            int userId = Convert.ToInt32(Session["USERS_ID"]);
            var eventService = new MockWebServiceCallClass();
            var eventDetails = eventService.GetCalendarEvents(userId);

            var eventList = from item in eventDetails
                            select new
                            {
                                id = item.ID,
                                title = item.Title,
                                url = item.url,
                                start = item.Start.ToString("s"),
                                end = item.End.ToString("s"),
                                allDay = false,
                                editable = true
                            };

            return Json(eventList.ToArray(), JsonRequestBehavior.AllowGet);
        }
    }
}