﻿using BusinessObjects.Utility;
using Stampade.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Stampade.Controllers
{
    public class ResultForWorkoutsController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        Utility utility = new Utility();
        //
        // GET: /ResultForWorkouts/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManageResultsForWorkouts()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            return View();
        }

        public ActionResult WorkoutsResultTable(string StartDate, string EndDate, string GroupId, string SectionID, string AtheleteID, string WorkoutTitle,string Flag)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                if (Flag == "1")
                {
                    DateTime dStartDate = Convert.ToDateTime(StartDate);
                    DateTime dEndDate = Convert.ToDateTime(EndDate);
                    List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    var client = new HttpClient();
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);
                    var response = client.GetAsync(sWebApiurl + "api/WorkOutResult/GetWorkouts?StartDate=" + dStartDate + "&EndDate=" + dEndDate + "&GroupId=" + GroupId + "&SectionID=" + SectionID + "&AtheleteID=" + AtheleteID + "&WorkoutTitle=" + WorkoutTitle + "&UserId=" + Session["USERS_ID"]).Result;
                    var responseAsString1 = response.Content.ReadAsStringAsync();
                    lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
                    return View(lstNewWorkout);
                }
                return View();
            }
            catch(Exception ex)
            {
                return null;
            }
        }
  
	}
}