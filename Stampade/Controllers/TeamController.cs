﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
namespace Stampade.Controllers
{
    public class TeamController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Team
        public ActionResult Index()
        {
            return View();
        }
        # region Manage Team

        public ActionResult ManageTeam(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if(Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }
          
            return View();
        }

        public ActionResult Team_table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_TEAM> _objTeam = new List<STAM_MA_TEAM>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Team/GetTeams?UserId=" + UserID).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objTeam = json_serializer.Deserialize<List<STAM_MA_TEAM>>(response);

            return View(_objTeam);
        }

        #endregion

        # region Create NewTeam
       
        public ActionResult CreateNewTeam(int? TeamId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (TeamId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + TeamId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(response);

            return View(objTeamRef);
        }


        public ActionResult Team_View(int? TeamId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (TeamId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + TeamId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(response);

            if (objTeamRef.STAM_ASSIGN_COACHID != 0)
            {
                STAM_MA_COACH _objCoach = new STAM_MA_COACH();
                string responses = client.GetStringAsync(sWebApiurl + "api/Coach/GetCoach?iCoach=" + objTeamRef.STAM_ASSIGN_COACHID).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_COACH>(responses);
                objTeamRef.CoachName = _objCoach.STAM_COACH_FIRSTNAME + "  " + _objCoach.STAM_COACH_LASTNAME;
            }
            return View(objTeamRef);
        }
      

        public JsonResult TeamCreation(STAM_MA_TEAM obj_CreateTeam)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_TEAM objJobRef = new STAM_MA_TEAM();
               
                var retunMsg = "";

                if (obj_CreateTeam.STAM_TEAM_ID == 0)
                {
                    obj_CreateTeam.STAM_STATUS = BusinessConstants.Status.Active;
                    obj_CreateTeam.STAM_CREATED_USER = UserID;
                    obj_CreateTeam.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateTeam.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateTeam.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    HttpResponseMessage response_Ref = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", obj_CreateTeam).Result;

                    if (response_Ref.StatusCode == HttpStatusCode.OK)
                    {
                        retunMsg = "Team Name Created Successfully";
                    }
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];                    
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);
                   
                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + obj_CreateTeam.STAM_TEAM_ID).Result;

                    STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);
                   
                    objTeamRef.STAM_LAST_UPDATED_USER = UserID;
                    objTeamRef.STAM_TEAM_NAME = obj_CreateTeam.STAM_TEAM_NAME;
                    objTeamRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    HttpResponseMessage response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    retunMsg = "Team Name Updated Successfully";
                }


                //if (response.StatusCode == HttpStatusCode.Ambiguous)
                //{
                //    return Json(new { Error = true, Message = "Team Name is already exist.", Status = 300 }, JsonRequestBehavior.AllowGet);
                //}

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DeleteTeam(long TeamId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                               
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);
                    string response = "";
                    if (TeamId > 0)
                    {
                    response = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + TeamId).Result;
                    }

                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(response);

                    objTeamRef.STAM_STATUS = BusinessConstants.Status.Deleted;
                    objTeamRef.STAM_LAST_UPDATED_USER = UserID;
                    objTeamRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    if (objTeamRef.STAM_COACHASSIGNED == true)
                    {
                        return Json(new { Error = false, Message = "Sorry, cannot be deleted as this team is assigned to coach admin", Status = 200 }, JsonRequestBehavior.AllowGet);
                    }

                    HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    string returnMsg = "";
                    if (responses.StatusCode == HttpStatusCode.OK)
                    {
                        returnMsg = "Selected Team Has Been Deleted Successfully ";
                    }

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChkTeamName(string Name)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (Name != null)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Team/GetTeamByName?Name=" + Name).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(response);

                string returnMsg = "";
                if (objTeamRef != null)
                {
                    returnMsg = "Team Name Already Exist";

                }
                return Json(new { Error = false, Message = returnMsg, data = (objTeamRef != null) ? objTeamRef.STAM_TEAM_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking Team Name", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TeamActionUpdate(long TeamId,string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_TEAM objJobRef = new STAM_MA_TEAM();

                var retunMsg = "";
               
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + TeamId).Result;

                    STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);

                    objTeamRef.STAM_LAST_UPDATED_USER = UserID;
                    objTeamRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    objTeamRef.STAM_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);

                    HttpResponseMessage response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");                

                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Team", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        

        #endregion
    }
}