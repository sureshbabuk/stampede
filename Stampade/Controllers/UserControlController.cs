﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
using System.Threading.Tasks;

namespace Stampade.Controllers
{      
    public class UserControlController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult GetAsstAthlete(string Name)
        //{
        //    if (Session["USERS_ID"] == null)
        //    {
        //        Response.Redirect("~/Account/Login");
        //    }

        //    long UserID = 0;
        //    long.TryParse(Session["USERS_ID"].ToString(), out UserID);

        //    List<STAM_MA_ASST_ATHLETE> _objATh = new List<STAM_MA_ASST_ATHLETE>();

        //    var client = new HttpClient();
        //    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        //    client.DefaultRequestHeaders.Add(strAppName, strUserId);

        //    string response = client.GetStringAsync(sWebApiurl + "api/AsstCoachAthlete/GetAthletes?AthID=" + Session["USERS_ID"]).Result;
        //    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //    _objATh = json_serializer.Deserialize<List<STAM_MA_ASST_ATHLETE>>(response);


        //    ViewData["AthName"] = Name;
        //    //  return View(_objATh.Where(x => x.STAM_ASST_ATH_STATUS == BusinessConstants.Status.Active && x.STAM_ASST_ATH_SECTIONID == 0 && x.STAM_ASST_ATH_SECTION == null).ToList());
        //    return View(_objATh.Where(x => x.STAM_ASST_ATH_STATUS == BusinessConstants.Status.Active).ToList());
        //}

        //public ActionResult GetAsstAthleteByGroup(string Name)
        //{
        //    if (Session["USERS_ID"] == null)
        //    {
        //        Response.Redirect("~/Account/Login");
        //    }

        //    long UserID = 0;
        //    long.TryParse(Session["USERS_ID"].ToString(), out UserID);

        //    List<STAM_MA_ASST_ATHLETE> _objATh = new List<STAM_MA_ASST_ATHLETE>();

        //    var client = new HttpClient();
        //    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        //    client.DefaultRequestHeaders.Add(strAppName, strUserId);

        //    string response = client.GetStringAsync(sWebApiurl + "api/AsstCoachAthlete/GetAthleteByGroup?Group=" + Name + "&UserID=" + Session["USERS_ID"]).Result;
        //    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //    _objATh = json_serializer.Deserialize<List<STAM_MA_ASST_ATHLETE>>(response);


        //    ViewData["AthName"] = Name;
        //    //  return View(_objATh.Where(x => x.STAM_ASST_ATH_STATUS == BusinessConstants.Status.Active && x.STAM_ASST_ATH_SECTIONID == 0 && x.STAM_ASST_ATH_SECTION == null).ToList());
        //    return View(_objATh.Where(x => x.STAM_ASST_ATH_STATUS == BusinessConstants.Status.Active && x.STAM_ASST_ATH_SECTIONID == 0 && x.STAM_ASST_ATH_SECTION == null).ToList());
        //}

        public ActionResult GetAsstCoachGroup(string GroupName)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);


            STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachByMailID?mailID=" + Session["USER_ID"]).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);
            
            List<STAM_MA_GROUP> _objGroup = new List<STAM_MA_GROUP>();
            string[] groupID =null;

            if (_objCoach != null)
            {
                groupID = _objCoach.STAM_ASSTCOACH_GROUPID.ToString().TrimEnd(',').Split(',');

                STAM_MA_GROUP _objgrp = new STAM_MA_GROUP();
               

                foreach (var item in groupID)
                {
                    string response_Group = client.GetStringAsync(sWebApiurl + "api/Group/GetGroup?iGroup=" + item).Result;
                    _objgrp = json_serializer.Deserialize<STAM_MA_GROUP>(response_Group);

                    if (_objgrp != null)
                    {
                        _objGroup.Add(_objgrp);
                    }
                }
            }
            else
            {
                string response_grp = client.GetStringAsync(sWebApiurl + "api/Group/GetGroups").Result;
                _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(response_grp);
            }

            ViewData["GroupName"] = GroupName;
            return View(_objGroup.Where(x => x.STAM_GROUP_STATUS == BusinessConstants.Status.Active).ToList());
        }

        public ActionResult GetSingleGroup(string GroupName)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_GROUP> _objGroup = new List<STAM_MA_GROUP>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroups").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(response);


            ViewData["GroupName"] = GroupName;
            return View(_objGroup.Where(x => x.STAM_GROUP_STATUS == BusinessConstants.Status.Active).ToList());
        }

        public ActionResult GetSingleGroupByUserID(string GroupName)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
            List<STAM_MA_GROUP> _objGroup = new List<STAM_MA_GROUP>();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            if (Session["UserType"].ToString() == "2" || Session["UserType"].ToString() == "1")//Coach
            {
                //string response = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;               
                //_objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response);

                string responses = client.GetStringAsync(sWebApiurl + "api/Group/GetGroups").Result;
                _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(responses);
            }
            else if (Session["UserType"].ToString() == "3")//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach1 = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach1 = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);

                string responses = client.GetStringAsync(sWebApiurl + "api/Group/GetGroupsByCoachID?CoachID=" + _objCoach1.STAM_ASSTCOACH_GROUPID).Result;
                _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(responses);
            }
            else
            {
                STAM_MA_ATHLETE _objAth = new STAM_MA_ATHLETE();
                string response1 = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + Session["ResourceId"]).Result;
                _objAth = json_serializer.Deserialize<STAM_MA_ATHLETE>(response1);

                string responses = client.GetStringAsync(sWebApiurl + "api/Group/GetGroupsByCoachID?CoachID=" + _objAth.STAM_ATH_ASSIGNGROUPID).Result;
                _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(responses);
            }

            ViewData["GroupName"] = GroupName;
            return View(_objGroup.Where(x => x.STAM_GROUP_STATUS == BusinessConstants.Status.Active).ToList());
        }
        

        public ActionResult GetGroup(string GroupName)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_GROUP> _objGroup = new List<STAM_MA_GROUP>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Group/GetGroups").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objGroup = json_serializer.Deserialize<List<STAM_MA_GROUP>>(response);


            ViewData["GroupName"] = GroupName;
            return View(_objGroup.Where(x => x.STAM_GROUP_STATUS == BusinessConstants.Status.Active).ToList());
        }

        public ActionResult GetAthleteSection(string Name,string grpId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            long userID= 0;

            string AsstCoachID = "";

            if (Session["UserType"].ToString() == "2")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += Session["USERS_ID"];              
            }
            else if (Session["UserType"].ToString() == "3")//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += _objCoach.STAM_CREATED_USER;               
            }
             else//Athlete 
            {
                STAM_MA_ATHLETE _objAthRef = new STAM_MA_ATHLETE();
                string response1 = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + Session["ResourceId"]).Result;
                _objAthRef = json_serializer.Deserialize<STAM_MA_ATHLETE>(response1);

                AsstCoachID = _objAthRef.STAM_CREATED_USER.ToString();
               
            }

            if (AsstCoachID != "" && AsstCoachID != null && grpId != null)
            {
                string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthSecByGroupList?Group=" + grpId + "&UserID=" + AsstCoachID).Result;
                _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response);
            }

            if(grpId == null || grpId =="")
            {
                return View();
            }


            ViewData["AthName"] = Name;
            return View(_objAth.Where(x => x.STAM_ATHSEC_STATUS == BusinessConstants.Status.Active).ToList());
        }

        //public ActionResult GetAsstAthleteSection(string Name)
        //{
        //    if (Session["USERS_ID"] == null)
        //    {
        //        Response.Redirect("~/Account/Login");
        //    }

        //    long UserID = 0;
        //    long.TryParse(Session["USERS_ID"].ToString(), out UserID);

        //    List<STAM_MA_ASST_ATHLETE_SECTION> _objAth = new List<STAM_MA_ASST_ATHLETE_SECTION>();
        //    JavaScriptSerializer json_serializer = new JavaScriptSerializer();

        //    var client = new HttpClient();
        //    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        //    client.DefaultRequestHeaders.Add(strAppName, strUserId);


        //    STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();  
        //    string responses = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
        //    _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(responses);

        //    string response = client.GetStringAsync(sWebApiurl + "api/AsstAthleteSection/GetAthleteSectionsByUserGroupID?iAth=" + Session["USERS_ID"]+"&GroupID="+_objCoach.STAM_ASSTCOACH_GROUPID).Result;           
        //    _objAth = json_serializer.Deserialize<List<STAM_MA_ASST_ATHLETE_SECTION>>(response);


        //    ViewData["AthName"] = Name;
        //    return View(_objAth.Where(x => x.STAM_ASST_ATHSEC_STATUS == BusinessConstants.Status.Active).ToList());
        //}

       
        public ActionResult GetAthlete(string Name)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE> _objATh = new List<STAM_MA_ATHLETE>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthletes?AthId=" + Session["USERS_ID"]).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objATh = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(response);


            ViewData["AthName"] = Name;
            return View(_objATh.Where(x => x.STAM_ATH_STATUS == BusinessConstants.Status.Active && x.STAM_ATH_SECTIONID == 0 && x.STAM_ATH_SECTION == null).ToList());
}



        public ActionResult GetAthleteDevice(string Name = "",long ID=0)
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            string AsstCoachID = "";

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            List<Stam_Athlete> _objATh = new List<Stam_Athlete>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
            string UserSID = json_serializer.Deserialize<string>(UserSIDres);
            string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
            AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
            AsstCoachID += Session["USERS_ID"];



            string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthletesDevice?AthId=" + AsstCoachID).Result;
          //  JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objATh = json_serializer.Deserialize<List<Stam_Athlete>>(response);


            ViewData["AthName"] = Name;
            ViewData["AthleteID"] = ID;
            return View(_objATh.Where(x => x.STAM_ATH_STATUS == BusinessConstants.Status.Active).ToList());
        }


        public ActionResult GetDevice(string Name)
        {

            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            List<Devices> _objDist = new List<Devices>();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetDeviceList").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objDist = json_serializer.Deserialize<List<Devices>>(response);

            if (Name != null)
            {
                ViewData["DeviceName"] = Name;
            }

            return View(_objDist.OrderBy(x => x.DeviceName).ToList());
        }

        public ActionResult GetAthleteByGroup(string Name)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE> _objAth = new List<STAM_MA_ATHLETE>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            //string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthleteByGroup?grp=" + Name ).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string AsstCoachID ="";

            //_objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(response);

            if (Session["UserType"].ToString() == "2")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += Session["USERS_ID"];               
            }
            else if (Session["UserType"].ToString() == "3")//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach1 = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach1 = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                AsstCoachID += _objCoach1.STAM_CREATED_USER;               
            }


            if (Name != null && AsstCoachID != null && Name !="")
            {
                string responseRef = client.GetStringAsync(sWebApiurl + "api/Athlete/GetCoachAdminGroupList?grp=" + Name + "&UserID=" + AsstCoachID).Result;
                _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(responseRef);
            }

            ViewData["AthName"] = Name;
                return View(_objAth.Where(x => x.STAM_ATH_STATUS == BusinessConstants.Status.Active && x.STAM_ATH_SECTIONID == 0 && x.STAM_ATH_SECTION == null).ToList());
           
        }

        public ActionResult GetTeam(string Name)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_TEAM> _objTeam = new List<STAM_MA_TEAM>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Team/GetTeams?UserId="+UserID).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objTeam = json_serializer.Deserialize<List<STAM_MA_TEAM>>(response);

            if (Name != null)
            {
                ViewData["TeamName"] = Name;
            }

            return View(_objTeam.Where(x => x.STAM_COACHASSIGNED == false && x.STAM_STATUS == BusinessConstants.Status.Active).ToList());
        }

        public ActionResult GetWorkout(string Name)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_WORKOUTS> _objGroup = new List<STAM_MA_WORKOUTS>();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string workoutCreatedUserId = "";

            if (Session["UserType"].ToString() == "2" || Session["UserType"].ToString()=="1")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                workoutCreatedUserId = json_serializer.Deserialize<string>(AsstCoachIDres);
                workoutCreatedUserId += Session["USERS_ID"]+",";
            }
            else if (Session["UserType"].ToString() == "3")//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                workoutCreatedUserId = json_serializer.Deserialize<string>(AsstCoachIDres);
                workoutCreatedUserId += _objCoach.STAM_CREATED_USER;
            }
            else//Athlete 
            {
                STAM_MA_ATHLETE _objAthRef = new STAM_MA_ATHLETE();
                string response1 = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + Session["ResourceId"]).Result;
                _objAthRef = json_serializer.Deserialize<STAM_MA_ATHLETE>(response1);
                workoutCreatedUserId = _objAthRef!=null?_objAthRef.STAM_CREATED_USER.ToString():"" + "," + 1;
            }

            string response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkouts?createdUser=" + workoutCreatedUserId).Result;          
            _objGroup = json_serializer.Deserialize<List<STAM_MA_WORKOUTS>>(response);


            ViewData["WorkName"] = Name;
            if (_objGroup!=null)
            {
                View(_objGroup.Where(x => x.STAM_WORKOUT_STATUS == BusinessConstants.Status.Active).ToList());
            }
            else{
                return View();
            }
            return View(_objGroup.Where(x => x.STAM_WORKOUT_STATUS == BusinessConstants.Status.Active).ToList());
        }


        public ActionResult GetWorkoutTitle(string Name)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_WORKOUTS> _objGroup = new List<STAM_MA_WORKOUTS>();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string workoutCreatedUserId = "";

            if (Session["UserType"].ToString() == "2" || Session["UserType"].ToString() == "1")//Coach
            {
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                workoutCreatedUserId = json_serializer.Deserialize<string>(AsstCoachIDres);
                workoutCreatedUserId += Session["USERS_ID"] + ",";
            }
            else if (Session["UserType"].ToString() == "3")//AsstCoach 
            {
                STAM_MA_ASSTCOACH _objCoach = new STAM_MA_ASSTCOACH();
                string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                _objCoach = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach.STAM_CREATED_USER).Result;
                string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach.STAM_CREATED_USER).Result;
                workoutCreatedUserId = json_serializer.Deserialize<string>(AsstCoachIDres);
                workoutCreatedUserId += _objCoach.STAM_CREATED_USER;
            }
            else//Athlete 
            {
                STAM_MA_ATHLETE _objAthRef = new STAM_MA_ATHLETE();
                string response1 = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthlete?iAth=" + Session["ResourceId"]).Result;
                _objAthRef = json_serializer.Deserialize<STAM_MA_ATHLETE>(response1);
                workoutCreatedUserId = _objAthRef != null ? _objAthRef.STAM_CREATED_USER.ToString() : "" + "," + 1;
            }

            string response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkouts?createdUser=" + workoutCreatedUserId).Result;
            _objGroup = json_serializer.Deserialize<List<STAM_MA_WORKOUTS>>(response);


            ViewData["WorkName"] = Name;
            if (_objGroup != null)
            {
                View(_objGroup.Where(x => x.STAM_WORKOUT_STATUS == BusinessConstants.Status.Active).ToList());
            }
            else
            {
                return View();
            }
            return View(_objGroup.Where(x => x.STAM_WORKOUT_STATUS == BusinessConstants.Status.Active).ToList());
        }

        public ActionResult GetDistance(string Name,string TabIndex="")
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Distance/DistancesList").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objDist = json_serializer.Deserialize<List<STAM_MA_DISTANCE>>(response);

            if (Name != null)
            {
                ViewData["DistName"] = Name;
            }
            if(!string.IsNullOrEmpty(TabIndex))
            {
                ViewData["Tab"] = TabIndex;
            }

            return View(_objDist.Where(x => x.STAM_DISTANCE_STATUS == BusinessConstants.Status.Active).OrderBy(x=>x.STAM_DISTANCE_NAME).ToList());
        }

        public ActionResult GetSummaryAthleteSection(string GroupName = "")
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthSecByNameList?Name=" + GroupName).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response);

            return View(_objAth.Where(x => x.STAM_ATHSEC_STATUS == BusinessConstants.Status.Active).Distinct().ToList());
        }



        public ActionResult GetAthleteSectionList(string GroupName = "")
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE_SECTION> _objAth = new List<STAM_MA_ATHLETE_SECTION>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAthleteSection?Name=" + GroupName + "&UserId=" + UserID).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objAth = json_serializer.Deserialize<List<STAM_MA_ATHLETE_SECTION>>(response);

            return View(_objAth.Where(x => x.STAM_ATHSEC_STATUS == BusinessConstants.Status.Active).Distinct().ToList());
        }

        public ActionResult SectionGrid(string GroupID = "", string Runs = "", string Distance="",string WorokoutID="")
        {


            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            
            long UserID = 0;
            long run;
            run=(long)Convert.ToDouble(Runs.ToString());


              long dist=0;

              if (Distance != "" && Distance != "--Select Distance --")
              {
                  dist = (Distance == "0" || string.IsNullOrEmpty(Distance)) ? 0 : (long)Convert.ToDouble(Distance.ToString());

              }


            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<AthleteSection> _objAth = new List<AthleteSection>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/AthleteSection/GetAllSectionsForGroup?GroupID=" + GroupID + "&UserID=" + UserID).Result;

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objAth = json_serializer.Deserialize<List<AthleteSection>>(response);
            List<string> lst = new List<string>();
            lst.AddRange(_objAth.Select(x => x.SectionName).ToList());

            List<string> lst1 = new List<string>();
            lst1.AddRange(_objAth.Select(y => y.SectionID).ToList());
            string response1 = "";
            if (WorokoutID != "0" && WorokoutID != "")
            {
                response1 = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/EditNewWorkoutSections?CoachID=" + WorokoutID).Result;
               // ViewData["WorkoutID"] = WorokoutID;

            }
            ViewData["WorkoutID"] = WorokoutID;
            WorkoutModel _objWorkOutModel = new WorkoutModel();

            json_serializer = new JavaScriptSerializer();
            _objWorkOutModel = json_serializer.Deserialize<WorkoutModel>(response1);
        

            Workout_Model wrkmodel=new Workout_Model();
            wrkmodel.Sections=lst;
            wrkmodel.Distance=(int)dist;
            wrkmodel.Runs=(int)run;
            wrkmodel.SectionIDs = lst1;
            if (WorokoutID != "0" && WorokoutID != "")
            {
                wrkmodel._objSTAM_TR_DETAILS = _objWorkOutModel._objSTAM_TR_DETAILS;
                wrkmodel._objSTAM_TR_DETAILS_SECTION = _objWorkOutModel._objSTAM_TR_DETAILS_SECTION;

            }
            ViewData["SecNo"] = lst.Count();
            TempData["SecNo"] = lst.Count();

           
            return View(wrkmodel);


        }
      

        //public ActionResult GetAsstSummaryAthleteSection(string GroupName)
        //{
        //    if (Session["USERS_ID"] == null)
        //    {
        //        Response.Redirect("~/Account/Login");
        //    }

        //    long UserID = 0;
        //    long.TryParse(Session["USERS_ID"].ToString(), out UserID);

        //    List<STAM_MA_ASST_ATHLETE_SECTION> _objAth = new List<STAM_MA_ASST_ATHLETE_SECTION>();

        //    var client = new HttpClient();
        //    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        //    client.DefaultRequestHeaders.Add(strAppName, strUserId);

        //    string response = client.GetStringAsync(sWebApiurl + "api/AsstAthleteSection/GetAthSecByNameList?Name=" + GroupName).Result;
        //    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //    _objAth = json_serializer.Deserialize<List<STAM_MA_ASST_ATHLETE_SECTION>>(response);

        //    return View(_objAth.Where(x => x.STAM_ASST_ATHSEC_STATUS == BusinessConstants.Status.Active).Distinct().ToList());
        //}



        public JsonResult GetDistanceList(string Name)
        {


            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);
            List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Distance/DistancesList").Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objDist = json_serializer.Deserialize<List<STAM_MA_DISTANCE>>(response);
            _objDist = _objDist.Where(x => x.STAM_DISTANCE_STATUS == BusinessConstants.Status.Active).OrderBy(x => x.STAM_DISTANCE_NAME).ToList();
            if (Name != null)
            {
                ViewData["DistName"] = Name;
            }

            return Json(new { Error = false, _objDist , Status = 200 }, JsonRequestBehavior.AllowGet);

          //  return View(_objDist.Where(x => x.STAM_DISTANCE_STATUS == BusinessConstants.Status.Active).OrderBy(x => x.STAM_DISTANCE_NAME).ToList());
        }

        public ActionResult GetAthleteForSection(string SectionName)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_ATHLETE> _objATh = new List<STAM_MA_ATHLETE>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthleteForSection?SectionName=" + SectionName).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objATh = json_serializer.Deserialize<List<STAM_MA_ATHLETE>>(response);

            if (_objATh != null)
            {
                return View(_objATh.Where(x => x.STAM_ATH_STATUS == BusinessConstants.Status.Active).ToList());
            }
            else
            {
                return View();
            }
        }


    }
}