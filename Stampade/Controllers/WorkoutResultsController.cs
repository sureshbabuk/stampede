﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
using System.Threading.Tasks;
using BusinessObjects.Utility;

namespace Stampade.Controllers
{
      
    public class WorkoutResultsController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        Utility utility = new Utility();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }
      
        # region Manage Workout Summary

        public ActionResult ManageWorkoutResults(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult WorkoutSummaryReport(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }
        

        public ActionResult WorkoutResults_Table(string GroupID, string _date, string SectionID)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<WorkoutResults> objWork = new List<WorkoutResults>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            if (GroupID != null && _date != null)
            {
                var response2 = client.GetAsync(sWebApiurl + "api/WorkOutResult/GetWorkouts?WorkoutDate=" + _date + "&GroupID=" + GroupID + "&SectionID=" + SectionID).Result;
                var responseAsString1 = response2.Content.ReadAsStringAsync();
                string  objWorksss = responseAsString1.Result;
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objWork = json_serializer.Deserialize<List<WorkoutResults>>(objWorksss);   
            }

            return View(objWork);
        }

        //public ActionResult WorkoutResults_Table(string GroupName, string _date, long AthName = 0)
        //{
        //    if (Session["USERS_ID"] == null)
        //    {
        //        Response.Redirect("~/Account/Login");
        //    }

        //    long UserID = 0;
        //    long.TryParse(Session["USERS_ID"].ToString(), out UserID);

        //    List<STAM_MA_COACH_WORKOUT> _objCoach = new List<STAM_MA_COACH_WORKOUT>();

        //    var client = new HttpClient();
        //    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        //    client.DefaultRequestHeaders.Add(strAppName, strUserId);

        //    if (GroupName != null && _date != null)
        //    {
        //        string response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkouts?CoachID=" + Session["USERS_ID"]).Result;
        //        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //        _objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);

        //        List<STAM_MA_COACH_WORKOUT> _objCoachRef = new List<STAM_MA_COACH_WORKOUT>();
        //        _objCoachRef = _objCoach.Where(x => (x.STAM_COACH_WORKOUT_GROUPNAME == GroupName) &&
        //                                    (x.STAM_COACH_WORKOUT_DATE == utility.FormatToDate(_date))).ToList();

        //        //List<STAM_MA_COACH_WORKOUT> _objCoach_result = new List<STAM_MA_COACH_WORKOUT>();
        //        // if( AthName != 0)
        //        // {
        //        //     List<STAM_MA_ATHLETE> objAth = new List<STAM_MA_ATHLETE>();  
        //        //     string responses = "";
        //        //     if (AthName > 0)
        //        //     {
        //        //         responses = client.GetStringAsync(sWebApiurl + "api/Athlete/GetAthleteByAthSec?iAth=" + AthName).Result;
        //        //     }
        //        //     objAth = json_serializer.Deserialize < List<STAM_MA_ATHLETE>>(responses);


        //        //          var test =_objCoach.Where(x => (x.STAM_COACH_WORKOUT_ATHID == item.STAM_ATH_ID)).ToList();
        //        //          if(test != null)
        //        //          {
        //        //              _objCoach_result.AddRange(test);
        //        //          }
        //        //      }
        //        // }
        //        // if (_objCoach_result.Count > 0)
        //        // {
        //        //     _objCoachRef.AddRange(_objCoach_result);

        //    }

        //    return View(_objCoach);
        //}
     

        #endregion

       

    }
}