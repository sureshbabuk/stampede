﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessObjects.Log;
using Stampade.Models;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using BusinessObjects.Constants;
using System.Net;
using System.Threading.Tasks;

namespace Stampade.Controllers
{
    public class WorkoutsController : Controller
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        // GET: Coach
        public ActionResult Index()
        {
            return View();
        }

        # region Manage Workouts

        public ActionResult ManageWorkouts(string Msg)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }
            if (Msg != null)
            {
                ViewData["ReturnMsg"] = Msg;
            }

            return View();
        }

        public ActionResult Workouts_Table()
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            long UserID = 0;
            long.TryParse(Session["USERS_ID"].ToString(), out UserID);

            List<STAM_MA_WORKOUTS> _objWorkouts = new List<STAM_MA_WORKOUTS>();

            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);

            string response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkouts?createdUser=" + UserID).Result;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            _objWorkouts = json_serializer.Deserialize<List<STAM_MA_WORKOUTS>>(response);

            return View(_objWorkouts);
        }

        #endregion

        # region Create NewCoach
        public ActionResult CreateNewWorkouts(int? WorkoutId,string AsstCoach,bool flag=false)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (WorkoutId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkout?iWorkouts=" + WorkoutId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(response);
            if(AsstCoach != null)
            {
                ViewData["AsstCoachWorkout"] = AsstCoach;
            }

            if(flag)
            {
                ViewData["NewWork"] = 1;
            }
            return View(objWorkoutRef);
        }

        public ActionResult Workout_View(int? WorkoutId)
        {
            if (Session["USERS_ID"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();
            var client = new HttpClient();
            string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            string response = "";
            if (WorkoutId > 0)
            {
                response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkout?iWorkouts=" + WorkoutId).Result;
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(response);

            return View(objWorkoutRef);
        }

        public JsonResult WorkoutCreation(STAM_MA_WORKOUTS obj_CreateWorkouts)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();

                var retunMsg = "";
                HttpResponseMessage response = new HttpResponseMessage();

                if (obj_CreateWorkouts.STAM_WORKOUT_ID == 0)
                {
                    obj_CreateWorkouts.STAM_WORKOUT_STATUS = BusinessConstants.Status.Active;
                    obj_CreateWorkouts.STAM_CREATED_USER = UserID;
                    obj_CreateWorkouts.STAM_CREATED_DATE = DateTime.Now;
                    obj_CreateWorkouts.STAM_LAST_UPDATED_USER = UserID;
                    obj_CreateWorkouts.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Workouts/GetCreateWorkouts", obj_CreateWorkouts).Result;

                    ////Save Assigned Team  in Team Table

                    //if (obj_CreateCoach.STAM_COACH_TEAMID != 0)
                    //{
                    //  string  responses = client.GetStringAsync(sWebApiurl + "api/Team/GetTeam?iTeam=" + obj_CreateCoach.STAM_COACH_TEAMID).Result;
                    //  STAM_MA_TEAM objTeamRef = new STAM_MA_TEAM();
                    //  JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    //  objTeamRef = json_serializer.Deserialize<STAM_MA_TEAM>(responses);
                    //  objTeamRef.STAM_COACHASSIGNED = true;
                    //  response = client.PostAsJsonAsync(sWebApiurl + "api/Team/GetCreateTeam", objTeamRef).Result;
                    //}

                    retunMsg = "Workout Created Successfully";
                }
                else
                {
                    var client = new HttpClient();
                    string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                    client.DefaultRequestHeaders.Add(strAppName, strUserId);

                    string responses = "";

                    responses = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkout?iWorkouts=" + obj_CreateWorkouts.STAM_WORKOUT_ID).Result;

                    STAM_MA_WORKOUTS _objWorkoutRef = new STAM_MA_WORKOUTS();
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    _objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(responses);

                    _objWorkoutRef.STAM_LAST_UPDATED_USER = UserID;
                    _objWorkoutRef.STAM_WORKOUT_TITLE = obj_CreateWorkouts.STAM_WORKOUT_TITLE;
                    _objWorkoutRef.STAM_WORKOUT_TARGET_MM = obj_CreateWorkouts.STAM_WORKOUT_TARGET_MM;
                    _objWorkoutRef.STAM_WORKOUT_TARGET_SS = obj_CreateWorkouts.STAM_WORKOUT_TARGET_SS;
                    _objWorkoutRef.STAM_WORKOUT_DISTANCE = obj_CreateWorkouts.STAM_WORKOUT_DISTANCE;
                    _objWorkoutRef.STAM_WORKOUT_DISTANCEID = obj_CreateWorkouts.STAM_WORKOUT_DISTANCEID;
                    _objWorkoutRef.STAM_WORKOUT_RUNS = obj_CreateWorkouts.STAM_WORKOUT_RUNS;
                    _objWorkoutRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                    response = client.PostAsJsonAsync(sWebApiurl + "api/Workouts/GetCreateWorkouts", _objWorkoutRef).Result;
                    retunMsg = "Workout Details Updated Successfully";
                }
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Save Create Workouts", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteWorkout(long  WorkoutId)
        {
            try
            {

                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (WorkoutId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkout?iWorkouts=" + WorkoutId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(response);

                objWorkoutRef.STAM_WORKOUT_STATUS = BusinessConstants.Status.Deleted;
                objWorkoutRef.STAM_LAST_UPDATED_USER = UserID;
                objWorkoutRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Workouts/GetCreateWorkouts", objWorkoutRef).Result;

                string returnMsg = "Selected Workout Has Been Deleted Successfully ";

                return Json(new { Error = false, Message = returnMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Delete Workout", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult ChkWorkoutTitle(string title, string Dist, int Runs)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["ID"].ToString(), out UserID);

                STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";

               JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                if (title != null)
                {
                    //string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                    //string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                    //string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                    //string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);

                    // response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/GetCoachWorkouts?CoachID=" + AsstCoachID).Result;
                    //_objCoach = json_serializer.Deserialize<List<STAM_MA_COACH_WORKOUT>>(response);

                    //response = client.GetStringAsync(sWebApiurl + "api/CoachWorkout/chkCoachWorkout?Name=" + Name + "&Group=" + GroupName + "&objdate=" + _date + "&Userid=" + AsstCoachID).Result;
                    if (Session["UserType"].ToString() == "2")//Coach
                    {
                        string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + Session["USERS_ID"]).Result;
                        string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                        string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + Session["USERS_ID"]).Result;
                        string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);
                        //  string AsstCoachID="";

                        AsstCoachID += Session["USERS_ID"];
                        response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkoutByTitle?title=" + title + "&Userid=" + AsstCoachID).Result;
                    }

                    else if (Session["UserType"].ToString() == "3")//AsstCoach 
                    {
                        STAM_MA_ASSTCOACH _objCoach1 = new STAM_MA_ASSTCOACH();
                        string response1 = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoach?iCoach=" + Session["ResourceId"]).Result;
                        _objCoach1 = json_serializer.Deserialize<STAM_MA_ASSTCOACH>(response1);
                        string UserSIDres = client.GetStringAsync(sWebApiurl + "api/AsstCoach/GetAsstCoachIDListByCoachID?UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                        string UserSID = json_serializer.Deserialize<string>(UserSIDres);
                        string AsstCoachIDres = client.GetStringAsync(sWebApiurl + "api/Login/GetUserIDByTypeID?typeID=" + UserSID + "&UserType=" + 3 + "&UserID=" + _objCoach1.STAM_CREATED_USER).Result;
                        string AsstCoachID = json_serializer.Deserialize<string>(AsstCoachIDres);

                        AsstCoachID += _objCoach1.STAM_CREATED_USER;
                        response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkoutByTitle?title=" + title + "&Userid=" + AsstCoachID).Result;
                    }

                }

              //  JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(response);

                string returnMsg = "";
                //if (objWorkoutRef != null && (objWorkoutRef.STAM_WORKOUT_DISTANCE == Dist) && (objWorkoutRef.STAM_WORKOUT_RUNS == Runs))

                if (objWorkoutRef != null && objWorkoutRef.STAM_WORKOUT_TITLE==title)
                {
                    returnMsg = "Workout Title Already Exists";
                }
                return Json(new { Error = false, Message = returnMsg, data = (objWorkoutRef != null) ? objWorkoutRef.STAM_WORKOUT_ID : 0, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Message = "Problem In Checking Workout title", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult WorkoutActionUpdate(long WorkoutId, string status)
        {
            try
            {
                if (Session["USERS_ID"] == null)
                {
                    Response.Redirect("~/Account/Login");
                }
                long UserID = 0;
                long.TryParse(Session["USERS_ID"].ToString(), out UserID);

                var retunMsg = "";
                STAM_MA_WORKOUTS objWorkoutRef = new STAM_MA_WORKOUTS();

                var client = new HttpClient();
                string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
                client.DefaultRequestHeaders.Add(strAppName, strUserId);
                string response = "";
                if (WorkoutId > 0)
                {
                    response = client.GetStringAsync(sWebApiurl + "api/Workouts/GetWorkout?iWorkouts=" + WorkoutId).Result;
                }

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                objWorkoutRef = json_serializer.Deserialize<STAM_MA_WORKOUTS>(response);

                objWorkoutRef.STAM_WORKOUT_STATUS = (status == "Active" ? BusinessConstants.Status.Active : BusinessConstants.Status.InActive);
                objWorkoutRef.STAM_LAST_UPDATED_USER = UserID;
                objWorkoutRef.STAM_LAST_UPDATED_DATE = DateTime.Now;

                HttpResponseMessage responses = client.PostAsJsonAsync(sWebApiurl + "api/Workouts/GetCreateWorkouts", objWorkoutRef).Result;

                retunMsg = (status == "Active" ? "Status Changed  as Active Successfully" : "Status Changed  as In-Active Successfully");
                return Json(new { Error = false, Message = retunMsg, Status = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = "Problem In Update Group", Status = 400 }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}