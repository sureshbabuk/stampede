﻿using BusinessObjects.Utility;
using Stampade.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace Stampade.MockClass
{
    public class MockWebServiceCallClass
    {
        string sWebApiurl = ConfigurationManager.AppSettings["sWebApiurl"];
        string strAppName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        string strUserId = ConfigurationManager.AppSettings["UserId"].ToString();
        Utility utility = new Utility();

        public IList<CalendarEventItem> GetCalendarEvents(long UserId)
        {
            var list = new List<CalendarEventItem>();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var client = new HttpClient();
            List<NewWorkout> lstNewWorkout = new List<NewWorkout>();
            client.DefaultRequestHeaders.Add(strAppName, strUserId);
            var response = client.GetAsync(sWebApiurl + "api/Dashboard/GetAllNewWorkoutResults?UserId=" + UserId).Result;
            var responseAsString1 = response.Content.ReadAsStringAsync();
            lstNewWorkout = json_serializer.Deserialize<List<NewWorkout>>(responseAsString1.Result);
            var random = new Random();
            foreach(var item in lstNewWorkout)
            {
                list.Add
                    (
                        new CalendarEventItem
                        {
                            ID =Convert.ToInt32(item.workoutId),
                            Title ="Title :"+ item.WorkoutTitle+" "+"Group Name:"+item,
                            url = "/CoachWorkout/CreateNewWrkoutNew?CoachId=" + item.workoutId,
                            IsAllDayEvent = false,
                            Start =Convert.ToDateTime(item.WorkoutDate),
                            End = Convert.ToDateTime(item.WorkoutDate)
                        }
                    );
            }
            return list;
        }
    }
}