﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class Login
    {
        public long id { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
        public string email { get; set; }
        public bool IsForgotPassword { get; set; }
        public bool RememberMe { get; set; }

    }

    public class STAM_MA_USERS
    {
        public long STAM_USERID { get; set; }
        public Nullable<int> STAM_USER_TYPE { get; set; }
        public Nullable<int> STAM_USER_STATUS_CODE { get; set; }
        public Nullable<int> SAM_USER_ROLE { get; set; }
        public string STAM_USER_FNAME { get; set; }
        public string STAM_USER_LNAME { get; set; }
        public string STAM_USER_GENDER { get; set; }
        public string STAM_USER_ADD1 { get; set; }
        public string STAM_USER_ADD2 { get; set; }
        public string STAM_USER_TELEPHONE { get; set; }
        public string STAM_USER_MOBILE { get; set; }
        public string STAM_USER_WEBSITE { get; set; }
        public string STAM_USER_EMAIL { get; set; }
        public byte[] STAM_USER_IMAGE { get; set; }
        public string STAM_USER_USERNAME { get; set; }
        public string STAM_USER_PSWD { get; set; }
        public Nullable<bool> STAM_IS_ACTIVATED { get; set; }
        public string STAM_ACTIVATED_STRING { get; set; }
        public Nullable<long> STAM_USER_TYPE_KEY_ID { get; set; }
        public Nullable<bool> STAM_ACTIVATED_STATUS { get; set; }
        public Nullable<long> SAM_CREATED_USER { get; set; }
        public Nullable<DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<DateTime> STAM_LAST_UPDATED_DATE { get; set; }
    }

    public class UserDetail
    {
        public long id { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string fName { get; set; }
        public string lName { get; set; }
        public bool IsFemale { get; set; }
        public string add1 { get; set; }
        public string add2 { get; set; }
        public string telephone { get; set; }
        public string mobile { get; set; }
        public string website { get; set; }
        public string email { get; set; }

    }
   
}