﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class STAM_MA_ASSTCOACH
    {        

        public long STAM_ASSTCOACH_ID { get; set; }
        public int STAM_ASSTCOACH_STATUS { get; set; }
        public string STAM_ASSTCOACH_FIRSTNAME { get; set; }
        public string STAM_ASSTCOACH_LASTNAME { get; set; }
        public string STAM_ASSTCOACH_EMAILADD { get; set; }
        public long STAM_ASSTCOACH_MOBILENO { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public string STAM_ASSTCOACH_GROUPID { get; set; }
        public string STAM_ASSTCOACH_GROUPNAME { get; set; }
    } 

    public class STAM_MA_ATHLETE
    {
        public long STAM_ATH_ID { get; set; }
        public string STAM_ATH_FIRTSNAME { get; set; }
        public string STAM_ATH_LASTNAME { get; set; }
        public string STAM_ATH_EMAILADD { get; set; }
        public long STAM_ATH_MOBILENO { get; set; }
        public string STAM_ATH_GENDER { get; set; }
        public string STAM_ATH_ASSIGNGROUPID { get; set; }
        public string STAM_ATH_ASSIGNGROUP { get; set; }
        public long STAM_ATH_SECTIONID { get; set; }
        public string STAM_ATH_SECTION { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public int STAM_ATH_STATUS { get; set; }
        public int STAM_USERTYPE { get; set; }
    }

    public class STAM_MA_COACH_WORKOUT
    {
        public long STAM_COACH_WORKOUT_ID { get; set; }
        public int STAM_COACH_WORKOUT_STATUS { get; set; }
        public DateTime STAM_COACH_WORKOUT_DATE { get; set; }
        public string STAM_COACH_WORKOUT_TITLE { get; set; }
        public string STAM_COACH_WORKOUT_DISTANCE { get; set; }
        public int STAM_COACH_WORKOUT_RUNS { get; set; }
        public string STAM_COACH_WORKOUT_TARGET_MM { get; set; }
        public string STAM_COACH_WORKOUT_TARGET_SS { get; set; }
        public long STAM_COACH_WORKOUT_GROUPID { get; set; }
        public string STAM_COACH_WORKOUT_GROUPNAME { get; set; }
        public string STAM_COACH_WORKOUT_NOTES { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public string STAM_COACH_WORKOUT_ATHSECID { get; set; }
        public string STAM_COACH_WORKOUT_ATHSECNAME { get; set; }
        public int STAM_USERTYPE { get; set; }

        public string WorkoutDate { get; set; }
    }

    public class STAM_MA_ATHLETE_SECTION
    {
        public long STAM_ATHSEC_ID { get; set; }
        public int STAM_ATHSEC_STATUS { get; set; }
        public string STAM_ATHSEC_NAME { get; set; }
        public long STAM_ATHSEC_GROUPID { get; set; }
        public string STAM_ATHSEC_GROUPNAME { get; set; }
        public string STAM_ATHSEC_ATHLETEID { get; set; }
        public string STAM_ATHSEC_ATHLETENAME { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public int STAM_USERTYPE { get; set; }
    }

    public class AthleteModel
    {
        public long STAM_ATH_ID { get; set; }
        public string STAM_ATH_FIRTSNAME { get; set; }
        public string STAM_ATH_LASTNAME { get; set; }
        public string STAM_ATH_EMAILADD { get; set; }
        public long STAM_ATH_MOBILENO { get; set; }
        public string STAM_ATH_GENDER { get; set; }
        public long STAM_ATH_ASSIGNGROUPID { get; set; }
        public string STAM_ATH_ASSIGNGROUP { get; set; }
        public long STAM_ATH_SECTIONID { get; set; }
        public string STAM_ATH_SECTION { get; set; }
        //public long STAM_CREATED_USER { get; set; }
        //public DateTime STAM_CREATED_DATE { get; set; }
        //public long STAM_LAST_UPDATED_USER { get; set; }
        //public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public int STAM_ATH_STATUS { get; set; }
    }


       
}