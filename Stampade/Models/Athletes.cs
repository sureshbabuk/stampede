﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class Athletes
    {
        public long athleteID { get; set; }
        public int groupID { get; set; }
        public int sectionID { get; set; }
        public string sectionName { get; set; }
        public string groupName { get; set; }
        public string athleteName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string status { get; set; }
        public string emailAdd { get; set; }
        public string gender { get; set; }
        public string deviceID { get; set; }
        public Nullable<long> mobileNo { get; set; }
    }
}