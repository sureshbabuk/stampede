﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{    
        public class STAM_MA_COACH
        {
            public long STAM_COACH_ID { get; set; }
            public int STAM_COACH_STATUS { get; set; }
            public string STAM_COACH_FIRSTNAME { get; set; }
            public string STAM_COACH_LASTNAME { get; set; }
            public string STAM_COACH_EMAILADD { get; set; }
            public long STAM_COACH_MOBILENO { get; set; }
            public long STAM_COACH_TEAMID { get; set; }
            public string STAM_COACH_TEAMNAME { get; set; }
            public long STAM_CREATED_USER { get; set; }
            public DateTime STAM_CREATED_DATE { get; set; }
            public long STAM_LAST_UPDATED_USER { get; set; }
            public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        }
}