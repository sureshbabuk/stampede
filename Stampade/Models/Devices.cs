﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class Devices
    {
        public int ID { get; set; }
        public string DeviceName { get; set; }
        public string DeviceId { get; set; }
        public Nullable<long> AthleteID { get; set; }


        public string AthleteName { get; set; }


        public string Name { get; set; }

    }

    public  class STAM_MA_DEVICES
    {
        public int STAM_ID { get; set; }
        public string STAM_DEVICE_NAME { get; set; }
        public string STAM_DEVICE_ID { get; set; }
    }
}