﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class STAM_MA_DISTANCE
    {
        public long STAM_DISTANCE_ID { get; set; }
        public int STAM_DISTANCE_STATUS { get; set; }
        public string STAM_DISTANCE_NAME { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
    }
}