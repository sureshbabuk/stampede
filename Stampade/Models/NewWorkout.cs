﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class NewWorkout
    {
        public long workoutId { get; set; }
        public Nullable<System.DateTime> WorkoutDate { get; set; }
        public string WorkoutTitle { get; set; }
        public string WorkoutNotes { get; set; }
        public string WorkoutType { get; set; }
        public string WorkoutDistance { get; set; }
        public Nullable<int> WorkoutRuns { get; set; }
        public string WorkoutGroupName { get; set; }
        public Nullable<long> WorkoutGroupId { get; set; }
        public Nullable<long> WorkoutUserId { get; set; }
        public List<Details> ListDetails { get; set; }
    }
    public class Details
    {
        public long DetailsID { get; set; }
        public Nullable<long> workoutId { get; set; }
        public Nullable<long> SectionID { get; set; }
        public string SectionName { get; set; }
        public string TargetTime { get; set; }
        public Nullable<int> Run { get; set; }
        public List<SectionResults> ListSectionResults { get; set; }
    }
    public class SectionResults
    {
        public long SectionResultsID { get; set; }
        public Nullable<long> DetailsID { get; set; }
        public Nullable<int> Lap { get; set; }
        public Nullable<decimal> LapTime { get; set; }
    }
}