﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class STAM_TR_WORKOUTS_MODEL
    {
        public long STAM_WORKOUT_ID { get; set; }
        public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public string STAM_WORKOUT_NOTES { get; set; }
        public string STAM_WORKOUT_TYPE { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
        public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_WORKOUT_STATUS { get; set; }

        public string STAM_WORKOUT_GROUP { get; set; }


        public string STAM_WORKOUT_DATE1 { get; set; }
    }
}