﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{

    public class Stam_Athlete
    {

        public long STAM_ATH_ID { get; set; }
        public Nullable<int> STAM_ATH_STATUS { get; set; }
        public string STAM_ATH_FIRTSNAME { get; set; }
        public string STAM_ATH_LASTNAME { get; set; }
        public string STAM_ATH_EMAILADD { get; set; }
        public Nullable<long> STAM_ATH_MOBILENO { get; set; }
        public string STAM_ATH_GENDER { get; set; }
        public string STAM_ATH_ASSIGNGROUPID { get; set; }
        public string STAM_ATH_ASSIGNGROUP { get; set; }
        public Nullable<long> STAM_ATH_SECTIONID { get; set; }
        public string STAM_ATH_SECTION { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_USERTYPE { get; set; }
        public string STAM_DEVICE_ID { get; set; }
        public string STAM_DEVICE_NAME { get; set; }
        public string STAM_NOTIFICATION_STATUS { get; set; }
        public Nullable<System.DateTime> STAM_NOTIFICATION_DATE { get; set; }
    }
}