﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
     public class STAM_MA_TEAM
    {
        public long STAM_TEAM_ID { get; set; }
        public int STAM_STATUS { get; set; }
        public string STAM_TEAM_NAME { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public bool STAM_COACHASSIGNED { get; set; }
        public long STAM_ASSIGN_COACHID { get; set; }
        public string CoachName { get; set; }
    }
}