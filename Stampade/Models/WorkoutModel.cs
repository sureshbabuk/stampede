﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Services.Models
{
    public class WorkoutModel
    {

        public List<string> Sections { get; set; }
        public int Runs { get; set; }
        public int Distance { get; set; }
        public List<string> SectionIDs { get; set; }


        public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();

      //  public List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();

        public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();
    }

    public class STAM_TR_DETAILS_MODEL 
    {
        public long STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_ID { get; set; }
        public Nullable<int> STAM_RUN { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }

       // public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();
      
    }
    public partial class STAM_TR_DETAILS_SECTION_MODEL
    {

        public long STAM_WORKOUT_DETAILS_SEC_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_SEC_ID { get; set; }
        public string STAM_SEC_TARGET_TIME { get; set; }

        
    }



}