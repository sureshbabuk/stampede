﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{

    public class WorkoutResults
    {
        public long WorkoutResultId { get; set; }
        public Nullable<long> WorkoutID { get; set; }
        public Nullable<long> WorkoutCoachId { get; set; }
        public Nullable<long> WorkoutGroupID { get; set; }
        public Nullable<System.DateTime> WorkoutDate { get; set; }
        public Nullable<decimal> WorkoutTargetTime { get; set; }
        public Nullable<long> WorkoutCreateUser { get; set; }
        public Nullable<System.DateTime> WorkoutCreateDate { get; set; }
        public Nullable<long> WorkoutLastUpdatedUser { get; set; }
        public Nullable<System.DateTime> WorkoutLastUpdatedDate { get; set; }
        public List<WorkoutSectionResult> WorkoutResultDetails { get; set; }
    }
    public class WorkoutSectionResult
    {
        public long WorkoutSectionResultId { get; set; }
        public Nullable<long> WorkoutSectionId { get; set; }
        public Nullable<decimal> WorkoutSectionRun { get; set; }
        public Nullable<decimal> WorkoutSectionTime { get; set; }
        public Nullable<int> WorkoutSectionSplit { get; set; }
        public Nullable<long> WorkoutID { get; set; }
    }




    public class STAM_TR_WORKOUTS_Model
    {


        public long STAM_WORKOUT_ID { get; set; }
        public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public string STAM_WORKOUT_NOTES { get; set; }
        public string STAM_WORKOUT_TYPE { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
        public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_WORKOUT_STATUS { get; set; }

        public string STAM_WORKOUT_GROUP { get; set; }


        public string STAM_WORKOUT_DATE1 { get; set; }

        List<STAM_TR_DETAILS_SECTION> _objsecDetail = new List<STAM_TR_DETAILS_SECTION>();


        List<STAM_TR_DETAILS> _objsecDetail1 = new List<STAM_TR_DETAILS>();


    }

}