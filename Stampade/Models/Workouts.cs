﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stampade.Models
{
    public class STAM_MA_WORKOUTS
    {
        public long STAM_WORKOUT_ID { get; set; }
        public int STAM_WORKOUT_STATUS { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public long STAM_WORKOUT_DISTANCEID { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public int STAM_WORKOUT_RUNS { get; set; }
        public long STAM_CREATED_USER { get; set; }
        public DateTime STAM_CREATED_DATE { get; set; }
        public long STAM_LAST_UPDATED_USER { get; set; }
        public DateTime STAM_LAST_UPDATED_DATE { get; set; }
        public int STAM_WORKOUT_TARGET_MM { get; set; }
        public int STAM_WORKOUT_TARGET_SS { get; set; }
    }

    public class Workout_Model
    {
        public List<string> Sections { get; set; }
        public int Runs { get; set; }
        public int Distance { get; set; }
        public List<string> SectionIDs { get; set; }

        List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();

        List<STAM_TR_DETAILS_SECTION> _objsecDetail = new List<STAM_TR_DETAILS_SECTION>();


        List<STAM_TR_DETAILS> _objsecDetail1 = new List<STAM_TR_DETAILS>();



        public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();


        public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();



    }


    public class WorkoutModel
    {

        public List<string> Sections { get; set; }
        public int Runs { get; set; }
        public int Distance { get; set; }
        public List<string> SectionIDs { get; set; }


        public List<STAM_TR_DETAILS_MODEL> _objSTAM_TR_DETAILS = new List<STAM_TR_DETAILS_MODEL>();

        public List<STAM_MA_DISTANCE> _objDist = new List<STAM_MA_DISTANCE>();

        public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();
    }




    public class STAM_TR_DETAILS_MODEL
    {
        public long STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_ID { get; set; }
        public Nullable<int> STAM_RUN { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_SEC_ID { get; set; }
        public string STAM_SEC_TARGET_TIME { get; set; }
        public Nullable<int> STAM_NO_ROW { get; set; }
        // public List<STAM_TR_DETAILS_SECTION_MODEL> _objSTAM_TR_DETAILS_SECTION = new List<STAM_TR_DETAILS_SECTION_MODEL>();

    }
    public partial class STAM_TR_DETAILS_SECTION_MODEL
    {

        public long STAM_WORKOUT_DETAILS_SEC_ID { get; set; }
        public Nullable<long> STAM_WORKOUT_DETAILS_ID { get; set; }
        public Nullable<long> STAM_SEC_ID { get; set; }
        public string STAM_SEC_TARGET_TIME { get; set; }


    }


    public class AthleteSection
    {

        public String SectionID{ get; set; }


        public string SectionName { get; set; }

    }

      public  class STAM_TR_WORKOUTS
    {
        public long STAM_WORKOUT_ID { get; set; }
        public Nullable<System.DateTime> STAM_WORKOUT_DATE { get; set; }
        public string STAM_WORKOUT_TITLE { get; set; }
        public string STAM_WORKOUT_NOTES { get; set; }
        public string STAM_WORKOUT_TYPE { get; set; }
        public string STAM_WORKOUT_DISTANCE { get; set; }
        public Nullable<int> STAM_WORKOUT_RUNS { get; set; }
        public Nullable<long> STAM_WORKOUT_GROUPID { get; set; }
        public Nullable<long> STAM_CREATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }
        public Nullable<long> STAM_LAST_UPDATED_USER { get; set; }
        public Nullable<System.DateTime> STAM_LAST_UPDATED_DATE { get; set; }
        public Nullable<int> STAM_WORKOUT_STATUS { get; set; }

        public string STAM_WORKOUT_GROUP { get; set; }


        public string STAM_WORKOUT_DATE1 { get; set; }



    }

      public  class STAM_TR_DETAILS_SECTION
      {
          public long STAM_WORKOUT_DETAILS_SEC_ID { get; set; }
          public Nullable<long> STAM_WORKOUT_DETAILS_ID { get; set; }
          public Nullable<long> STAM_SEC_ID { get; set; }
          public Nullable<System.DateTime> STAM_SEC_TARGET_TIME { get; set; }

          
      }

      public  class STAM_TR_DETAILS
      {

          public long STAM_WORKOUT_DETAILS_ID { get; set; }
          
          public Nullable<long> STAM_WORKOUT_ID { get; set; }
          public Nullable<int> STAM_RUN { get; set; }
          public Nullable<System.DateTime> STAM_CREATED_DATE { get; set; }

          public virtual ICollection<STAM_TR_DETAILS_SECTION> STAM_TR_DETAILS_SECTION { get; set; }
          public virtual STAM_TR_WORKOUTS STAM_TR_WORKOUTS { get; set; }
      }
     

    public class STAM_CUSTOM_MODEL
    {
        public string STAM_TARGET_TIME { get; set; }



        public List<string> STAM_SEC_TARGET_TIME { get; set; }
        public string STAM_SEC_ID { get; set; }
        public List<long> STAM_SECID { get; set; }

        public string DISTANCE { get; set; }


    }






}